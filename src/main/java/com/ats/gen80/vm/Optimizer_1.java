/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.Gen80Misc;
import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_1 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {
        if ((testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE) || testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.HL))
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 4, Opcode.Mnemonic.XCHG)) {

            int val = convert(opcodes.get(idx + 1).data);
            Register guessReg = opcodes.get(idx).srcReg == Register.HL ? Register.DE : Register.HL;
            if (Math.abs(val) < 4) {
                for (int i = 0; i < Math.abs(val); i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
                    }
                }
                idx += 4;
                return true;
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(guessReg, val));
                optimized.add(Opcode.Mnemonic.DAD.create(Register.DE));
                idx += 3;
                return true;
            }

        }

        if ((testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.HL))
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && (test(idx + 2, Opcode.Mnemonic.CALL) && Gen80Misc.SUB_INT.equals(getOpcode(idx + 2).sym))) {

            int val = -convert(opcodes.get(idx + 1).data);
            Register guessReg = opcodes.get(idx).srcReg == Register.HL ? Register.DE : Register.HL;
            if (Math.abs(val) < 4) {
                for (int i = 0; i < Math.abs(val); i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    }
                }
                optimized.add(Opcode.Mnemonic.XCHG.create());
                idx += 2;
                return true;
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(guessReg, val));
                optimized.add(Opcode.Mnemonic.DAD.create(Register.DE));
                optimized.add(Opcode.Mnemonic.XCHG.create());
                idx += 2;
                return true;
            }

        }

        if ((testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE))
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && (test(idx + 2, Opcode.Mnemonic.CALL) && Gen80Misc.SUB_INT.equals(getOpcode(idx + 2).sym))) {

            int val = -convert(opcodes.get(idx + 1).data);
            Register guessReg = opcodes.get(idx).srcReg == Register.HL ? Register.DE : Register.HL;
            if (Math.abs(val) < 4) {
                for (int i = 0; i < Math.abs(val); i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
                    }
                }
//                optimized.add(Opcode.Mnemonic.XCHG.create());
                idx += 2;
                return true;
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(guessReg, val));
                optimized.add(Opcode.Mnemonic.DAD.create(Register.DE));
                if (testSrcReg(idx + 3, Opcode.Mnemonic.POP, Register.HL)) {
                    optimized.add(Opcode.Mnemonic.POP.create(Register.DE));
                    idx += 3;
                } else {
                    optimized.add(Opcode.Mnemonic.XCHG.create());
                    idx += 2;
                }
                return true;
            }

        }
        return false;
    }

}
