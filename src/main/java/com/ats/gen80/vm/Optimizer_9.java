/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_9 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {

        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 3, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 4, Opcode.Mnemonic.LXI, Register.DE)) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
//            optimized.add(getOpcode(idx + 2));
            idx += 3;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            return true;
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 4, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && testDstRegSymNull(idx + 6, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 7, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 8, Opcode.Mnemonic.POP, Register.DE)) {

            int val = convert(getOpcode(idx + 6).data);
            if (Math.abs(val) < 5) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, convert(getOpcode(idx + 1).data) - 2));
                optimized.add(getOpcode(idx + 2));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
                optimized.add(getOpcode(idx + 4));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
                for (int i = 0; i < val; i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    }
                }
                idx += 8;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && testDstRegSymNull(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 5, Opcode.Mnemonic.POP, Register.DE)) {

            int val = convert(getOpcode(idx + 3).data);
            if (Math.abs(val) < 5) {
                optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
                optimized.add(getOpcode(idx + 1));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
                for (int i = 0; i < val; i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    }
                }
                idx += 4;
                return true;
            }
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.A, Register.M)
                && testSrcReg(idx + 4, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.H, Register.M)
                && test(idx + 6, Opcode.Mnemonic.MOV, Register.L, Register.A)
                && testDstReg(idx + 7, Opcode.Mnemonic.POP, Register.DE)) {

            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, convert(getOpcode(idx + 1).data) - 2));
            for (int i = 2; i < 7; i++) {
                optimized.add(getOpcode(idx + i));
            }
            idx += 7;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.LHLD)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && (test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M) )) {
            int val = convert(getOpcode(idx + 1).data);
            if (Math.abs(val) < 5) {
                optimized.add(getOpcode(idx));
                for (int i = 0; i < val; i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    }
                }
                idx += 2;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && (testSrcReg(idx + 1, Opcode.Mnemonic.DCX, Register.HL) || testDstReg(idx + 1, Opcode.Mnemonic.INX, Register.HL))
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.LHLD)
                && testDstRegSymNull(idx + 4, Opcode.Mnemonic.LXI, Register.DE)) {

            if (getOpcode(idx + 1).mnem == Opcode.Mnemonic.DCX) {
                optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
            } else {
                optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
            }

            optimized.add(Opcode.Mnemonic.PUSH.create(Register.DE));

            idx += 2;
            return true;

        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testDstRegSymNull(idx + 2, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.DE)) {

            int val = convert(getOpcode(idx + 2).data);
            if (Math.abs(val) < 5) {
                optimized.add(getOpcode(idx + 1));
                for (int i = 0; i < val; i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    }
                }
                idx += 4;
                return true;
            }
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && test(idx + 2, Opcode.Mnemonic.LHLD)
                && test(idx + 3, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.HL)) {
            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 2));
            optimized.add(getOpcode(idx + 3));
            idx += 4;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.D, Register.E)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.DE)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.SP)) {

            if (getOpcode(idx).sym == null) {
                optimized.add(Opcode.Mnemonic.MVI.create(Register.D, getOpcode(idx).data & 0xff));
            } else {
                optimized.add(Opcode.Mnemonic.MVI.create(Register.D, getOpcode(idx).sym));
            }
            idx += 1;
            return true;
        }
        if (testDstReg(idx, Opcode.Mnemonic.MVI, Register.D) && testDstReg(idx + 1, Opcode.Mnemonic.MVI, Register.D)) {
            return true;
        }

        return false;
    }
}
