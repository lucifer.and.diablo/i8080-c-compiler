/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_7 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {
        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.DE)) {
            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 2));
            idx += 3;
            return true;
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.DE)) {
            optimized.add(getOpcode(idx + 1));
            idx += 2;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)) {
            if (getOpcode(idx).sym != null) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx).sym + "+1"));
                idx += 1;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.XCHG)) {

            if (getOpcode(idx + 1).sym == null) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, getOpcode(idx + 1).data));
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, getOpcode(idx + 1).sym));
            }

            optimized.add(Opcode.Mnemonic.PUSH.create(Register.DE));
            idx += 3;
            return true;
        }

//        if (test(idx, Opcode.Mnemonic.XCHG)
//                && test(idx + 1, Opcode.Mnemonic.LHLD)
//                && test(idx + 2, Opcode.Mnemonic.XCHG)
//                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)) {
//        }
        return false;
    }

}
