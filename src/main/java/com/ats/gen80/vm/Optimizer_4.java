/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_4 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)) {

            if (testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.DE)
                    && testDstReg(idx + 6, Opcode.Mnemonic.LXI, Register.HL)) {
                if (getOpcode(idx).sym == null) {
                    optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx).data));
                } else {
                    optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx).sym));
                }
                if (getOpcode(idx + 1).sym == null) {
                    optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).data));
                } else {
                    optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).sym));
                }
                idx += 4;
                return true;
            } else if (testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.HL)
                    && testDstReg(idx + 6, Opcode.Mnemonic.DAD, Register.SP)
                    && testDstReg(idx + 7, Opcode.Mnemonic.MOV, Register.E)) {
                if (getOpcode(idx).sym == null) {
                    optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx).data));
                } else {
                    optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx).sym));
                }
                if (getOpcode(idx + 1).sym == null) {
                    optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).data));
                } else {
                    optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).sym));
                }
                idx += 4;
                return true;
            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && test(idx + 4, Opcode.Mnemonic.LHLD)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.E, Register.M)) {

            optimized.add(Opcode.Mnemonic.XCHG.create());
            if (getOpcode(idx).sym == null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).data));
            } else {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).sym));
            }
            idx += 3;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.SHLD) && test(idx + 1, Opcode.Mnemonic.LHLD)) {
            if (getOpcode(idx).sym != null && getOpcode(idx).sym.equals(getOpcode(idx + 1).sym)) {
                optimized.add(getOpcode(idx));
                idx += 1;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.DE)) {
            idx += 2;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.DE)) {
            if (getOpcode(idx).sym == null) {
                int val = convert(getOpcode(idx).data);
                if (Math.abs(val) == 1) {
                    idx += 1;
                    if (val == -1) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    }
                    return true;
                }
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.POP, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.DE)) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.HL));
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && testDstReg(idx + 5, Opcode.Mnemonic.POP, Register.DE)) {
            if (getOpcode(idx + 1).sym != null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).sym));
                idx += 4;
                return true;
            }
        }
        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            if (getOpcode(idx + 1).sym == null) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, getOpcode(idx + 1).data));
                idx += 1;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.DE));
            idx += 1;
            return true;
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.HL)) {

            if (getOpcode(idx + 1).sym == null) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, getOpcode(idx + 1).data));
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, getOpcode(idx + 1).sym));
            }
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.HL)) {
            optimized.add(Opcode.Mnemonic.XCHG.create());
            idx += 2;
            return true;
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.DE)) {

            if (getOpcode(idx + 1).sym == null) {
                optimized.add(Opcode.Mnemonic.LHLD.create(getOpcode(idx + 1).data));
            } else {
                optimized.add(Opcode.Mnemonic.LHLD.create(getOpcode(idx + 1).sym));
            }
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }
        if ((testDstReg(idx, Opcode.Mnemonic.DCR, Register.E) || testDstReg(idx, Opcode.Mnemonic.INR, Register.E))
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }

        if ((testDstReg(idx, Opcode.Mnemonic.DCR, Register.E) || testDstReg(idx, Opcode.Mnemonic.INR, Register.E))
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 2, Opcode.Mnemonic.MOV, Register.E)) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }

        // LAST
        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)) {

//            optimized.add(Opcode.Mnemonic.XCHG.create());
            if (getOpcode(idx + 1).sym == null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).data));
            } else {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).sym));
            }
            optimized.add(Opcode.Mnemonic.XCHG.create());
            idx += 4;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.XCHG)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.DE)
                && test(idx + 2, Opcode.Mnemonic.XCHG)) {
            optimized.add(Opcode.Mnemonic.PUSH.create(Register.HL));
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.DE)) {
            optimized.add(Opcode.Mnemonic.PUSH.create(Register.HL));
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG) && test(idx + 1, Opcode.Mnemonic.XCHG)) {
            idx += 1;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.POP, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.HL));
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.DE)) {
            idx += 1;
            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
            optimized.add(Opcode.Mnemonic.XCHG.create());
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && (testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + 1, Opcode.Mnemonic.DCX, Register.HL))
                && test(idx + 2, Opcode.Mnemonic.XCHG)) {
            if (getOpcode(idx + 1).mnem == Opcode.Mnemonic.INX) {
                optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
            } else {
                optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
            }
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && (testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.DE) || testSrcReg(idx + 1, Opcode.Mnemonic.DCX, Register.DE))
                && test(idx + 2, Opcode.Mnemonic.XCHG)) {
            if (getOpcode(idx + 1).mnem == Opcode.Mnemonic.INX) {
                optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
            } else {
                optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
            }
            idx += 2;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.XCHG)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.DE)) {

            optimized.add(Opcode.Mnemonic.PUSH.create(Register.HL));
            optimized.add(Opcode.Mnemonic.XCHG.create());
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)) {
            if (getOpcode(idx + 1).sym == null) {
                int val = convert(getOpcode(idx + 1).data);
                if (Math.abs(val) == 0) {

                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                        } else {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                        }
                    }
                    idx += 2;
                    return true;
                } else {
                    optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val));
                    idx += 1;
                    return true;
                }
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)) {
            if (getOpcode(idx).sym == null) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx).data));
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx).sym));
            }
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 4, Opcode.Mnemonic.LXI, Register.DE)) {
            optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
            optimized.add(Opcode.Mnemonic.PUSH.create(Register.DE));
            idx += 2;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && test(idx + 4, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 5, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 6, Opcode.Mnemonic.LXI, Register.DE)) {

            optimized.add(Opcode.Mnemonic.XCHG.create());
            if (getOpcode(idx).sym == null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).data));
            } else {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).sym));
            }
            idx += 3;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && test(idx + 4, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.DE)) {

            optimized.add(Opcode.Mnemonic.XCHG.create());
            if (getOpcode(idx).sym == null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).data));
            } else {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).sym));
            }
            idx += 3;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && test(idx + 3, Opcode.Mnemonic.CALL)) {
            ///idx += 1;
            return true;
        }
        return false;
    }

}
