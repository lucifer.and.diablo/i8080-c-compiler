/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_8 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {
        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 3, Opcode.Mnemonic.XCHG)
                && test(idx + 4, Opcode.Mnemonic.SHLD)
                && test(idx + 5, Opcode.Mnemonic.XCHG)
                && (testDstReg(idx + 6, Opcode.Mnemonic.LXI, Register.HL) || test(idx + 6, Opcode.Mnemonic.LHLD))) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
//            optimized.add(getOpcode(idx + 2));
            idx += 3;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
//            idx += 3;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 2, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)) {

            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, convert(getOpcode(idx).data)));

            if (getOpcode(idx + 1).sym == null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).data));
            } else {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).sym));
            }
            optimized.add(Opcode.Mnemonic.XCHG.create());

            idx += 4;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 5, Opcode.Mnemonic.XCHG)
                && test(idx + 6, Opcode.Mnemonic.SHLD)
                && test(idx + 7, Opcode.Mnemonic.XCHG)) {
            optimized.add(getOpcode(idx));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 3));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
//            optimized.add(getOpcode(idx + 2));
            idx += 5;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 5, Opcode.Mnemonic.XCHG)
                && test(idx + 6, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            optimized.add(getOpcode(idx));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 3));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
//            optimized.add(getOpcode(idx + 2));
            idx += 5;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.MVI, Register.D)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && test(idx + 3, Opcode.Mnemonic.SHLD)
                && test(idx + 4, Opcode.Mnemonic.LHLD)) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.M));
            optimized.add(Opcode.Mnemonic.MVI.create(Register.H, getOpcode(idx + 1).data));
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 3, Opcode.Mnemonic.XCHG)
                && test(idx + 4, Opcode.Mnemonic.SHLD)
                && test(idx + 5, Opcode.Mnemonic.LHLD)) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
            idx += 3;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && test(idx + 3, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 4, Opcode.Mnemonic.PUSH, Register.HL)
                && (testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + 5, Opcode.Mnemonic.DCX, Register.HL))
                && test(idx + 6, Opcode.Mnemonic.SHLD)
                && testDstReg(idx + 7, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 8, Opcode.Mnemonic.POP, Register.DE)) {
            optimized.add(getOpcode(idx));
            optimized.add(getOpcode(idx + 1));
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG) && getOpcode(idx + 1) instanceof Opcode.Label) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            optimized.add(getOpcode(idx));
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.SHLD)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && test(idx + 2, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 3, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.XCHG)
                && test(idx + 5, Opcode.Mnemonic.LHLD)) {
            optimized.add(getOpcode(idx));
//            optimized.add(getOpcode(idx + 1));
            idx += 1;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && testDstReg(idx + 5, Opcode.Mnemonic.POP, Register.DE)) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
//            optimized.add(getOpcode(idx + 2));
            idx += 4;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
//            optimized.add(getOpcode(idx + 2));
            idx += 4;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 3, Opcode.Mnemonic.XCHG)
                && test(idx + 4, Opcode.Mnemonic.SHLD)
                && getOpcode(idx + 5) instanceof Opcode.Label) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
//            optimized.add(getOpcode(idx + 2));
            idx += 3;
            return true;
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.HL)
                && testSrcReg(idx + 3, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.LXI, Register.DE)) {
            optimized.add(getOpcode(idx));
            idx += 1;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL) && getOpcode(idx).sym != null
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.DE)) {
            optimized.add(Opcode.Mnemonic.XCHG.create());
            optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).sym));
            idx += 3;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL) && getOpcode(idx).sym == null && getOpcode(idx).data == 0
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.DE)) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.D));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.E));
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.XCHG)) {
//            optimized.add(Opcode.Mnemonic.XCHG.create());
//            optimized.add(Opcode.Mnemonic.PUSH.create(Register.HL));
            idx += 2;
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.DE)
                && testDstRegSymNull(idx + 2, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)) {
            int val1 = convert(getOpcode(idx).data);
            int val2 = convert(getOpcode(idx + 2).data);
            int res = val1 + val2;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, res));
            idx += 2;
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testDstRegSymNull(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.SP)
                && getOpcode(idx).data == getOpcode(idx + 3).data) {
            optimized.add(getOpcode(idx));
            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 2));
            idx += 4;
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.MVI, Register.D)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testDstRegSymNull(idx + 4, Opcode.Mnemonic.MVI, Register.D)
                && getOpcode(idx).data == getOpcode(idx + 4).data) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.D, Register.E)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.DE)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.SP)) {
            optimized.add(Opcode.Mnemonic.MOV.create(Register.D, Register.M));
            idx += 1;
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.MVI, Register.D)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.A, Register.D)
                && testSrcReg(idx + 2, Opcode.Mnemonic.ORA, Register.E)) {
            optimized.add(Opcode.Mnemonic.MVI.create(Register.A, getOpcode(idx).data));
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.D, Register.M)) {
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.DE)) {
            idx += 1;
            optimized.add(Opcode.Mnemonic.XCHG.create());
            return true;
        }

        if (test(idx, Opcode.Mnemonic.LHLD)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.DE)) {
            optimized.add(getOpcode(idx));
            idx += 1;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.DE) && getOpcode(idx).sym != null
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M)) {

            int val = convert(getOpcode(idx + 1).data);
            String res = getOpcode(idx).sym;
            if (val < 0) {
                res = res + "-" + val;
            } else {
                res = res + "+" + val;
            }
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, res));

            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && (testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + 2, Opcode.Mnemonic.DCX, Register.HL))
                && test(idx + 3, Opcode.Mnemonic.SHLD)
                && (testSrcReg(idx + 4, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + 4, Opcode.Mnemonic.DCX, Register.HL))) {
            return true;
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && (testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + 2, Opcode.Mnemonic.DCX, Register.HL))
                && test(idx + 3, Opcode.Mnemonic.SHLD)
                && (testSrcReg(idx + 4, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + 4, Opcode.Mnemonic.DCX, Register.HL))
                && testDstReg(idx + 5, Opcode.Mnemonic.POP, Register.DE)) {

            for (int i = 1; i < 5; i++) {
                optimized.add(getOpcode(idx + i));
            }
            idx += 5;
            return true;
        }
//        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
//                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
//                && test(idx + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)
//                && test(idx + 3, Opcode.Mnemonic.XCHG)
//                && testDstReg(idx + 4, Opcode.Mnemonic.LXI, Register.DE)) {
//            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
//            optimized.add(getOpcode(idx + 1));
//            optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.M));
//            optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.A));
////            optimized.add(getOpcode(idx + 2));
//            idx += 3;
//            return true;
//        }
        return false;
    }
}
