/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_10 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 3, Opcode.Mnemonic.MVI, Register.D)
                && testDstReg(idx + 4, Opcode.Mnemonic.INR, Register.E)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 6, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 5).sym == null) {
                if (getOpcode(idx).data == getOpcode(idx + 5).data) {
                    for (int i = 0; i < 5; i++) {
                        optimized.add(getOpcode(idx + i));
                    }
                    idx += 6;
                    return true;
                }
            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 3, Opcode.Mnemonic.MVI, Register.D)
                && testDstReg(idx + 4, Opcode.Mnemonic.DCR, Register.E)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 6, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 5).sym == null) {
                if (getOpcode(idx).data == getOpcode(idx + 5).data) {
                    for (int i = 0; i < 5; i++) {
                        optimized.add(getOpcode(idx + i));
                    }
                    idx += 6;
                    return true;
                }
            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 3).sym == null) {
                int val = getOpcode(idx).data - (getOpcode(idx + 3).data - 2);

                if (Math.abs(val) < 4) {
                    for (int i = 0; i < 3; i++) {
                        optimized.add(getOpcode(idx + i));
                    }

                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                        } else {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                        }
                    }
                    idx += 4;
                    return true;
                }
            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 3, Opcode.Mnemonic.MVI, Register.D)
                && testDstReg(idx + 4, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 5, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 4).sym == null) {
                int val = getOpcode(idx).data - (getOpcode(idx + 4).data);

                if (Math.abs(val) < 4) {
                    for (int i = 0; i < 4; i++) {
                        optimized.add(getOpcode(idx + i));
                    }

                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                        } else {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                        }
                    }
                    idx += 5;
                    return true;
                }
            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 3).sym == null) {
                int val = getOpcode(idx).data - (getOpcode(idx + 3).data);

                if (Math.abs(val) < 4) {
                    for (int i = 0; i < 3; i++) {
                        optimized.add(getOpcode(idx + i));
                    }

                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                        } else {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                        }
                    }
                    idx += 4;
                    return true;
                }
            }
        }

        if (test(idx, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.HL)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.SHLD)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.HL)) {
            if (getOpcode(idx).sym != null && getOpcode(idx + 3).sym != null) {
                optimized.add(getOpcode(idx));
                optimized.add(getOpcode(idx + 2));
                optimized.add(getOpcode(idx + 3));
                optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                idx += 4;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.HL)
                && testSrcReg(idx + 2, Opcode.Mnemonic.DCX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.SHLD)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.HL)) {
            if (getOpcode(idx).sym != null && getOpcode(idx + 3).sym != null) {
                optimized.add(getOpcode(idx));
                optimized.add(getOpcode(idx + 2));
                optimized.add(getOpcode(idx + 3));
                optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                idx += 4;
                return true;
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && (testSrcReg(idx + 5, Opcode.Mnemonic.DCX, Register.DE) || testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.DE))
                && testDstReg(idx + 6, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 7, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 6).sym == null) {
                if (getOpcode(idx).data == getOpcode(idx + 6).data) {
                    for (int i = 0; i < 6; i++) {
                        optimized.add(getOpcode(idx + i));
                    }
                    optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    idx += 7;
                    return true;
                }
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 6, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 7, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 8, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 9, Opcode.Mnemonic.MOV, Register.D, Register.M)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 5).sym == null) {
                if (getOpcode(idx).data == getOpcode(idx + 5).data) {
                    for (int i = 0; i < 5; i++) {
                        optimized.add(getOpcode(idx + i));
                    }
                    idx += 9;
                    return true;
                }
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && testSrcReg(idx + 5, Opcode.Mnemonic.PUSH, Register.DE)
                && (testSrcReg(idx + 6, Opcode.Mnemonic.DCX, Register.DE) || testSrcReg(idx + 6, Opcode.Mnemonic.INX, Register.DE))
                && testDstReg(idx + 7, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 8, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 9, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 10, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 11, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && testDstReg(idx + 12, Opcode.Mnemonic.POP, Register.HL)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 7).sym == null) {
                if (getOpcode(idx).data == getOpcode(idx + 7).data - 2) {
                    optimized.add(getOpcode(idx + 0));
                    optimized.add(getOpcode(idx + 1));
                    optimized.add(getOpcode(idx + 2));
                    optimized.add(getOpcode(idx + 3));
                    optimized.add(getOpcode(idx + 4));
                    optimized.add(getOpcode(idx + 6));
                    optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    optimized.add(getOpcode(idx + 9));
                    optimized.add(getOpcode(idx + 10));
                    optimized.add(getOpcode(idx + 11));
                    if (testSrcReg(idx + 6, Opcode.Mnemonic.DCX, Register.DE)) {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
                    } else {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
                    }
                    optimized.add(Opcode.Mnemonic.XCHG.create());
                    idx += 12;
                    return true;
                }
            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 2, Opcode.Mnemonic.MVI, Register.D)
                && (testDstReg(idx + 3, Opcode.Mnemonic.DCR, Register.E) || testDstReg(idx + 3, Opcode.Mnemonic.INR, Register.E))
                && testDstReg(idx + 4, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && (testDstReg(idx + 6, Opcode.Mnemonic.DCR, Register.E) || testDstReg(idx + 6, Opcode.Mnemonic.INR, Register.E))) {
            boolean sym = getOpcode(idx).sym == null && getOpcode(idx + 7).sym == null;

            if ((sym && (getOpcode(idx).data == getOpcode(idx + 4).data - 2)) || (!sym && getOpcode(idx).sym.equals(getOpcode(idx + 4).sym))) {
                optimized.add(getOpcode(idx + 0));
                optimized.add(getOpcode(idx + 1));
                optimized.add(getOpcode(idx + 2));
                optimized.add(getOpcode(idx + 3));
                optimized.add(getOpcode(idx + 5));
                idx += 5;
                return true;
            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 6, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 7, Opcode.Mnemonic.MOV, Register.A, Register.M)
                && testSrcReg(idx + 8, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 9, Opcode.Mnemonic.MOV, Register.H, Register.M)
                && test(idx + 10, Opcode.Mnemonic.MOV, Register.L, Register.A)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 5).sym == null) {
                if (getOpcode(idx).data == getOpcode(idx + 5).data) {
                    for (int i = 0; i < 5; i++) {
                        optimized.add(getOpcode(idx + i));
                    }
                    optimized.add(Opcode.Mnemonic.XCHG.create());
                    idx += 10;
                    return true;
                }
            }

        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 6, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 5).sym == null) {
                int val = getOpcode(idx).data + 1 - (getOpcode(idx + 5).data);

                if (Math.abs(val) < 4) {
                    for (int i = 0; i < 5; i++) {
                        optimized.add(getOpcode(idx + i));
                    }

                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                        } else {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                        }
                    }
                    idx += 6;
                    return true;
                }

            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 6, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 5).sym == null) {
                int val = getOpcode(idx).data - (getOpcode(idx + 5).data - 1);

                if (Math.abs(val) < 4) {
                    for (int i = 0; i < 5; i++) {
                        optimized.add(getOpcode(idx + i));
                    }

                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                        } else {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                        }
                    }
                    idx += 6;
                    return true;
                }

            }
        }
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && testSrcReg(idx + 5, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 6, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 7, Opcode.Mnemonic.DAD, Register.SP)) {
            int val = getOpcode(idx).data - (getOpcode(idx + 6).data - 3);

            if (Math.abs(val) < 4) {
                for (int i = 0; i < 6; i++) {
                    optimized.add(getOpcode(idx + i));
                }

                for (int i = 0; i < Math.abs(val); i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    }
                }
                idx += 7;
                return true;
            }
        }
        return false;
    }
}
