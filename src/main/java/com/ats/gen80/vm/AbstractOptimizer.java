/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucifer
 */
public abstract class AbstractOptimizer {

    public static int convert(int num) {
        num = num & 0xffff;
        return num > 0x7fff ? num - 0x10000 : num;
    }

    protected List<Opcode> opcodes;
//    protected boolean found = false;
    protected int idx = 0;
    protected boolean optimizationDisabled = false;
    protected List<Opcode> optimized;

    public List<Opcode> optimize(List<Opcode> opcodes) {
        this.opcodes = opcodes;
        List<Opcode> ret = new ArrayList<>();

        for (idx = 0; idx < opcodes.size(); ++idx) {
            if (opcodes.get(idx) instanceof Opcode.DisableOprimizer) {
                optimizationDisabled = true;
            }

            optimized = new ArrayList<>();

            if (!optimizationDisabled && optimizeCode()) {
                if (idx == -1) {
                    ret.clear();
                    break;
                }
                ret.addAll(optimized);
            } else {
                ret.add(opcodes.get(idx));
            }

            if (opcodes.get(idx) instanceof Opcode.EnableOprimizer) {
                optimizationDisabled = false;
            }

        }

        removeDeadCode(ret);

        return ret;
    }

    public abstract boolean optimizeCode();

    public Opcode getOpcode(int idx) {
//        Opcode op = opcodes.get(idx);
//        while (opcodes.get(idx) != null && opcodes.get(idx) instanceof Opcode.Label) {
//            idx++;
//        }
//        if (opcodes.get(idx) == null) {
//            return op;
//        }

        return opcodes.get(idx);
    }

    public boolean testDstReg(int idx, Register dst) {
        Opcode o = getOpcode(idx);
        return o.dstReg == dst;
    }

    public boolean testSrcReg(int idx, Register src) {
        Opcode o = getOpcode(idx);
        return o.srcReg == src;
    }

    public boolean testDstReg(int idx, Opcode.Mnemonic mnem, Register dst) {
        Opcode o = getOpcode(idx);
        return o.mnem == mnem && o.dstReg == dst;
    }

    public boolean testDstRegSymNull(int idx, Opcode.Mnemonic mnem, Register dst) {
        Opcode o = getOpcode(idx);
        return o.mnem == mnem && o.dstReg == dst && o.sym == null;
    }

    public boolean testSrcRegSymNull(int idx, Opcode.Mnemonic mnem, Register dst) {
        Opcode o = getOpcode(idx);
        return o.mnem == mnem && o.dstReg == dst && o.sym == null;
    }

    public boolean testSrcReg(int idx, Opcode.Mnemonic mnem, Register src) {
        Opcode o = getOpcode(idx);
        return o.mnem == mnem && o.srcReg == src;
    }

    public boolean test(int idx, Opcode.Mnemonic mnem) {
        Opcode o = getOpcode(idx);
        return o.mnem == mnem;
    }

    public boolean test(int idx, Opcode.Mnemonic mnem, Register dst, Register src) {
        Opcode o = getOpcode(idx);
        return o.mnem == mnem && o.dstReg == dst && o.srcReg == src;
    }

    protected static boolean identical(List<Opcode> optimized, List<Opcode> initial) {
        if (optimized.size() != initial.size()) {
            return false;
        }

        for (int i = 0; i < optimized.size(); i++) {
            if (!optimized.get(i).equals(initial.get(i))) {
                return false;
            }
        }

        return true;
    }

    private void removeDeadCode(List<Opcode> ret) {
        List<Opcode> old = new ArrayList<>(opcodes);
        opcodes.clear();
        opcodes.addAll(ret);

        List<Integer> labels = new ArrayList<>();
        for (int i = 0; i < opcodes.size(); i++) {
            if (getOpcode(i) instanceof Opcode.Label) {
                if (test(i + 1, Opcode.Mnemonic.JMP)) {
                    labels.add(i);
                }
            }
        }
        for (int i = 0; i < opcodes.size(); i++) {
            if (test(i, Opcode.Mnemonic.JMP) || test(i, Opcode.Mnemonic.JNZ) || test(i, Opcode.Mnemonic.JZ)) {
                int t = 0;
                for (t = 0; t < labels.size(); t++) {
                    Opcode.Label label = (Opcode.Label) getOpcode(labels.get(t));
                    if (label.label.equals(getOpcode(i).sym)) {
                        break;
                    }
                }
                if (t < labels.size()) {
                    labels.remove(t);
                }
            }
        }
        if (labels.size() > 0) {
            List<String> l = new ArrayList<>();
            for (int i = 0; i < labels.size(); i++) {
                Opcode.Label label = (Opcode.Label) getOpcode(labels.get(i));
                l.add(label.label);
            }
            for (int i = 0; i < opcodes.size(); i++) {
                if (getOpcode(i) instanceof Opcode.Label) {
                    Opcode.Label label = (Opcode.Label) getOpcode(i);
                    for (int t = 0; t < l.size(); t++) {
                        if (l.contains(label.label) && test(i - 1, Opcode.Mnemonic.JMP)) {
//                            System.out.println(label.label);
//                            System.out.print(opcodes.get(i - 1).toString());
                            opcodes.remove(i);
                            opcodes.remove(i);
                            i = 0;
                            break;
                        }
                    }
                }
            }
        }
        ret.clear();
        ret.addAll(opcodes);
        opcodes.clear();
        opcodes.addAll(old);
    }
}
