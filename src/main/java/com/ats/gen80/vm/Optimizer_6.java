/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;
import static com.ats.gen80.vm.AbstractOptimizer.convert;

/**
 *
 * @author lucifer
 */
public class Optimizer_6 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {
        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && testDstRegSymNull(idx + 2, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)) {
            int val1 = convert(getOpcode(idx).data);
            int val2 = convert(getOpcode(idx + 2).data);
            int res = val1 + val2;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, res));
            optimized.add(Opcode.Mnemonic.DAD.create(Register.SP));
            idx += 3;
            return true;
        }
        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.SP)
                && testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.DE)) {

            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 2));
            idx += 3;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL) && getOpcode(idx).sym != null
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)) {
            if (getOpcode(idx + 1).sym == null) {
                int val2 = convert(getOpcode(idx + 1).data);
                String res = getOpcode(idx).sym + (val2 < 0 ? "-" : "+") + Math.abs(val2);
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, res));
                idx += 2;
                return true;
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL) && getOpcode(idx).sym != null
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstRegSymNull(idx + 2, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.DE)) {
            
            if (getOpcode(idx + 2).sym == null) {
                int val2 = convert(getOpcode(idx + 2).data);
                String res = getOpcode(idx).sym + (val2 < 0 ? "-" : "+") + Math.abs(val2);
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, res));
                idx += 4;
                return true;
            }
            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 2));
            idx += 3;
            return true;
        }
        return false;
    }

}
