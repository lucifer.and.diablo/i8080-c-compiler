/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_11 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && testSrcReg(idx + 5, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 6, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 7, Opcode.Mnemonic.DAD, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 6).sym == null) {
                int val = getOpcode(idx).data - (getOpcode(idx + 6).data - 3);

                if (Math.abs(val) < 4) {
                    for (int i = 0; i < 6; i++) {
                        optimized.add(getOpcode(idx + i));
                    }
                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                        } else {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                        }
                    }
                    idx += 7;
                    return true;
                }
            }
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && (test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M) || testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.DE))) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.DE));
            idx += 1;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 1, Opcode.Mnemonic.POP, Register.DE)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && (test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M) || testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.DE))) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.HL));
            idx += 1;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 1, Opcode.Mnemonic.MVI, Register.D)
                && testDstRegSymNull(idx + 2, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 4, Opcode.Mnemonic.XCHG)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.D, Register.E)) {
            int val = getOpcode(idx + 2).data & 0xff;

            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            optimized.add(Opcode.Mnemonic.ADI.create(val));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.D, Register.A));
            idx += 5;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 2, Opcode.Mnemonic.MVI, Register.D)
                && testDstRegSymNull(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 5, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 6, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 7, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && test(idx + 8, Opcode.Mnemonic.JMP)) {
            int val = getOpcode(idx + 3).data & 0xff;

            optimized.add(getOpcode(idx));
            optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
            if (val > 0x7f) {
                optimized.add(Opcode.Mnemonic.SUI.create((-val) & 0xff));
            } else {
                optimized.add(Opcode.Mnemonic.ADI.create(val));
            }

            if (getOpcode(idx).sym != null && getOpcode(idx).sym.equals(getOpcode(idx + 6).sym)) {
                optimized.add(Opcode.Mnemonic.MOV.create(Register.M, Register.A));
                idx += 7;
            } else if (getOpcode(idx).sym == null && getOpcode(idx + 6).sym == null && getOpcode(idx).data == getOpcode(idx + 6).data) {
                optimized.add(Opcode.Mnemonic.MOV.create(Register.E, Register.A));
                idx += 7;
            } else {
                optimized.add(Opcode.Mnemonic.MOV.create(Register.E, Register.A));
                idx += 5;
            }
            return true;
        }

        // ---------------------------------------------------------
        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 1, Opcode.Mnemonic.MVI, Register.A)
                && testSrcReg(idx + 2, Opcode.Mnemonic.ORA, Register.E)) {

            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.ORA.create(Register.M));
            idx += 2;
            return true;
        }

        return false;
    }
}
