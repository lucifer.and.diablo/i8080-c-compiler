/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class IntShrink_1 extends AbstractOptimizer {

    public static int convert(int num) {
        num = num & 0xffff;
        return num > 0x7fff ? num - 0x10000 : num;
    }

    public static void main(String[] args) {
        System.out.println(convert(0x8000));
    }

    @Override
    public boolean optimizeCode() {

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstRegSymNull(idx + 2, Opcode.Mnemonic.LXI, Register.DE)) {
            int val1 = convert(opcodes.get(idx).data);
            int val2 = convert(opcodes.get(idx + 2).data);

            if (test(idx + 3, Opcode.Mnemonic.CALL)) {
                if (opcodes.get(idx + 3).sym == null) {
                    return false;
                }
                switch (opcodes.get(idx + 3).sym) {
                    case ".subInt":
                        val1 = convert(val1 - val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".addInt":
                        val1 = convert(val1 + val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".signedMul":
                        val1 = convert(val1 * val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".intDiv":
                        val1 = convert(val1 / val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".unsignedDiv":
                        val1 = val1 < 0 ? 0x10000 + val1 : val1;
                        val2 = val2 < 0 ? 0x10000 + val2 : val2;
                        val1 = convert(val1 / val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".modInt":
                        val1 = convert(val1 % val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".xorInt":
                        val1 = convert(val1 ^ val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".orInt":
                        val1 = convert(val1 | val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    case ".andInt":
                        val1 = convert(val1 & val2);
                        optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val1));
                        break;
                    default:
                        return false;
                }
                idx += 3;
                return true;
            } else if (testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.HL)
                    && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.DE)
                    && test(idx + 5, Opcode.Mnemonic.XCHG)) {
                int res = convert(val1 + val2);
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, res));
                idx += 5;
                return true;
            } 

        } /*else if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testDstRegSymNull(idx+1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.DE)
                && testDstRegSymNull(idx+3, Opcode.Mnemonic.LXI, Register.DE)) {
            int res = convert(val1 + val2);
            optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, res));
            idx += 3;
            return true;
        }*/

        return false;
    }

}
