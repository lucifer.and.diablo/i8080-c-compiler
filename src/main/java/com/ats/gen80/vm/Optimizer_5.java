/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_5 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {
        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && testSrcReg(idx + 4, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 5, Opcode.Mnemonic.LXI, Register.DE)) {

            optimized.add(Opcode.Mnemonic.XCHG.create());
            if (getOpcode(idx).sym == null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).data));
            } else {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx).sym));
            }
            optimized.add(Opcode.Mnemonic.PUSH.create(Register.HL));
            idx += 4;
            return true;
        }

        if ((testDstReg(idx, Register.D) || testDstReg(idx, Register.E))
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)) {
            optimized.add(getOpcode(idx));
            optimized.add(Opcode.Mnemonic.XCHG.create());
            if (getOpcode(idx + 1).sym == null) {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).data));
            } else {
                optimized.add(Opcode.Mnemonic.SHLD.create(getOpcode(idx + 1).sym));
            }
            optimized.add(Opcode.Mnemonic.XCHG.create());
            idx += 4;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && !test(idx + 2, Opcode.Mnemonic.DAD)
                && (testDstReg(idx + 2, Register.E) || testDstReg(idx + 2, Register.DE))) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.MOV, Register.H, Register.D)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.L, Register.E)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.DE)) {
            idx += 1;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.E, Register.M)) {

            if (getOpcode(idx).sym != null && getOpcode(idx).sym.equals(getOpcode(idx + 2).sym)) {
                optimized.add(getOpcode(idx));
                optimized.add(getOpcode(idx + 1));
                idx += 3;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.SHLD)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && test(idx + 2, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 3, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.LXI, Register.DE)) {
            if (getOpcode(idx).sym != null && getOpcode(idx).sym.equals(getOpcode(idx + 2).sym)) {
                optimized.add(getOpcode(idx));
                idx += 2;
                return true;
            }

        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 5, Opcode.Mnemonic.POP, Register.DE)) {

            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 5));
            idx += 3;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 3, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.DAD, Register.DE)
                && testSrcReg(idx + 5, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 6, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 7, Opcode.Mnemonic.LXI, Register.DE)) {

            optimized.add(getOpcode(idx + 1));
            optimized.add(Opcode.Mnemonic.POP.create(Register.DE));
            idx += 3;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 4, Opcode.Mnemonic.MOV, Register.E)) {
            optimized.add(getOpcode(idx));
            optimized.add(getOpcode(idx + 1));
            idx += 2;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.MVI, Register.D) && testDstReg(idx + 1, Opcode.Mnemonic.MOV, Register.D)) {
            return true;
        }

        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.SHLD)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.DE)
                && test(idx + 4, Opcode.Mnemonic.LHLD)) {
            optimized.add(getOpcode(idx));
            optimized.add(getOpcode(idx + 1));
            idx += 2;
            return true;
        }

        if (test(idx, Opcode.Mnemonic.LHLD)
                && test(idx + 1, Opcode.Mnemonic.MOV)
                && test(idx + 2, Opcode.Mnemonic.LHLD)) {
            if (getOpcode(idx).sym != null && getOpcode(idx).sym.equals(getOpcode(idx + 2).sym)) {
                optimized.add(getOpcode(idx));
                optimized.add(getOpcode(idx + 1));
                idx += 2;
                return true;
            }
        }

        if (test(idx, Opcode.Mnemonic.SHLD)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && test(idx + 2, Opcode.Mnemonic.LHLD)) {
            if (getOpcode(idx).sym != null && getOpcode(idx).sym.equals(getOpcode(idx + 2).sym)) {
                optimized.add(getOpcode(idx));
                optimized.add(getOpcode(idx + 1));
                idx += 2;
                return true;
            }
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.DE)
                && testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)) {
            if (getOpcode(idx + 1).sym != null) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx + 1).sym));
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, getOpcode(idx + 1).data));
            }
            idx += 2;
            return true;
        }

        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.DE)) {
            optimized.add(Opcode.Mnemonic.XCHG.create());
            optimized.add(getOpcode(idx + 1));
            idx += 2;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && test(idx + 3, Opcode.Mnemonic.LHLD)) {
            if (getOpcode(idx).sym == null) {
                int val = convert(getOpcode(idx).data);
                if (Math.abs(val) < 5) {
                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
                        } else {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
                        }
                    }
                    idx += 2;
                    return true;
                }
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 2, Opcode.Mnemonic.XCHG)
                && test(idx + 3, Opcode.Mnemonic.LHLD)) {
            if (getOpcode(idx).sym == null) {
                int val = convert(getOpcode(idx).data);
                if (Math.abs(val) < 5) {
                    for (int i = 0; i < Math.abs(val); i++) {
                        if (val < 0) {
                            optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
                        } else {
                            optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
                        }
                    }
                    idx += 2;
                    return true;
                }
            }
        }

        if ((testDstReg(idx, Opcode.Mnemonic.DCR, Register.E) || testDstReg(idx, Opcode.Mnemonic.INR, Register.E))
                && test(idx + 1, Opcode.Mnemonic.LHLD)
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.LXI, Register.DE)) {
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.POP, Register.DE)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.DE)) {
            idx += 1;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.POP, Register.HL)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.HL)) {
            idx += 1;
            return true;
        }

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.XCHG)
                && test(idx + 2, Opcode.Mnemonic.LHLD)) {
            if (getOpcode(idx).sym != null) {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, getOpcode(idx).sym));
            } else {
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, getOpcode(idx).data));
            }

            idx += 1;
            return true;
        }
        return false;
    }

}
