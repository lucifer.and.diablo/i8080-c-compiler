/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.Gen80Misc;
import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_99 extends AbstractOptimizer {

    private static boolean isAddedMisc = false;
    private static boolean isDeRefput = false;
    private static boolean isIncAdded = false;
    private static boolean isDecAdded = false;
    private static boolean isIntLoad = false;
    private static boolean isIntSave = false;
    private static boolean isRef = false;
    private static final String DEREF = ".deref";
    private static final String PUT_DEREF = ".put.deref";
    private static final String REF = ".ref";
    private static final String POST_DEREF = ".postderef";
    private static final String PUT_POSTDEREF = ".put.postderef";
    private static final String INC_OP = ".inc.op";
    private static final String DEC_OP = ".dec.op";
    private static final String ILOAD = ".iload";
    private static final String ISAVE = ".isave";
    private static final String IVLOAD = ".ivload";
    private static final String IVSAVE = ".ivsave";

    @Override
    public boolean optimizeCode() {

        boolean ret = optimize();

        return ret;
    }

    private boolean optimize() {
        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.A, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.H, Register.M)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.L, Register.A)
                && test(idx + 6, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 7, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 8, Opcode.Mnemonic.MOV, Register.D, Register.M)) {
            int val = getOpcode(idx).data + 2;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, val));
            optimized.add(Opcode.Mnemonic.CALL.create(DEREF));
            idx += 8;
            addMiscDeref();
            return true;
        }
        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.A, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.H, Register.M)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.L, Register.A)
                && test(idx + 6, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 7, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 8, Opcode.Mnemonic.MOV, Register.M, Register.D)) {
            int val = getOpcode(idx).data + 2;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, val));
            optimized.add(Opcode.Mnemonic.CALL.create(PUT_DEREF));
            idx += 8;
            addMiscPutref();
            return true;
        }
        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && (testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.DE) || testSrcReg(idx + 5, Opcode.Mnemonic.DCX, Register.DE))
                && testSrcReg(idx + 6, Opcode.Mnemonic.DCX, Register.HL)
                && test(idx + 7, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 8, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 9, Opcode.Mnemonic.MOV, Register.M, Register.D)) {
            if (getOpcode(idx + 5).mnem == Opcode.Mnemonic.DCX) {
                decOp();
            } else {
                incOp();
            }

            int val = getOpcode(idx).data + 2;

            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, val));
            if (getOpcode(idx + 5).mnem == Opcode.Mnemonic.DCX) {
                optimized.add(Opcode.Mnemonic.CALL.create(DEC_OP));
            } else {
                optimized.add(Opcode.Mnemonic.CALL.create(INC_OP));
            }

            idx += 9;
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && (!(testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.HL) && !(testSrcReg(idx + 5, Opcode.Mnemonic.DCX, Register.HL))))) {
            int val = getOpcode(idx).data + 2;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, val));
            optimized.add(Opcode.Mnemonic.CALL.create(ILOAD));
            iLoadTest();
            idx += 4;
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && (!(testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.HL) && !(testSrcReg(idx + 5, Opcode.Mnemonic.DCX, Register.HL))))) {
            int val = getOpcode(idx).data + 2;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, val));
            optimized.add(Opcode.Mnemonic.CALL.create(ISAVE));
            iSaveTest();
            idx += 4;
            return true;
        }
        // --------------------
        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && testDstRegSymNull(idx+2, Opcode.Mnemonic.LXI, Register.DE)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 4, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.M, Register.D)
                && (!(testSrcReg(idx + 6, Opcode.Mnemonic.INX, Register.HL) && !(testSrcReg(idx + 6, Opcode.Mnemonic.DCX, Register.HL))))) {
            int val = getOpcode(idx).data + 2;
            int val2 = getOpcode(idx+2).data;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, val));
            optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val2));
            optimized.add(Opcode.Mnemonic.CALL.create(ISAVE));
            iLoadTest();
            idx += 5;
            return true;
        }

        // --------------------

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.DE)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)) {
            int val = convert(getOpcode(idx).data);
            if (Math.abs(val) < 4) {

                for (int i = 0; i < val; i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.DE));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.DE));
                    }
                }
                optimized.add(Opcode.Mnemonic.XCHG.create());
                idx += 1;
                return true;
            }
        }
        // ----------------------------
        if (test(idx, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && (testDstRegSymNull(idx + 1, Opcode.Mnemonic.MVI, Register.D) && getOpcode(idx + 1).data == 0)
                && testDstRegSymNull(idx + 2, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 3, Opcode.Mnemonic.DAD, Register.DE)
                && testDstReg(idx + 4, Opcode.Mnemonic.POP, Register.DE)) {
            int val = convert(getOpcode(idx + 2).data);
            if (Math.abs(val) < 4) {
                optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.M));
                optimized.add(Opcode.Mnemonic.MVI.create(Register.H, 0));
                for (int i = 0; i < val; i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    }
                }

                idx += 3;
                return true;
            }
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 2, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testDstRegSymNull(idx + 4, Opcode.Mnemonic.MVI, Register.D)) {
            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 2));
            optimized.add(Opcode.Mnemonic.MVI.create(Register.M, getOpcode(idx).data & 0xff));
            idx += 3;
            return true;

        }
        //--------------------
        if (test(idx, Opcode.Mnemonic.MOV, Register.A, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.H, Register.M)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.L, Register.A)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 6, Opcode.Mnemonic.MOV, Register.D, Register.M)) {
            optimized.add(Opcode.Mnemonic.CALL.create(POST_DEREF));
            idx += 6;
            addMiscDeref();
            return true;
        }
        if (test(idx, Opcode.Mnemonic.MOV, Register.A, Register.M)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.H, Register.M)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.L, Register.A)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.M, Register.E)
                && testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 6, Opcode.Mnemonic.MOV, Register.M, Register.D)) {
            optimized.add(Opcode.Mnemonic.CALL.create(PUT_POSTDEREF));
            idx += 6;
            addMiscPutref();
            return true;
        }
        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && testDstReg(idx + 1, Opcode.Mnemonic.DAD, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.A, Register.M)
                && testSrcReg(idx + 3, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 4, Opcode.Mnemonic.MOV, Register.H, Register.M)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.L, Register.A)) {
            int val = getOpcode(idx).data + 2;
            optimized.add(Opcode.Mnemonic.LXI.create(Register.HL, val));
            optimized.add(Opcode.Mnemonic.CALL.create(REF));
            addRef();
            idx += 5;
            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.DE)
                && testDstRegSymNull(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && (test(idx + 2, Opcode.Mnemonic.CALL) && getOpcode(idx + 2).sym != null && getOpcode(idx + 2).sym.equals(ISAVE))
                && testDstRegSymNull(idx + 3, Opcode.Mnemonic.LXI, Register.DE)
                && testDstRegSymNull(idx + 4, Opcode.Mnemonic.LXI, Register.HL)
                && (test(idx + 5, Opcode.Mnemonic.CALL) && getOpcode(idx + 5).sym != null && getOpcode(idx + 5).sym.equals(ISAVE))) {

            optimized.add(getOpcode(idx));
            optimized.add(getOpcode(idx + 1));
            optimized.add(getOpcode(idx + 2));

            if (getOpcode(idx).data != getOpcode(idx + 3).data) {
                optimized.add(getOpcode(idx + 3));
            }

            int val = getOpcode(idx + 1).data + 1 - getOpcode(idx + 4).data;

            if (getOpcode(idx).data == getOpcode(idx + 3).data) {
                if (val > getOpcode(idx + 4).data + 1 - getOpcode(idx + 1).data) {
                    val = getOpcode(idx + 4).data + 1 - getOpcode(idx + 1).data;
                    optimized.get(1).data = getOpcode(idx + 4).data;
                }
            }

            if (Math.abs(val) < 4) {
                for (int i = 0; i < Math.abs(val); i++) {
                    if (val < 0) {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    }
                }
                Opcode op = getOpcode(idx + 5).copy();
                op.sym = op.sym + "+1";
                optimized.add(op);
            } else {
                optimized.add(getOpcode(idx + 4));
                optimized.add(getOpcode(idx + 5));
            }

            idx += 5;

            return true;
        }

        if (testDstRegSymNull(idx, Opcode.Mnemonic.LXI, Register.HL)
                && (test(idx + 1, Opcode.Mnemonic.CALL) && getOpcode(idx + 1).sym != null && getOpcode(idx + 1).sym.equals(ILOAD))
                && testSrcReg(idx + 2, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstRegSymNull(idx + 3, Opcode.Mnemonic.LXI, Register.HL)
                && (test(idx + 4, Opcode.Mnemonic.CALL) && getOpcode(idx + 4).sym != null && getOpcode(idx + 4).sym.equals(ILOAD))) {
            int val = getOpcode(idx + 3).data - getOpcode(idx).data - 1;
            if (Math.abs(val) < 4) {
                optimized.add(getOpcode(idx));
                optimized.add(getOpcode(idx + 1));
                optimized.add(getOpcode(idx + 2));
                for (int i = 0; i < Math.abs(val); i++) {
                    if (val > 0) {
                        optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                    } else {
                        optimized.add(Opcode.Mnemonic.DCX.create(Register.HL));
                    }
                }
                Opcode op = getOpcode(idx + 4).copy();
                op.sym = op.sym + " + 1";
                optimized.add(op);
                idx += 4;
                return true;
            }
        }

        // --------------------------------------------------------------------
        return false;
    }

    private void addMiscDeref() {
        if (isAddedMisc) {
            return;
        }
        isAddedMisc = true;
        Gen80Misc.misc
                .label(DEREF)
                .dad(Register.SP)
                .label(POST_DEREF)
                .mov(Register.A, Register.M)
                .inx(Register.HL)
                .mov(Register.H, Register.M)
                .mov(Register.L, Register.A)
                .mov(Register.E, Register.M)
                .inx(Register.HL)
                .mov(Register.D, Register.M)
                .ret();
    }

    private void decOp() {
        if (isDecAdded) {
            return;
        }
        isDecAdded = true;
        Gen80Misc.misc
                .label(DEC_OP)
                .dad(Register.SP)
                .mov(Register.E, Register.M)
                .inx(Register.HL)
                .mov(Register.D, Register.M)
                .dcx(Register.HL)
                .dcx(Register.DE)
                .mov(Register.M, Register.E)
                .inx(Register.HL)
                .mov(Register.M, Register.D)
                .ret();
    }

    private void incOp() {
        if (isIncAdded) {
            return;
        }
        isIncAdded = true;

        Gen80Misc.misc
                .label(INC_OP)
                .dad(Register.SP)
                .mov(Register.E, Register.M)
                .inx(Register.HL)
                .mov(Register.D, Register.M)
                .dcx(Register.HL)
                .inx(Register.DE)
                .mov(Register.M, Register.E)
                .inx(Register.HL)
                .mov(Register.M, Register.D)
                .ret();
    }

    private void iLoadTest() {
        if (isIntLoad) {
            return;
        }
        isIntLoad = true;

        Gen80Misc.misc
                .label(ILOAD)
                .dad(Register.SP)
                .mov(Register.E, Register.M)
                .inx(Register.HL)
                .mov(Register.D, Register.M)
                .ret();

    }

    private void iSaveTest() {
        if (isIntSave) {
            return;
        }
        isIntSave = true;

        Gen80Misc.misc
                .label(ISAVE)
                .dad(Register.SP)
                .mov(Register.M, Register.E)
                .inx(Register.HL)
                .mov(Register.M, Register.D)
                .ret();
    }

    private void addRef() {
        if (isRef) {
            return;
        }
        isRef = true;
        Gen80Misc.misc
                .label(REF)
                .dad(Register.SP)
                .mov(Register.A, Register.M)
                .inx(Register.HL)
                .mov(Register.H, Register.M)
                .mov(Register.L, Register.A)
                .ret();
    }

    private void addMiscPutref() {
        if (isDeRefput) {
            return;
        }
        isDeRefput = true;
        Gen80Misc.misc
                .label(PUT_DEREF)
                .dad(Register.SP)
                .label(PUT_POSTDEREF)
                .mov(Register.A, Register.M)
                .inx(Register.HL)
                .mov(Register.H, Register.M)
                .mov(Register.L, Register.A)
                .mov(Register.M, Register.E)
                .inx(Register.HL)
                .mov(Register.M, Register.D)
                .ret();
    }
}
