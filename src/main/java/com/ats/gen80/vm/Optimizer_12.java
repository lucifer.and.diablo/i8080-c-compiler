/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Opcode.Label;
import com.ats.gen80.opcodes.Register;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucifer
 */
public class Optimizer_12 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {
        if (testSrcReg(idx, Opcode.Mnemonic.INX, Register.SP)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.SP)
                && (testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.DE) || testDstReg(idx + 2, Opcode.Mnemonic.POP, Register.HL))) {
            optimized.add(getOpcode(idx + 2));
            idx += 1;
            return true;
        }
        if (testSrcReg(idx, Opcode.Mnemonic.INX, Register.SP)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.SP)
                && (testDstReg(idx + 2, Opcode.Mnemonic.MVI, Register.D)
                || testDstReg(idx + 2, Opcode.Mnemonic.MVI, Register.E)
                || testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.DE))) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.DE));
            idx += 1;
            return true;
        }
        if (testSrcReg(idx, Opcode.Mnemonic.INX, Register.SP)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.SP)
                && (testDstReg(idx + 2, Opcode.Mnemonic.MVI, Register.H)
                || testDstReg(idx + 2, Opcode.Mnemonic.MVI, Register.L)
                || testDstReg(idx + 2, Opcode.Mnemonic.LXI, Register.HL))) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.HL));
            idx += 1;
            return true;
        }
        if (testSrcReg(idx, Opcode.Mnemonic.INX, Register.SP)
                && testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.SP)
                && test(idx + 2, Opcode.Mnemonic.LHLD)) {
            optimized.add(Opcode.Mnemonic.POP.create(Register.HL));
            idx += 1;
            return true;
        }
        if (test(idx, Opcode.Mnemonic.XCHG)
                && test(idx + 1, Opcode.Mnemonic.CALL)) {

            return getOpcode(idx + 1).sym != null && !getOpcode(idx + 1).sym.startsWith(".");
        }

        if (test(idx, Opcode.Mnemonic.JMP) || test(idx, Opcode.Mnemonic.JNZ) || test(idx, Opcode.Mnemonic.JZ)) {

            if (getOpcode(idx).sym != null && !getOpcode(idx).sym.startsWith(".")) {
                int dd = findLabelIdx(getOpcode(idx).sym);
                if (dd != -1) {
                    if (test(dd + 1, Opcode.Mnemonic.JMP)) {
                        getOpcode(idx).sym = getOpcode(dd + 1).sym;
                        getOpcode(idx).data = getOpcode(dd + 1).data;
                        idx--;
                        return true;
                    }
                }
            }
        }

        if (test(idx, Opcode.Mnemonic.JMP) || test(idx, Opcode.Mnemonic.JNZ) || test(idx, Opcode.Mnemonic.JZ)) {
            if (getOpcode(idx + 1) instanceof Opcode.Label) {
                Label label = (Label) getOpcode(idx + 1);
                if (getOpcode(idx).sym.equals(label.label)) {
                    return true;
                }
            }
        }
        // testDstReg(idx , Opcode.Mnemonic.POP, Register.DE)
        // testSrcReg(idx + 3, Opcode.Mnemonic.PUSH, Register.HL)
        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testDstReg(idx + 3, Opcode.Mnemonic.MVI, Register.D)) {

            int i = 4;
            while (testSrcReg(idx + i, Opcode.Mnemonic.INX, Register.DE) || testSrcReg(idx + i, Opcode.Mnemonic.DCX, Register.DE)) {
                i++;
            }
            if (testDstReg(idx + i, Opcode.Mnemonic.POP, Register.HL)
                    && testDstReg(idx + i + 1, Opcode.Mnemonic.DAD, Register.DE)) {

                optimized.add(getOpcode(idx + 1));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.M));
                optimized.add(Opcode.Mnemonic.MVI.create(Register.H, 0));
                for (int t = 4; t < i; t++) {
                    Opcode op = getOpcode(idx + t);
                    op.srcReg = Register.HL;
                    optimized.add(op);
                }
                optimized.add(Opcode.Mnemonic.DAD.create(Register.DE));
                idx += i + 1;
                return true;
            }
        }
        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && (testSrcReg(idx + 1, Opcode.Mnemonic.INX, Register.HL)
                || testSrcReg(idx + 1, Opcode.Mnemonic.DCX, Register.HL))) {

            int i = 2;
            while (testSrcReg(idx + i, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + i, Opcode.Mnemonic.DCX, Register.HL)) {
                i++;
            }
            if (test(idx + i, Opcode.Mnemonic.MOV, Register.E, Register.M)
                    && testSrcReg(idx + i + 1, Opcode.Mnemonic.INX, Register.HL)
                    && test(idx + i + 2, Opcode.Mnemonic.MOV, Register.D, Register.M)) {

                optimized.add(getOpcode(idx + 1));
                for (int t = 1; t < i; t++) {
                    Opcode op = getOpcode(idx + t);
                    op.srcReg = Register.HL;
                    optimized.add(op);
                }
                optimized.add(Opcode.Mnemonic.MOV.create(Register.A, Register.M));
                optimized.add(Opcode.Mnemonic.INX.create(Register.HL));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.M));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.H, Register.A));
                idx += i + 3;
                return true;
            }
        }
        if (testSrcReg(idx, Opcode.Mnemonic.PUSH, Register.DE)
                && testDstReg(idx + 1, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 2, Opcode.Mnemonic.MOV, Register.L, Register.M)
                && testDstReg(idx + 3, Opcode.Mnemonic.MVI, Register.H)) {

            int i = 4;
            while (testSrcReg(idx + i, Opcode.Mnemonic.INX, Register.HL) || testSrcReg(idx + i, Opcode.Mnemonic.DCX, Register.HL)) {
                i++;
            }
            if (testDstReg(idx + i, Opcode.Mnemonic.POP, Register.DE)
                    && testDstReg(idx + i + 1, Opcode.Mnemonic.DAD, Register.DE)) {

                optimized.add(getOpcode(idx + 1));
                optimized.add(Opcode.Mnemonic.MOV.create(Register.L, Register.M));
                optimized.add(Opcode.Mnemonic.MVI.create(Register.H, 0));
                for (int t = 4; t < i; t++) {
                    Opcode op = getOpcode(idx + t);
                    op.srcReg = Register.HL;
                    optimized.add(op);
                }
                optimized.add(Opcode.Mnemonic.DAD.create(Register.DE));
                idx += i + 1;
                return true;
            }
        }

        if (testDstReg(idx, Opcode.Mnemonic.MVI, Register.D)
                && testSrcReg(idx + 1, Opcode.Mnemonic.PUSH, Register.DE)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.SP)
                && testDstReg(idx + 3, Opcode.Mnemonic.MVI, Register.D)
                && testSrcReg(idx + 4, Opcode.Mnemonic.PUSH, Register.DE)
                && testSrcReg(idx + 5, Opcode.Mnemonic.INX, Register.SP)) {
            if (getOpcode(idx).sym == null && getOpcode(idx + 3).sym == null) {

                int val = (getOpcode(idx + 3).data & 0xff) | ((getOpcode(idx).data & 0xff) << 8);
                optimized.add(Opcode.Mnemonic.LXI.create(Register.DE, val));
                optimized.add(Opcode.Mnemonic.PUSH.create(Register.DE));
                idx += 5;
                return true;
            }
        }

        return false;
    }

    private int findLabelIdx(String target) {
        int ret = -1;
        for (int i = idx; i < opcodes.size(); i++) {
            if (getOpcode(i) instanceof Opcode.Label) {
                Opcode.Label label = (Opcode.Label) getOpcode(i);
                if (label.label.equals(target)) {
                    return i;
                }
            }
        }
        return ret;
    }
}
