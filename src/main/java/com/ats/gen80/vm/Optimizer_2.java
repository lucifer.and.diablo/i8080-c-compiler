/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.vm;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;

/**
 *
 * @author lucifer
 */
public class Optimizer_2 extends AbstractOptimizer {

    @Override
    public boolean optimizeCode() {

        if (testDstReg(idx, Opcode.Mnemonic.LXI, Register.HL)
                && test(idx + 1, Opcode.Mnemonic.MOV, Register.E, Register.M)
                && testSrcReg(idx + 2, Opcode.Mnemonic.INX, Register.HL)
                && test(idx + 3, Opcode.Mnemonic.MOV, Register.D, Register.M)
                && test(idx + 4, Opcode.Mnemonic.XCHG)
                && test(idx + 5, Opcode.Mnemonic.MOV, Register.E, Register.M)) {

            if (opcodes.get(idx).sym != null) {
                optimized.add(Opcode.Mnemonic.LHLD.create(opcodes.get(idx).sym));
                idx += 4;
                return true;
            }
        }
        return false;
    }

}
