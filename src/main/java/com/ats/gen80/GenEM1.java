/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.Keyword;
import com.ats.gen80.node.Node;
import com.ats.gen80.node.NodeAsm;
import com.ats.gen80.node.NodeBinary;
import com.ats.gen80.node.NodeCompoundStatement;
import com.ats.gen80.node.NodeDeclaration;
import com.ats.gen80.node.NodeDest;
import com.ats.gen80.node.NodeFloat;
import com.ats.gen80.node.NodeFunctionCall;
import com.ats.gen80.node.NodeFunctionDeclaration;
import com.ats.gen80.node.NodeFunctionPointerCall;
import com.ats.gen80.node.NodeGlobalLabel;
import com.ats.gen80.node.NodeGoto;
import com.ats.gen80.node.NodeIf;
import com.ats.gen80.node.NodeInitializer;
import com.ats.gen80.node.NodeInteger;
import com.ats.gen80.node.NodeJump;
import com.ats.gen80.node.NodeLabel;
import com.ats.gen80.node.NodeLocalLabel;
import com.ats.gen80.node.NodeReturn;
import com.ats.gen80.node.NodeString;
import com.ats.gen80.node.NodeStruct;
import com.ats.gen80.node.NodeUnary;
import com.ats.gen80.node.NodeVariable;
import com.ats.gen80.node.Type;
import com.ats.gen80.node.TypeKind;
import com.ats.gen80.opcodes.Opcode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lucifer
 */
public class GenEM1 {

    public Gen80 code = new Gen80();
    public Gen80 data = new Gen80();
    public Map<Integer, Gen80> dataList = new HashMap<Integer, Gen80>();
    public Gen80 bss = new Gen80();
    public Preprocessor proc;

    public GenEM1(Preprocessor proc) {
        this.proc = proc;
        dataList.put(0, data);
    }

    private static final com.ats.gen80.opcodes.Register A = com.ats.gen80.opcodes.Register.A;
    private static final com.ats.gen80.opcodes.Register B = com.ats.gen80.opcodes.Register.B;
    private static final com.ats.gen80.opcodes.Register C = com.ats.gen80.opcodes.Register.C;
    private static final com.ats.gen80.opcodes.Register D = com.ats.gen80.opcodes.Register.D;
    private static final com.ats.gen80.opcodes.Register E = com.ats.gen80.opcodes.Register.E;
    private static final com.ats.gen80.opcodes.Register H = com.ats.gen80.opcodes.Register.H;
    private static final com.ats.gen80.opcodes.Register L = com.ats.gen80.opcodes.Register.L;
    private static final com.ats.gen80.opcodes.Register M = com.ats.gen80.opcodes.Register.M;

    private static final com.ats.gen80.opcodes.Register BC = com.ats.gen80.opcodes.Register.BC;
    private static final com.ats.gen80.opcodes.Register DE = com.ats.gen80.opcodes.Register.DE;
    private static final com.ats.gen80.opcodes.Register HL = com.ats.gen80.opcodes.Register.HL;
    private static final com.ats.gen80.opcodes.Register SP = com.ats.gen80.opcodes.Register.SP;
    private NodeFunctionDeclaration func;

    private void saveLocalBase() {
        code.push(BC);
        code.lxi(HL, 0);
        code.dad(SP);
        code.mov(B, H);
        code.mov(C, L);
    }

    private void restoreLocalBase() {
        code.pop(BC);
    }

    public int align(int size) {
        return size;// < 2 ? 2 : size % 2 == 1 ? size + 1 : size;
    }

    public void genProlog(NodeFunctionDeclaration n) {
        func = n;
        int val = 0;
        int localoff = 0;

        code.prefix = "code." + n.functionName;
        data.prefix = "data." + n.functionName;

        saveLocalBase();

        int fp = -4;

        List<Node> fn = new ArrayList<>(n.functionParameters);
        Collections.reverse(fn);

        for (Node l : fn) {
            int al = align(l.cast(NodeLocalLabel.class).type.size);
            l.cast(NodeLocalLabel.class).localOffset = fp;
            fp -= al;
        }

        for (Node l : n.localVariables) {
            l.cast(NodeLocalLabel.class).localOffset = localoff + align(l.type.size);
            localoff += align(l.cast(NodeLocalLabel.class).type.size);
        }

        switch (localoff) {
            case 4:
                code.push(HL);
            case 2:
                code.push(HL);
            case 0:
                break;
            default:
                code.lxi(HL, -localoff);
                code.dad(SP);
                code.sphl();
        }

        code.setStack(localoff);

    }

    public void genEpilog(NodeFunctionDeclaration n) {
        code.label("L." + n.functionName + ".leave");

        if (code.getStack() != 0) {
            switch (code.getStack()) {
                case 4:
                    code.pop(HL);
                case 2:
                    code.pop(HL);
                case 0:
                    break;
                default:
                    code.lxi(HL, code.getStack());
                    code.dad(SP);
                    code.sphl();
            }
        }

        restoreLocalBase();
        code.ret();
    }

    public GenEM1 emitExpr(Node node) {
//        System.out.println(node.kind);
        switch (node.kind) {
            case AST_LITERAL:
                emitLiteral(node);
                return this;
            case AST_LVAR:
                emitLVar(node);
                return this;
            case AST_GVAR:
                emitGVar(node);
                return this;
            case AST_FUNCDESG:
                return this;
            case AST_FUNCALL:
                if (maybeEmitBuiltin(node)) {
                    return this;
                }
            case AST_FUNCPTR_CALL:
                emitFuncCall(node);
                return this;
            case AST_DECL:
                emitDecl(node);
                return this;
            case AST_CONV:
                emitConv(node);
                return this;
            case AST_ADDR:
                emitAddr(node.cast(NodeUnary.class).operand);
                return this;
            case AST_DEREF:
                emitDeref(node);
                return this;
            case AST_IF:
            case AST_TERNARY:
                emitTernary(node);
                return this;
            case AST_GOTO:
                emitGoto(node);
                return this;
            case AST_LABEL:
                if (node instanceof NodeDest) {
                    if (node.cast(NodeDest.class).newLabel != null) {
                        emitLabel(node.cast(NodeDest.class).newLabel);
                    }
                } else if (node.cast(NodeLabel.class).newLabel != null) {
                    emitLabel(node.cast(NodeLabel.class).newLabel);
                }
                return this;
            case AST_RETURN:
                emitReturn(node);
                return this;
            case AST_COMPOUND_STMT:
                emitCompoundStmt(node);
                return this;
            case AST_STRUCT_REF:
                emitLoadStructRef(node.cast(NodeStruct.class).struct, node.type, 0);
                return this;
            case OP_PRE_INC:
                emitPreIncDec(node, "add");
                return this;
            case OP_PRE_DEC:
                emitPreIncDec(node, "sub");
                return this;
            case OP_POST_INC:
                emitPostIncDec(node, "add");
                return this;
            case OP_POST_DEC:
                emitPostIncDec(node, "sub");
                return this;
            case C_EXPOINT:
                emitLogNot(node);
                return this;
            case C_AND:
                emitBitAnd(node);
                return this;
            case C_OR:
                emitBitOr(node);
                return this;
            case C_TILDA:
                emitBitNot(node);
                return this;
            case OP_LOGAND:
                emitLogAnd(node);
                return this;
            case OP_LOGOR:
                emitLogOr(node);
                return this;
            case OP_CAST:
                emitCast(node);
                return this;
            case C_COMMA:
                emitComma(node);
                return this;
            case C_EQ:
                emitAssign(node);
                return this;
            case OP_LABEL_ADDR:
                emitLabelAddr(node);
                return this;
            case AST_COMPUTED_GOTO:
                emitComputedGoto(node);
                return this;
            case AST_ASM:
                emitAsm(node);
                return this;
            default:
                emitBinOp(node);
        }
        return this;
    }

    private void emitLiteral(Node node) {
        switch (node.type.kind) {
            case KIND_CHAR:
            case KIND_INT:
                int val = node.cast(NodeInteger.class).val;
                code.lxi(DE, val);
                break;
            case KIND_LONG:
                val = node.cast(NodeInteger.class).val;
                code.lxi(DE, val >> 16);
                code.push(DE);
                code.lxi(DE, val);
                break;
            case KIND_FLOAT:
                val = Float.floatToRawIntBits((float) node.cast(NodeFloat.class).val);
                code.lxi(DE, val >> 16);
                code.push(DE);
                code.lxi(DE, val);
                break;
            case KIND_ARRAY:
                NodeString s = node.cast();
                if (s.name == null) {
                    s.name = data.genLabel();
                    data.label(s.name);
                    data.db(s.val);
                }
                code.lxi(DE, s.name);
                break;

        }
    }

    private void emitLVar(Node node) {
        ensureLVarInit(node);
        emitLLoad(node.type, BC, node.cast(NodeLocalLabel.class).localOffset);
    }

    private void emitGLoad(Type type, String label, int off) {
        code.lxi(HL, label);
        if (off != 0) {
            code.lxi(DE, off);
            code.dad(DE);
        }
        if (type.kind == TypeKind.KIND_ARRAY) {
            code.xchg();
        } else if (type.kind == TypeKind.KIND_INT || type.kind == TypeKind.KIND_PTR) {
            Gen80Misc.loadInt(code);
        } else if (type.kind == TypeKind.KIND_CHAR) {
            Gen80Misc.loadChar(code);
        } else {
            Gen80Misc.loadLong(code);
        }
    }

    private void emitGVar(Node node) {
        emitGLoad(node.type, node.cast(NodeGlobalLabel.class).globaLabel, 0);
    }

    private boolean maybeEmitBuiltin(Node node) {
        return false;
    }

    private void classifyArgs(List<Node> ints, List<Node> floats, List<Node> rest, List<Node> args) {
        for (Node node : args) {
            if (node.type.kind == TypeKind.KIND_STRUCT) {
                rest.add(node);
            } else if (node.type.kind == TypeKind.KIND_FLOAT) {
                floats.add(node);
            } else {
                ints.add(node);
            }
        }
    }

    private int pushStruct(int size) {
        code.lxi(HL, -size);
        code.dad(SP).sphl().xchg();

        code.push(HL);
        code.push(DE);
        code.lxi(HL, size);
        code.push(HL);

        Gen80Misc.copyStruct(code);
        code.modifyStack(size);
        return size;
    }

    private void emitFuncCall(Node node) {
        NodeFunctionCall func = node.cast();
//        List<Node> ints = new ArrayList<>();
//        List<Node> floats = new ArrayList<>();
//        List<Node> rest = new ArrayList<>();
//
//        classifyArgs(ints, floats, rest, func.arguments);

        int offset = 0;

        for (Node n : func.arguments) {
            if (n.type.kind == TypeKind.KIND_STRUCT) {
                emitAddr(n);
                offset += pushStruct(align(n.type.size));
            } else {
                emitExpr(n);
                if (n.type.kind == TypeKind.KIND_CHAR) {

                    code.modifyStack(-1);
                    code.mov(D, E);
                    code.push(DE);
                    code.inx(SP);
                } else {
                    code.push(DE);
                }

                offset += align(n.type.size);
            }
        }

        if (node.kind == Keyword.AST_FUNCPTR_CALL) {
            NodeFunctionPointerCall fpc = node.cast();
            emitExpr(fpc.functionPointer);
            code.push(DE);
            code.lxi(HL, "$+5");
            code.xthl();
            code.pchl();
        } else {
            code.call(func.functionName);
        }

        if (offset != 0) {
            code.lxi(HL, offset);
            code.dad(SP);
            code.sphl();
            code.modifyStack(-offset);
        }
    }

    private void emitDecl(Node node) {
        NodeDeclaration nd = node.cast();
        if (nd.declarationInit == null || nd.declarationInit.size() == 0) {
            return;
        }
        NodeLocalLabel nll = nd.declarationVariable.cast(NodeLocalLabel.class);
        emitDeclInit(nd.declarationInit, nll.localOffset, nll.type.size);
    }

    private void emitConv(Node node) {
        emitExpr(node.cast(NodeUnary.class).operand);
        emitLoadConvert(node.type, node.cast(NodeUnary.class).operand.type);
    }

    private void emitAddr(Node node) {
        switch (node.kind) {
            case AST_LVAR:
                ensureLVarInit(node);
                code.lxi(HL, -(node.cast(NodeLocalLabel.class).localOffset));
                code.dad(BC);
                code.xchg();
                break;
            case AST_GVAR:
                code.lxi(DE, node.cast(NodeGlobalLabel.class).globaLabel);
                break;
            case AST_DEREF:
                emitExpr(node.cast(NodeUnary.class).operand);
                break;
            case AST_STRUCT_REF:
                emitAddr(node.cast(NodeStruct.class).struct);
                code.lxi(HL, node.type.offset);
                code.dad(DE);
                code.xchg();
                break;
            case AST_FUNCDESG:
                code.lxi(DE, node.cast(NodeFunctionCall.class).functionName);
                break;
            default:
                throw new RuntimeException("Illegal operand");
        }
    }

    private void emitDeref(Node node) {
        NodeUnary nu = node.cast();
        emitExpr(nu.operand);
        emitLLoad(nu.operand.type.pointer, HL, 0);
        emitLoadConvert(node.type, nu.operand.type.pointer);
    }

    private void emitTernary(Node node) {
        NodeIf nif = node.cast();
        emitExpr(nif.condition);
        String ne = code.genLabel();
        code.mov(A, D);
        code.ora(E);
        if (nif.condition.type.size == 4) {
//            code.stack += 2;
            code.pop(HL);
            code.ora(H);
            code.ora(L);
        }
        code.jz(ne);

        if (nif.then != null) {
            emitExpr(nif.then);
        }

        if (nif.els != null) {
            String end = code.genLabel();
            code.jmp(end);
            code.label(ne);
            emitExpr(nif.els);
            code.label(end);
        } else {
            code.label(ne);
        }
    }

    private void emitGoto(Node node) {
        if (node instanceof NodeJump) {
            code.jmp(node.cast(NodeJump.class).newLabel);
        } else {
            code.jmp(node.cast(NodeGoto.class).label);
        }
    }

    private void emitLabel(String newLabel) {
        code.label(newLabel);
    }

    private void emitReturn(Node node) {
        NodeReturn nf = node.cast();
        if (nf.returnValue != null) {
            emitExpr(nf.returnValue);
        }

        code.jmp("L." + func.functionName + ".leave");
    }

    private void emitCompoundStmt(Node node) {
        NodeCompoundStatement ncs = node.cast();
        for (Node n : ncs.statements) {
            int stack = code.getStack();
            emitExpr(n);
            if (code.getStack() != stack) {
                code.lxi(HL, code.getStack() - stack);
                code.dad(SP);
                code.sphl();
                code.setStack(stack);
            }
        }
    }

    private void emitLoadStructRef(Node struct, Type field, int off) {
        switch (struct.kind) {
            case AST_LVAR:
                NodeLocalLabel nll = struct.cast();
                ensureLVarInit(struct);
                emitLLoad(field, BC, off - field.offset + nll.localOffset);
                break;
            case AST_GVAR:
                emitGLoad(field, struct.cast(NodeGlobalLabel.class).globaLabel, off + field.offset);
                break;
            case AST_STRUCT_REF:
                emitLoadStructRef(struct.cast(NodeStruct.class).struct, field, off + struct.type.offset);
                break;
            case AST_DEREF:
                emitExpr(struct.cast(NodeUnary.class).operand);
                emitLLoad(field, BC, off + field.offset);
                break;
            default:
                throw new RuntimeException("Internal error");
        }
    }

    private void emitPreIncDec(Node node, String add) {
        NodeUnary nu = node.cast(NodeUnary.class);

        emitExpr(nu.operand);

        switch (nu.type.kind) {
            case KIND_CHAR:
                switch (add) {
                    case "add":
                        code.inr(E);
                        break;
                    case "sub":
                        code.dcr(E);
                        break;
                }
                emitStore(node.cast(NodeUnary.class).operand);
                break;
            case KIND_INT:
                switch (add) {
                    case "add":
                        code.inx(DE);
                        break;
                    case "sub":
                        code.dcx(DE);
                        break;
                }
                emitStore(node.cast(NodeUnary.class).operand);
                break;
            case KIND_PTR:
                code.xchg();
                code.lxi(DE, add.equals("add") ? node.type.pointer.size : -node.type.pointer.size);
                code.dad(DE);
                code.xchg();
                emitStore(node.cast(NodeUnary.class).operand);
                break;
            case KIND_LONG:
                code.push(DE);
                code.lxi(DE, 0);
                code.push(DE);
                code.inr(E);

//                code.stack -= 2;
                switch (add) {
                    case "add":
                        Gen80Misc.addLong(code);
                        break;
                    case "sub":
                        Gen80Misc.subLong(code);
                        break;
                }

                code.pop(HL);
                code.push(HL);
                code.push(DE);
                code.push(HL);
                emitStore(node.cast(NodeUnary.class).operand);
                code.pop(HL);
                code.pop(DE);
                break;
            default:
                throw new RuntimeException("Invalid operand");

        }

    }

    private void emitPostIncDec(Node node, String add) {
        NodeUnary nu = node.cast(NodeUnary.class);

        emitExpr(nu.operand);

        switch (nu.type.kind) {
            case KIND_CHAR:
                code.push(DE);
                switch (add) {
                    case "add":
                        code.inr(E);
                        break;
                    case "sub":
                        code.dcr(E);
                        break;
                }
                emitStore(node.cast(NodeUnary.class).operand);
                code.pop(DE);
                break;
            case KIND_INT:
                code.push(DE);
                switch (add) {
                    case "add":
                        code.inx(DE);
                        break;
                    case "sub":
                        code.dcx(DE);
                        break;
                }
                emitStore(node.cast(NodeUnary.class).operand);
                code.pop(DE);
                break;
            case KIND_PTR:
                code.push(DE);
                code.xchg();
                code.lxi(DE, add.equals("add") ? node.type.pointer.size : -node.type.pointer.size);
                code.dad(DE);
                code.xchg();
                emitStore(node.cast(NodeUnary.class).operand);
                code.pop(DE);
                break;
            case KIND_LONG:
                code.pop(HL);
                code.push(HL);
                code.push(DE);
                code.push(HL);

                code.push(DE);
                code.lxi(DE, 0);
                code.push(DE);
                code.inr(E);

                switch (add) {
                    case "add":
                        Gen80Misc.addLong(code);
                        break;
                    case "sub":
                        Gen80Misc.subLong(code);
                        break;
                }

                emitStore(node.cast(NodeUnary.class).operand);

                code.pop(DE);
                break;
            default:
                throw new RuntimeException("Invalid operand");

        }

    }

    private void emitLogNot(Node node) {
        NodeUnary nu = node.cast();
        emitExpr(nu.operand);
        Gen80Misc.logicalNot(nu.operand.type, code);
        if (nu.operand.type.size == 4) {
            code.pop(HL);
        }
    }

    private void emitBitAnd(Node node) {
        NodeBinary nu = node.cast();
        emitExpr(nu.left);
        code.push(DE);
        emitExpr(nu.right);
        Gen80Misc.binaryAnd(nu.type, code);
    }

    private void emitBitOr(Node node) {
        NodeBinary nu = node.cast();
        emitExpr(nu.left);
        code.push(DE);
        emitExpr(nu.right);
        Gen80Misc.binaryOr(nu.type, code);
    }

    private void emitBitNot(Node node) {
        NodeUnary nu = node.cast();
        emitExpr(nu.operand);
        Gen80Misc.binaryNeg(nu.operand.type, code);
    }

    private void emitTestAnd(Node node, String endLabel) {
        emitExpr(node);
        code.mov(A, E);
        code.ora(D);
        if (node.type.size == 4) {
            code.modifyStack(+2);
            code.pop(HL);
            code.ora(H);
            code.ora(L);
        }
        code.lxi(DE, 0);
        code.jz(endLabel);
        code.inr(E);
    }

    private void emitLogAnd(Node node) {

        NodeBinary nu = node.cast();
        String testEnd = code.genLabel();

        emitTestAnd(nu.left, testEnd);
        emitTestAnd(nu.right, testEnd);

        code.label(testEnd);
        if (node.type.size == 4) {
            code.lxi(HL, 0);
            code.push(HL);
        }
    }

    private void emitTestOr(Node node, String endLabel) {
        emitExpr(node);
        code.mov(A, E);
        code.ora(D);
        if (node.type.size == 4) {
            code.modifyStack(2);
            code.pop(HL);
            code.ora(H);
            code.ora(L);
        }
        code.lxi(DE, 1);
        code.jnz(endLabel);
        code.dcr(E);
    }

    private void emitLogOr(Node node) {
        NodeBinary nu = node.cast();

        String testEnd = code.genLabel();

        emitTestOr(nu.left, testEnd);
        emitTestOr(nu.right, testEnd);

        code.label(testEnd);
        if (node.type.size == 4) {
            code.lxi(HL, 0);
            code.push(HL);
        }
    }

    private void emitCast(Node node) {
        NodeUnary op = node.cast();
        emitExpr(op.operand);
        emitLoadConvert(op.type, op.operand.type);
    }

    private void emitComma(Node node) {
        NodeBinary n = node.cast();
        emitExpr(n.left);
        emitExpr(n.right);
    }

    private void emitAssign(Node node) {
        NodeBinary n = node.cast();
        if (n.left.type.kind == TypeKind.KIND_STRUCT && n.left.type.size > 2) {
            emitCopyStruct(n.left, n.right);
        } else {
            emitExpr(n.right);
            emitLoadConvert(n.type, n.right.type);
            emitStore(n.left);
        }
    }

    private void emitLabelAddr(Node node) {
        code.lxi(DE, node.cast(NodeLabel.class).newLabel);
    }

    private void emitComputedGoto(Node node) {
        emitExpr(node.cast(NodeUnary.class).operand);
        code.xchg();
        code.pchl();
    }

    private void emitBinOp(Node node) {
        if (node.type.kind == TypeKind.KIND_PTR) {
            NodeBinary nb = node.cast();
            emitPointerArith(node.kind, nb.left, nb.right);
            return;
        }

        switch (node.kind) {
            case C_LT:
                emitComp("L", node);
                return;
            case OP_EQ:
                emitComp("E", node);
                return;
            case OP_LE:
                emitComp("LE", node);
                return;
            case OP_NE:
                emitComp("NE", node);
                return;
        }

        if (Node.isIntType(node.type)) {
            emitBinOpIntArith(node);
        } else if (Node.isFloType(node.type)) {
            emitBinOpFloatArith(node);
        }
    }

    public void emitNode(Node node) {
        if (node.kind == Keyword.AST_FUNC) {
            emitFunc(node);
        } else {
            emitGlobalVar(node);
        }
    }

    public List<Opcode> getOpcodes() {
        List<Opcode> result = new ArrayList<>();
        result.addAll(code.opcodes);
        result.addAll(data.opcodes);

        return result;
    }

    private void emitFunc(Node node) {
        NodeFunctionDeclaration n = node.cast();
        code.label(n.functionName);

        genProlog(n);
        emitExpr(n.functionBody);
        genEpilog(n);
    }

    private void emitDeclInit(List<Node> inits, int off, int size) {
        emitFillHoles(inits, off, size);
        for (Node n : inits) {
            NodeInitializer node = n.cast(NodeInitializer.class);
            boolean isBitField = node.toType.bitSize > 0;
            if (node.initValue.kind == Keyword.AST_LITERAL && !isBitField) {
                emitSaveLiteral(node.initValue, node.toType, node.initOffset + off);
            } else {
                emitExpr(node.initValue);
                emitLSave(node.toType, node.initOffset + off);
            }
        }
    }

    private void emitFillHoles(List<Node> inits, int off, int size) {

    }

    private void emitSaveLiteral(Node initValue, Type toType, int i) {

        code.lxi(HL, -(i));
        code.dad(BC);

        switch (toType.kind) {
            case KIND_CHAR:
                Gen80Misc.saveChar(code, initValue.cast(NodeInteger.class).val & 0xff);
                break;
            case KIND_INT:
                int val = initValue.cast(NodeInteger.class).val & 0xffff;
                Gen80Misc.saveInt(code, val);
                break;
            case KIND_LONG:
                val = initValue.cast(NodeInteger.class).val;
                Gen80Misc.saveLong(code, val);
                break;
            case KIND_FLOAT:
                val = Float.floatToIntBits((float) initValue.cast(NodeFloat.class).val);
                Gen80Misc.saveLong(code, val);
                break;

        }

    }

    private void emitLSave(Type type, int i) {
        code.lxi(HL, -(i));
        code.dad(BC);
        if (type.kind == TypeKind.KIND_LONG || type.kind == TypeKind.KIND_FLOAT) {
            Gen80Misc.saveLong(code);
        } else if (type.kind == TypeKind.KIND_CHAR) {
            Gen80Misc.saveChar(code);
        } else {
            Gen80Misc.saveInt(code);
        }
    }

    private void emitConvInt2Long(Type to, Type from) {
        code.xchg();
        code.lxi(DE, 0);
        code.push(DE);
        code.xchg();
    }

    private void emitConvLong2Int(Type to, Type from) {
        code.inx(SP);
        code.inx(SP);
        code.modifyStack(-2);
    }

    private void emitLoadConvert(Type to, Type from) {
        if (from.kind == TypeKind.KIND_INT && to.kind == TypeKind.KIND_FLOAT) {
            code.call(".intToFlo");
        } else if (from.kind == TypeKind.KIND_LONG && to.kind == TypeKind.KIND_FLOAT) {
            code.call(".longToFlo");
        } else if (from.kind == TypeKind.KIND_INT && to.kind == TypeKind.KIND_LONG) {
            emitConvInt2Long(to, from);
        } else if (from.kind == TypeKind.KIND_LONG && to.kind == TypeKind.KIND_INT) {
            emitConvLong2Int(to, from);
        } else if (from.kind == TypeKind.KIND_CHAR && to.kind == TypeKind.KIND_LONG) {
            emitConvInt2Long(to, from);
        } else if (from.kind == TypeKind.KIND_LONG && to.kind == TypeKind.KIND_CHAR) {
            emitConvLong2Int(to, from);
        }
    }

    private void emitCopyStruct(Node left, Node right) {
//        code.push(HL);
        emitAddr(right);
        code.push(DE);
        emitAddr(left);
        code.push(DE);
        code.lxi(HL, left.type.size);
        code.push(HL);
        Gen80Misc.copyStruct(code);
//        code.pop(HL);
    }

    private void emitStore(Node var) {
        switch (var.kind) {
            case AST_DEREF:
                emitAssignDeref(var);
                break;
            case AST_STRUCT_REF:
                emitAssignStructRef(var.cast(NodeStruct.class).struct, var.type, 0);
                break;
            case AST_LVAR:
                ensureLVarInit(var);
                emitLSave(var.type, var.cast(NodeLocalLabel.class).localOffset);
                break;
            case AST_GVAR:
                emitGSave(var.cast(NodeGlobalLabel.class).globaLabel, var.type, 0);
                break;
        }
    }

    private void doEmitAssignDeref(Type type, int offset) {
        code.mov(H, D);
        code.mov(L, E);

        if (offset != 0) {
            code.lxi(DE, offset);
            code.xchg();
            code.dad(DE);
        }
        code.pop(DE);

        switch (type.size) {
            case 1:
                Gen80Misc.saveChar(code);
                break;
            case 2:
                Gen80Misc.saveInt(code);
                break;
            case 4:
                Gen80Misc.saveLong(code);
                break;
        }
    }

    private void emitAssignDeref(Node var) {
        code.push(DE);
        emitExpr(var.cast(NodeUnary.class).operand);
        doEmitAssignDeref(var.cast(NodeUnary.class).operand.type.pointer, 0);
    }

    private void emitAssignStructRef(Node struct, Type type, int off) {

        switch (struct.kind) {
            case AST_LVAR:
                NodeLocalLabel st = struct.cast();
                ensureLVarInit(struct);
                emitLSave(type, off - type.offset + st.localOffset);
                break;
            case AST_GVAR:
                NodeGlobalLabel ng = struct.cast();
                emitGSave(ng.globaLabel, type, off + type.offset);
                break;
            case AST_DEREF:
                code.push(DE);
                emitExpr(struct.cast(NodeUnary.class).operand);
                doEmitAssignDeref(type, off + type.offset);
                break;
            case AST_STRUCT_REF:
                NodeStruct stu = struct.cast();
                emitAssignStructRef(stu.struct, type, off + stu.type.offset);
                break;
            default:
                throw new RuntimeException("Internal error");
        }
    }

    private void ensureLVarInit(Node var) {
        NodeLocalLabel v = var.cast();
        if (v.localVariableInit != null) {
            emitDeclInit(v.localVariableInit, v.localOffset, v.type.size);
        }

        v.localVariableInit = null;
    }

    private void emitGSave(String globaLabel, Type type, int i) {
        code.lxi(HL, globaLabel);
        if (i != 0) {
            code.push(DE);
            code.lxi(DE, i);
            code.dad(DE);
            code.pop(DE);
        }

        if (type.kind == TypeKind.KIND_LONG || type.kind == TypeKind.KIND_FLOAT) {
            Gen80Misc.saveLong(code);
        } else if (type.kind == TypeKind.KIND_CHAR) {
            Gen80Misc.saveChar(code);
        } else {
            Gen80Misc.saveInt(code);
        }
    }

//    private void emitLLoad(Type type, int localOffset) {
//        code.lxi(HL, -(localOffset + align(type.size)));
//        code.dad(BC);
//        if (type.kind == TypeKind.KIND_ARRAY) {
//            code.xchg();
//        } else if (type.kind == TypeKind.KIND_INT || type.kind == TypeKind.KIND_PTR) {
//            Gen80Misc.loadInt(code);
//        } else if (type.kind == TypeKind.KIND_CHAR) {
//            Gen80Misc.loadChar(code);
//        } else {
//            Gen80Misc.loadLong(code);
//        }
//    }
    private void emitLLoad(Type type, com.ats.gen80.opcodes.Register base, int localOffset) {
        if (base == HL) {
//            code.xchg();
            if (localOffset != 0) {
                code.lxi(HL, -(localOffset));
                code.dad(DE);
            } else {
                code.xchg();
            }
        } else {
            code.lxi(HL, -(localOffset));
            code.dad(base);
        }

        if (type.kind == TypeKind.KIND_ARRAY) {
            code.xchg();
        } else if (type.kind == TypeKind.KIND_INT || type.kind == TypeKind.KIND_PTR || type.kind == TypeKind.KIND_STRUCT) {
            Gen80Misc.loadInt(code);
        } else if (type.kind == TypeKind.KIND_CHAR) {
            Gen80Misc.loadChar(code);
        } else {
            Gen80Misc.loadLong(code);
        }
    }

    private void emitPointerArith(Keyword kind, Node left, Node right) {
        emitExpr(left);
        code.push(DE);
        emitExpr(right);
        int size = left.type.pointer.size;
        if (size > 1) {
            code.push(DE);
            code.lxi(DE, size);
            Gen80Misc.mulInt(code);
        }

        if (right.type.size == 4) {
            code.inx(SP);
            code.inx(SP);
            code.modifyStack(-2);
        }

        switch (kind) {
            case C_PLUS:
                code.pop(HL);
                code.dad(DE);
                code.xchg();
                break;
            case C_MINUS:
                code.pop(HL);
                code.mov(A, L);
                code.sub(E);
                code.mov(E, A);
                code.mov(A, H);
                code.sbb(D);
                code.mov(D, A);
                break;
            default:
                throw new RuntimeException("Invalid operation");
        }

    }

    private void emitComp(String l, Node node) {
        NodeBinary nb = node.cast();

        emitExpr(nb.left);
        code.push(DE);
        emitExpr(nb.right);

        Gen80Misc.compare(l, nb.left.type, code);

    }

    private void emitBinOpIntArith(Node node) {
        NodeBinary nu = node.cast();
        emitExpr(nu.left);
        code.push(DE);
        emitExpr(nu.right);

        Type t = nu.left.type;
        if (t.size != nu.right.type.size) {
            emitLoadConvert(t, nu.right.type);
        }

        switch (nu.kind) {
            case C_PLUS:
                Gen80Misc.addInt(code, nu.type);
                break;
            case C_MINUS:
                Gen80Misc.subInt(code, nu.type);
                break;
            case C_MUL:
                Gen80Misc.mulInt(code, nu.type);
                break;
            case C_DIV:
                Gen80Misc.divInt(code, nu.type);
                break;
            case C_MOD:
                Gen80Misc.modInt(code, nu.type);
                break;
            case OP_SAL:
                Gen80Misc.salInt(code, nu.type);
                break;
            case OP_SAR:
                Gen80Misc.sarInt(code, nu.type);
                break;
            case OP_SHR:
                Gen80Misc.shrInt(code, nu.type);
                break;
            case C_XOR:
                Gen80Misc.binaryXor(nu.type, code);
                break;

        }

    }

    public void emitBss(Node v) {
        NodeDeclaration nd = v.cast(NodeDeclaration.class);
        NodeGlobalLabel ng = nd.declarationVariable.cast();

        bss.label(ng.globaLabel);
        int maxSize = ng.type.size;
        for (; maxSize > 0; maxSize -= 8) {
            int buf[] = new int[maxSize > 8 ? 8 : maxSize];
            bss.db(buf);
        }

//        int max = v.cast(NodeGlobalLabel.class).
    }

    private void emitBinOpFloatArith(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void emitGlobalVar(Node node) {
        NodeDeclaration n = node.cast();
        if (n.declarationInit != null && !n.declarationInit.isEmpty()) {
            emitData(node, 0, 0);
        } else {
            emitBss(node);
        }
    }

    private void emitData(Node node, int off, int depth) {
        data.label(node.cast(NodeDeclaration.class).declarationVariable.cast(NodeGlobalLabel.class).globaLabel);
        doEmitData(node.cast(NodeDeclaration.class).declarationInit, node.cast(NodeDeclaration.class).declarationVariable.type.size, off, depth);
    }

    private void doEmitData(List<Node> declarationInit, int size, int off, int depth) {
        for (Node node : declarationInit) {
            NodeInitializer n = node.cast();
            Node v = n.initValue;

            off += n.toType.size;
            size -= n.toType.size;

            if (v.kind == Keyword.AST_ADDR) {
                emitDataAddr(n.initValue.cast(NodeUnary.class).operand, depth);
                continue;
            }
            if (v.kind == Keyword.AST_LVAR && (n.initValue instanceof NodeInitializer)) {
                doEmitData(v.cast(NodeLocalLabel.class).localVariableInit, v.type.size, 0, depth);
                continue;
            }
            emitDataPrimType(n.toType, n.initValue, depth);
        }
    }

    private void emitDataAddr(Node operand, int depth) {
        switch (operand.kind) {
            case AST_LVAR:
                Gen80 d = dataList.get(depth + 1);
                if (d == null) {
                    d = new Gen80();
                    dataList.put(depth + 1, d);
                }
                String label;
                d.label(label = data.genLabel());
                doEmitData(operand.cast(NodeLocalLabel.class).localVariableInit, operand.type.size, 0, depth + 1);
                d = dataList.get(depth);
                d.dw(label);
                break;
            case AST_GVAR:
                d = dataList.get(depth);
                d.dw(operand.cast(NodeGlobalLabel.class).globaLabel);
                break;
        }
    }

    private void emitDataPrimType(Type toType, Node initValue, int depth) {
        Gen80 d = dataList.get(depth);
        switch (toType.kind) {
            case KIND_LONG:
                int f = (int) proc.evalIntExpr(initValue, null);
                d.dw((f >> 16) & 0xffff);
                d.dw((f) & 0xffff);
                break;
            case KIND_FLOAT:
                f = Float.floatToRawIntBits((float) initValue.cast(NodeFloat.class).val);

                d.dw((f >> 16) & 0xffff);
                d.dw((f) & 0xffff);
                break;
            case KIND_INT:
                f = (int) proc.evalIntExpr(initValue, null);
                d.dw(f);
                break;
            case KIND_CHAR:
                f = initValue.cast(NodeInteger.class).val;
                d.db(new int[]{f});
                break;
            case KIND_PTR:
                if (initValue.kind == Keyword.OP_LABEL_ADDR) {
                    d.label(initValue.cast(NodeLabel.class).newLabel);
                    break;
                }
                boolean isCharPtr = false;
                if (initValue instanceof NodeUnary) {
                    NodeUnary val = initValue.cast();
                    isCharPtr = (val.operand.type.kind == TypeKind.KIND_ARRAY) && val.operand.type.pointer.kind == TypeKind.KIND_CHAR;
                }
                if (isCharPtr) {
                    NodeUnary val = initValue.cast();
                    emitDataCharPtr(val.operand.cast(NodeString.class).val, depth);
                } else if (initValue.kind == Keyword.AST_GVAR) {
                    d.dw(initValue.cast(NodeGlobalLabel.class).globaLabel);
                } else {
                    DataRef<Node> base = new DataRef<>(null);
                    int v = (int) proc.evalIntExpr(initValue, base);
                    if (base.getData() == null) {
                        d.dw(v);
                        break;
                    }
                    Type type = base.getData().type;

                    if (base.getData().kind == Keyword.AST_CONV || base.getData().kind == Keyword.AST_ADDR) {
                        base.setData(base.getData().cast(NodeUnary.class).operand);
                    }
                    if (base.getData().kind != Keyword.AST_GVAR) {
                        throw new RuntimeException("Global variable expected, but got " + base.getData().kind);
                    }
                    d.dw(base.getData().cast(NodeGlobalLabel.class).globaLabel + (v * type.pointer.size));
                }
        }
    }

    private void emitDataCharPtr(String val, int depth) {
        Gen80 d = dataList.get(depth + 1);
        if (d == null) {
            d = new Gen80();
            dataList.put(depth + 1, d);
        }
        String label;
        d.label(label = data.genLabel());
        d.db(val);
        d = dataList.get(depth);
        d.dw(label);
    }

    private void emitAsm(Node node) {
        NodeAsm asm = node.cast();
        String[] source = asm.source.toUpperCase().split("\r");

        for (String line : source) {
            String[] tmp = line.split("[\\s|,]");
            List<String> tokens = new ArrayList<>();
            for (String token : tmp) {
                token = token.trim();
                if (token.isEmpty()) {
                    continue;
                }
                tokens.add(token);
            }
            if (tokens.isEmpty()) {
                continue;
            }
            String label = tokens.get(0);
            String opcode = "";
            String left = "";
            String right = "";
            if (!label.endsWith(":")) {
                opcode = label;
                label = "";
            } else if (tokens.size() > 1) {
                tokens.remove(0);
                opcode = tokens.get(0);

            }

            if (tokens.size() > 1) {
                left = tokens.get(1).replace(",", "");
            }
            if (tokens.size() > 2) {
                right = tokens.get(2);
            }

            if (!label.isEmpty()) {
                label = label.substring(0, label.length() - 1);
                code.label(label);
            }

            if (!opcode.isEmpty()) {
                Opcode.Mnemonic mnem = Opcode.Mnemonic.valueOf(opcode);
                if (left.isEmpty() && right.isEmpty()) {
                    code.opcodes.add(mnem.create());
                } else if (!left.isEmpty() && right.isEmpty()) {
                    com.ats.gen80.opcodes.Register reg = com.ats.gen80.opcodes.Register.value(left);
                    if (reg == null) {
                        if (left.matches("[0-9a-fA-F]+H")) {
                            code.opcodes.add(mnem.create(parseNum(left)));
                        } else {
                            code.opcodes.add(mnem.create(left));
                        }
//                        code.opcodes.add(mnem.create(parseNum(left)));
                    } else {
                        code.opcodes.add(mnem.create(reg));
                    }
                } else {
                    com.ats.gen80.opcodes.Register reg1 = com.ats.gen80.opcodes.Register.value(left);
                    com.ats.gen80.opcodes.Register reg2 = com.ats.gen80.opcodes.Register.value(right);

                    if (reg1 == null) {
                        throw new RuntimeException("Internal error parse source of " + source);
                    }

                    if (reg2 == null) {
                        if (right.matches("[0-9a-fA-F]+[hH]")) {
                            code.opcodes.add(mnem.create(reg1, parseNum(right)));
                        } else {
                            code.opcodes.add(mnem.create(reg1, right));
                        }
                    } else {
                        code.opcodes.add(mnem.create(reg1, reg2));
                    }
                }
            }
        }
    }

    private int parseNum(String left) {
        if (left.matches("[0-9a-fA-F]+[hH]")) {
            return Integer.parseInt(left.substring(0, left.length() - 1), 16);
        }

        return 0;
    }
}
