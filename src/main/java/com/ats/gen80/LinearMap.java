/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 *
 * @author lucifer
 */
public class LinearMap<K, V> implements Map<K, V> {

    public LinearMap() {
    }

    public LinearMap(Map<K, V> m) {
        putAll(m);
    }

    private List<LinearEntry> data = new ArrayList<>();

    public class LinearEntry implements Entry {

        public K key;
        public V value;

        public LinearEntry(Object key, Object value) {
            this.key = (K) key;
            this.value = (V) value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(Object value) {
            V old = this.value;
            this.value = (V) value;
            return old;
        }

    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        for (LinearEntry e : data) {
            if (e.key.equals(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        for (LinearEntry e : data) {
            if (e.value.equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        for (LinearEntry e : data) {
            if (e.key.equals(key)) {
                return e.value;
            }
        }
        return null;
    }

    @Override
    public Object put(Object key, Object value) {
        for (LinearEntry e : data) {
            if (e.key.equals(key)) {
                return e.setValue(value);
            }
        }
        LinearEntry e = new LinearEntry(key, value);
        data.add(e);
        return value;
    }

    @Override
    public V remove(Object key) {
        for (LinearEntry e : data) {
            if (e.key.equals(key)) {
                data.remove(e);
                return e.getValue();
            }
        }
        return null;
    }

    @Override
    public void putAll(Map m) {
        for (Iterator it = m.entrySet().iterator(); it.hasNext();) {
            Entry e = (Entry) it.next();
            put(e.getKey(), e.getValue());
        }
    }

    @Override
    public void clear() {
        data.clear();
    }

    @Override
    public Set keySet() {
        Set<K> set = new CopyOnWriteArraySet<>();
        for (LinearEntry e : data) {
            set.add(e.key);
        }
        return set;
    }

    @Override
    public Collection values() {
        List<V> ret = new ArrayList<>();
        for (LinearEntry e : data) {
            ret.add(e.value);
        }
        return ret;
    }

    @Override
    public Set entrySet() {
        Set<Entry> set = new HashSet<>();
        for (LinearEntry e : data) {
            set.add(e);
        }
        return set;
    }

}
