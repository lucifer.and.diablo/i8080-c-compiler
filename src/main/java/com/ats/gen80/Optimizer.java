/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.vm.AbstractOptimizer;
import com.ats.gen80.vm.IntShrink_1;
import com.ats.gen80.vm.Optimizer_1;
import com.ats.gen80.vm.Optimizer_10;
import com.ats.gen80.vm.Optimizer_11;
import com.ats.gen80.vm.Optimizer_12;
import com.ats.gen80.vm.Optimizer_2;
import com.ats.gen80.vm.Optimizer_3;
import com.ats.gen80.vm.Optimizer_4;
import com.ats.gen80.vm.Optimizer_5;
import com.ats.gen80.vm.Optimizer_6;
import com.ats.gen80.vm.Optimizer_7;
import com.ats.gen80.vm.Optimizer_8;
import com.ats.gen80.vm.Optimizer_9;
import com.ats.gen80.vm.Optimizer_99;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucifer
 */
public class Optimizer {

    public List<Opcode> opcodes = new ArrayList<>();
    private final List<AbstractOptimizer> optimizers = new ArrayList<AbstractOptimizer>() {
        {
            add(new Optimizer_6());
            add(new IntShrink_1());
            add(new Optimizer_4());
            add(new Optimizer_3());
            add(new Optimizer_2());
            add(new Optimizer_1());
            add(new Optimizer_5());
            add(new Optimizer_7());
//            add(new Optimizer_8());
            add(new Optimizer_9());
            add(new Optimizer_10());
            add(new Optimizer_11());
//            add(new Optimizer_12());

            add(new Optimizer_99());
        }
    };

    public Optimizer(List<Opcode> opcodes) {
        this.opcodes.addAll(opcodes);
    }

    public List<Opcode> optimize(boolean optimize) {
        if (!optimize) {
            return opcodes;
        }
        List<Opcode> ret = null;
        int step = 0;
        while (true) {
            ++step;
//            if (step > 1) {
//                System.out.println("Optimized " + (step - 1) + ": " + ((Opcode.Label) opcodes.get(0)).label);
//            }
            ret = opcodes;
            for (AbstractOptimizer op : optimizers) {
                List<Opcode> tmp;
                do {
                    tmp = new ArrayList<>(ret);
                    ret = op.optimize(ret);
                } while (!identical(ret, tmp));
            }
            if (!identical(opcodes, ret)) {
                opcodes.clear();
                opcodes.addAll(ret);
                ret = null;
            } else {
                break;
            }
        }
        
        
        return ret;
    }

    public static boolean identical(List<Opcode> optimized, List<Opcode> initial) {
        if (optimized.size() != initial.size()) {
            return false;
        }

        for (int i = 0; i < optimized.size(); i++) {
            if (!optimized.get(i).equals(initial.get(i))) {
                return false;
            }
        }

        return true;
    }
}
