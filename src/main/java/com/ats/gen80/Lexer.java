/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.EncodingEnum;
import com.ats.gen80.node.Keyword;
import com.ats.gen80.node.Token;
import com.ats.gen80.node.TokenEnum;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import static java.lang.Character.isDigit;
import java.nio.charset.Charset;

/**
 *
 * @author lucifer
 */
public class Lexer extends FileBuffer {

    protected final List<List<Token>> buffers = new ArrayList<>();
    protected Pos pos;

    public void lexInitString(String s) {
        buffers.add(new ArrayList<Token>());
        streamPush(makeFileString(s));
    }

    public void lexInitFile(File s) {
        buffers.add(new ArrayList<Token>());
        streamPush(makeFile(s, s.getName()));
    }

    public Token makeIdent(String s) {
        Token tok = new Token(currentFile(), pos);
        tok.kind = TokenEnum.TIDENT;
        tok.sval = s;
        return tok;
    }

    public Token makeStrTok(String s) {
        Token tok = new Token(currentFile(), pos);
        tok.kind = TokenEnum.TSTRING;
        tok.sval = s;
        tok.slen = s.length();
        tok.enc = EncodingEnum.ENC_NONE;
        return tok;
    }

    public Token makeKeyword(Keyword k) {
        Token tok = new Token(currentFile(), pos);
        tok.kind = TokenEnum.TKEYWORD;
        tok.id = k;
        return tok;
    }

    public Token makeNumber(String num) {
        Token tok = new Token(currentFile(), pos);
        tok.kind = TokenEnum.TNUMBER;
        tok.sval = num;
        return tok;
    }

    public Token makeInvalid(char num) {
        Token tok = new Token(currentFile(), pos);
        tok.kind = TokenEnum.TINVALID;
        tok.c = num;
        return tok;
    }

    public Token makeChar(char num) {
        Token tok = new Token(currentFile(), pos);
        tok.kind = TokenEnum.TCHAR;
        tok.c = num;
        tok.enc = EncodingEnum.ENC_NONE;
        return tok;
    }

    public boolean isWhitespace(char c) {
        return (c == ' ' || c == '\t' || c == '\f' || c == '\r');
    }

    public int peek() {
        int r = read();
        unread(r);
        return r;
    }

    public boolean next(int expect) {
        int c = read();
        if (c == expect) {
            return true;
        }

        unread(c);
        return false;
    }

    public boolean next(Keyword keyword) {
        Token tok = lex();
        if (isKeyword(tok, keyword)) {
            return true;
        }
        ungetToken(tok);
        return false;
    }

    public void skipLine() {
        while (true) {
            int c = read();
            if (c == -1) {
                return;
            }

            if (c == '\n') {
                unread(c);
                return;
            }
        }
    }

    public boolean doSkipSpace() {
        int c = read();

        if (c == -1) {
            return false;
        }

        if (isWhitespace((char) c)) {
            return true;
        }

        if (c == '/') {
            if (next('*')) {
                skipBlockComment();
                return true;
            }
            if (next('/')) {
                skipLine();
                return true;
            }
        }

        unread(c);

        return false;
    }

    public boolean skipSpace() {
        if (!doSkipSpace()) {
            return false;
        }

        while (doSkipSpace()) {
        }

        return true;
    }

    public void skipChar() {
        if (read() == '\\') {
            read();
        }

        int c = read();
        while (c != -1 && c != '\'') {
            c = read();
        }
    }

    public void skipString() {
        for (int c = read(); c != -1 && c != '"'; c = read()) {
            if (c == '\\') {
                read();
            }
        }
    }

    public void mark() {
        // TODO: Make POS
        pos = getPos(0);
    }

    public Token doReadToken() {
        if (skipSpace()) {
            return Token.SPACE_TOKEN;
        }

        mark();

        int c = read();
        if ((c >= 'a' && c <= 't')
                || (c >= 'v' && c <= 'z')
                || (c >= 'A' && c <= 'K')
                || (c >= 'M' && c <= 'T')
                || (c >= 'V' && c <= 'Z')
                || (c == '_')
                || (c == '$')) {

            return readIdent(c);
        }

        if (c >= '0' && c <= '9') {
            return readNumber(c);
        }

        switch (c) {
            case '\n':
                return Token.NEWLINE_TOKEN;
            case ':':
                return makeKeyword(next('>') ? Keyword.C_RSBRACE : Keyword.C_COLON);
            case '#':
                return makeKeyword(next('#') ? Keyword.KHASHHASH : Keyword.C_HASH);
            case '+':
                return readRep2('+', Keyword.OP_INC, '=', Keyword.OP_A_ADD, Keyword.C_PLUS);
            case '*':
                return readRep('=', Keyword.OP_A_MUL, Keyword.C_MUL);
            case '=':
                return readRep('=', Keyword.OP_EQ, Keyword.C_EQ);
            case '!':
                return readRep('=', Keyword.OP_NE, Keyword.C_EXPOINT);
            case '&':
                return readRep2('&', Keyword.OP_LOGAND, '=', Keyword.OP_A_AND, Keyword.C_AND);
            case '|':
                return readRep2('|', Keyword.OP_LOGOR, '=', Keyword.OP_A_OR, Keyword.C_OR);
            case '^':
                return readRep('=', Keyword.OP_A_XOR, Keyword.C_XOR);
            case '"':
                return readString(EncodingEnum.ENC_NONE);
            case '\'':
                return readChar(EncodingEnum.ENC_NONE);
            case '/':
                return makeKeyword(next('=') ? Keyword.OP_A_DIV : Keyword.C_DIV);
            case 'L':
            case 'U':
                EncodingEnum enc = (c == 'L') ? EncodingEnum.ENC_WCHAR : EncodingEnum.ENC_CHAR32;
                if (next('"')) {
                    return readString(enc);
                }
                if (next('\'')) {
                    return readChar(enc);
                }
                return readIdent(c);
            case 'u':
                if (next('"')) {
                    return readString(EncodingEnum.ENC_CHAR16);
                }
                if (next('\'')) {
                    return readChar(EncodingEnum.ENC_CHAR16);
                }
                if (next('8')) {
                    if (next('"')) {
                        return readString(EncodingEnum.ENC_UTF8);
                    }
                    unread('8');
                }
                return readIdent(c);
            case '.':
                if (isDigit(peek())) {
                    return readNumber(c);
                }
                if (next('.')) {
                    if (next('.')) {
                        return makeKeyword(Keyword.KELLIPSIS);
                    }
                    return makeIdent("..");
                }
                return makeKeyword(Keyword.C_POINT);
            case '(':
                return makeKeyword(Keyword.C_LPAREN);
            case ')':
                return makeKeyword(Keyword.C_RPAREN);
            case ',':
                return makeKeyword(Keyword.C_COMMA);
            case ';':
                return makeKeyword(Keyword.C_SEMICOLON);
            case '[':
                return makeKeyword(Keyword.C_LSBRACE);
            case ']':
                return makeKeyword(Keyword.C_RSBRACE);
            case '{':
                return makeKeyword(Keyword.C_LBRACE);
            case '}':
                return makeKeyword(Keyword.C_RBRACE);
            case '?':
                return makeKeyword(Keyword.C_QMARK);
            case '~':
                return makeKeyword(Keyword.C_TILDA);
            case '-':
                if (next('-')) {
                    return makeKeyword(Keyword.OP_DEC);
                }
                if (next('>')) {
                    return makeKeyword(Keyword.OP_ARROW);
                }
                if (next('=')) {
                    return makeKeyword(Keyword.OP_A_SUB);
                }
                return makeKeyword(Keyword.C_MINUS);
            case '<':
                if (next('<')) {
                    return readRep('=', Keyword.OP_A_SAL, Keyword.OP_SAL);
                }
                if (next('=')) {
                    return makeKeyword(Keyword.OP_LE);
                }
                if (next(':')) {
                    return makeKeyword(Keyword.C_LSBRACE);
                }
                if (next('%')) {
                    return makeKeyword(Keyword.C_LBRACE);
                }
                return makeKeyword(Keyword.C_LT);
            case '>':
                if (next('>')) {
                    return readRep('=', Keyword.OP_A_SAR, Keyword.OP_SAR);
                }
                if (next('=')) {
                    return makeKeyword(Keyword.OP_GE);
                }
                if (next(':')) {
                    return makeKeyword(Keyword.C_RSBRACE);
                }
                if (next('%')) {
                    return makeKeyword(Keyword.C_RBRACE);
                }
                return makeKeyword(Keyword.C_GT);
            case '%':
                Token tok = readHashDigraph();
                if (tok != null) {
                    return tok;
                }
                return readRep('=', Keyword.OP_A_MOD, Keyword.C_MOD);
            case -1:
                return Token.EOF_TOKEN;
            default:
                return makeInvalid((char) c);
        }
    }

    public Token doReadTokenAsm() {
        if (skipSpace()) {
            return Token.SPACE_TOKEN;
        }

        mark();

        int c = read();
        if ((c >= 'a' && c <= 't')
                || (c >= 'v' && c <= 'z')
                || (c >= 'A' && c <= 'K')
                || (c >= 'M' && c <= 'T')
                || (c >= 'V' && c <= 'Z')
                || (c == '_')
                || (c == '$') || (c == '.')) {

            return readAsmIdent(c);
        }

        if (c >= '0' && c <= '9') {
            return readNumber(c);
        }

        switch (c) {
            case '\n':
                return Token.NEWLINE_TOKEN;
            case ':':
                return makeKeyword(next('>') ? Keyword.C_RSBRACE : Keyword.C_COLON);
            case '#':
                return makeKeyword(next('#') ? Keyword.KHASHHASH : Keyword.C_HASH);
            case '+':
                return readRep2('+', Keyword.OP_INC, '=', Keyword.OP_A_ADD, Keyword.C_PLUS);
            case '*':
                return readRep('=', Keyword.OP_A_MUL, Keyword.C_MUL);
            case '=':
                return readRep('=', Keyword.OP_EQ, Keyword.C_EQ);
            case '!':
                return readRep('=', Keyword.OP_NE, Keyword.C_EXPOINT);
            case '&':
                return readRep2('&', Keyword.OP_LOGAND, '=', Keyword.OP_A_AND, Keyword.C_AND);
            case '|':
                return readRep2('|', Keyword.OP_LOGOR, '=', Keyword.OP_A_OR, Keyword.C_OR);
            case '^':
                return readRep('=', Keyword.OP_A_XOR, Keyword.C_XOR);
            case '"':
                return readString(EncodingEnum.ENC_NONE);
            case '\'':
                return readChar(EncodingEnum.ENC_NONE);
            case '/':
                return makeKeyword(next('=') ? Keyword.OP_A_DIV : Keyword.C_DIV);
            case 'L':
            case 'U':
                EncodingEnum enc = (c == 'L') ? EncodingEnum.ENC_WCHAR : EncodingEnum.ENC_CHAR32;
                if (next('"')) {
                    return readString(enc);
                }
                if (next('\'')) {
                    return readChar(enc);
                }
                return readAsmIdent(c);
            case 'u':
                if (next('"')) {
                    return readString(EncodingEnum.ENC_CHAR16);
                }
                if (next('\'')) {
                    return readChar(EncodingEnum.ENC_CHAR16);
                }
                if (next('8')) {
                    if (next('"')) {
                        return readString(EncodingEnum.ENC_UTF8);
                    }
                    unread('8');
                }
                return readAsmIdent(c);
            case '(':
                return makeKeyword(Keyword.C_LPAREN);
            case ')':
                return makeKeyword(Keyword.C_RPAREN);
            case ',':
                return makeKeyword(Keyword.C_COMMA);
            case ';':
                return makeKeyword(Keyword.C_SEMICOLON);
            case '[':
                return makeKeyword(Keyword.C_LSBRACE);
            case ']':
                return makeKeyword(Keyword.C_RSBRACE);
            case '{':
                return makeKeyword(Keyword.C_LBRACE);
            case '}':
                return makeKeyword(Keyword.C_RBRACE);
            case '?':
                return makeKeyword(Keyword.C_QMARK);
            case '~':
                return makeKeyword(Keyword.C_TILDA);
            case '-':
                if (next('-')) {
                    return makeKeyword(Keyword.OP_DEC);
                }
                if (next('>')) {
                    return makeKeyword(Keyword.OP_ARROW);
                }
                if (next('=')) {
                    return makeKeyword(Keyword.OP_A_SUB);
                }
                return makeKeyword(Keyword.C_MINUS);
            case '<':
                if (next('<')) {
                    return readRep('=', Keyword.OP_A_SAL, Keyword.OP_SAL);
                }
                if (next('=')) {
                    return makeKeyword(Keyword.OP_LE);
                }
                if (next(':')) {
                    return makeKeyword(Keyword.C_LSBRACE);
                }
                if (next('%')) {
                    return makeKeyword(Keyword.C_LBRACE);
                }
                return makeKeyword(Keyword.C_LT);
            case '>':
                if (next('>')) {
                    return readRep('=', Keyword.OP_A_SAR, Keyword.OP_SAR);
                }
                if (next('=')) {
                    return makeKeyword(Keyword.OP_GE);
                }
                if (next(':')) {
                    return makeKeyword(Keyword.C_RSBRACE);
                }
                if (next('%')) {
                    return makeKeyword(Keyword.C_RBRACE);
                }
                return makeKeyword(Keyword.C_GT);
            case '%':
                Token tok = readHashDigraph();
                if (tok != null) {
                    return tok;
                }
                return readRep('=', Keyword.OP_A_MOD, Keyword.C_MOD);
            case -1:
                return Token.EOF_TOKEN;
            default:
                return makeInvalid((char) c);
        }
    }

    public Token lex() {
        List<Token> buf = buffers.get(buffers.size() - 1);
        if (buf.size() > 0) {
            return buf.remove(buf.size() - 1);
        }

        if (buffers.size() > 1) {
            return Token.EOF_TOKEN;
        }

        boolean bol = currentFile().column == 1;
        Token tok = doReadToken();
        while (tok.kind == TokenEnum.TSPACE) {
            tok = doReadToken();
            tok.space = true;
        }

        tok.bol = bol;

        return tok;
    }

    public Token lexAsm() {
        List<Token> buf = buffers.get(buffers.size() - 1);
        if (buf.size() > 0) {
            return buf.remove(buf.size() - 1);
        }

        if (buffers.size() > 1) {
            return Token.EOF_TOKEN;
        }

        boolean bol = currentFile().column == 1;
        Token tok = doReadTokenAsm();
        while (tok.kind == TokenEnum.TSPACE) {
            tok = doReadTokenAsm();
            tok.space = true;
        }

        tok.bol = bol;

        return tok;
    }

    public Token readRep2(char c, Keyword keyword, char c0, Keyword keyword0, Keyword keyword1) {
        if (next(c)) {
            return makeKeyword(keyword);
        }

        return makeKeyword(next(c0) ? keyword0 : keyword1);
    }

    public Token readRep(char c, Keyword keyword, Keyword keyword0) {
        return makeKeyword(next(c) ? keyword : keyword0);
    }

    public Token readString(EncodingEnum enc) {
        String b = "";
        while (true) {
            int c = read();
            if (c == -1) {
                Lexer.this.error(pos, "unterminated string");
            }

            if (c == '"') {
                break;
            }
            if (c != '\\') {
                b += (char) c;
                continue;
            }

            boolean isucs = (peek() == 'u' || peek() == 'U');
            c = readEscapedChar();
            if (isucs) {
                b += (char) c;
                continue;
            }
            b += (char) c;
        }

//        String test = new String(new String(b.getBytes(), Charset.forName("UTF-8")).getBytes(Charset.forName("KOI8-R")));
//        System.out.println(test);
        return makeStrTok(b);
    }

    public int readEscapedChar() {
        Pos p = getPos(-1);
        int c = read();
        switch (c) {
            case '\'':
            case '"':
            case '?':
            case '\\':
                return c;
            case 'a':
                return 1;
            case 'b':
                return 8;
            case 'f':
                return '\f';
            case 'n':
                return '\n';
            case 'r':
                return '\r';
            case 't':
                return '\t';
            case 'v':
                return 11;
            case 'e':
                return 27;
            case 'x':
                return readHexChar();
            case 'u':
                return readUniversalChar(4);
            case 'U':
                return readUniversalChar(8);
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
                return readOctalChar(c);
        }

        Lexer.this.warning(p, "Unknown escape character: \\%c", c);
        return c;
    }

    public int readHexChar() {
        Pos p = getPos(-2);
        int c = read();
        if (!isXDigit(c)) {
            Lexer.this.error(p, "\\x is not followed by a hexadecimal character: %c", c);
        }

        int r = 0;

        for (;; c = read()) {
            if (c >= '0' && c <= '9') {
                r = (r << 4) | (c - '0');
                continue;
            }
            if (c >= 'a' && c <= 'f') {
                r = (r << 4) | (c - 'a' + 10);
                continue;
            }
            if (c >= 'A' && c <= 'F') {
                r = (r << 4) | (c - 'A' + 10);
                continue;
            }

            unread(c);
            return r;

        }
    }

    public boolean isXDigit(int c) {
        return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' || c <= 'f');
    }

    public int readOctalChar(int c) {
        int r = c - '0';
        if (!nextoct()) {
            return r;
        }
        r = (r << 3) | (read() - '0');
        if (!nextoct()) {
            return r;
        }

        return (r << 3) | (read() - '0');
    }

    public boolean nextoct() {
        int c = peek();
        return '0' <= c & c <= '7';
    }

    public int readUniversalChar(int len) {
        Pos p = getPos(-2);
        long r = 0;
        for (int i = 0; i < len; i++) {
            int c = read();
            if (c >= '0' && c <= '9') {
                r = (r << 4) | (c - '0');
                continue;
            }
            if (c >= 'a' && c <= 'f') {
                r = (r << 4) | (c - 'a' + 10);
                continue;
            }
            if (c >= 'A' && c <= 'F') {
                r = (r << 4) | (c - 'A' + 10);
                continue;
            }
            Lexer.this.error(p, "invalid universal character: %c", c);
        }
        if (!isValidUcn(r)) {
            Lexer.this.error(p, "invalid universal character: \\%c%0*x", (len == 4) ? 'u' : 'U', len, r);
        }
        return (int) r;
    }

    public static void error(String msg) {
        System.out.println("[SEVERE] " + msg);
        System.exit(0);
    }

    public boolean isValidUcn(long r) {
        if (0xD800 <= r && r <= 0xDFFF) {
            return false;
        }

        return 0xA0 <= r || r == '$' || r == '@' || r == '`';
    }

    Pos getPos(int i) {
        FileStruct f = currentFile();
        return new Pos(f.column - 2, f.line);
    }

    public static void error(String s, Object... args) {
        String ret = String.format("[ERROR  ]: ") + String.format(s, args);
        throw new RuntimeException(ret);
    }

    public void errorC(String s, Object... args) {
        String ret = String.format("[ERROR  ]: %s:%d:%d -> ", currentFile().name, currentFile().line, currentFile().column) + String.format(s, args);
        throw new RuntimeException(ret);
    }

    public void error(Pos p, String s, Object... args) {
        String ret = String.format("[ERROR  ]: %s:%d:%d -> ", currentFile().name, p.line, p.column) + String.format(s, args);
        throw new RuntimeException(ret);
    }

    public void warning(Pos p, String s, Object... args) {
        System.out.print(String.format("[WARNING]: %s:%d:%d -> ", currentFile().name, p.line, p.column));
        System.out.println(String.format(s, args));
    }

    public Token readIdent(int c) {
        String b = "";

        b += (char) c;

        for (;;) {
            c = read();
            if (isAlnum(c) || ((c & 0x80) == 0x80) || c == '_' || c == '$') {
                b += (char) c;
                continue;
            }

            if (c == '\\' && (peek() == 'u' || peek() == 'U')) {
                c += (char) readEscapedChar();
                continue;
            }
            unread(c);
            return makeIdent(b);
        }
    }

    public Token readAsmIdent(int c) {
        String b = "";

        b += (char) c;

        for (;;) {
            c = read();
            if (isAlnum(c) || ((c & 0x80) == 0x80) || c == '_' || c == '$' || c == '.') {
                b += (char) c;
                continue;
            }

            if (c == '\\' && (peek() == 'u' || peek() == 'U')) {
                c += (char) readEscapedChar();
                continue;
            }
            unread(c);
            return makeIdent(b);
        }
    }

    public boolean isAlnum(int c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9');
    }

    public Token readNumber(int c) {
        String b = "";
        b += (char) c;
        int last = c;
        while (true) {
            c = read();
            boolean flonum = "eEpP".indexOf((char) last) != -1 && "+-".indexOf((char) c) != -1;

            if (!isDigit(c) && !Character.isAlphabetic(c) && c != '.' && !flonum) {
                unread(c);
                return makeNumber(b);
            }

            b += (char) c;
            last = c;
        }
    }

    public Token readChar(EncodingEnum encodingEnum) {
        int c = read();
        int r = (c == '\\') ? readEscapedChar() : c;
        c = read();
        if (c != '\'') {
            Lexer.this.error(pos, "unterminated char");
        }

        if (encodingEnum == EncodingEnum.ENC_NONE) {
            return makeChar((char) r);
        }

        return makeChar((char) r);
    }

    public Token readHashDigraph() {
        if (next('>')) {
            return makeKeyword(Keyword.C_RBRACE);
        }
        if (next(':')) {
            if (next('%')) {
                if (next(':')) {
                    return makeKeyword(Keyword.KHASHHASH);
                }
                unread('%');
            }
            return makeKeyword(Keyword.C_HASH);
        }
        return null;
    }

    public void skipBlockComment() {
        Pos p = getPos(-2);
        boolean maybe_end = false;
        while (true) {
            int c = read();
            if (c == -1) {
                Lexer.this.error(p, "premature end of block comment");
            }
            if (c == '/' && maybe_end) {
                return;
            }
            maybe_end = (c == '*');
        }
    }

    public String readHeaderFileName(DataRef<Boolean> std) {
        if (buffers.isEmpty()) {
            return null;
        }

        skipSpace();

        Pos p = getPos(0);
        char close;

        if (next('"')) {
            std.setData(false);
            close = '"';
        } else if (next('<')) {
            std.setData(false);
            close = '>';
        } else {
            return null;
        }

        String b = "";
        while (!next(close)) {
            int c = read();
            if (c == -1 || c == '\n') {
                Lexer.this.error(p, "permature end of header name");
            }
            b += (char) c;
        }

        if (b.length() == 0) {
            Lexer.this.error(p, "header name should not be empty");
        }

        return b;
    }

//    public boolean isKeyword(Token tok, char id) {
//        return (tok.kind == TokenEnum.TKEYWORD) && (tok.id.keyword.equals(id + ""));
//    }
    public void error(Token tok, String str, Object... args) {
        Lexer.this.error(tokenPos(tok), str, args);
    }

    public void warning(Token tok, String str, Object... args) {
        Lexer.this.warning(tokenPos(tok), str, args);
    }

    public Pos tokenPos(Token tok) {
        return new Pos(tok.column, tok.line);
    }

    public void ungetToken(Token tok) {
        if (tok.kind == TokenEnum.TEOF) {
            return;
        }

        buffers.get(buffers.size() - 1).add(tok);
    }

    public boolean isKeyword(Token tok, Keyword keyword) {
        return (tok.kind == TokenEnum.TKEYWORD) && (tok.id == keyword);
    }

    public void tokenBuffersStash(List<Token> reverse) {
        buffers.add(reverse);
    }

    public void tokenBufferUnstash() {
        buffers.remove(buffers.size() - 1);
    }

    public boolean isIdent(Token tok, String s) {
        return tok.kind == TokenEnum.TIDENT && tok.sval.equals(s);
    }

    public Token skipNewLines() {
        Token tok = lex();
        while (tok.kind == TokenEnum.TNEWLINE) {
            tok = lex();
        }
        ungetToken(tok);
        return tok;
    }

    public static class Pos {

        public int column;
        public int line;

        public Pos(int column, int line) {
            this.column = column;
            this.line = line;
        }
    }

    public Token lexString(String s) {
        streamStash(makeFileString(s));
        Token r = doReadToken();
        next('\n');
        Pos p = getPos(0);
        if (peek() != -1) {
            error(p, "unconsumed input: %s", s);
        }

        streamUnstash();

        return r;
    }

    public Token readIdent() {
        Token tok = lex();
        if (tok.kind != TokenEnum.TIDENT) {
            error(tok, "Identifier expected but got %s", tok.toString());
        }

        return tok;
    }
}
