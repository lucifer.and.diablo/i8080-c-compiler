/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.opcodes.Register;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucifer
 */
public class Gen80 {

    public List<com.ats.gen80.opcodes.Opcode> opcodes = new ArrayList<>();
    private int stack = 0;
    public static volatile int localLabel = 0;
    public String prefix;
    public boolean debug = false;

    public void modifyStack(int a) {

        Throwable t = new Throwable();
        StackTraceElement se1 = t.getStackTrace()[1];
        StackTraceElement se2 = t.getStackTrace()[2];
        StackTraceElement se3 = t.getStackTrace()[3];

        if (se1.getClassName().endsWith("Gen80")) {
            se1 = se2;
        }
        if (se1.getClassName().endsWith("Gen80")) {
            se1 = se3;
        }
        if (debug) {
            System.out.println(se1.getClassName() + "." + se1.getMethodName() + " " + a);
        }

        stack += a;
    }

    public void setStack(int a) {
        stack = a;
    }

    public int getStack() {
        return stack;
    }

    public String genLabel() {
        if (prefix == null) {
            prefix = "DL";
        }
        String ret = prefix + "." + localLabel;
//        System.out.println(ret);
        localLabel++;
        return ret;
    }

    public Gen80 label(String label) {
        opcodes.add(new com.ats.gen80.opcodes.Opcode.Label(label));
        return this;
    }

    public Gen80 mvi(com.ats.gen80.opcodes.Register reg, int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.MVI.create(reg, data));
        return this;
    }

    public Gen80 cpi(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CPI.create(data));
        return this;
    }

    public Gen80 ori(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ORI.create(data));
        return this;
    }

    public Gen80 xri(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.XRI.create(data));
        return this;
    }

    public Gen80 sui(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.SUI.create(data));
        return this;
    }

    public Gen80 ani(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ANI.create(data));
        return this;
    }

    public Gen80 adi(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ADI.create(data));
        return this;
    }

    public Gen80 aci(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ACI.create(data));
        return this;
    }

    public Gen80 sbi(int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.SBI.create(data));
        return this;
    }

    public Gen80 mov(com.ats.gen80.opcodes.Register reg1, com.ats.gen80.opcodes.Register reg2) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.MOV.create(reg1, reg2));
        return this;
    }

    public Gen80 lxi(com.ats.gen80.opcodes.Register reg, int data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.LXI.create(reg, data));
        return this;
    }

    public Gen80 lxi(com.ats.gen80.opcodes.Register reg, String data) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.LXI.create(reg, data));
        return this;
    }

    public Gen80 push(com.ats.gen80.opcodes.Register reg) {
        modifyStack(2);
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.PUSH.create(reg));
        return this;
    }

    public Gen80 pop(com.ats.gen80.opcodes.Register reg) {
        modifyStack(-2);
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.POP.create(reg));
        return this;
    }

    public Gen80 dad(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.DAD.create(reg));
        return this;
    }

    public Gen80 stax(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.STAX.create(reg));
        return this;
    }

    public Gen80 ldax(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.LDAX.create(reg));
        return this;
    }

    public Gen80 ora(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ORA.create(reg));
        return this;
    }

    public Gen80 sbb(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.SBB.create(reg));
        return this;
    }

    public Gen80 add(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ADD.create(reg));
        return this;
    }

    public Gen80 ana(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ANA.create(reg));
        return this;
    }

    public Gen80 xra(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.XRA.create(reg));
        return this;
    }

    public Gen80 jmp(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JMP.create(label));
        return this;
    }

    public Gen80 jmp(int label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JMP.create(label));
        return this;
    }

    public Gen80 jm(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JM.create(label));
        return this;
    }

    public Gen80 jz(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JZ.create(label));
        return this;
    }

    public Gen80 jnz(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JNZ.create(label));
        return this;
    }

    public Gen80 jnc(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JNC.create(label));
        return this;
    }

    public Gen80 call(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CALL.create(label));
        return this;
    }

    public Gen80 call(int label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CALL.create(label));
        return this;
    }

    public Gen80 cz(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CZ.create(label));
        return this;
    }

    public Gen80 cc(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CC.create(label));
        return this;
    }

    public Gen80 cnz(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CNZ.create(label));
        return this;
    }

    public Gen80 cnc(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CNC.create(label));
        return this;
    }

    public Gen80 xchg() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.XCHG.create());
        return this;
    }

    public Gen80 xthl() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.XTHL.create());
        return this;
    }

    public Gen80 pchl() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.PCHL.create());
        return this;
    }

    public Gen80 sphl() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.SPHL.create());
        return this;
    }

    public Gen80 ret() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RET.create());
        return this;
    }

    public Gen80 rnc() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RNC.create());
        return this;
    }

    public Gen80 rc() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RC.create());
        return this;
    }

    public Gen80 rz() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RZ.create());
        return this;
    }

    public Gen80 rnz() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RNZ.create());
        return this;
    }

    public Gen80 rm() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RM.create());
        return this;
    }

    public Gen80 rp() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RP.create());
        return this;
    }

    public Gen80 rlc() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RLC.create());
        return this;
    }

    public Gen80 lda(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.LDA.create(label));
        return this;
    }

    public Gen80 sta(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.STA.create(label));
        return this;
    }

    public Gen80 sta(int label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.STA.create(label));
        return this;
    }

    public Gen80 lhld(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.LHLD.create(label));
        return this;
    }

    public Gen80 lhld(int addr) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.LHLD.create(addr));
        return this;
    }

    public Gen80 shld(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.SHLD.create(label));
        return this;
    }

    public Gen80 inx(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.INX.create(reg));
        return this;
    }

    public Gen80 dcx(com.ats.gen80.opcodes.Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.DCX.create(reg));
        return this;
    }

    public Gen80 db(String data) {
        opcodes.add(new com.ats.gen80.opcodes.Opcode.Byte(data));
        return this;
    }

    public Gen80 db(int[] data) {
        opcodes.add(new com.ats.gen80.opcodes.Opcode.Byte(data));
        return this;
    }

    public Gen80 db(int data) {
        opcodes.add(new com.ats.gen80.opcodes.Opcode.Byte(data));
        return this;
    }

    public Gen80 ds(int size) {
        opcodes.add(new com.ats.gen80.opcodes.Opcode.Byte(new int[size]));
        return this;
    }

    public Gen80 sub(Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.SUB.create(reg));
        return this;
    }

    public Gen80 adc(Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.ADC.create(reg));
        return this;
    }

    public Gen80 rar() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RAR.create());
        return this;
    }

    public Gen80 dcr(Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.DCR.create(reg));
        return this;
    }

    public Gen80 inr(Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.INR.create(reg));
        return this;
    }

    public Gen80 ral() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RAL.create());
        return this;
    }

    public Gen80 rrc() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.RRC.create());
        return this;
    }

    public Gen80 cmc() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CMC.create());
        return this;
    }

    public Gen80 cma() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CMA.create());
        return this;
    }

    public Gen80 stc() {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.STC.create());
        return this;
    }

    public Gen80 jp(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JP.create(label));
        return this;
    }

    public Gen80 cp(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CP.create(label));
        return this;
    }

    public Gen80 jc(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.JC.create(label));
        return this;
    }

    public Gen80 cm(String label) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CM.create(label));
        return this;
    }

    public Gen80 cmp(Register reg) {
        opcodes.add(com.ats.gen80.opcodes.Opcode.Mnemonic.CMP.create(reg));
        return this;
    }

    public Gen80 dw(String label) {
        opcodes.add(new com.ats.gen80.opcodes.Opcode.Word(label));
        return this;
    }

    public Gen80 dw(int label) {
        opcodes.add(new com.ats.gen80.opcodes.Opcode.Word(label));
        return this;
    }

    public Gen80 saveBC() {
        return pop(Register.HL).xthl().push(Register.BC).push(Register.HL);
    }

    public Gen80 restoreBC() {
        return pop(Register.BC);
    }
}
