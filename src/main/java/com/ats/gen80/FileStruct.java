/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import java.io.File;
import java.io.Reader;

/**
 *
 * @author lucifer
 */
public class FileStruct {

    public File file;
    public String data;
    public String name;
    public int line;
    public int column;
    public int ntok;
    public int last;
    public int[] buf = new int[10];
    public int buflen;
    public long mtime;
    public Reader is;

}
