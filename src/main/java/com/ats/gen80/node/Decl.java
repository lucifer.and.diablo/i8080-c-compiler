/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.node;

/**
 *
 * @author lucifer
 */
public enum Decl {
    DECL_BODY,
    DECL_PARAM,
    DECL_PARAM_TYPEONLY,
    DECL_CAST
}
