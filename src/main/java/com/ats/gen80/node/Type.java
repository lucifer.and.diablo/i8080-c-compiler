/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.node;

import com.ats.gen80.Globals;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lucifer
 */
public class Type {

    @Override
    public String toString() {
        return kind.name();
    }

    public static Type makeNumType(TypeKind kindEnum, boolean b) {
        Type r = new Type();
        r.kind = kindEnum;
        r.unsigned = b;
        switch (kindEnum) {
            case KIND_VOID:
                r.size = r.align = Globals.typedefs.get("void").size;
                break;
            case KIND_CHAR:
                r.size = r.align = Globals.typedefs.get("char").size;
                break;
            case KIND_INT:
                r.size = r.align = Globals.typedefs.get("int").size;
                break;
            case KIND_LONG:
                r.size = r.align = Globals.typedefs.get("long").size;
                break;
            case KIND_FLOAT:
                r.size = r.align = Globals.typedefs.get("float").size;
                break;
            default:
                throw new RuntimeException("internal error");
        }
        return r;
    }

    public static Type makeStubType() {
        Type ret = new Type();
        ret.kind = TypeKind.KIND_STUB;
        return ret;
    }

    public static Type makeRecType(boolean struct) {
        Type type = new Type();
        type.kind = TypeKind.KIND_STRUCT;
        type.isStruct = struct;
        return type;
    }

    public TypeKind kind;
    public int size;
    public int align;
    public boolean unsigned;
    public boolean isStatic;
    public Type pointer;
    public int len;
    public Map<String, Type> fields;
    public int offset;
    public boolean isStruct;
    public int bitOffset;
    public int bitSize;
    public Type returnType;
    public List<Type> params;
    public boolean hasVarArg;
    public boolean isOldStyle;

    public Type() {
    }

    public Type(TypeKind kind, int size, int align, boolean unsigned) {
        this.kind = kind;
        this.size = size;
        this.align = align;
        this.unsigned = unsigned;
    }

    public Type(TypeKind kind, int size, int align, boolean unsigned, boolean isStatic, Type pointer, int len, Map<String, Type> fields, int offset, boolean isStruct, int bitOffset, int bitSize, Type returnType, List<Type> params, boolean hasVarArg, boolean isOldStyle) {
        this.kind = kind;
        this.size = size;
        this.align = align;
        this.unsigned = unsigned;
        this.isStatic = isStatic;
        this.pointer = pointer;
        this.fields = fields;
        this.offset = offset;
        this.isStruct = isStruct;
        this.bitOffset = bitOffset;
        this.bitSize = bitSize;
        this.returnType = returnType;
        this.params = params;
        this.isOldStyle = isOldStyle;
        this.len = len;
        this.hasVarArg = hasVarArg;
    }

    public Type copy() {
        return new Type(kind, size, align, unsigned, isStatic, pointer, len, fields, offset, isStruct, bitOffset, bitSize, returnType, params, hasVarArg, isOldStyle);
    }

    public static Type copy(Type t) {
        return new Type(t.kind, t.size, t.align, t.unsigned, t.isStatic, t.pointer, t.len, t.fields, t.offset, t.isStruct, t.bitOffset, t.bitSize, t.returnType, t.params, t.hasVarArg, t.isOldStyle);
    }

    public static Type makePointerType(Type pointerTo) {
        Type ret = new Type();
        ret.kind = TypeKind.KIND_PTR;
        ret.pointer = pointerTo;
        ret.size = 2;
        ret.align = 2;
        return ret;
    }

    public static Type makeNumberType(TypeKind kind, boolean isUnsigned) {
        Type ret = new Type();
        ret.unsigned = isUnsigned;
        ret.kind = kind;
        switch (kind) {
            case KIND_VOID:
                ret.size = ret.align = 0;
                break;
            case KIND_CHAR:
                ret.size = ret.align = 1;
                break;
            case KIND_INT:
                ret.size = ret.align = 2;
                break;
            case KIND_LONG:
                ret.size = ret.align = 4;
                break;
            case KIND_FLOAT:
                ret.size = ret.align = 4;
                break;
            default:
                throw new RuntimeException("Internal error");
        }
        return ret;
    }

    public static boolean isString(Type ty) {
        return ty.kind == TypeKind.KIND_ARRAY && ty.pointer.kind == TypeKind.KIND_CHAR;
    }

    public static Type makeArrayType(Type type, int len) {
        int size = 0;
        if (len < 0) {
            size = -1;
        } else {
            size = type.size * len;
        }

        Type ret = new Type();
        ret.kind = TypeKind.KIND_ARRAY;
        ret.pointer = type;
        ret.size = size;
        ret.len = len;
        ret.align = type.align;

        return ret;
    }

    public static Type makeFuncType(Type retType, List<Type> paramTypes, boolean hasVarArg, boolean isOldStyle) {
        Type type = new Type();

        type.kind = TypeKind.KIND_FUNC;
        type.returnType = retType;
        type.params = paramTypes;
        type.hasVarArg = hasVarArg;
        type.isOldStyle = isOldStyle;

        return type;
    }

    public void copyTo(Type stub) {
        stub.align = align;
        stub.bitOffset = bitOffset;
        stub.bitSize = bitSize;
        stub.fields = fields;
        stub.hasVarArg = hasVarArg;
        stub.isOldStyle = isOldStyle;
        stub.isStatic = isStatic;
        stub.isStruct = isStruct;
        stub.kind = kind;
        stub.len = len;
        stub.offset = offset;
        stub.params = params;
        stub.pointer = pointer;
        stub.returnType = returnType;
        stub.size = size;
        stub.unsigned = unsigned;
    }
}
