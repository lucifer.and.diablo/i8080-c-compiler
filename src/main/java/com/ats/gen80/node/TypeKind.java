/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.node;

/**
 *
 * @author lucifer
 */
public enum TypeKind {
    KIND_VOID,
    KIND_CHAR,
    KIND_INT,
    KIND_LONG,
    KIND_FLOAT,
    //KIND_DOUBLE,
    KIND_ARRAY,
    KIND_ENUM,
    KIND_PTR,
    KIND_STRUCT,
    KIND_FUNC,
    KIND_STUB
}
