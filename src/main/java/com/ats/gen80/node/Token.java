/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.node;

import com.ats.gen80.CustomSet;
import com.ats.gen80.FileStruct;
import com.ats.gen80.Lexer;

/**
 *
 * @author lucifer
 */
public class Token {

    public static final Token SPACE_TOKEN = new Token(TokenEnum.TSPACE);
    public static final Token NEWLINE_TOKEN = new Token(TokenEnum.TNEWLINE);
    public static final Token EOF_TOKEN = new Token(TokenEnum.TEOF);

    public static final Token CPP_TOKEN_ZERO = new Token(TokenEnum.TNUMBER, "0");
    public static final Token CPP_TOKEN_ONE = new Token(TokenEnum.TNUMBER, "1");

    public TokenEnum kind;
    public FileStruct file;
    public int line;
    public int column;
    public boolean space;
    public boolean bol;
    public int count;
    public CustomSet<String> hideset;

    public Keyword id;

    public String sval;
    public int slen;
    public int c;
    public EncodingEnum enc;

    public boolean isVararg;
    public int position;

    public Token(TokenEnum kind, FileStruct file, int line, int column, boolean space, boolean bol, int count, CustomSet<String> hideset, Keyword id, String sval, int slen, int c, EncodingEnum enc, boolean isVararg, int position) {
        this.kind = kind;
        this.file = file;
        this.line = line;
        this.column = column;
        this.space = space;
        this.bol = bol;
        this.count = count;
        this.hideset = hideset;
        this.id = id;
        this.sval = sval;
        this.slen = slen;
        this.c = c;
        this.enc = enc;
        this.isVararg = isVararg;
        this.position = position;
    }

    public Token(FileStruct file, Lexer.Pos pos, TokenEnum kind) {
        this(file, pos);
        this.kind = kind;
    }

    public Token(FileStruct file, Lexer.Pos pos, TokenEnum kind, String val) {
        this(file, pos);
        this.kind = kind;
        this.sval = val;
    }

    private Token(TokenEnum kind) {
        this.kind = kind;
    }

    private Token(TokenEnum kind, String val) {
        this.kind = kind;
        this.sval = val;
    }

    public Token(FileStruct file, Lexer.Pos pos) {

        this.file = file;
        this.line = pos.line;
        this.column = pos.column;
        this.count = file.ntok++;

    }

    public Token copy() {
        return new Token(kind, file, line, column, space, bol, count, hideset, id, sval, slen, c, enc, isVararg, position);
    }

    @Override
    public String toString() {

        switch (kind) {
            case TIDENT:
                return sval;
            case TKEYWORD:
                return id.keyword;
            case TCHAR:
                return ((char) c) + "";
            case TNUMBER:
                return sval;
            case TSTRING:
                return sval;
            case TEOF:
                return "(eof)";
            case TINVALID:
                return ((char) c) + "";
            case TNEWLINE:
                return "(newline)";
            case TSPACE:
                return "(space)";
            case TMACRO_PARAM:
                return "(macro-param)";
        }

        return "(unknown)";
    }

}
