/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.node;

import java.util.List;

/**
 *
 * @author lucifer
 */
public class NodeFunctionCall extends NodeVariable {

    public String functionName;
    public List<Node> arguments;
    public Type functionType;
}
