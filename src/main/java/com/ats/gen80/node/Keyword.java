/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.node;

/**
 *
 * @author lucifer
 */
public enum Keyword {

    C_RSBRACE("]"), // ]
    C_LSBRACE("["), // [
    C_COLON(":"), // :
    C_SEMICOLON(";"), // ;
    C_LPAREN("("), // (
    C_RPAREN(")"), // )
    C_HASH("#"), // #
    C_PLUS("+"), // +
    C_MINUS("-"), // +
    C_EQ("="), // =
    C_EXPOINT("!"), // !
    C_QMARK("?"), // ?
    C_MUL("*"), // *
    C_DIV("/"), // /
    C_AND("&"), // &
    C_OR("|"), // |
    C_XOR("^"), // ^
    C_POINT("."), // .
    C_COMMA(","), // ,
    C_LBRACE("{"), // {
    C_RBRACE("}"), // }
    C_TILDA("~"),
    C_LT("<"), // <
    C_GT(">"), // >
    C_MOD("%"), // %
    //    C_SHARP("#"), // #

    AST_LITERAL,
    AST_LVAR,
    AST_GVAR,
    AST_TYPEDEF,
    AST_FUNCALL,
    AST_FUNCPTR_CALL,
    AST_FUNCDESG,
    AST_FUNC,
    AST_DECL,
    AST_INIT,
    AST_CONV,
    AST_ADDR,
    AST_DEREF,
    AST_IF,
    AST_TERNARY,
    AST_DEFAULT,
    AST_RETURN,
    AST_COMPOUND_STMT,
    AST_STRUCT_REF,
    AST_GOTO,
    AST_COMPUTED_GOTO,
    AST_LABEL,
    AST_ASM,
    OP_SIZEOF,
    OP_CAST,
    OP_SHR,
    OP_SHL,
    OP_A_SHR,
    OP_A_SHL,
    OP_PRE_INC,
    OP_PRE_DEC,
    OP_POST_INC,
    OP_POST_DEC,
    OP_LABEL_ADDR,
    OP_ARROW("->"),
    OP_A_ADD("+="),
    OP_A_AND("&="),
    OP_A_DIV("/="),
    OP_A_MOD("%="),
    OP_A_MUL("*="),
    OP_A_OR("|="),
    OP_A_SAL("<<="),
    OP_A_SAR(">>="),
    OP_A_SUB("-="),
    OP_A_XOR("^="),
    OP_DEC("--"),
    OP_EQ("=="),
    OP_GE(">="),
    OP_INC("++"),
    OP_LE("<="),
    OP_LOGAND("&&"),
    OP_LOGOR("||"),
    OP_NE("!="),
    OP_SAL("<<"),
    OP_SAR(">>"),
    KAUTO("auto", true),
    KBREAK("break", false),
    KCASE("case", false),
    KCHAR("char", true),
    KCONST("const", true),
    KCONTINUE("continue", false),
    KDEFAULT("default", false),
    KDO("do", false),
    //    KDOUBLE("double", true),
    KELSE("else", false),
    KENUM("enum", true),
    KEXTERN("extern", true),
    KFLOAT("float", true),
    KFOR("for", false),
    KGOTO("goto", false),
    KIF("if", false),
    KASM("asm", false),
    KINLINE("inline", true),
    KINT("int", true),
    KLONG("long", true),
    KREGISTER("register", true),
    KRESTRICT("restrict", true),
    KRETURN("return", false),
    KHASHHASH("##", false),
    //    KSHORT("short", true),
    KSIGNED("signed", true),
    KSIZEOF("sizeof", false),
    KSTATIC("static", true),
    KSTRUCT("struct", true),
    KSWITCH("switch", false),
    KELLIPSIS("...", false),
    KTYPEDEF("typedef", true),
    KTYPEOF("typeof", true),
    KUNION("union", true),
    KUNSIGNED("unsigned", true),
    KVOID("void", true),
    KWHILE("while", false);

    public static Keyword getByChar(char c) {
        for (Keyword key : Keyword.values()) {
            if (key.keyword.equals(c + "")) {
                return key;
            }
        }

        return null;
    }

    public final String keyword;
    public final boolean bool;

    private Keyword(String keyword) {
        this.keyword = keyword;
        this.bool = false;
    }

    private Keyword(String keyword, boolean bool) {
        this.keyword = keyword;
        this.bool = bool;
    }

    private Keyword() {
        this.keyword = null;
        this.bool = false;
    }

}
