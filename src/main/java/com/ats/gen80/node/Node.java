/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.node;

import com.ats.gen80.Globals;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lucifer
 */
public class Node {

    public static NodeBinary astBinOp(Type type, Keyword keyword, Node node, Node expr) {
        NodeBinary ret = new NodeBinary();
        ret.left = node;
        ret.right = expr;
        ret.type = type;
        ret.kind = keyword;

        return ret;
    }

    public static Node conv(Node node) {
        if (node == null) {
            return null;
        }

        Type type = node.type;
        switch (type.kind) {
            case KIND_ARRAY:
                return astUOp(Keyword.AST_CONV, Type.makePointerType(type.pointer), node);
            case KIND_FUNC:
                return astUOp(Keyword.AST_ADDR, Type.makePointerType(type), node);
            case KIND_INT:
                if (type.bitSize > 0) {
                    return astConv(Globals.typedefs.get("int"), node);
                }
        }

        return node;
    }

    public static NodeUnary astUOp(Keyword keyword, Type type, Node node) {
        NodeUnary ret = new NodeUnary();
        ret.operand = node;
        ret.type = type;
        ret.kind = keyword;

        return ret;
    }

    public static NodeUnary astConv(Type type, Node operand) {
        NodeUnary ret = new NodeUnary();
        ret.kind = Keyword.AST_CONV;
        ret.type = type;
        ret.operand = operand;

        return ret;
    }

    public static boolean sameArithType(Type t, Type u) {
        return t.kind == u.kind && t.unsigned == u.unsigned;
    }

    public static Node wrap(Type t, Node node) {
        if (sameArithType(t, node.type)) {
            return node;
        }

        return astUOp(Keyword.AST_CONV, t, node);
    }

    public static boolean validPointerBinop(Keyword op) {
        switch (op) {
            case C_MINUS:
            case C_PLUS:
            case C_GT:
            case C_LT:
            case OP_EQ:
            case OP_NE:
            case OP_GE:
            case OP_LE:
                return true;
            default:
                return false;
        }
    }

    public static Node binOp(Keyword op, Node left, Node right) {
        if (left.type.kind == TypeKind.KIND_PTR && right.type.kind == TypeKind.KIND_PTR) {
            if (!validPointerBinop(op)) {
                throw new RuntimeException("invalid pointer arithmetic");
            }

            if (op == Keyword.C_MINUS) {
                return astBinOp(Globals.typedefs.get("int"), op, left, right);
            }
            return astBinOp(Globals.typedefs.get("int"), op, left, right);
        }

        if (left.type.kind == TypeKind.KIND_PTR) {
            return astBinOp(left.type, op, left, right);
        }
        if (right.type.kind == TypeKind.KIND_PTR) {
            return astBinOp(right.type, op, left, right);
        }

        Type r = usualArithConv(left.type, right.type);
        return astBinOp(r, op, wrap(r, left), wrap(r, right));
    }

    public static boolean isIntType(Type t) {
        switch (t.kind) {
            case KIND_CHAR:
            case KIND_INT:
            case KIND_LONG:
                return true;
            default:
                return false;
        }
    }

    public static boolean isFloType(Type t) {
        switch (t.kind) {
            case KIND_FLOAT:
                return true;
            default:
                return false;
        }
    }

    public static Type usualArithConv(Type t, Type u) {
        if (t.kind.ordinal() < u.kind.ordinal()) {
            Type tmp = t;
            t = u;
            u = tmp;
        }

        if (isFloType(t)) {
            return t;
        }

        if (t.size > u.size) {
            return t;
        }

        if (t.unsigned == u.unsigned) {
            return t;
        }
        Type r = t.copy();
        r.unsigned = true;
        return r;
    }

    public static boolean isArithType(Type type) {
        return isFloType(type) || isIntType(type);
    }

    public static Keyword getCompoundAssignOp(Token tok) {
        if (tok.kind != TokenEnum.TKEYWORD) {
            return null;
        }
        switch (tok.id) {
            case OP_A_ADD:
                return Keyword.C_PLUS;
            case OP_A_SUB:
                return Keyword.C_MINUS;
            case OP_A_MUL:
                return Keyword.C_MUL;
            case OP_A_DIV:
                return Keyword.C_DIV;
            case OP_A_MOD:
                return Keyword.C_MOD;
            case OP_A_AND:
                return Keyword.C_AND;
            case OP_A_OR:
                return Keyword.C_OR;
            case OP_A_XOR:
                return Keyword.C_XOR;
            case OP_A_SAL:
                return Keyword.OP_SAL;
            case OP_A_SAR:
                return Keyword.OP_SAR;
            case OP_A_SHR:
                return Keyword.OP_SHR;
            default:
                return null;
        }
    }

    public static Node astIntType(Type type, int size) {
        NodeInteger ret = new NodeInteger();
        ret.kind = Keyword.AST_LITERAL;
        ret.val = size;
        ret.type = type;

        return ret;
    }

    public static NodeLabel astLabelAddr(String sval) {
        NodeLabel label = new NodeLabel();
        label.label = sval;
        label.type = Type.makePointerType(Globals.typedefs.get("void"));
        label.kind = Keyword.AST_LABEL;
        return label;
    }

    static int error = 0;

    public static NodeLocalLabel astLVar(Type ty, String name) {
        NodeLocalLabel r = new NodeLocalLabel();
        r.kind = Keyword.AST_LVAR;
        r.type = ty;
        r.variableName = name;
        if (Globals.localEnv != null) {
            if (Globals.localEnv.containsKey(name)) {
                Globals.localEnv.put(name + "." + error++, r);
            } else {
                Globals.localEnv.put(name, r);
            }
        }

        if (Globals.localVars != null) {
            if (Globals.localVars.containsKey(name)) {
                Globals.localVars.put(name + "." + error++, r);
            } else {
                Globals.localVars.put(name, r);
            }

        }

        return r;
    }

    public static Node astGVar(Type ty, String name) {
        NodeGlobalLabel r = new NodeGlobalLabel();
        r.kind = Keyword.AST_GVAR;
        r.type = ty;
        r.variableName = name;
        r.globaLabel = name;
        Globals.globalEnv.put(name, r);
        Globals.env().put(name, r);
        return r;
    }

    public static NodeIf astTernary(Type type, Node condition, Node then, Node els) {
        NodeIf ret = new NodeIf();
        ret.kind = Keyword.AST_TERNARY;
        ret.type = type;
        ret.condition = condition;
        ret.then = then;
        ret.els = els;
        return ret;
    }

    public static NodeFloat astFloatType(Type type, double v) {
        NodeFloat ret = new NodeFloat();
        ret.kind = Keyword.AST_LITERAL;
        ret.type = type;
        ret.val = v;
        return ret;
    }

    public static NodeString astString(EncodingEnum enc, String str, int len) {
        String body = null;
        Type ty = null;
        switch (enc) {
            case ENC_NONE:
            case ENC_UTF8:
            case ENC_CHAR16:
            case ENC_WCHAR:
            case ENC_CHAR32:
                ty = Type.makeArrayType(Globals.typedefs.get("char"), len);
                body = str;
                break;

        }

        NodeString node = new NodeString();
        node.kind = Keyword.AST_LITERAL;
        node.type = ty;
        node.val = body;
        return node;
    }

    public static Node astFuncDesg(Type ty, String name) {
        NodeFunctionPointer node = new NodeFunctionPointer();
        node.kind = Keyword.AST_FUNCDESG;
        node.type = ty;
        node.functionName = name;

        return node;
    }

    public static NodeCompoundStatement astCompoundStmt(List<Node> list) {
        NodeCompoundStatement node = new NodeCompoundStatement();

        node.kind = Keyword.AST_COMPOUND_STMT;
        node.statements = list;

        return node;
    }

    public static Node astTypedef(Type ty, String data) {
        Node r = new Node();
        r.kind = Keyword.AST_TYPEDEF;
        r.type = ty;
        Globals.env().put(data, r);
        return r;

    }

    public static NodeDeclaration astDecl(Node var, List<Node> init) {
        NodeDeclaration node = new NodeDeclaration();
        node.kind = Keyword.AST_DECL;
        node.declarationVariable = var;
        node.declarationInit = init;

        return node;
    }

    public static NodeInitializer astInit(Node init, Type ty, int i) {
        NodeInitializer node = new NodeInitializer();
        node.kind = Keyword.AST_INIT;
        node.initValue = init;
        node.initOffset = i;
        node.toType = ty;
        return node;
    }

    public static NodeGlobalLabel astStaticLVar(Type ty, String name) {
        NodeGlobalLabel r = new NodeGlobalLabel();
        r.kind = Keyword.AST_GVAR;
        r.type = ty;
        r.variableName = name;
        r.globaLabel = Globals.makeStaticLabel(name);
        Globals.localEnv.put(name, r);
        return r;
    }

    public static NodeStruct astStructRef(Type field, Node struc, String sval) {
        NodeStruct ret = new NodeStruct();
        ret.kind = Keyword.AST_STRUCT_REF;
        ret.type = field;
        ret.struct = struc;
        ret.field = sval;

        return ret;
    }

    public static NodeLabel astLabel(String label) {
        NodeLabel node = new NodeLabel();
        node.kind = Keyword.AST_LABEL;
        node.label = label;

        return node;
    }

    public static NodeFunctionCall astFuncCall(Type type, String fname, List<Node> args) {
        NodeFunctionCall node = new NodeFunctionCall();
        node.kind = Keyword.AST_FUNCALL;
        node.type = type.returnType;
        node.functionName = fname;
        node.arguments = args;
        node.functionType = type;

        return node;
    }

    public static NodeFunctionPointerCall astFuncPtrCall(Node fp, List<Node> args) {
        NodeFunctionPointerCall node = new NodeFunctionPointerCall();
        node.kind = Keyword.AST_FUNCPTR_CALL;
        node.type = fp.type.pointer.returnType;
        node.functionPointer = fp;
        node.arguments = args;

        return node;
    }

    public static Node astGoto(String sval) {
        NodeGoto node = new NodeGoto();
        node.kind = Keyword.AST_GOTO;
        node.label = sval;

        return node;
    }

    public static Node astComputedGoto(Node expr) {
        NodeComputedGoto node = new NodeComputedGoto();
        node.kind = Keyword.AST_COMPUTED_GOTO;
        node.operand = expr;

        return node;
    }

    public static Node astIf(Node cond, Node then, Node els) {
        NodeIf node = new NodeIf();
        node.kind = Keyword.AST_IF;
        node.condition = cond;
        node.then = then;
        node.els = els;

        return node;
    }

    public static Node astDest(String label) {
        NodeDest node = new NodeDest();
        node.kind = Keyword.AST_LABEL;
        node.label = node.newLabel = label;

        return node;
    }

    public static Node astJump(String label) {
        NodeJump node = new NodeJump();
        node.kind = Keyword.AST_GOTO;
        node.label = node.newLabel = label;

        return node;
    }

    public static Node astFunc(Type funcType, String data, List<Node> params, Node body, Map<String, Node> localVars) {
        NodeFunctionDeclaration node = new NodeFunctionDeclaration();

        node.kind = Keyword.AST_FUNC;
        node.type = funcType;
        node.functionName = data;
        node.functionParameters = params;
        node.localVariables = new ArrayList<>(localVars.values());
        node.functionBody = body;

        return node;
    }

    public static Node astReturn(Node retval) {
        NodeReturn node = new NodeReturn();

        node.kind = Keyword.AST_RETURN;
        node.returnValue = retval;

        return node;
    }

    public static Node astAsm(String sval) {
        NodeAsm asm = new NodeAsm();
        asm.kind = Keyword.AST_ASM;
        asm.source = sval;
        return asm;
    }

    public Keyword kind;
    public Type type;
    public SourceLocation sourceLoc;

    public static class SourceLocation {

        public String file;
        public int line;
    }

    public static <T extends Node> T cast(Node node) {
        return (T) node;
    }

    public <T extends Node> T cast() {
        return (T) this;
    }

    public <T extends Node> T cast(Class<T> type) {
        return (T) this;
    }
}
