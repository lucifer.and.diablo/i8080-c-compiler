/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucifer
 */
public class FileBuffer {

    private List<FileStruct> files = new ArrayList<>();
    private final List<List<FileStruct>> stashed = new ArrayList<>();

    public void includeFile(File file, String name) {
        files.add(makeFile(file, name));
    }

    public void includeFile(String s) {
        files.add(makeFileString(s));
    }

    public FileStruct makeFile(File file, String name) {
        FileStruct r = new FileStruct();
        r.file = file;
        r.name = name;
        r.line = 1;
        r.column = 1;

        r.mtime = file.lastModified();

        return r;
    }

    public FileStruct makeFileString(String s) {
        FileStruct r = new FileStruct();
        r.data = s;
        r.line = 1;
        r.column = 1;

        return r;
    }

    public void openFile(FileStruct f) {
        if (f.is == null) {
            if (f.file == null) {
                f.is = new BufferedReader(new StringReader(f.data));
            } else {
                try {
                    f.is = new BufferedReader(new FileReader(f.file));
                } catch (FileNotFoundException ex) {
                    System.out.println("WARN: File " + f.name + " cannot be opened");
                    f.is = new StringReader("");
                }
            }
        }
    }

    public void closeFile(FileStruct f) {
        if (f.is != null) {
            try {
                f.is.close();
            } catch (Exception e) {
            }
            f.is = null;
        }
    }

    public int readc(FileStruct fs) {
        try {
            openFile(fs);

            int c = fs.is.read();

            if (c == -1) {
                c = (fs.last == '\n' || fs.last == -1) ? -1 : '\n';
            } else if (c == '\r') {
                fs.is.mark(2);
                int c2 = fs.is.read();
                if (c2 != '\n') {
                    fs.is.reset();
                }
                c = '\n';
            }

            fs.last = c;

            return c;
        } catch (Exception e) {
            return -1;
        }
    }

    public int get() {
        FileStruct f = files.get(files.size() - 1);

        int c;
        if (f.buflen > 0) {
            c = f.buf[--f.buflen];
        } else {
            c = readc(f);
        }

        if (c == '\n') {
            f.line++;
            f.column = 1;
        } else if (c != -1) {
            f.column++;
        }

        return c;
    }

    public void unread(int c) {

        if (c == -1) {
            return;
        }

        FileStruct f = files.get(files.size() - 1);

        if (f.buflen >= f.buf.length) {
            throw new RuntimeException("Buffer overflow");
        }

        f.buf[f.buflen++] = c;

        if (c == '\n') {
            f.column = 1;
            f.line--;
        } else {
            f.column--;
        }
    }

    public int read() {
        while (true) {
            int c = get();
            if (c == -1) {

                if (files.size() == 1) {
                    return c;
                }

                closeFile(files.remove(files.size() - 1));

                continue;
            }

            if (c != '\\') {
                return c;
            }

            int c2 = get();

            if (c2 == '\n') {
                continue;
            }

            unread(c2);
            return c;
        }
    }

    public FileStruct currentFile() {
        return files.get(files.size() - 1);
    }

    public int streamDepth() {
        return files.size();
    }

    public String inputPosition() {
        if (files.isEmpty()) {
            return "(unknown)";
        }

        FileStruct f = currentFile();

        return String.format("%s:%d:%d", f.name == null ? "(generated)" : f.name, f.line, f.column);
    }

    public void streamStash(FileStruct fs) {
        stashed.add(files);
        files = new ArrayList<>();
        files.add(fs);
    }

    public void streamUnstash() {
        files = stashed.remove(stashed.size() - 1);
    }

    public void streamPush(FileStruct fs) {
        files.add(fs);
    }
}
