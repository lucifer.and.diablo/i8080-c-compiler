/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.Type;
import com.ats.gen80.node.TypeKind;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucifer
 */
public class Gen80Misc {

    private static final List<String> registeredOperations = new ArrayList<>();
    private static final com.ats.gen80.opcodes.Register A = com.ats.gen80.opcodes.Register.A;
    private static final com.ats.gen80.opcodes.Register B = com.ats.gen80.opcodes.Register.B;
    private static final com.ats.gen80.opcodes.Register C = com.ats.gen80.opcodes.Register.C;
    private static final com.ats.gen80.opcodes.Register D = com.ats.gen80.opcodes.Register.D;
    private static final com.ats.gen80.opcodes.Register E = com.ats.gen80.opcodes.Register.E;
    private static final com.ats.gen80.opcodes.Register H = com.ats.gen80.opcodes.Register.H;
    private static final com.ats.gen80.opcodes.Register L = com.ats.gen80.opcodes.Register.L;
    private static final com.ats.gen80.opcodes.Register M = com.ats.gen80.opcodes.Register.M;

    private static final com.ats.gen80.opcodes.Register BC = com.ats.gen80.opcodes.Register.BC;
    private static final com.ats.gen80.opcodes.Register DE = com.ats.gen80.opcodes.Register.DE;
    private static final com.ats.gen80.opcodes.Register HL = com.ats.gen80.opcodes.Register.HL;
    private static final com.ats.gen80.opcodes.Register SP = com.ats.gen80.opcodes.Register.SP;

    private static final String SAVE_LONG = ".saveLong";
    private static final String LOAD_LONG = ".loadLong";
    private static final String ADD_LONG = ".longAdd";
    private static final String SUB_LONG = ".longSub";
    private static final String NEG_LONG = ".longNeg";
    private static final String MUL_LONG = ".longMul";
    private static final String DIV_LONG = ".longDiv";
    private static final String MOD_LONG = ".longMod";

    private static final String MUL_SIGNED = ".signedMul";
    private static final String MUL_MLP = ".mulLP";
    private static final String MUL_MSK = ".mulSK";

    private static final String DIV_INT = ".intDiv";
    private static final String DIV_UINT = ".unsignedDiv";
    private static final String DIV_INT_D1 = ".intDivD1";
    private static final String DIV_INT_DV3 = ".intDivDV3";
    private static final String DIV_INT_DV1 = ".intDivDV1";
    private static final String DIV_INT_DV4 = ".intDivDV4";
    private static final String DIV_INT_DV5 = ".intDivDV5";
    private static final String DIV_INT_DV6 = ".intDivDV6";

    private static final String NEG_INT = ".intNeg";
    private static final String COM_INT = ".intCom";

    private static final String SAL_INT = ".salInt";
    private static final String SAL_INT_LOOP = ".salIntLoop";
    private static final String SAL_INT_END = ".salIntEnd";

    private static final String SAR_INT = ".sarInt";
    private static final String SAR_INT_END = ".sarIntEnd";
    private static final String SHR_INT = ".shrInt";
    private static final String SAR_INT_LOOP = ".sarIntLoop";
    private static final String SHF_INT = ".shfInt";

    private static final String SAR_LONG = ".sarLong";
    private static final String SAR_LONG2 = ".sarLong2";
    private static final String SAR_LONG1 = ".sarLong1";
    private static final String SAR_LONG3 = ".sarLong3";
    private static final String SAR_LONG9 = ".sarLong9";
    private static final String SAR_LONG8 = ".sarLong8";
    private static final String SAR_LONG7 = ".sarLong7";

    private static final String SAL_LONG = ".salLong";
    private static final String SAL_LONG2 = ".salLong2";
    private static final String SAL_LONG4 = ".salLong4";
    private static final String SAL_LONG9 = ".salLong9";

    private static final String BC_SAVE = ".bcSave";
//    private static final String DE_SAVE = ".deSave";
//    static final String HL_SAVE = ".hlSave";
    private static final String RET_SAVE = ".retSave";

    private static final String MUL_LOMG_SHTX = ".LmulSHTX";
    private static final String MUL_LOMG_RTX = ".LmulRTX";
    private static final String MUL_LOMG_0 = ".Lmul";
    private static final String MUL_LOMG_NXT = ".LmulNXT";
    private static final String MUL_LOMG_SHTY = ".LmulSHTY";
    private static final String MUL_LOMG_CHK = ".LmulCHK";
    private static final String MUL_LOMG_LFTY = ".LmulLFTY";
    private static final String MUL_LOMG_EXI = ".LmulEXI";

    private static final String DIV_LONG_0 = ".longDiv0";
    private static final String DIV_LONG_1 = ".longDiv1";
    private static final String DIV_LONG_2 = ".longDiv2";
    private static final String DIV_LONG_10 = ".longDiv10";
    private static final String DIV_LONG_3 = ".longDiv3";
    private static final String DIV_LONG_5 = ".longDiv5";
    private static final String DIV_LONG_44 = ".longDiv44";
    private static final String DIV_LONG_4 = ".longDiv4";
    private static final String DIV_LONG_7 = ".longDiv7";
    private static final String DIV_LONG_9 = ".longDiv9";
    private static final String DIV_LONG_8 = ".longDiv8";
    private static final String DIV_LONG_21 = ".longDiv21";
    private static final String DIV_LONG_20 = ".longDiv20";
    private static final String OUT_LONG_LL = ".outLongLL";
    private static final String NEG_ST_4 = ".negSt4";
    private static final String NEG_ST_41 = ".negSt41";

    private static final String AND_INT = ".andInt";
    private static final String OR_INT = ".orInt";
    private static final String XOR_INT = ".xorInt";

    private static final String AND_LONG = ".andLong";
    private static final String OR_LONG = ".orLong";
    private static final String XOR_LONG = ".xorLong";

    private static final String COMP_INT = ".compInt";
    private static final String COMP_LONG = ".compLong";

    private static final String NOT_INT = ".notInt";
    private static final String NOT_LONG = ".notLong";

    private static final String INT_COMPARE_PREFIX = ".intCompare";

    public static final String SUB_INT = ".subInt";

    private static final String LONG_COMPARE_PERFIX = ".longCompare";

    private static final String COPY_STRUCT = ".copyStruct";

    private static final String DOS_SP = ".dosSP";
    private static final String DOS_HL = ".dosHL";
    private static final String BDOS_CALL = "bdos";
    private static final String BIOS_CALL = "bios";

    private static final String CLEAN_BSS = ".cleanBss";
    private static final String CONSOLE_SETUP = ".consoleSetup";
    private static final String CONSOLE_DONE = ".consoleDone";

    private static final String START = "start";
    private static final String ARG_LOOP1 = ".argParseLoop";
    private static final String ARG_LOOP2 = ".argLoop2";
    private static final String ARG_LOOP3 = ".argLoop3";
    private static final String ARG_LOOP4 = ".argLoop4";
    private static final String ARG_PARSE_END = ".argParseEnd";
    private static final String ARG_LOOP6 = ".argLoop6";
    private static final String ARC_COUNTER = ".argCount";

    private static final String CONVERT = ".convert";
    private static final String I2L = ".i2l";
    private static final String C2L = ".c2l";
    private static final String L2I = ".l2i";
    private static final String L2C = ".l2c";

    public static Gen80 misc = new Gen80();

    static {
        misc.label(START)
                .shld(DOS_HL)
                .lxi(HL, 0)
                .dad(SP)
                .shld(DOS_SP)
                .lhld(6)
                .sphl()
                .call(CLEAN_BSS)
                .call(CONSOLE_SETUP)
                .lxi(HL, 0x80)
                .mov(E, M)
                .mvi(M, ' ')
                .mvi(D, 0)
                .dad(DE)
                .inr(E)
                .lxi(BC, 0)
                .label(ARG_LOOP1)
                .push(BC)
                .mov(B, M)
                .dcx(HL)
                .mov(C, M)
                .dcx(HL)
                .dcr(E)
                .dcr(E)
                .jp(ARG_LOOP1)
                .lxi(HL, 0x202a)
                .push(HL)
                .lxi(HL, 0x80)
                .push(HL)
                .lxi(HL, 2)
                .dad(SP)
                .label(ARG_LOOP2)
                .mov(A, M)
                .inx(HL)
                .ora(A)
                .jz(ARG_PARSE_END)
                .cpi(' ')
                .jz(ARG_LOOP2)
                .mov(C, A)
                .cpi('"')
                .jz(ARG_LOOP3)
                .cpi('\'')
                .jz(ARG_LOOP3)
                .mvi(C, ' ')
                .dcx(HL)
                .label(ARG_LOOP3)
                .pop(DE)
                .mov(A, L)
                .stax(DE)
                .inx(DE)
                .mov(A, H)
                .stax(DE)
                .inx(DE)
                .push(DE)
                .push(HL)
                .dcx(HL)
                .label(ARG_LOOP6)
                .inx(HL)
                .mov(A, M)
                .ora(A)
                .jz(ARG_LOOP4)
                .cmp(C)
                .jnz(ARG_LOOP6)
                .mvi(M, 0)
                .inx(HL)
                .label(ARG_LOOP4)
                .xthl()
                .lxi(HL, ARC_COUNTER)
                .inr(M)
                .pop(HL)
                .jmp(ARG_LOOP2)
                .label(ARG_PARSE_END)
                .pop(HL)
                .mvi(M, 0)
                .inx(HL)
                .mvi(M, 0)
                .lhld(ARC_COUNTER)
                .push(HL)
                .lxi(HL, 0x80)
                .push(HL)
                .call("main")
                .call(CONSOLE_DONE)
                .lhld(DOS_SP)
                .sphl()
                .lhld(DOS_HL)
                .jmp(0)
                .label(ARC_COUNTER)
                .dw(0);

        misc.label(DOS_SP).db(new int[]{0, 0});
        misc.label(DOS_HL).db(new int[]{0, 0});

        cleanBss();
        consoleSetup();
        consoleDone();

//        genFloat();

//        misc.label(BC_SAVE).db(new int[]{0, 0});
//        misc.label(DE_SAVE).db(new int[]{0, 0});
//        misc.label(HL_SAVE).db(new int[]{0, 0});
//        misc.label(RET_SAVE).db(new int[]{0, 0});
        misc.debug = false;
    }

    public static void consoleSetup() {
        misc.label(CONSOLE_SETUP)
                .mvi(A, 0x17)
                .sta(0xF720)
                .mvi(A, 1)
                .sta(0xF721)
                .mvi(A, 0)
                .sta(0xF731)
                .mvi(A, 0)
                .sta(0xF72D)
                .ret();
    }

    public static void consoleDone() {
        misc.label(CONSOLE_DONE)
                .mvi(A, 0x1E)
                .sta(0xF720)
                .mvi(A, 2)
                .sta(0xF721)
                .mvi(A, 0xFF)
                .sta(0xF731)
                .mvi(A, 0)
                .sta(0xF72D)
                .ret();
    }

    public static void cleanBss() {
        misc.label(CLEAN_BSS)
                .lxi(HL, ".bss")
                .lxi(BC, ".end")
                .label(CLEAN_BSS + "Loop")
                .mov(A, L)
                .cmp(C)
                .jnz(CLEAN_BSS + "Step")
                .mov(A, H)
                .cmp(B)
                .jnz(CLEAN_BSS + "Step")
                .ret()
                .label(CLEAN_BSS + "Step")
                .mvi(M, 0)
                .inx(HL)
                .jmp(CLEAN_BSS + "Loop");
    }

//    public static void bdos() {
//        misc.label(BDOS_CALL)
//                .mov(H, B)
//                .mov(L, C)
//                .shld(BC_SAVE)
//                .pop(HL)
//                .pop(DE)
//                .pop(BC)
//                .push(BC)
//                .push(DE)
//                .push(HL)
//                .call(5)
//                .mov(E, A)
//                .rlc()
//                .sbb(A)
//                .mov(D, A)
//                .lhld(BC_SAVE)
//                .mov(B, H)
//                .mov(C, L)
//                .ret();
//    }
//    public static void bios() {
//        misc.label(BIOS_CALL)
//                .mov(H, B)
//                .mov(L, C)
//                .shld(BC_SAVE)
//                .pop(HL)
//                .pop(DE)
//                .pop(BC)
//                .push(BC)
//                .push(DE)
//                .push(HL)
//                .mov(A, E)
//                .ral()
//                .add(E)
//                .mvi(D, 0)
//                .mov(E, A)
//                .lxi(HL, 0xDA00)
//                .dad(DE)
//                .lxi(DE, BIOS_CALL + "Finish")
//                .push(DE)
//                .pchl()
//                .label(BIOS_CALL + "Finish")
//                .lhld(BC_SAVE)
//                .mov(B, H)
//                .mov(C, L)
//                .ret();
//    }
    private static void generateLongNegative() {
        if (register(NEG_LONG)) {
            return;
        }
        misc.label(NEG_LONG);
        misc.mvi(A, 0);
        misc.sub(E);
        misc.mov(E, A);
        misc.mvi(A, 0);
        misc.sbb(D);
        misc.mov(D, A);
        misc.mvi(A, 0);
        misc.sbb(C);
        misc.mov(C, A);
        misc.mvi(A, 0);
        misc.sbb(B);
        misc.mov(B, A);
        misc.ret();
    }

    private static final String FARGADDR = ".faddr";
    private static final String FPREX = ".fprex";
    private static final String FACCE = ".facce";
    private static final String FACCS = ".faccs";
    private static final String FACC1 = ".facc1";
    private static final String FACC2 = ".facc2";
    private static final String FACC3 = ".facc3";
    private static final String FSFLAG = ".fsflag";
    private static final String FADRL = ".fadrl";
    private static final String FTMP1 = ".ftmp1";
    private static final String FTMP2 = ".ftmp2";
    private static final String FTMP3 = ".ftmp3";
    private static final String FVALE = ".fvale";
    private static final String FTMP4 = ".ftmp4";
    private static final String FSIG = ".fsig";
    private static final String FFPTXN = ".ffptxn";
    private static final String FFPSUM = ".ffPsum";
    private static final String FVECT = ".fvect";
    private static final String FFINSN = ".ffinsn";
    private static final String FFINFP = ".ffinfp";
    private static final String FFPTXX = ".ffptxx";
    private static final String FFPTF = ".ffptf";
    private static final String FFPTP = ".ffptp";
    private static final String FFPSGN = ".ffpsgn";
    private static final String FFPTZ0 = ".ffptz0";
    private static final String FFPTZ1 = ".ffptz1";
    private static final String FFLTZI = ".ffltzi";
    private static final String FFLTZ2 = ".ffltz2";
    private static final String FFLNK = ".fflnk";
    private static final String FFLNF = ".fflnf";
    private static final String FFSCXF = ".ffscxf";
    private static final String FFSCSG = ".ffscsg";
    private static final String FFSCZ = ".ffscz";
    private static final String FFSCZ2 = ".ffscz2";
    private static final String FFTMPA = ".fftmpa";
    private static final String FFTMPB = ".fftmpb";
    private static final String FFPMAX = ".ffpmax";

    private static final String FSTORE0 = ".fstore0";
    private static final String FSTORE = ".fstore";
    private static final String FSTORE1 = ".fstore1";
    private static final String FZERO = ".fzero";
    private static final String FCHANGESIGN = ".fchangesign";
    private static final String FTEST = ".ftest";
    private static final String FABS = ".fabs";
    private static final String FLOAD = ".fload";

    private static final String FADD = ".fadd";
    private static final String FSUB = ".fsub";
    private static final String FMUL = ".fmul";
    private static final String FDIV = ".fdiv";
    private static final String FOVER = ".foverflow";

    private static final String MDEX = ".mdex";
    private static final String MULX = ".mulX";
    private static final String MULX1 = ".mulX1";
    private static final String MULX2 = ".mulX2";
    private static final String MULX3 = ".mulX3";
    private static final String MULX4 = ".mulX4";
    private static final String RNDA = ".round";
    private static final String LSH = ".leftShift";
    private static final String RSH = ".rightShift";
    private static final String RSH1 = ".rightShift1";
    private static final String RSH2 = ".rightShift2";
    private static final String RSH3 = ".rightShift3";
    private static final String ROND = ".roundSub";
    private static final String DIVX = ".divX";
    private static final String DIVX1 = ".divX1";
    private static final String DIVX2 = ".divX2";
    private static final String DIVX3 = ".divX3";
    private static final String DIVX4 = ".divX4";
    private static final String DIVX5 = ".divX5";
    private static final String DIVX6 = ".divX6";
    private static final String FADD2 = ".fadd2";
    private static final String FADD3 = ".fadd3";
    private static final String FADD4 = ".fadd4";
    private static final String FADD5 = ".fadd5";
    private static final String FADD6 = ".fadd6";
    private static final String FADD7 = ".fadd7";
    private static final String FADD8 = ".fadd8";
    private static final String FADD9 = ".fadd9";
    private static final String FCOMP = ".fcomp";
    private static final String FCOMP1 = ".fcomp1";
    private static final String FCOMP2 = ".fcomp1";

    private static final String FNORM = ".fnorm";
    private static final String FNORM1 = ".fnorm1";
    private static final String FNORM2 = ".fnorm2";
    private static final String FNORM3 = ".fnorm3";
    private static final String OVUN = ".ovun";
    private static final String RNDR = ".rndr";
    private static final String FFIX = ".ffix";
    private static final String FFIX0 = ".ffix0";
    private static final String FFIX1 = ".ffix1";
    private static final String FFIX2 = ".ffix2";
    private static final String FFIX3 = ".ffix3";
    private static final String FFLOAT = ".ffloat";
    private static final String FFLOAT1 = ".ffloat1";

    private static void genFloat() {
        if (register("float")) {
            return;
        }

        misc.label(FARGADDR).ds(2);
        misc.label(FPREX).ds(1);
        misc.label(FACCE).ds(1);
        misc.label(FACCS).ds(1);
        misc.label(FACC1).ds(1);
        misc.label(FACC2).ds(1);
        misc.label(FACC3).ds(1);
        misc.label(FSFLAG).ds(1);
        misc.label(FADRL).ds(2);
        misc.label(FTMP1).ds(1);
        misc.label(FTMP2).ds(1);
        misc.label(FTMP3).ds(1);
        misc.label(FVALE).ds(4);
        misc.label(FTMP4).ds(1);
        misc.label(FFPMAX).db(new int[]{0x7F, 0x99, 0x99, 0x99});

//        misc.label(FSIG).ds(1);
//        misc.label(FFPTXN).ds(8);
//        misc.label(FFPSUM).ds(4);
//        misc.label(FVECT).ds(2);
//        misc.label(F).ds(1);
        misc.label(FSTORE0)
                .mov(M, E)
                .inx(HL);
        misc.label(FSTORE)
                .mov(M, A);
        misc.label(FSTORE1)
                .inx(HL)
                .mov(M, B)
                .inx(HL)
                .mov(M, C)
                .inx(HL)
                .mov(M, D)
                .ret();

        misc.label(FZERO)
                .lxi(HL, FACCE)
                .xra(A)
                .mov(M, A)
                .ret();

        misc.label(FCHANGESIGN)
                .mvi(A, 0x80)
                .db(6);

        misc.label(FABS)
                .xra(A)
                .lxi(HL, FACCS)
                .ana(M)
                .xri(0x80)
                .mov(M, A);

        misc.label(FTEST)
                .lxi(HL, FACCE)
                .mov(A, M)
                .ana(A)
                .jz(FZERO)
                .mov(E, A)
                .inx(HL)
                .mov(A, M)
                .inx(HL)
                .xra(M)
                .inx(HL)
                .mov(C, M)
                .inx(HL)
                .mov(D, M)
                .jmp(FADD8);

        misc.label(FLOAD)
                .mov(A, M)
                .ana(A)
                .jz(FZERO)
                .mov(E, A)
                .inx(HL)
                .mov(A, M)
                .inx(HL)
                .mov(C, M)
                .inx(HL)
                .mov(D, M)
                .mov(L, A)
                .ori(0x80)
                .mov(B, A)
                .xra(L)
                .lxi(HL, FACCE)
                .call(FSTORE0)
                .xra(B)
                .mov(B, A)
                .ori(1)
                .mov(A, E)
                .ret();

        misc.label(FMUL)
                .mov(A, M)
                .ana(A)
                .cnz(MDEX)
                .jz(FZERO)
                .jc(FOVER)
                .call(MULX)
                .mov(A, B)
                .ana(A)
                .jm(RNDA)
                .lxi(HL, FACCE)
                .mov(A, M)
                .sbi(1)
                .mov(M, A)
                .rz()
                .call(LSH);

        misc.label(RNDA)
                .call(ROND)
                .jc(FOVER)
                .mov(B, A)
                .ori(1)
                .mov(A, E)
                .ret();

        misc.label(FDIV)
                .xra(A)
                .sub(M)
                .cpi(1)
                .cnc(MDEX)
                .jc(FOVER)
                .jz(FZERO)
                .mov(C, A)
                .call(DIVX)
                .jc(RNDA);

        misc.label(FOVER)
                .lxi(HL, FFPMAX)
                .jmp(FLOAD);

        misc.label(FSUB)
                .mvi(A, 0x80)
                .db(6);

        misc.label(FADD)
                .xra(A)
                .mov(E, M)
                .inx(HL)
                .xra(M)
                .mov(B, A)
                .inx(HL)
                .mov(C, M)
                .inx(HL)
                .mov(D, M)
                .lxi(HL, FACCE)
                .mov(A, M)
                .dcx(HL)
                .mov(M, A)
                .mov(A, E)
                .ana(A)
                .jz(FTEST)
                .mov(L, B)
                .mov(A, B)
                .ori(0x80)
                .mov(B, A)
                .xra(L)
                .lxi(HL, FACCS)
                .xra(M)
                .sta(FSFLAG)
                .dcx(HL)
                .mov(A, M)
                .ana(A)
                .jz(FADD9)
                .sub(E)
                .jc(FADD2)
                .jm(FTEST)
                .cpi(0x19)
                .jc(FADD3)
                .jmp(FTEST);
        misc.label(FADD2)
                .jp(FADD9)
                .cpi(0xe7)
                .jc(FADD9)
                .mov(M, E)
                .mov(E, A)
                .lda(FSFLAG)
                .lxi(HL, FACCS)
                .xra(M)
                .mov(M, A)
                .xra(A)
                .sub(E)
                .inx(HL)
                .mov(E, M)
                .mov(M, B)
                .mov(B, E)
                .inx(HL)
                .mov(E, M)
                .mov(M, C)
                .mov(C, E)
                .inx(HL)
                .mov(E, M)
                .mov(M, D)
                .mov(D, E);
        misc.label(FADD3)
                .call(RSH)
                .lda(FSFLAG)
                .ana(A)
                .lxi(HL, FACC3)
                .jm(FADD5);
        misc.label(FADD4)
                .mov(A, M)
                .add(D)
                .mov(D, A)
                .dcx(HL)
                .mov(A, M)
                .adc(C)
                .mov(C, A)
                .dcx(HL)
                .mov(A, M)
                .adc(B)
                .mov(B, A)
                .jnc(FADD7)
                .rar()
                .mov(B, A)
                .mov(A, C)
                .rar()
                .mov(C, A)
                .mov(A, D)
                .rar()
                .mov(D, A)
                .rar()
                .mov(E, A)
                .lxi(HL, FACCE)
                .mov(A, M)
                .adi(1)
                .jc(FOVER)
                .mov(M, A)
                .jmp(FADD7);
        misc.label(FADD5)
                .xra(A)
                .sub(E)
                .mov(E, A)
                .mov(A, M)
                .sbb(D)
                .mov(D, A)
                .dcx(HL)
                .mov(A, M)
                .sbb(C)
                .mov(C, A)
                .dcx(HL)
                .mov(A, M)
                .sbb(B)
                .mov(B, A);
        misc.label(FADD6)
                .cc(FCOMP)
                .cp(FNORM)
                .jp(FZERO);
        misc.label(FADD7)
                .call(ROND)
                .jc(FOVER);
        misc.label(FADD8)
                .mov(B, A)
                .lxi(HL, FPREX)
                .mov(A, E)
                .sub(M)
                .mov(L, A)
                .mov(A, B)
                .ori(1)
                .mov(A, E)
                .mov(E, L)
                .ret();
        misc.label(FADD9)
                .lda(FSFLAG)
                .lxi(HL, FACCS)
                .xra(M)
                .dcx(HL)
                .call(FSTORE0)
                .xra(B)
                .jmp(FADD8);

        misc.label(MDEX)
                .mov(B, A)
                .inx(HL)
                .mov(C, M)
                .inx(HL)
                .mov(D, M)
                .inx(HL)
                .mov(E, M)
                .lxi(HL, FACCE)
                .mov(A, M)
                .ana(A)
                .rz()
                .add(B)
                .mov(B, A)
                .rar()
                .xra(B)
                .mov(A, B)
                .mvi(B, 0x80)
                .jp(OVUN)
                .sub(B)
                .rz()
                .mov(M, A)
                .inx(HL)
                .mov(A, M)
                .xra(C)
                .ana(B)
                .mov(M, A)
                .mov(A, C)
                .ora(B)
                .ret();
        misc.label(OVUN)
                .rlc()
                .rc()
                .xra(A)
                .ret();
        misc.label(LSH)
                .mov(A, E)
                .ral()
                .mov(E, A)
                .mov(A, D)
                .ral()
                .mov(D, A)
                .mov(A, C)
                .ral()
                .mov(C, A)
                .mov(A, B)
                .adc(A)
                .mov(B, A)
                .ret();
        misc.label(RSH)
                .mvi(E, 0)
                .mvi(L, 8);
        misc.label(RSH1)
                .cmp(L)
                .jm(RSH2)
                .mov(E, D)
                .mov(D, C)
                .mov(C, B)
                .mvi(B, 0)
                .sub(L)
                .jnz(RSH1);
        misc.label(RSH2)
                .ana(A)
                .rz()
                .mov(L, A);
        misc.label(RSH3)
                .ana(A)
                .mov(A, B)
                .rar()
                .mov(B, A)
                .mov(A, C)
                .rar()
                .mov(C, A)
                .mov(A, D)
                .rar()
                .mov(D, A)
                .mov(A, E)
                .rar()
                .mov(E, A)
                .dcr(L)
                .jnz(RSH3)
                .ret();
        misc.label(FCOMP)
                .dcx(HL)
                .mov(A, M)
                .xri(0x80)
                .mov(M, A);
        misc.label(FCOMP1)
                .xra(A)
                .mov(L, A)
                .sub(E)
                .mov(E, A)
                .mov(A, L)
                .sbb(D)
                .mov(D, A)
                .mov(A, L)
                .sbb(C)
                .mov(C, A)
                .mov(A, L)
                .sbb(B)
                .mov(B, A)
                .ret();
        misc.label(FNORM)
                .mvi(L, 0x20);
        misc.label(FNORM1)
                .mov(A, B)
                .ana(A)
                .jnz(FNORM3)
                .mov(B, C)
                .mov(C, D)
                .mov(D, E)
                .mov(E, A)
                .mov(A, L)
                .sui(8)
                .mov(L, A)
                .jnz(FNORM1)
                .ret();
        misc.label(FNORM2)
                .dcr(L)
                .mov(A, E)
                .ral()
                .mov(E, A)
                .mov(A, D)
                .ral()
                .mov(D, A)
                .mov(A, C)
                .ral()
                .mov(C, A)
                .mov(A, B)
                .adc(A)
                .mov(B, A);
        misc.label(FNORM3)
                .jp(FNORM2)
                .mov(A, L)
                .sui(0x20)
                .lxi(HL, FACCE)
                .add(M)
                .mov(M, A)
                .rz()
                .rar()
                .ana(A)
                .ret();
        misc.label(ROND)
                .lxi(HL, FACCE)
                .mov(A, E)
                .ana(A)
                .mov(E, M)
                .cm(RNDR)
                .rc()
                .mov(A, B)
                .inx(HL)
                .xra(M)
                .jmp(FSTORE1);
        misc.label(RNDR)
                .inr(D)
                .rnz()
                .inr(C)
                .rnz()
                .inr(B)
                .rnz()
                .mov(A, E)
                .adi(1)
                .mov(E, A)
                .mvi(B, 0x80)
                .mov(M, A)
                .ret();

        misc.label(MULX)
                .lxi(HL, MULX4 + "+9")
                .mov(M, A)
                .lxi(HL, MULX4 + "+5")
                .mov(M, D)
                .lxi(HL, MULX4 + "+1")
                .mov(M, E)
                .xra(A)
                .mov(E, A)
                .mov(D, A)
                .lxi(HL, FACC3)
                .call(MULX2)
                .lxi(HL, FACC2)
                .call(MULX1)
                .lxi(HL, FACC1);
        misc.label(MULX1)
                .mov(A, D)
                .mov(E, C)
                .mov(D, B);
        misc.label(MULX2)
                .mov(B, M)
                .mov(L, A)
                .xra(A)
                .mov(C, A)
                .sub(B)
                .jnc(FFIX0);
        misc.label(MULX3)
                .mov(A, L)
                .adc(A)
                .rz()
                .mov(L, A)
                .mov(A, E)
                .ral()
                .mov(E, A)
                .mov(A, D)
                .ral()
                .mov(D, A)
                .mov(A, C)
                .ral()
                .mov(C, A)
                .mov(A, B)
                .ral()
                .mov(B, A)
                .jnc(MULX3)
                .mov(A, E)
                .call(MULX4)
                .mov(C, A)
                .jnc(MULX3)
                .inr(B)
                .ana(A)
                .jmp(MULX3);
        misc.label(MULX4)
                .adi(0)
                .mov(E, A)
                .mov(A, D)
                .aci(0)
                .mov(D, A)
                .mov(A, C)
                .aci(0)
                .ret();
        misc.label(DIVX)
                .lxi(HL, FACC3)
                .mov(A, M)
                .sub(E)
                .mov(M, A)
                .dcx(HL)
                .mov(A, M)
                .sbb(D)
                .mov(M, A)
                .dcx(HL)
                .mov(A, M)
                .sbb(C)
                .mov(M, A)
                .mov(A, C)
                .ral()
                .mov(A, C)
                .rar()
                .sta(DIVX5 + "+12")
                .sta(DIVX6 + "+9")
                .mov(A, D)
                .rar()
                .sta(DIVX5 + "+8")
                .sta(DIVX6 + "+5")
                .mov(A, E)
                .rar()
                .sta(DIVX5 + "+4")
                .sta(DIVX6 + "+1")
                .mvi(B, 0)
                .mov(A, B)
                .rar()
                .sta(DIVX5 + "+1")
                .sta(DIVX5 + "+15")
                .sta(DIVX6 + "+12")
                .lxi(HL, FACC1)
                .mov(A, M)
                .inx(HL)
                .mov(D, M)
                .inx(HL)
                .mov(E, M)
                .ana(A)
                .jm(DIVX4)
                .lxi(HL, FACCE)
                .mov(C, M)
                .inr(C)
                .rz()
                .mov(M, C)
                .mov(L, E)
                .mov(H, D)
                .mov(E, A)
                .mvi(D, 1)
                .mov(C, B);
        misc.label(DIVX1)
                .xra(A)
                .call(DIVX5);
        misc.label(DIVX2)
                .rlc()
                .mov(A, B)
                .ral()
                .rc()
                .rar()
                .mov(A, L)
                .ral()
                .mov(L, A)
                .mov(A, H)
                .ral()
                .mov(H, A)
                .call(LSH)
                .mov(A, D)
                .rrc()
                .jc(DIVX1);
        misc.label(DIVX3)
                .mov(A, L)
                .call(DIVX6)
                .jmp(DIVX2);
        misc.label(DIVX4)
                .mov(L, E)
                .mov(H, D)
                .mov(E, A)
                .mov(D, B)
                .mov(C, B)
                .jmp(DIVX3);
        misc.label(DIVX5)
                .sui(0)
                .mov(A, L)
                .sbi(0)
                .mov(L, A)
                .mov(A, H)
                .sbi(0)
                .mov(H, A)
                .mov(A, E)
                .sbi(0)
                .mov(E, A)
                .mvi(A, 0)
                .ret();
        misc.label(DIVX6)
                .adi(0)
                .mov(L, A)
                .mov(A, H)
                .aci(0)
                .mov(H, A)
                .mov(A, E)
                .aci(0)
                .mov(E, A)
                .mvi(A, 0)
                .ret();
        misc.label(FFLOAT)
                .mov(L, E)
                .mov(E, D)
                .mov(D, C)
                .mov(C, B)
                .mov(B, A)
                .mov(A, L);
        misc.label(FFLOAT1)
                .xri(0x80)
                .lxi(HL, FACCE)
                .mov(M, A)
                .inx(HL)
                .mvi(M, 0x80)
                .inx(HL)
                .mov(A, B)
                .ana(A)
                .ral()
                .jmp(FADD6);
        misc.label(FFIX)
                .lxi(HL, FACCE)
                .mov(A, M)
                .ana(A)
                .jz(FFIX1)
                .mov(A, E)
                .adi(0x7f)
                .sub(M)
                .jc(FFIX2)
                .cpi(0x1f)
                .jnc(FFIX1)
                .adi(1)
                .lxi(HL, FACC1)
                .mov(B, M)
                .inx(HL)
                .mov(C, M)
                .inx(HL)
                .mov(D, M)
                .call(RSH)
                .lxi(HL, FACCS)
                .mov(A, M)
                .ana(A)
                .cp(FCOMP1)
                .mvi(A, 1)
                .ora(B)
                .mov(A, B)
                .mov(B, C);
        misc.label(FFIX0)
                .mov(C, D)
                .mov(D, E)
                .ret();
        misc.label(FFIX1)
                .xra(A)
                .mov(B, A)
                .mov(C, A)
                .mov(D, A)
                .ret();
        misc.label(FFIX2)
                .mov(A, M)
                .sui(0x90)
                .inx(HL)
                .ora(M)
                .inx(HL)
                .mov(C, M)
                .inx(HL)
                .ora(M)
                .inx(HL)
                .ora(M)
                .stc()
                .rnz()
                .mov(B, A)
                .mov(A, C)
                .cpi(0x80)
                .rz()
                .stc()
                .ret();
    }

    private static void genAddSubLong() {
        generateLongNegative();

        if (register(SUB_LONG)) {
            return;
        }

        misc.label(SUB_LONG);
        misc.call(NEG_LONG);
        misc.label(ADD_LONG);
        misc.pop(HL).xthl();
        misc.dad(DE).xchg();
        misc.pop(HL).xthl();
        misc.mov(A, L);
        misc.adc(C);
        misc.mov(C, A);
        misc.mov(A, H);
        misc.adc(B);
        misc.mov(B, A);
        misc.ret();
//        longNegative();
    }

    public static void addLong(Gen80 code) {
        code.modifyStack(-4);
        genAddSubLong();
        code.call(ADD_LONG);
    }

    public static void subLong(Gen80 code) {
        code.modifyStack(-4);
        genAddSubLong();
        code.call(SUB_LONG);
    }

    static void loadChar(Gen80 code) {
        code.mov(E, M);
        code.mvi(D, 0);
    }

    static void loadInt(Gen80 code) {
        code.mov(E, M);
        code.inx(HL);
        code.mov(D, M);
    }

    private static void genLoadLong() {
        if (register(LOAD_LONG)) {
            return;
        }
        misc.label(LOAD_LONG);

        misc.mov(E, M);
        misc.inx(HL);
        misc.mov(D, M);
        misc.inx(HL);
        misc.mov(C, M);
        misc.inx(HL);
        misc.mov(B, M);
        misc.ret();
    }

    static void loadLong(Gen80 code) {
        genLoadLong();
        code.call(LOAD_LONG);
    }

    public static void saveChar(Gen80 code, int schar) {
        code.mvi(M, schar & 0xff);
//        code.inx(HL);
//        code.mvi(M, 0);
    }

    public static void saveInt(Gen80 code, int val) {
        code.lxi(DE, val);
        saveInt(code);
    }

    public static void saveChar(Gen80 code) {
        code.mov(M, E);
    }

    public static void saveInt(Gen80 code) {
        code.mov(M, E);
        code.inx(HL);
        code.mov(M, D);
    }

    public static void saveLong(Gen80 code, int val) {
        genSaveLong();
        code.lxi(BC, (val >> 16) & 0xFFFF);
        code.lxi(DE, val & 0xFFFF);
        code.call(SAVE_LONG);
    }

    public static void saveLong(Gen80 code) {
        genSaveLong();
        code.call(SAVE_LONG);
    }

    private static void genSaveLong() {
        if (register(SAVE_LONG)) {
            return;
        }
        misc.label(SAVE_LONG);
        misc.mov(M, E);
        misc.inx(HL);
        misc.mov(M, D);
        misc.inx(HL);
        misc.mov(M, C);
        misc.inx(HL);
        misc.mov(M, B);
        misc.ret();
    }

    private static boolean test(String op) {
        return registeredOperations.contains(op);
    }

    private static boolean register(String op) {
        boolean b = test(op);

        if (!b) {
            registeredOperations.add(op);
        }

        return b;
    }

    static void addInt(Gen80 code, Type type) {
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:
            case KIND_PTR:
                code.pop(HL);
                code.dad(DE);
                code.xchg();
                break;
            case KIND_LONG:
                addLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    private static void generateSubInt() {
        if (register(SUB_INT)) {
            return;
        }
        misc.label(SUB_INT)
                .pop(HL)
                .xthl()
                .mov(A, L)
                .sub(E)
                .mov(E, A)
                .mov(A, H)
                .sbb(D)
                .mov(D, A)
                .ret();

    }

    static void subInt(Gen80 code, Type type) {
//        code.stack -= 2;
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:
            case KIND_PTR:
                code.modifyStack(-2);
                generateSubInt();
                code.call(SUB_INT);
                break;
            case KIND_LONG:
//                code.stack -= 4;
                subLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void mulLong(Gen80 code) {
        generateLongMul();
        code.modifyStack(-4);
        code.call(MUL_LONG);

    }

    static void mulInt(Gen80 code, Type type) {
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:

                mulInt(code);
                break;
            case KIND_LONG:

                mulLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void divInt(Gen80 code, Type type) {
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:
                if (type.unsigned) {
                    udivInt(code);
                } else {
                    divInt(code);
                }
                break;
            case KIND_LONG:

                divLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void modInt(Gen80 code, Type type) {
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:
                if (type.unsigned) {
                    udivInt(code);
                } else {
                    divInt(code);
                }
                code.xchg();
                break;
            case KIND_LONG:
                modLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void salInt(Gen80 code, Type type) {
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:
                salInt(code);
                break;
            case KIND_LONG:
                salLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void sarInt(Gen80 code, Type type) {
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:
                if (type.unsigned) {
                    shrInt(code);
                } else {
                    sarInt(code);
                }
                break;
            case KIND_LONG:
                sarLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void shrInt(Gen80 code, Type type) {
        switch (type.kind) {
            case KIND_INT:
            case KIND_CHAR:
                shrInt(code);
                break;
            case KIND_LONG:
                sarLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    private static void generateLongMul() {
        if (register(MUL_LONG)) {
            return;
        }

        misc.label(MUL_LONG);

        misc.push(BC);
        misc.push(DE);
        misc.lxi(DE, 0);
        misc.push(DE);
        misc.push(DE);

        misc.label(MUL_LOMG_SHTX);
        misc.mvi(B, 4);
        misc.lxi(HL, 7);
        misc.dad(SP);
        misc.xra(A);

        misc.label(MUL_LOMG_RTX);
        misc.mov(A, M);
        misc.rar();
        misc.mov(M, A);
        misc.dcx(HL);
        misc.dcr(B);
        misc.jnz(MUL_LOMG_RTX);
        misc.jnc(MUL_LOMG_0);
        misc.mvi(B, 4);
        misc.lxi(HL, 10);
        misc.dad(SP);
        misc.xchg();
        misc.lxi(HL, 0);
        misc.dad(SP);
        misc.xra(A);

        misc.label(MUL_LOMG_NXT);
        misc.ldax(DE);
        misc.adc(M);
        misc.mov(M, A);
        misc.inx(DE);
        misc.inx(HL);
        misc.dcr(B);
        misc.jnz(MUL_LOMG_NXT);

        misc.label(MUL_LOMG_0);
        misc.lxi(HL, 4);
        misc.mov(B, L);
        misc.mov(A, H);
        misc.dad(SP);

        misc.label(MUL_LOMG_CHK);
        misc.ora(M);
        misc.jnz(MUL_LOMG_SHTY);
        misc.inx(HL);
        misc.dcr(B);
        misc.jnz(MUL_LOMG_CHK);
        misc.jmp(MUL_LOMG_EXI);

        misc.label(MUL_LOMG_SHTY);
        misc.mvi(B, 4);
        misc.lxi(HL, 10);
        misc.dad(SP);
        misc.xra(A);

        misc.label(MUL_LOMG_LFTY);
        misc.mov(A, M);
        misc.ral();
        misc.mov(M, A);
        misc.inx(HL);
        misc.dcr(B);
        misc.jnz(MUL_LOMG_LFTY);
        misc.jmp(MUL_LOMG_SHTX);

        misc.label(MUL_LOMG_EXI);
        misc.lxi(HL, 8);
        misc.dad(SP);
        misc.mov(E, M);
        misc.inx(HL);
        misc.mov(D, M);
        misc.inx(HL);
        misc.inx(HL);
        misc.inx(HL);
        misc.mov(M, E);
        misc.inx(HL);
        misc.mov(M, D);
        misc.pop(DE);
        misc.pop(BC);

        misc.lxi(HL, 8);
        misc.dad(SP);
        misc.sphl();

        misc.ret();
    }

    private static void divLong(Gen80 code) {
        code.modifyStack(-4);
        generateLongDiv();
        code.call(DIV_LONG);
    }

    private static void generateLongDiv() {
        if (register(DIV_LONG)) {
            return;
        }

        misc.label(MOD_LONG);
        misc.mvi(L, 0);
        misc.jmp(DIV_LONG_0);

        misc.label(DIV_LONG);
        misc.mvi(L, 1);

        misc.label(DIV_LONG_0);

        misc.push(HL);
        misc.push(BC);
        misc.push(DE);

        misc.mov(A, B);
        misc.ora(A);
        misc.jp(DIV_LONG_1);
        misc.call(NEG_ST_4);
        misc.mvi(A, 0xFF);

        misc.label(DIV_LONG_1);
        misc.push(com.ats.gen80.opcodes.Register.PSW);
        misc.lxi(HL, 0);
        misc.push(HL);
        misc.push(HL);
        misc.mvi(L, 17);
        misc.dad(SP);
        misc.mov(B, M);
        misc.dcx(HL);
        misc.mov(C, M);
        misc.dcx(HL);
        misc.mov(D, M);
        misc.dcx(HL);
        misc.mov(E, M);
        misc.push(BC);
        misc.push(DE);
        misc.mov(A, B);
        misc.ora(A);
        misc.jp(DIV_LONG_2);
        misc.call(NEG_ST_4);
        misc.mvi(A, 0xFF);

        misc.label(DIV_LONG_2);
        misc.lxi(HL, 9);
        misc.dad(SP);
        misc.xra(M);
        misc.mov(M, A);
        misc.lxi(HL, 0);
        misc.push(HL);
        misc.push(HL);
        misc.mvi(A, 32);

        misc.label(DIV_LONG_10);
        misc.push(com.ats.gen80.opcodes.Register.PSW);
        misc.lxi(HL, 6);
        misc.dad(SP);
        misc.xra(A);
        misc.mvi(B, 8);

        misc.label(DIV_LONG_3);
        misc.mov(A, M);
        misc.ral();
        misc.mov(M, A);
        misc.inx(HL);
        misc.dcr(B);
        misc.jnz(DIV_LONG_3);
        misc.lxi(HL, 13);
        misc.dad(SP);
        misc.xchg();
        misc.lxi(HL, 19);
        misc.dad(SP);
        misc.lxi(BC, 1024);

        misc.label(DIV_LONG_5);
        misc.ldax(DE);
        misc.cmp(M);
        misc.jc(DIV_LONG_4);
        misc.jnz(DIV_LONG_44);
        misc.dcx(HL);
        misc.dcx(DE);
        misc.dcr(B);
        misc.jnz(DIV_LONG_5);

        misc.label(DIV_LONG_44);
        misc.inr(C);

        misc.label(DIV_LONG_4);
        misc.cmc();
        misc.push(com.ats.gen80.opcodes.Register.PSW);
        misc.lxi(HL, 4);
        misc.dad(SP);
        misc.pop(com.ats.gen80.opcodes.Register.PSW);
        misc.mvi(B, 4);

        misc.label(DIV_LONG_7);
        misc.mov(A, M);
        misc.ral();
        misc.mov(M, A);
        misc.inx(HL);
        misc.dcr(B);
        misc.jnz(DIV_LONG_7);
        misc.mov(A, C);
        misc.ora(A);
        misc.jz(DIV_LONG_8);
        misc.lxi(HL, 16);
        misc.dad(SP);
        misc.xchg();
        misc.lxi(HL, 10);
        misc.dad(SP);
        misc.xra(A);
        misc.xchg();
        misc.mvi(B, 4);

        misc.label(DIV_LONG_9);
        misc.ldax(DE);
        misc.sbb(M);
        misc.stax(DE);
        misc.inx(HL);
        misc.inx(DE);
        misc.dcr(B);
        misc.jnz(DIV_LONG_9);

        misc.label(DIV_LONG_8);
        misc.pop(com.ats.gen80.opcodes.Register.PSW);
        misc.dcr(A);
        misc.jnz(DIV_LONG_10);
        misc.lxi(HL, 18);
        misc.dad(SP);
        misc.mov(A, M);
        misc.ora(A);
        misc.jnz(DIV_LONG_20);
        misc.lxi(HL, 8);
        misc.dad(SP);
        misc.xchg();
        misc.lxi(HL, 0);
        misc.dad(SP);
        misc.xchg();
        misc.mvi(B, 4);

        misc.label(DIV_LONG_21);
        misc.mov(A, M);
        misc.stax(DE);
        misc.inx(HL);
        misc.inx(DE);
        misc.dcr(B);
        misc.jnz(DIV_LONG_21);

        misc.label(DIV_LONG_20);
        misc.lxi(HL, 13);
        misc.dad(SP);
        misc.mov(A, M);
        misc.ora(A);
        misc.cm(NEG_ST_4);
        misc.pop(DE);
        misc.pop(BC);
        misc.lxi(HL, 16);
        misc.dad(SP);
        misc.sphl();

        misc.label(OUT_LONG_LL);
        misc.pop(HL);
        misc.inx(SP);
        misc.inx(SP);
        misc.xthl();
        misc.ret();

        misc.label(NEG_ST_4);
        misc.push(HL);
        misc.push(BC);
        misc.lxi(BC, 1024);
        misc.lxi(HL, 6);
        misc.dad(SP);
        misc.xra(A);

        misc.label(NEG_ST_41);
        misc.mov(A, C);
        misc.sbb(M);
        misc.mov(M, A);
        misc.inx(HL);
        misc.dcr(B);
        misc.jnz(NEG_ST_41);
        misc.pop(BC);
        misc.pop(HL);
        misc.ret();
    }

    private static void modLong(Gen80 code) {
        code.modifyStack(-4);
        generateLongDiv();
        code.call(MOD_LONG);
    }

    protected static void mulInt(Gen80 code) {
        code.modifyStack(-2);
        generateIntMul();
        code.call(MUL_SIGNED);
    }

    private static void generateIntMul() {
        if (register(MUL_SIGNED)) {
            return;
        }

        misc.label(MUL_SIGNED)
                //                .saveBC()
                .pop(HL).xthl().push(BC).push(HL).pop(BC)
                .lxi(HL, 0)
                .mov(A, D)
                .ora(A)
                .mvi(A, 16)
                .jnz(MUL_MLP)
                .mov(D, E)
                .mov(E, H)
                .rrc();
        misc.label(MUL_MLP)
                .dad(HL)
                .xchg()
                .dad(HL)
                .xchg()
                .jnc(MUL_MSK)
                .dad(BC);
        misc.label(MUL_MSK)
                .dcr(A)
                .jnz(MUL_MLP)
                .xchg()
                .pop(BC)
                .ret();
    }

    private static void divInt(Gen80 code) {
        code.modifyStack(-2);
        generateIntDiv();
        code.call(DIV_INT);
    }

    private static void udivInt(Gen80 code) {
        code.modifyStack(-2);
        generateIntDiv();
        code.call(DIV_UINT);
    }

    private static void generateIntDiv() {
        if (register(DIV_INT)) {
            return;
        }

        generateNegInt();

        misc.label(DIV_UINT)
                .pop(HL)
                .xthl()
                .xchg()
                .xra(A)
                .push(com.ats.gen80.opcodes.Register.PSW)
                .jmp(DIV_INT_D1);
        misc.label(DIV_INT)
                .pop(HL)
                .xthl()
                .xchg()
                .mov(A, D)
                .xra(H)
                .push(com.ats.gen80.opcodes.Register.PSW)
                .xra(H)
                .cm(NEG_INT)
                .mov(A, H)
                .ora(A);
        misc.label(DIV_INT_D1)
                .xchg()
                .cp(NEG_INT)
                .xchg()
                .mov(C, L)
                .mov(B, H)
                .lxi(HL, 0)
                .mov(A, B)
                .inr(A)
                .jnz(DIV_INT_DV3)
                .mov(A, D)
                .add(C)
                .mvi(A, 16)
                .jc(DIV_INT_DV1);
        misc.label(DIV_INT_DV3)
                .mov(L, D)
                .mov(D, E)
                .mov(E, H)
                .mvi(A, 8);
        misc.label(DIV_INT_DV1)
                .dad(HL)
                .xchg()
                .dad(HL)
                .xchg()
                .jnc(DIV_INT_DV4)
                .inx(HL);
        misc.label(DIV_INT_DV4)
                .push(HL)
                .dad(BC)
                .pop(HL)
                .jnc(DIV_INT_DV5)
                .dad(BC)
                .inx(DE);
        misc.label(DIV_INT_DV5)
                .dcr(A)
                .jnz(DIV_INT_DV1)
                .xchg()
                .pop(com.ats.gen80.opcodes.Register.PSW)
                .jp(DIV_INT_DV6)
                .xchg()
                .call(NEG_INT)
                .xchg()
                .call(NEG_INT);
        misc.label(DIV_INT_DV6)
                .xchg()
                .ret();

    }

    private static void generateNegInt() {
        if (register(NEG_INT)) {
            return;
        }
        misc.label(NEG_INT)
                .dcx(DE);
        misc.label(COM_INT)
                .mov(A, D)
                .cma()
                .mov(D, A)
                .mov(A, E)
                .cma()
                .mov(E, A)
                .ret();
    }

    private static void salInt(Gen80 code) {
        code.modifyStack(-2);
        generateSalInt();
        code.call(SAL_INT);
    }

    private static void generateSalInt() {
        if (register(SAL_INT)) {
            return;
        }

        misc.label(SAL_INT)
                .pop(HL)
                .xthl();
        misc.label(SAL_INT_LOOP)
                .dcr(E)
                .jm(SAL_INT_END)
                .dad(HL)
                .jmp(SAL_INT_LOOP);
        misc.label(SAL_INT_END)
                .xchg()
                .ret();
    }

    private static void sarInt(Gen80 code) {
        code.modifyStack(-2);
        generateSarInt();
        code.call(SAR_INT);
    }

    private static void generateSarInt() {
        if (register(SAR_INT)) {
            return;
        }

        misc.label(SAR_INT)
                .pop(HL)
                .xthl();
        misc.label(SAR_INT_LOOP)
                .mov(A, H)
                .ral();
        misc.label(SHF_INT)
                .dcr(E)
                .jm(SAR_INT_END)
                .mov(A, H)
                .rar()
                .mov(H, A)
                .mov(A, L)
                .rar()
                .mov(L, A)
                .jmp(SAR_INT_LOOP);
        misc.label(SHR_INT)
                .pop(HL)
                .xthl()
                .xra(A)
                .jmp(SHF_INT);
        misc.label(SAR_INT_END)
                .xchg()
                .ret();
    }

    private static void shrInt(Gen80 code) {
        code.modifyStack(-2);
        generateSarInt();
        code.call(SHR_INT);
    }

    private static void sarLong(Gen80 code) {
        code.modifyStack(-4);
        generateSarSalLong();
        code.call(SAR_LONG);
    }

    private static void salLong(Gen80 code) {
        code.modifyStack(-4);
        generateSarSalLong();
        code.call(SAL_LONG);
    }

    private static void generateSarSalLong() {
        if (register(SAR_LONG)) {
            return;
        }

        misc.label(SAR_LONG)
                .xra(A)
                .ora(B)
                .ora(C)
                .ora(D)
                .jnz(SAR_LONG9)
                .ora(E)
                .jz(SAR_LONG3)
                .mov(A, E)
                .cpi(32)
                .jnc(SAR_LONG9);
        misc.label(SAR_LONG2)
                .mvi(B, 4)
                .lxi(HL, 5)
                .dad(SP)
                .xra(A)
                .mov(A, M)
                .ora(A)
                .jp(SAR_LONG1)
                .stc();
        misc.label(SAR_LONG1)
                .mov(A, M)
                .rar()
                .mov(M, A)
                .dcx(HL)
                .dcr(B)
                .jnz(SAR_LONG1)
                .dcr(E)
                .jnz(SAR_LONG2);

        misc.label(SAR_LONG3)
                .pop(HL)
                .pop(DE)
                .pop(BC)
                .pchl();

        misc.label(SAR_LONG9)
                .lxi(HL, 5)
                .dad(SP)
                .mov(A, M)
                .ora(A)
                .mvi(A, 0)
                .jp(SAR_LONG8)
                .dcr(A);
        misc.label(SAR_LONG8)
                .mvi(B, 4);
        misc.label(SAR_LONG7)
                .mov(M, A)
                .dcx(HL)
                .dcr(B)
                .jnz(SAR_LONG7)
                .jmp(SAR_LONG3);
        misc.label(SAL_LONG)
                .xra(A)
                .ora(B)
                .ora(C)
                .ora(D)
                .jnz(SAL_LONG9)
                .ora(E)
                .jz(SAR_LONG3)
                .mov(A, E)
                .cpi(32)
                .jnc(SAL_LONG9);
        misc.label(SAL_LONG2)
                .mvi(B, 4)
                .lxi(HL, 2)
                .dad(SP)
                .xra(A);
        misc.label(SAL_LONG4)
                .mov(A, M)
                .ral()
                .mov(M, A)
                .inx(HL)
                .dcr(B)
                .jnz(SAL_LONG4)
                .dcr(E)
                .jnz(SAL_LONG2)
                .jmp(SAR_LONG3);
        misc.label(SAL_LONG9)
                .lxi(HL, 5)
                .dad(SP)
                .xra(A)
                .jmp(SAR_LONG8);
    }

    static void binaryAnd(Type type, Gen80 code) {
        switch (type.size) {
            case 1:
            case 2:
                andInt(code);
                break;
            case 4:
                andLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void binaryOr(Type type, Gen80 code) {
        switch (type.size) {
            case 1:
            case 2:

                orInt(code);
                break;
            case 4:

                orLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    static void binaryXor(Type type, Gen80 code) {
        switch (type.size) {
            case 1:
            case 2:

                xorInt(code);
                break;
            case 4:

                xorLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    private static void andInt(Gen80 code) {
        code.modifyStack(-2);
        generateIntAnd();
        code.call(AND_INT);

    }

    private static void andLong(Gen80 code) {
        code.modifyStack(-4);
        generateLongAnd();
        code.call(AND_LONG);
    }

    private static void generateIntAnd() {

        if (register(AND_INT)) {
            return;
        }
        misc.label(AND_INT);
        misc.pop(HL);
        misc.xthl();
        misc.mov(A, E).ana(L).mov(E, A);
        misc.mov(A, D).ana(H).mov(D, A);
        misc.ret();
    }

    private static void generateIntOr() {
        if (register(OR_INT)) {
            return;
        }
        misc.label(OR_INT);
        misc.pop(HL);
        misc.xthl();
        misc.mov(A, E).ora(L).mov(E, A);
        misc.mov(A, D).ora(H).mov(D, A);
        misc.ret();
    }

    private static void generateIntXor() {
        if (register(XOR_INT)) {
            return;
        }
        misc.label(XOR_INT);
        misc.pop(HL);
        misc.xthl();
        misc.mov(A, E).xra(L).mov(E, A);
        misc.mov(A, D).xra(H).mov(D, A);
        misc.ret();
    }

    private static void generateLongAnd() {
        if (register(AND_LONG)) {
            return;
        }

        misc.label(AND_LONG)
                .lxi(HL, 2)
                .dad(SP)
                .mov(A, M).ana(E).mov(M, A).inx(HL)
                .mov(A, M).ana(D).mov(M, A).inx(HL)
                .mov(A, M).ana(C).mov(M, A).inx(HL)
                .mov(A, M).ana(B).mov(M, A)
                .pop(HL)
                .pop(DE)
                .pop(BC)
                .push(HL)
                .ret();
    }

    private static void generateLongOr() {
        if (register(OR_LONG)) {
            return;
        }

        misc.label(OR_LONG)
                .lxi(HL, 2)
                .dad(SP)
                .mov(A, M).ora(E).mov(M, A).inx(HL)
                .mov(A, M).ora(D).mov(M, A).inx(HL)
                .mov(A, M).ora(C).mov(M, A).inx(HL)
                .mov(A, M).ora(B).mov(M, A)
                .pop(HL)
                .pop(DE)
                .pop(BC)
                .push(HL)
                .ret();
    }

    private static void generateLongXor() {
        if (register(XOR_LONG)) {
            return;
        }

        misc.label(XOR_LONG)
                .lxi(HL, 2)
                .dad(SP)
                .mov(A, M).xra(E).mov(M, A).inx(HL)
                .mov(A, M).xra(D).mov(M, A).inx(HL)
                .mov(A, M).xra(C).mov(M, A).inx(HL)
                .mov(A, M).xra(B).mov(M, A)
                .pop(HL)
                .pop(DE)
                .pop(BC)
                .push(HL)
                .ret();
    }

    private static void orInt(Gen80 code) {
        code.modifyStack(-2);
        generateIntOr();
        code.call(OR_INT);
    }

    private static void xorInt(Gen80 code) {
        code.modifyStack(-2);
        generateIntXor();
        code.call(XOR_INT);
    }

    private static void xorLong(Gen80 code) {
        code.modifyStack(-4);
        generateLongXor();
        code.call(XOR_LONG);
    }

    private static void orLong(Gen80 code) {
        code.modifyStack(-4);
        generateLongOr();
        code.call(OR_LONG);
    }

    static void binaryNeg(Type type, Gen80 code) {
        switch (type.size) {
            case 1:
            case 2:
//                code.stack -= 2;
                negInt(code);
                break;
            case 4:
//                code.stack -= 4;
                negLong(code);
                break;
            default:
                throw new RuntimeException("Illegal expression");
        }
    }

    private static void negInt(Gen80 code) {
        generateIntComp();
        code.call(COMP_INT);
    }

    private static void negLong(Gen80 code) {
        generateLongComp();
        code.call(COMP_LONG);
    }

    private static void generateIntComp() {
        if (register(COMP_INT)) {
            return;
        }
        misc.label(COMP_INT)
                .mov(A, E).cma().mov(E, A)
                .mov(A, D).cma().mov(D, A)
                .ret();
    }

    private static void generateLongComp() {
        if (register(COMP_LONG)) {
            return;
        }
        misc.label(COMP_LONG)
                .pop(HL)
                .xthl()
                .mov(A, E).cma().mov(E, A)
                .mov(A, D).cma().mov(D, A)
                .mov(A, L).cma().mov(L, A)
                .mov(A, H).cma().mov(H, A)
                .xthl()
                .push(HL)
                .ret();
    }

    static void logicalNot(Type type, Gen80 code) {
        switch (type.size) {
            case 1:
                code.mvi(D, 0);
            case 2:
                notInt(code);
                break;
            case 4:
                notLong(code);
                break;
            default:
//                Lexer.error("Illegal expression");
                throw new RuntimeException("Illegal expression");
        }
    }

    private static void notInt(Gen80 code) {
        generateNotInt();
        code.call(NOT_INT);
    }

    private static void notLong(Gen80 code) {
//        code.stack -= 2;
        generateNotLong();
        code.call(NOT_LONG);
    }

    private static void generateNotInt() {
        if (register(NOT_INT)) {
            return;
        }

        misc.label(NOT_INT)
                .mov(A, D)
                .ora(E)
                .lxi(DE, 1)
                .rz()
                .dcr(E)
                .ret();

    }

    private static void generateNotLong() {
        if (register(NOT_LONG)) {
            return;
        }

        misc.label(NOT_LONG)
                .pop(HL)
                .xthl()
                .mov(A, D)
                .ora(E)
                .ora(L)
                .ora(H)
                .lxi(HL, 0)
                .xthl()
                .push(HL)
                .lxi(DE, 1)
                .rz()
                .dcr(E)
                .ret();

    }

    static void compare(String l, Type type, Gen80 code) {
        switch (type.size) {
            case 1:
            case 2:
                if (type.unsigned || type.kind == TypeKind.KIND_PTR) {
                    intCompare(l + "U", code);
                } else {
                    intCompare(l, code);
                }
                break;
            case 4:
                longCompare(l, code);
                break;
            default:
                throw new RuntimeException("Invalid operation");
        }
    }

    private static void intCompare(String l, Gen80 code) {
        code.modifyStack(-2);
        generateIntCompare(l);
        code.call(INT_COMPARE_PREFIX + l);
    }

    private static void longCompare(String l, Gen80 code) {
        code.modifyStack(-4);
        generateLongCompare(l);
        code.call(LONG_COMPARE_PERFIX + l);
    }

    private static void generateIntCompare(String l) {
        switch (l) {
            case "L":
            case "LE":
                generateIntLess();
                break;
            case "E":
            case "EU":
                generateIntEqual();
                break;
            case "NE":
            case "NEU":
                generateIntNotEqual();
                break;
            case "LU":
            case "LEU":
                generateIntLessUnsigned();
                break;
        }
    }

    private static void generateIntEqual() {
        if (register(INT_COMPARE_PREFIX + "E")) {
            return;
        }

        misc.label(INT_COMPARE_PREFIX + "E")
                .label(INT_COMPARE_PREFIX + "EU")
                .pop(HL)
                .xthl()
                .mov(A, D)
                .cmp(H)
                .mov(A, E)
                .lxi(DE, 0)
                .rnz()
                .cmp(L)
                .rnz()
                .inr(E)
                .ret();
    }

    private static void generateIntNotEqual() {
        if (register(INT_COMPARE_PREFIX + "NE")) {
            return;
        }

        misc.label(INT_COMPARE_PREFIX + "NE")
                .label(INT_COMPARE_PREFIX + "NEU")
                .pop(HL)
                .xthl()
                .mov(A, D)
                .cmp(H)
                .mov(A, E)
                .lxi(DE, 1)
                .rnz()
                .cmp(L)
                .rnz()
                .dcr(E)
                .ret();
    }

    private static void generateIntLessUnsigned() {
        if (register(INT_COMPARE_PREFIX + "LEU")) {
            return;
        }

        misc.label(INT_COMPARE_PREFIX + "LEU")
                .pop(HL)
                .xthl()
                .mov(A, D)
                .cmp(H)
                .jnz(INT_COMPARE_PREFIX + "LEU1")
                .mov(A, E)
                .cmp(L);
        misc.label(INT_COMPARE_PREFIX + "LEU1")
                .lxi(DE, 1)
                .rnc()
                .dcr(E)
                .ret();

        misc.label(INT_COMPARE_PREFIX + "LU")
                .pop(HL)
                .xthl()
                .mov(A, H)
                .cmp(D)
                .jnz(INT_COMPARE_PREFIX + "LU1")
                .mov(A, L)
                .cmp(E);
        misc.label(INT_COMPARE_PREFIX + "LU1")
                .lxi(DE, 1)
                .rc()
                .dcr(E)
                .ret();
    }

    private static void generateIntLess() {
        if (register(INT_COMPARE_PREFIX + "LE")) {
            return;
        }

        misc.label(INT_COMPARE_PREFIX + "LE")
                .pop(HL)
                .xthl()
                //                .xchg()
                .mov(A, H)
                .cmp(D)
                .jnz(INT_COMPARE_PREFIX + "L1")
                .mov(A, L)
                .cmp(E)
                .jnz(INT_COMPARE_PREFIX + "L2")
                .lxi(DE, 1)
                .ret();
        misc.label(INT_COMPARE_PREFIX + "L")
                .pop(HL)
                .xthl()
                //                .xchg()
                .mov(A, H);
        misc.label(INT_COMPARE_PREFIX + "L1")
                .xra(D)
                .jm(INT_COMPARE_PREFIX + "negHL")
                .mov(A, H)
                .cmp(D)
                .jnz(INT_COMPARE_PREFIX + "L2")
                .mov(A, L)
                .cmp(E);
        misc.label(INT_COMPARE_PREFIX + "L2")
                .lxi(DE, 1)
                .rc();
        misc.label(INT_COMPARE_PREFIX + "L3")
                .dcr(E)
                .ret();
        misc.label(INT_COMPARE_PREFIX + "negHL")
                .mov(A, D)
                .ana(A)
                .lxi(DE, 1)
                .jm(INT_COMPARE_PREFIX + "L3")
                .ret();

    }

    private static void generateLongCompare(String l) {
        switch (l) {
            case "L":
            case "LE":
                generateLongLess();
                break;
            case "E":
            case "NE":
                generateLongEqual();
                break;
        }
    }

    private static void generateLongLess() {
        if (register(LONG_COMPARE_PERFIX + "LE")) {
            return;
        }

        misc.label(LONG_COMPARE_PERFIX + "Stack")
                .lxi(HL, 6)
                .dad(SP)
                .mov(A, M).sub(E).mov(E, A).inx(HL)
                .mov(A, M).sbb(D).mov(D, A).inx(HL)
                .mov(A, M).sbb(C).mov(C, A).inx(HL)
                .mov(A, M)
                .mov(L, B)
                .mov(H, A)
                .sbb(B)
                .mov(B, A)
                .push(com.ats.gen80.opcodes.Register.PSW)
                .mov(A, H)
                .xra(L)
                .jp(LONG_COMPARE_PERFIX + "Normal")
                .pop(com.ats.gen80.opcodes.Register.PSW)
                .cmc()
                .ret();
        misc.label(LONG_COMPARE_PERFIX + "Normal")
                .pop(com.ats.gen80.opcodes.Register.PSW)
                .ret();

        misc.label(LONG_COMPARE_PERFIX + "L")
                .lxi(HL, 4)
                .call(LONG_COMPARE_PERFIX + "Swap")
                .lxi(HL, 1)
                .jmp(LONG_COMPARE_PERFIX + "Lstart");
        misc.label(LONG_COMPARE_PERFIX + "LE")
                .lxi(HL, 4)
                .call(LONG_COMPARE_PERFIX + "Swap")
                .lxi(HL, 2);
        misc.label(LONG_COMPARE_PERFIX + "Lstart")
                .push(HL)
                .call(LONG_COMPARE_PERFIX + "Stack")
                .pop(HL)
                .push(com.ats.gen80.opcodes.Register.PSW)
                .call(LONG_COMPARE_PERFIX + "Long")
                .jnz(LONG_COMPARE_PERFIX + "L1")
                .pop(com.ats.gen80.opcodes.Register.PSW)
                .jmp(LONG_COMPARE_PERFIX + "L2");
        misc.label(LONG_COMPARE_PERFIX + "L1")
                .pop(com.ats.gen80.opcodes.Register.PSW)
                .lxi(HL, 2)
                .jnc(LONG_COMPARE_PERFIX + "L2")
                .dcr(L);
        misc.label(LONG_COMPARE_PERFIX + "L2")
                .pop(DE)
                .pop(BC)
                .pop(BC)
                .push(DE)
                .dcr(L)
                .xchg()
                .ret();
        misc.label(LONG_COMPARE_PERFIX + "Long")
                .mov(A, B)
                .ora(C)
                .ora(D)
                .ora(E)
                .ret();

        misc.label(LONG_COMPARE_PERFIX + "Swap")
                //.lxi(HL, 2)
                .dad(SP)
                .mov(A, M).mov(M, E).mov(E, A).inx(HL)
                .mov(A, M).mov(M, D).mov(D, A).inx(HL)
                .mov(A, M).mov(M, C).mov(C, A).inx(HL)
                .mov(A, M).mov(M, B).mov(B, A)
                .ret();

    }

    private static void generateLongEqual() {
        if (register(LONG_COMPARE_PERFIX + "E")) {
            return;
        }

        misc.label(LONG_COMPARE_PERFIX + "E")
                .pop(HL)
                .push(BC)
                .mvi(C, 1)
                .jmp(LONG_COMPARE_PERFIX + "Ecomp");
        misc.label(LONG_COMPARE_PERFIX + "NE")
                .pop(HL)
                .push(BC)
                .mvi(C, 0);
        misc.label(LONG_COMPARE_PERFIX + "Ecomp")
                .push(DE)
                .push(HL)
                .lxi(HL, 2)
                .dad(SP)
                .xchg()
                .lxi(HL, 6)
                .dad(SP)
                .mvi(B, 4);
        misc.label(LONG_COMPARE_PERFIX + "Eneq")
                .ldax(DE)
                .cmp(M)
                .inx(HL)
                .inx(DE)
                .jnz(LONG_COMPARE_PERFIX + "Ediff")
                .dcr(B)
                .jnz(LONG_COMPARE_PERFIX + "Eneq")
                .xra(A)
                .jmp(LONG_COMPARE_PERFIX + "Eexneq");
        misc.label(LONG_COMPARE_PERFIX + "Ediff")
                .mvi(A, 1);
        misc.label(LONG_COMPARE_PERFIX + "Eexneq")
                .pop(HL)
                .pop(DE).pop(DE).pop(DE)
                .xthl()
                .mvi(D, 0)
                .xra(C)
                .mov(E, A)
                .lhld(BC_SAVE)
                .push(HL)
                .pop(BC)
                .ret();
    }

    static void copyStruct(Gen80 code) {
        generateCopyStruct();
        code.modifyStack(-6);
        code.call(COPY_STRUCT);
    }

    private static void generateCopyStruct() {
        if (register(COPY_STRUCT)) {
            return;
        }
        misc.label(BC_SAVE).db(new int[]{0, 0});
        misc.label(RET_SAVE).db(new int[]{0, 0});
        misc.label(COPY_STRUCT)
                .pop(HL)
                .shld(RET_SAVE)
                .push(BC)
                .pop(HL)
                .shld(BC_SAVE)
                .pop(BC)
                .pop(DE)
                .pop(HL)
                .mov(A, C)
                .ora(B)
                .jz(COPY_STRUCT + "End")
                .label(COPY_STRUCT + "Loop")
                .mov(A, M)
                .stax(DE)
                .inx(HL)
                .inx(DE)
                .dcr(C)
                .jnz(COPY_STRUCT + "Loop")
                .xra(A)
                .cmp(B)
                .jz(COPY_STRUCT + "End")
                .dcr(B)
                .jnz(COPY_STRUCT + "Loop")
                .label(COPY_STRUCT + "End")
                .lhld(BC_SAVE)
                .push(HL)
                .pop(BC)
                .lhld(RET_SAVE)
                .pchl();
    }

    static void convertIntToLong(Gen80 code, Type to, Type from) {
        generateConvertLong();
        code.call(I2L);
    }

    private static void generateConvertLong() {
        if (register(CONVERT)) {
            return;
        }

        misc.label(I2L)
                .mov(A, D)
                .ora(A)
                .mvi(A, -1)
                .jm(I2L + "test")
                .inr(A)
                .label(I2L + "test")
                .mov(B, A)
                .mov(C, A)
                .ret();
    }

}
