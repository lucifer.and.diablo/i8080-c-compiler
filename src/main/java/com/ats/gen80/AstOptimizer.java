/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.Keyword;
import com.ats.gen80.node.Node;
import com.ats.gen80.node.NodeBinary;
import com.ats.gen80.node.NodeCompoundStatement;
import com.ats.gen80.node.NodeFunctionCall;
import com.ats.gen80.node.NodeFunctionDeclaration;
import com.ats.gen80.node.NodeInteger;
import com.ats.gen80.node.NodeString;
import com.ats.gen80.node.NodeUnary;

/**
 *
 * @author lucifer
 */
class AstOptimizer {

    static Node optimize(Node n) {
        n = optimizeNode(n);
//        System.out.println();
        return n;
    }

    static Node optimizeNode(Node n) {
//        System.out.print(n.kind + " ");
        if (n instanceof NodeCompoundStatement) {
            n = optimizeCompoundStmt((NodeCompoundStatement) n);
        }
        if (n instanceof NodeFunctionDeclaration) {
            n = optimizeFunction((NodeFunctionDeclaration) n);
        }
        if (n instanceof NodeFunctionCall) {
            n = optimizeFunctionCall((NodeFunctionCall) n);
        }
        if (n instanceof NodeBinary) {
            n = optimizeBinary((NodeBinary) n);
        }
        if (n instanceof NodeUnary) {
            n = optimizeUnary((NodeUnary) n);
        }
        return n;
    }

    private static Node optimizeCompoundStmt(NodeCompoundStatement node) {
        for (int i = 0; i < node.statements.size(); i++) {
            node.statements.set(i, optimizeNode(node.statements.get(i)));
        }
        return node;
    }

    private static Node optimizeFunction(NodeFunctionDeclaration node) {
        node.functionBody = optimizeNode(node.functionBody);
        return node;
    }

    private static Node optimizeFunctionCall(NodeFunctionCall node) {
        for (int i = 0; i < node.arguments.size(); i++) {
            node.arguments.set(i, optimizeNode(node.arguments.get(i)));
        }
        return node;
    }

    private static Node optimizeBinary(NodeBinary n) {
        n.left = optimizeNode(n.left);
        n.right = optimizeNode(n.right);
        if (n.left.kind == Keyword.AST_LITERAL && n.right.kind == Keyword.AST_LITERAL) {
            NodeInteger nl = n.left.cast();
            NodeInteger nr = n.right.cast();
            System.out.println("GOT " + nl.val + " " + n.kind + " " + nr.val);
            switch (n.kind) {
                case C_PLUS:
                    nl.val += nr.val;
                    return nl;
                case C_MINUS:
                    nl.val -= nr.val;
                    return nl;
                case C_MUL:
                    nl.val *= nr.val;
                    return nl;
                case C_DIV:
                    nl.val /= nr.val;
                    return nl;
                case C_MOD:
                    nl.val %= nr.val;
                    return nl;
                case OP_SAL:
                    nl.val <<= nr.val;
                    return nl;
                case OP_SAR:
                    nl.val >>= nr.val;
                    return nl;
                case OP_SHR:
                    nl.val >>= nr.val;
                    return nl;
                case C_XOR:
                    nl.val ^= nr.val;
                    return nl;
                default:

            }

        }
        return n;
    }

    private static Node optimizeUnary(NodeUnary node) {
        node.operand = optimizeNode(node.operand);
        if (node.kind == Keyword.AST_CONV) {
            if (node.operand.kind == Keyword.AST_LITERAL) {
                if (node.operand.type == node.type) {
                    return node.operand;
                } else {
                    if (node.operand instanceof NodeString) {
                        return node;
                    }
                    NodeInteger i = node.operand.cast();
                    switch (node.type.size) {
                        case 1:
                            i.val = i.val & 0xff;
                            break;
                        case 2:
                            i.val = i.val & 0xffff;
                            break;
                        case 4:
                            i.val = i.val & 0xffffffff;
                            break;
                    }
                    i.type = node.type;
//                    System.out.println(node.type + " -> " + node.operand.type);
                    return i;
                }
            }
        }
        return node;
    }

}
