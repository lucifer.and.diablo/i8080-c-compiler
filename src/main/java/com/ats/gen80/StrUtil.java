/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

/**
 *
 * @author lucifer
 */
public class StrUtil {

    static char cvtIn[] = {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
        100, 100, 100, 100, 100, 100, 100, 
        10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
        30, 31, 32, 33, 34, 35,
        100, 100, 100, 100, 100, 100, 
        10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
        30, 31, 32, 33, 34, 35};
    static int maxExponent = 511;

    static double powersOf10[] = {
        10.,
        100.,
        1.0e4,
        1.0e8,
        1.0e16,
        1.0e32,
        1.0e64,
        1.0e128,
        1.0e256
    };

    public static boolean strpbrk(String s, String need) {
        for (int i = 0; i < need.length(); i++) {
            if (s.contains("" + need.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static double strod(String s, DataRef<Integer> end) {
        boolean sign = false;
        boolean exprSign = false;

        double fraction;
        double dblExp;
        double d;

        String p;
        int c;
        int exp = 0;
        int fracExp = 0;

        int mantSize;
        int decPt;

        int pExp;

        p = s;

        int v = 0;

        while (Character.isWhitespace(p.charAt(0))) {
            v++;
            p = p.substring(1);
        }

        if (p.charAt(0) == '-') {
            sign = true;
            p = p.substring(1);
            v++;
        } else {
            if (p.charAt(0) == '+') {
                v++;
                p = p.substring(1);
            }

            sign = false;
        }

        decPt = -1;
        for (mantSize = 0;; mantSize += 1) {
            c = p.length() == 0 ? 0 : p.charAt(0);
            if (!Character.isDigit(c)) {
                if ((c != '.') || (decPt >= 0)) {
                    break;
                }
                decPt = mantSize;
            }
            v++;
            p = p.substring(1);
        }

        pExp = v;
        p = s.substring(v - mantSize);
        v = v - mantSize;

        if (decPt < 0) {
            decPt = mantSize;
        } else {
            mantSize -= 1;
        }

        if (mantSize > 18) {
            fracExp = decPt - 18;
            mantSize = 18;
        } else {
            fracExp = decPt - mantSize;
        }

        if (mantSize == 0) {
            fraction = 0.0;
            p = s;

            if (end != null) {
                end.setData(v);
            }

            if (sign) {
                return -fraction;
            }

            return fraction;
        } else {
            int frac1, frac2;
            frac1 = 0;
            for (; mantSize > 9; mantSize -= 1) {
                c = p.charAt(0);
                v++;
                p = p.substring(1);
                if (c == '.') {
                    c = p.charAt(0);
                    v++;
                    p = p.substring(1);
                }
                frac1 = 10 * frac1 + (c - '0');
            }

            frac2 = 0;
            for (; mantSize > 0; mantSize -= 1) {
                c = p.charAt(0);
                p = p.substring(1);
                v++;
                if (c == '.') {
                    c = p.charAt(0);
                    v++;
                    p = p.substring(1);
                }
                frac2 = 10 * frac2 + (c - '0');
            }

            fraction = (1.0e9 * frac1) + frac2;
        }

        p = s.substring(pExp);
        v = pExp;

        if (p.length() > 0 && (p.charAt(0) == 'E' || p.charAt(0) == 'e')) {
            v++;
            p = p.substring(1);
            if (p.charAt(0) == '-') {
                exprSign = true;
                v++;
                p = p.substring(1);
            } else {
                if (p.charAt(0) == '+') {
                    p = p.substring(1);
                    v++;
                }
                exprSign = false;
            }
            if (Character.isDigit(p.charAt(0))) {
                p = s.substring(pExp);
                v = pExp;
                if (end != null) {
                    end.setData(v);
                }

                if (sign) {
                    return -fraction;
                }

                return fraction;
            }
            while (Character.isDigit(p.charAt(0))) {
                exp = exp * 10 + (p.charAt(0) - '0');
                p = p.substring(1);
                v++;
            }
        }

        if (exprSign) {
            exp = fracExp - exp;
        } else {
            exp = fracExp + exp;
        }

        if (exp < 0) {
            exprSign = true;
            exp = -exp;
        } else {
            exprSign = false;
        }

        if (exp > maxExponent) {
            exp = maxExponent;
        }

        dblExp = 1.0;

        int f = 0;
        for (d = powersOf10[0]; exp != 0; exp >>= 1, d = powersOf10[f + 1], f++) {
            if ((exp & 1) == 1) {
                dblExp *= d;
            }
        }

        if (exprSign) {
            fraction /= dblExp;
        } else {
            fraction *= dblExp;
        }

        if (end != null) {
            end.setData(v);
        }

        if (sign) {
            return -fraction;
        }

        return fraction;

    }

    public static long strtoul(String string, DataRef<Integer> end, int base) {
        String p = string;
        boolean negative = false;
        boolean anyDigits = false;
        long result = 0;
        boolean overflow = false;
        int digit = 0;
        int c = 0;
        while (Character.isWhitespace(p.charAt(0))) {
            p = p.substring(1);
            c++;
        }

        if (p.charAt(0) == '-') {
            negative = true;
            p = p.substring(1);
            c++;
        } else if (p.charAt(0) == '+') {
            p = p.substring(1);
            c++;
        }

        if (base == 0) {
            if (p.charAt(0) == '0') {
                p = p.substring(1);
                c++;
                if (p.length() > 0) {
                    if (p.charAt(0) == 'x' || p.charAt(0) == 'X') {
                        p = p.substring(1);
                        c++;
                        base = 16;
                    } else {
                        anyDigits = true;
                        base = 8;
                    }
                }
            } else {
                base = 10;
            }
        } else if (base == 16) {
            if (p.charAt(0) == '0' && (p.charAt(1) == 'x' || p.charAt(1) == 'X')) {
                p = p.substring(2);
                c++;
            }
        }

        switch (base) {
            case 8: {
                long maxres = Long.MAX_VALUE >> 3;
                for (; p.length() > 0; p = p.substring(1), c++) {
                    digit = p.charAt(0) - '0';
                    if (digit > 7) {
                        break;
                    }
                    if (result > maxres) {
                        overflow = true;
                    }
                    result = result << 3;
                    if (digit > (Long.MAX_VALUE - result)) {
                        overflow = true;
                    }
                    result += digit;
                    anyDigits = true;
                }
                break;
            }
            case 10: {
                long maxres = Long.MAX_VALUE / 10;
                for (; p.length() > 0; p = p.substring(1), c++) {
                    digit = p.charAt(0) - '0';
                    if (digit > 9) {
                        break;
                    }
                    if (result > maxres) {
                        overflow = true;
                    }
                    result = result * 10;
                    if (digit > (Long.MAX_VALUE - result)) {
                        overflow = true;
                    }
                    result += digit;
                    anyDigits = true;
                }
                break;
            }
            case 16: {
                long maxres = Long.MAX_VALUE >> 4;
                for (; p.length() > 0; p = p.substring(1), c++) {
                    digit = p.charAt(0) - '0';

                    if (digit > ('z' - '0')) {
                        break;
                    }
                    digit = cvtIn[digit];
                    if (digit > 15) {
                        break;
                    }
                    if (result > maxres) {
                        overflow = true;
                    }
                    result = result << 4;
                    if (digit > (Long.MAX_VALUE - result)) {
                        overflow = true;
                    }
                    result += digit;
                    anyDigits = true;
                }
                break;
            }
            default:
                break;
        }

        if (!anyDigits) {
            p = string;
        }

        if (end != null) {
            end.setData(c);
        }

        if (overflow) {
            return Long.MAX_VALUE;
        }

        if (negative) {
            return -result;
        }

        return result;

    }

}
