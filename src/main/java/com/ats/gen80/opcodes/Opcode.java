/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.opcodes;

import java.nio.charset.Charset;
import java.util.Objects;

/**
 *
 * @author lucifer
 */
public class Opcode {

    public Register dstReg;
    public Register srcReg;
    public int align;
    public int opcode;
    public String sym;
    public int data;
    public String mnemonic;
    public int opcodeSize;
    public int addr;
    public boolean resolve = false;
    public Mnemonic mnem;

    public int alignDestReg(int op, Register reg) {
        return op | reg.dst;
    }

    public int alignSrcReg(int op, Register reg) {
        return op | reg.src;
    }

    public int alignDestSrcReg(int op, Register dst, Register src) {
        return op | dst.dst | src.src;
    }

    public boolean is8Bit(Register reg) {
        return reg.bits == 8;
    }

    public boolean is16Bit(Register reg) {
        return reg.bits == 16;
    }

    public int alignData(int data) {
        return data & (align == 8 ? 0xFF : 0xFFFF);
    }

    public byte[] getData() {
        byte[] ret = new byte[opcodeSize];
        int op = opcode;

        if (srcReg != null) {
            op = alignSrcReg(op, srcReg);
        }
        if (dstReg != null) {
            op = alignDestReg(op, dstReg);
        }

        if (ret.length == 0) {
            return ret;
        }

        ret[0] = (byte) op;

        if (opcodeSize > 1) {
            ret[1] = (byte) (data & 0xff);
            if (opcodeSize == 3) {
                ret[2] = (byte) ((data >> 8) & 0xff);
            }
        }

        return ret;
    }

    public void fillCode(StringBuilder sb) {
        if (mnemonic == null) {
            return;
        }
//        sb.append("L").append(toHex4(addr)).append(":");
        sb.append("\t");
        sb.append(String.format("%-8s", mnemonic));

        if (dstReg != null) {
            sb.append(dstReg.regMnem);
            if (opcodeSize > 1 || srcReg != null) {
                sb.append(",");
            }
        }

        if (srcReg != null) {
            sb.append(srcReg.regMnem);
        } else if (opcodeSize > 1) {
            if (sym != null) {
                sb.append(sym);
            } else {
                sb.append(toHex(alignData(data)));
            }
        }

        sb.append("\t; ");
        sb.append(genHex(addr, true).substring(0, 4));
        sb.append(": ");
        sb.append(genHex(getData()));

        sb.append("\n");

    }

    public String toHex(int data) {
        String ret = String.format("%0" + (opcodeSize == 2 ? "2" : "4") + "xH", data).toUpperCase();
        if (ret.charAt(0) >= 'A' && ret.charAt(0) <= 'F') {
            ret = "0" + ret;
        }

        return ret;
    }

    public String toHex4(int data) {
        String ret = String.format("%04xH", data).toUpperCase();
        if (ret.charAt(0) >= 'A' && ret.charAt(0) <= 'F') {
            ret = "0" + ret;
        }

        return ret;
    }

    private String genHex(byte[] data) {
        String ret = "";

        for (int i = 0; i < data.length; i++) {
            ret += String.format("%02x ", data[i]).toUpperCase();
        }

        return ret;
    }

    private static String genHex(int g, boolean d) {
        g = g < 0 ? g + 65536 : g;
        String h = String.format("%0" + (!d ? "2" : "4") + "xH", g).toUpperCase();
        if (h.charAt(0) >= 'A' && h.charAt(0) <= 'F') {
            h = "0" + h;
        }

        return h;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        fillCode(sb);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.dstReg);
        hash = 67 * hash + Objects.hashCode(this.srcReg);
        hash = 67 * hash + this.opcode;
        hash = 67 * hash + Objects.hashCode(this.sym);
        hash = 67 * hash + this.data;
        hash = 67 * hash + Objects.hashCode(this.mnemonic);
        hash = 67 * hash + this.opcodeSize;
        hash = 67 * hash + Objects.hashCode(this.mnem);
        hash = 67 * hash + this.addr;
        hash = 67 * hash + hash();
        return hash;
    }

    public int hash() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Opcode other = (Opcode) obj;
        if (this.opcode != other.opcode) {
            return false;
        }
        if (this.data != other.data) {
            return false;
        }
        if (this.opcodeSize != other.opcodeSize) {
            return false;
        }
        if (!Objects.equals(this.sym, other.sym)) {
            return false;
        }
        if (!Objects.equals(this.mnemonic, other.mnemonic)) {
            return false;
        }
        if (this.dstReg != other.dstReg) {
            return false;
        }
        if (this.srcReg != other.srcReg) {
            return false;
        }
        if (this.mnem != other.mnem) {
            return false;
        }
        if (this.addr != other.addr) {
            return false;
        }
        return true;
    }

    public static final Opcode DISABLE_OPTIMIZER = new DisableOprimizer();
    public static final Opcode ENABLE_OPTIMIZER = new EnableOprimizer();

    public Opcode copy() {
        Opcode ret = new Opcode();
        ret.addr = addr;
        ret.align = align;
        ret.data = data;
        ret.dstReg = dstReg;
        ret.mnem = mnem;
        ret.mnemonic = mnemonic;
        ret.opcode = opcode;
        ret.opcodeSize = opcodeSize;
        ret.resolve = resolve;
        ret.srcReg = srcReg;
        ret.sym = sym;
        return ret;
    }

    public static class DisableOprimizer extends Opcode {

        @Override
        public void fillCode(StringBuilder sb) {
            sb.append("; ----------------------------------------------------\r\n");
            sb.append("; ASM CODE {\r\n");
            sb.append("; ----------------------------------------------------\r\n");
        }

    }

    public static class EnableOprimizer extends Opcode {

        @Override
        public void fillCode(StringBuilder sb) {
            sb.append("; ----------------------------------------------------\r\n");
            sb.append("; }\r\n");
            sb.append("; ----------------------------------------------------\r\n");
        }

    }

    public static class Byte extends Opcode {

        private final String sdata;
        private final int[] bdata;

        public Byte(int data) {
            this.sdata = null;
            this.bdata = new int[]{data};
            this.opcodeSize = 1;
        }

        public Byte(int data[]) {
            this.sdata = null;
            this.bdata = data;
            this.opcodeSize = bdata.length;
        }

        public Byte(String sdata) {
            this.sdata = sdata;
            this.bdata = parseString(sdata);
            this.opcodeSize = bdata.length;
        }

        @Override
        public byte[] getData() {
            byte[] b = new byte[bdata.length];
            for (int i = 0; i < bdata.length; i++) {
                b[i] = (byte) bdata[i];
            }
            return b;
        }

        private int[] parseString(String s) {
            s = new String(s.getBytes(), Charset.forName("UTF-8"));
            byte code[] = s.getBytes(Charset.forName("KOI8-R"));

            int[] data = new int[code.length + 1];

            for (int i = 0; i < s.length(); i++) {
                data[i] = code[i] < 0 ? 256 + code[i] : code[i];
            }
            return data;
        }

        @Override
        public void fillCode(StringBuilder sb) {
            sb.append("\t").append(String.format("%-8s", "DB"));
            boolean isQoute = false;
            for (int i = 0; i < bdata.length; i++) {
                int b = bdata[i];

                if (!isQoute && i != 0) {
                    sb.append(", ");
                }
                if (b < 32 || b > 127 || b == '"') {
                    if (isQoute) {
                        sb.append("\"");
                        isQoute = false;
                        if (i != 0) {
                            sb.append(", ");
                        }
                    }

                    sb.append(genHex(b & 0xff, false));
                } else {
                    if (!isQoute) {
                        sb.append("\"");
                        isQoute = true;
                    }
                    sb.append((char) b);
                }
            }

            if (isQoute) {
                sb.append("\"");
            }

            sb.append("\n");
        }

    }

    public static class Word extends Opcode {

        private final String sdata;
        private final int bdata;

        public Word(int data) {
            this.sdata = null;
            this.bdata = data;
            this.opcodeSize = 2;
        }

        public Word(String sdata) {
            this.sdata = sdata;
            this.bdata = 0;
            this.opcodeSize = 2;
            this.resolve = true;

        }

        @Override
        public int hash() {
            return sdata == null ? bdata : sdata.hashCode();
        }

        @Override
        public byte[] getData() {
            byte[] ret = new byte[2];
            ret[1] = (byte) ((bdata >> 8) & 0xff);
            ret[0] = (byte) ((bdata) & 0xff);
            return ret;
        }

        @Override
        public void fillCode(StringBuilder sb) {
            sb.append("\t").append(String.format("%-8s", "DW"));
            if (sdata == null) {
                sb.append(genHex(bdata, true));
            } else {
                sb.append(sdata);
            }
            sb.append("\n");
        }

        public String getSdata() {
            return sdata;
        }

    }

    public static class Long extends Opcode {

        private final String sdata;
        private final int bdata;

        public Long(int data) {
            this.sdata = null;
            this.bdata = data;
            this.opcodeSize = 4;
        }

        public Long(String sdata) {
            this.sdata = sdata;
            this.bdata = 0;
            this.opcodeSize = 4;
        }

        @Override
        public void fillCode(StringBuilder sb) {
            sb.append("\t").append(String.format("%-8s", "DW"));
            if (sdata == null) {
                sb.append(genHex((bdata >> 16) & 0xffff, true));
                sb.append(",");
                sb.append(genHex((bdata) & 0xffff, true));
            } else {
                sb.append(sdata);
                sb.append(",");
                sb.append(genHex(0, true));
            }
        }

    }

    public static class Float extends Opcode {

        private final String sdata;
        private final float bdata;

        public Float(float data) {
            this.sdata = null;
            this.bdata = data;
            this.opcodeSize = 4;
        }

        public Float(String sdata) {
            this.sdata = sdata;
            this.bdata = 0;
            this.opcodeSize = 4;
        }

        @Override
        public void fillCode(StringBuilder sb) {
            sb.append("\t").append(String.format("%-8s", "DW"));
            if (sdata == null) {
                int fdata = java.lang.Float.floatToIntBits(bdata);
                sb.append(genHex((fdata >> 16) & 0xffff, true));
                sb.append(",");
                sb.append(genHex((fdata) & 0xffff, true));
            } else {
                sb.append(sdata);
                sb.append(",");
                sb.append(genHex(0, true));
            }
        }

    }

    public static class Label extends Opcode {

        public String label;

        public Label(String label) {
            this.label = label;
        }

        @Override
        public void fillCode(StringBuilder sb) {
            sb.append(label).append(":").append("\n");
        }
    }

    public static enum Mnemonic {
        NOP("NOP", 0x00, 1, false, false, 8, new Register[]{}),
        ADI("ADI", 0xC6, 2, false, false, 8, new Register[]{}),
        ACI("ACI", 0xCE, 2, false, false, 8, new Register[]{}),
        SUI("SUI", 0xD6, 2, false, false, 8, new Register[]{}),
        SBI("SBI", 0xDE, 2, false, false, 8, new Register[]{}),
        ANI("ANI", 0xE6, 2, false, false, 8, new Register[]{}),
        XRI("XRI", 0xEE, 2, false, false, 8, new Register[]{}),
        ORI("ORI", 0xF6, 2, false, false, 8, new Register[]{}),
        CPI("CPI", 0xFE, 2, false, false, 8, new Register[]{}),
        CMA("CMA", 0x2F, 1, false, false, 8, new Register[]{}),
        CMC("CMC", 0x3F, 1, false, false, 8, new Register[]{}),
        DAA("DAA", 0x27, 1, false, false, 8, new Register[]{}),
        STC("STC", 0x37, 1, false, false, 8, new Register[]{}),
        DCR("DCR", 0x05, 1, true, false, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        INR("INR", 0x04, 1, true, false, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        DAD("DAD", 0x09, 1, true, false, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.SP}),
        LDAX("LDAX", 0x0A, 1, false, true, 16, new Register[]{Register.BC, Register.DE}),
        STAX("STAX", 0x02, 1, false, true, 16, new Register[]{Register.BC, Register.DE}),
        LHLD("LHLD", 0x2A, 3, false, false, 16, new Register[]{}),
        SHLD("SHLD", 0x22, 3, false, false, 16, new Register[]{}),
        INX("INX", 0x03, 1, false, true, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.SP}),
        DCX("DCX", 0x0B, 1, false, true, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.SP}),
        PUSH("PUSH", 0xC5, 1, false, true, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.PSW}),
        POP("POP", 0xC1, 1, true, false, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.PSW}),
        DI("DI", 0xF3, 1, false, false, 8, new Register[]{}),
        EI("EI", 0xFB, 1, false, false, 8, new Register[]{}),
        HLT("HLT", 0x76, 1, false, false, 8, new Register[]{}),
        PCHL("PCHL", 0xE9, 1, false, false, 8, new Register[]{}),
        IN("IN", 0xDB, 2, false, false, 8, new Register[]{}),
        OUT("OUT", 0xD3, 2, false, false, 8, new Register[]{}),
        ADD("ADD", 0x80, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        ADC("ADC", 0x88, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        SUB("SUB", 0x90, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        SBB("SBB", 0x98, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        CMP("CMP", 0xB8, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        ANA("ANA", 0xA0, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        XRA("XRA", 0xA8, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        ORA("ORA", 0xB0, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        CALL("CALL", 0xCD, 3, false, false, 16, new Register[]{}),
        JMP("JMP", 0xC3, 3, false, false, 16, new Register[]{}),
        RET("RET", 0xC9, 1, false, false, 16, new Register[]{}),
        CNZ("CNZ", 0xC4, 3, false, false, 16, new Register[]{}),
        CZ("CZ", 0xCC, 3, false, false, 16, new Register[]{}),
        CNC("CNC", 0xD4, 3, false, false, 16, new Register[]{}),
        CC("CC", 0xDC, 3, false, false, 16, new Register[]{}),
        CPO("CPO", 0xE4, 3, false, false, 16, new Register[]{}),
        CPE("CPE", 0xEC, 3, false, false, 16, new Register[]{}),
        CP("CP", 0xF4, 3, false, false, 16, new Register[]{}),
        CM("CM", 0xFC, 3, false, false, 16, new Register[]{}),
        JNZ("JNZ", 0xC2, 3, false, false, 16, new Register[]{}),
        JZ("JZ", 0xCA, 3, false, false, 16, new Register[]{}),
        JNC("JNC", 0xD2, 3, false, false, 16, new Register[]{}),
        JC("JC", 0xDA, 3, false, false, 16, new Register[]{}),
        JPO("JPO", 0xE2, 3, false, false, 16, new Register[]{}),
        JPE("JPE", 0xEA, 3, false, false, 16, new Register[]{}),
        JP("JP", 0xF2, 3, false, false, 16, new Register[]{}),
        JM("JM", 0xFA, 3, false, false, 16, new Register[]{}),
        RNZ("RNZ", 0xC0, 1, false, false, 16, new Register[]{}),
        RZ("RZ", 0xC8, 1, false, false, 16, new Register[]{}),
        RNC("RNC", 0xD0, 1, false, false, 16, new Register[]{}),
        RC("RC", 0xD8, 1, false, false, 16, new Register[]{}),
        RPO("RPO", 0xE0, 1, false, false, 16, new Register[]{}),
        RPE("RPE", 0xE8, 1, false, false, 16, new Register[]{}),
        RP("RP", 0xF0, 1, false, false, 16, new Register[]{}),
        RM("RM", 0xF8, 1, false, false, 16, new Register[]{}),
        LDA("LDA", 0x3A, 3, false, false, 16, new Register[]{}),
        STA("STA", 0x32, 3, false, false, 16, new Register[]{}),
        LXI("LXI", 0x01, 3, true, false, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.SP}),
        MOV("MOV", 0x40, 1, true, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        MVI("MVI", 0x06, 2, true, false, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        RAL("RAL", 0x17, 1, false, false, 8, new Register[]{}),
        RAR("RAR", 0x1F, 1, false, false, 8, new Register[]{}),
        RLC("RLC", 0x07, 1, false, false, 8, new Register[]{}),
        RRC("RRC", 0x0F, 1, false, false, 8, new Register[]{}),
        SPHL("SPHL", 0xF9, 1, false, false, 16, new Register[]{}),
        XCHG("XCHG", 0xEB, 1, false, false, 16, new Register[]{}),
        XTHL("XTHL", 0xE3, 1, false, false, 16, new Register[]{});

        public final String mnem;
        public final int opcode;
        public final int opcodeSize;
        public final boolean destAllowed;
        public final boolean srcAllowed;
        public final int align;
        public final Register[] allowedRegisters;

        private Mnemonic(String mnem, int opcode, int opcodeSize, boolean destAllowed, boolean srcAllowed, int align, Register[] allowedRegisters) {
            this.mnem = mnem;
            this.opcode = opcode;
            this.destAllowed = destAllowed;
            this.srcAllowed = srcAllowed;
            this.align = align;
            this.allowedRegisters = allowedRegisters;
            this.opcodeSize = opcodeSize;
        }

        public Opcode create(int data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            Opcode op = new Opcode();
            op.mnem = this;
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = data;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(Register dest, int data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " should have destination register");
            }
            if (!checkRegister(dest, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + dest.regMnem + " not allowed");
            }
            Opcode op = new Opcode();
            op.mnem = this;
            op.align = align;
            op.dstReg = dest;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = data;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(Register dest, String data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " should have destination register");
            }
            if (!checkRegister(dest, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + dest.regMnem + " not allowed");
            }
            Opcode op = new Opcode();
            op.mnem = this;
            op.align = align;
            op.dstReg = dest;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = data;
            op.opcodeSize = opcodeSize;
            op.resolve = true;
            return op;
        }

        public Opcode create() {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            Opcode op = new Opcode();
            op.mnem = this;
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(String sym) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            Opcode op = new Opcode();
            op.mnem = this;
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = sym;
            op.opcodeSize = opcodeSize;
            op.resolve = true;
            return op;
        }

        public Opcode create(Register oneReg) {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (!srcAllowed) {
                if (!destAllowed) {
                    throw new RuntimeException("Opcode " + mnem + " should have source or destination register");
                }
            }
            if (!checkRegister(oneReg, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + oneReg.regMnem + " not allowed");
            }
            Opcode op = new Opcode();
            op.mnem = this;
            op.align = align;
            op.srcReg = srcAllowed ? oneReg : null;
            op.dstReg = destAllowed ? oneReg : null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(Register dst, Register src) {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination register");
            }
            if (!srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have source register");
            }
            if (!checkRegister(src, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + src.regMnem + " not allowed");
            }
            if (!checkRegister(dst, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": destination register " + src.regMnem + " not allowed");
            }

            Opcode op = new Opcode();
            op.mnem = this;
            op.align = align;
            op.dstReg = dst;
            op.srcReg = src;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        private boolean checkRegister(Register r, Register[] allowed) {
            for (Register c : allowed) {
                if (r == c) {
                    return true;
                }
            }

            return false;
        }
    }
}
