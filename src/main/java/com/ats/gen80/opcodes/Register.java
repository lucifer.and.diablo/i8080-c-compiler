/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.opcodes;

/**
 *
 * @author lucifer
 */
public enum Register {

    A("A", 8, 7 << 3, 7),
    B("B", 8, 0 << 3, 0),
    C("C", 8, 1 << 3, 1),
    D("D", 8, 2 << 3, 2),
    E("E", 8, 3 << 3, 3),
    H("H", 8, 4 << 3, 4),
    L("L", 8, 5 << 3, 5),
    M("M", 8, 6 << 3, 6),
    PSW("PSW", 16, 48, 48),
    BC("B", 16, 0, 0),
    DE("D", 16, 16, 16),
    HL("H", 16, 32, 32),
    SP("SP", 16, 48, 48);

    public final String regMnem;
    public final int src;
    public final int dst;
    public final int bits;

    private Register(String regMnem, int bits, int dst, int src) {
        this.src = src;
        this.dst = dst;
        this.bits = bits;
        this.regMnem = regMnem;
    }

    public static Register value(String mnem) {
        Register ret = null;

        for (Register reg : values()) {
            if (reg.toString().equals(mnem)) {
                ret = reg;
            }
        }

        return ret;
    }
}
