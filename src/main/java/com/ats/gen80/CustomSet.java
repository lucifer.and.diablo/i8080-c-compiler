/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

/**
 *
 * @author lucifer
 */
public class CustomSet<T> {

    public T v;
    private CustomSet next;

    public static <T> CustomSet<T> setAdd(CustomSet<T> s, T v) {
        CustomSet<T> r = new CustomSet<>();
        r.next = s;
        r.v = v;
        return r;
    }

    public static <T> boolean setHas(CustomSet<T> s, T v) {
        for (; s != null; s = s.next) {
            if (s.v.equals(v)) {
                return true;
            }
        }

        return false;
    }

    public static <T> CustomSet<T> setUnion(CustomSet<T> a, CustomSet<T> b) {
        CustomSet<T> r = b;
        for (; a != null; a = a.next) {
            if (setHas(b, a.v)) {
                r = setAdd(r, a.v);
            }
        }
        return r;
    }

    public static <T> CustomSet<T> setIntersection(CustomSet<T> a, CustomSet<T> b) {
        CustomSet<T> r = null;
        for (; a != null; a = a.next) {
            if (setHas(b, a.v)) {
                r = setAdd(r, a.v);
            }
        }
        return r;
    }
}
