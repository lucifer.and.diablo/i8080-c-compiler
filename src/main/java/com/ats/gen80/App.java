/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.Node;
import com.ats.gen80.node.Token;
import com.ats.gen80.node.TokenEnum;
import com.ats.gen80.opcodes.Opcode;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lucifer
 */
public class App {

    public static void main(String[] args) throws FileNotFoundException, IOException {
//        System.out.println(String.format("%08x", Float.floatToIntBits(1)));
//        System.out.println(String.format("%08x", Float.floatToRawIntBits(10)));
//        System.out.println(String.format("%08x", Float.floatToRawIntBits(0.0000005f)));
//        if (true) {
//            return;
//        }
        Preprocessor prep = new Preprocessor();
//        prep.includeFile(new File("../diskeditor/main.c"), "../diskeditor/main.c");

//        prep.stdIncludePath.add("../diskeditor/");
//        prep.lexInitFile(new File("../diskeditor/main.c"));
//        prep.lexInitFile(new File("dt.c"));
        prep.lexInitFile(new File("./diskedit/main.c"));
//        prep.lexInitFile(new File("opt.c"));
        String path = "diskedit/";
        prep.stdIncludePath.add(path);
//        prep.lexInitFile(new File(path + "main.c"));

        List<Node> lev = prep.readTopLevels();

        StringBuilder sb = new StringBuilder();

        List<Opcode> code = new ArrayList<>();
        List<Opcode> data = new ArrayList<>();
        List<Opcode> bss = new ArrayList<>();
        List<Opcode> core = new ArrayList<>();

        for (Node n : lev) {
            GenEM g = new GenEM(prep);
//            g.emitNode(AstOptimizer.optimize(n));
            g.emitNode(n);

            Optimizer optimizer = new Optimizer(g.code.opcodes);

            code.addAll(optimizer.optimize(true));
            data.addAll(g.data.opcodes);
            List<Integer> idxs = new ArrayList<>(g.dataList.keySet());
            Collections.sort(idxs);

            for (Integer i : idxs) {
                if (i == 0) {
                    continue;
                }
                if (g.dataList.containsKey(i)) {
                    data.addAll(g.dataList.get(i).opcodes);
                }
            }

            bss.addAll(g.bss.opcodes);

            for (String global : g.globals) {
                sb.append(".GLOBAL ").append(global).append("\r\n");
            }
        }

        core.add(new Opcode.Label(".code"));
        core.addAll(Gen80Misc.misc.opcodes);
        core.addAll(code);
        core.add(new Opcode.Label(".data"));
        core.addAll(data);
        core.add(new Opcode.Label("codeMapping"));
        core.add(new Opcode.Word(".code"));
        core.add(new Opcode.Word(".data"));
        core.add(new Opcode.Word(".bss"));
        core.add(new Opcode.Word(".end"));
        core.add(new Opcode.Label(".bss"));
        core.addAll(bss);
        core.add(new Opcode.Label(".end"));

        int mem[] = new int[65535];

        int start = 0x100;

        Map<String, Integer> addresses = new HashMap<>();
        List<Opcode> toResolve = new ArrayList<>();

        for (Opcode o : core) {

            if (o instanceof Opcode.Label) {
                addresses.put(((Opcode.Label) o).label.toUpperCase(), start);
                continue;
            }
            if (o.resolve) {
                toResolve.add(o);
            }
            byte[] codes = o.getData();
            o.addr = start;
            for (int i = 0; i < codes.length; i++) {
                mem[start + i] = codes[i];
            }
            start += codes.length;

        }

        for (Opcode o : toResolve) {
            if (o instanceof Opcode.Word) {
                if (((Opcode.Word) o).getSdata() == null) {
                    continue;
                }
                Integer i = resolve(addresses, ((Opcode.Word) o).getSdata().toUpperCase());
                if (i == null) {
                    System.out.println("Cannot resolve: " + String.format("0x%x -> %s", o.addr, ((Opcode.Word) o).getSdata()));
                    continue;
                }
                Opcode.Word n = new Opcode.Word(i);
                byte[] d = n.getData();
                for (int v = 0; v < d.length; v++) {
                    mem[v + ((Opcode.Word) o).addr] = d[v];
                }
                n.sym = o.sym;
                n.addr = o.addr;
//                core.set(core.indexOf(o), n);
                continue;
            }

            String label = o.sym;
            Integer i = resolve(addresses, label.toUpperCase());
            if (i == null) {
                if (label.startsWith("$")) {
                    Integer sub = 0;
                    if (label.length() > 1) {
                        sub = Integer.parseInt(label.substring(1));
                    }
                    i = o.addr + sub;
                } else {
                    System.out.println("Cannot resolve: " + String.format("0x%x -> %s", o.addr, label));
                    continue;
                }

            }

            Opcode c = null;
            if (o.dstReg != null) {
                c = Opcode.Mnemonic.valueOf(o.mnemonic).create(o.dstReg, i);

            } else {
                c = Opcode.Mnemonic.valueOf(o.mnemonic).create(i);
            }

            byte[] d = c.getData();
            for (int v = 0; v < d.length; v++) {
                mem[v + o.addr] = d[v];
            }
            c.sym = o.sym;
            c.addr = o.addr;
            core.set(core.indexOf(o), c);
        }

        for (Opcode o : core) {
//            if (o instanceof Opcode.Label) {
//                continue;
//            }
            o.fillCode(sb);
        }

        FileOutputStream fs = new FileOutputStream("source.asm");
        fs.write(sb.toString().getBytes());
        fs.close();

        int size = addresses.get(".BSS") - addresses.get(".CODE");
        byte[] buf = new byte[size];

        int t = 0;
        for (int i = addresses.get(".CODE"); i < addresses.get(".BSS"); i++) {
            buf[t++] = (byte) mem[i];
        }
        fs = new FileOutputStream("K:/Java/kdi/data/test2.com");
//        fs = new FileOutputStream("../kdi/data/test2.com");
        fs.write(buf);
        fs.close();

        System.out.println("Code size: " + (addresses.get(".DATA") - addresses.get(".CODE")));
        System.out.println("Data size: " + (addresses.get(".BSS") - addresses.get(".DATA")));
        System.out.println("Bss size:  " + (addresses.get(".END") - addresses.get(".BSS")));
        System.out.println("Size:      " + (addresses.get(".END") - addresses.get(".CODE")));

//        System.out.println(sb.toString().replace(".", ""));
//        System.out.println(addresses);
//        System.out.println(toResolve);
//        int lim = 0;
//        for (int i = addresses.get(".code"); i <= addresses.get(".end"); i++) {
//            if (lim == 0) {
//                System.out.print("DB ");
//            }
//            System.out.print(String.format("0%xH", (mem[i] < 0 ? mem[i] + 256 : mem[i])));
//            lim++;
//            if (lim < 16) {
//                System.out.print(", ");
//            } else {
//                System.out.println("");
//                lim = 0;
//            }
//        }
//        System.out.println("");
    }

    private static Integer resolve(Map<String, Integer> addrs, String label) {
        label = label.trim();
        if (addrs.containsKey(label)) {
            return addrs.get(label);
        }

        Lexer lex = new Lexer();
        lex.lexInitString(label);

        Token tok = null;

        int result = 0;

        while ((tok = lex.lexAsm()) != Token.EOF_TOKEN) {
            if (tok == Token.NEWLINE_TOKEN) {
                continue;
            }
            if (tok.kind == TokenEnum.TIDENT) {
                Integer f = addrs.get(tok.sval);
                if (f == null) {
                    return null;
                }

                result = f;
            }
            while ((tok = lex.lexAsm()) != Token.EOF_TOKEN) {
                if (tok == Token.NEWLINE_TOKEN) {
                    continue;
                }
                if (tok.kind != TokenEnum.TKEYWORD) {
                    System.out.println("Excpected keyword " + label);
                    return null;
                }

                Token data = lex.lexAsm();

                if (data.kind != TokenEnum.TNUMBER) {
                    System.out.println("Excpected integer" + label);
                    return null;
                }

                switch (tok.id) {
                    case C_PLUS:
                        result += Integer.parseInt(data.sval);
                        break;
                    case C_MINUS:
                        result -= Integer.parseInt(data.sval);
                        break;
                    default:
                        System.out.println("Error");
                        return null;

                }

            }
            break;
        }

        return result;
    }

}
