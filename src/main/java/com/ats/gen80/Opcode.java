/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

/**
 *
 * @author lucifer
 */
public class Opcode {

    protected Register dstReg;
    protected Register srcReg;
    protected int align;
    protected int opcode;
    protected String sym;
    protected int data;
    protected String mnemonic;
    protected int opcodeSize;
    protected boolean resolve = false;

    public int alignDestReg(int op, Register reg) {
        return op | (reg.dst << 3);
    }

    public int alignDestSrcReg(int op, Register dst, Register src) {
        return op | (dst.dst << 3) | src.src;
    }

    public boolean is8Bit(Register reg) {
        return reg.bits == 8;
    }

    public boolean is16Bit(Register reg) {
        return reg.bits == 16;
    }

    public int alignData(int data) {
        return data & (align == 8 ? 0xFF : 0xFFFF);
    }

    public void fillCode(StringBuilder sb) {
        if (mnemonic == null) {
            return;
        }
        sb.append("\t");
        sb.append(String.format("%-8s", mnemonic));

        if (dstReg != null) {
            sb.append(dstReg.regMnem);
            if (opcodeSize > 1) {
                sb.append(",");
            }
        }

        if (srcReg != null) {
            sb.append(srcReg.regMnem);
        } else if (opcodeSize > 1) {
            if (sym != null) {
                sb.append(sym);
            } else {
                sb.append(toHex(alignData(data)));
            }
        }

        sb.append("\n");

    }

    public String toHex(int data) {
        String ret = String.format("%0" + (opcodeSize == 2 ? "2" : "4") + "xH", data).toUpperCase();
        if (ret.charAt(0) >= 'A' && ret.charAt(0) <= 'F') {
            ret = "0" + ret;
        }

        return ret;
    }

    public static enum Mnemonic {

        ADI("ADI", 0xC6, 2, false, false, 8, new Register[]{}),
        ACI("ACI", 0xCE, 2, false, false, 8, new Register[]{}),
        ANI("ANI", 0xE6, 2, false, false, 8, new Register[]{}),
        XRI("XRI", 0xEE, 2, false, false, 8, new Register[]{}),
        ORI("ORI", 0xF6, 2, false, false, 8, new Register[]{}),
        CPI("CPI", 0xFE, 2, false, false, 8, new Register[]{}),
        CMA("CMA", 0x2F, 1, false, false, 8, new Register[]{}),
        CMC("CMC", 0x3F, 1, false, false, 8, new Register[]{}),
        DAA("DAA", 0x27, 1, false, false, 8, new Register[]{}),
        DCR("DCR", 0x05, 1, true, false, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        INR("INR", 0x04, 1, true, false, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        DAD("DAD", 0x09, 1, false, false, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.SP}),
        LDAX("LDAX", 0x0A, 3, false, true, 16, new Register[]{Register.BC, Register.DE}),
        STAX("STAX", 0x02, 3, false, true, 16, new Register[]{Register.BC, Register.DE}),
        LHLD("LHLD", 0x2A, 3, false, false, 16, new Register[]{}),
        SHLD("SHLD", 0x22, 3, false, false, 16, new Register[]{}),
        INX("INX", 0x03, 1, false, true, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.SP}),
        DCX("DCX", 0x0B, 1, false, true, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.SP}),
        PUSH("PUSH", 0xC5, 1, false, true, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.PSW}),
        POP("POP", 0xC1, 1, false, true, 16, new Register[]{Register.BC, Register.DE, Register.HL, Register.PSW}),
        DI("DI", 0xF3, 1, false, false, 8, new Register[]{}),
        EI("EI", 0xE3, 1, false, false, 8, new Register[]{}),
        HLT("HLT", 0x76, 1, false, false, 8, new Register[]{}),
        PCHL("PCHL", 0xE9, 1, false, false, 8, new Register[]{}),
        IN("IN", 0xDB, 2, false, false, 8, new Register[]{}),
        OUT("OUT", 0xD3, 2, false, false, 8, new Register[]{}),
        ADD("ADD", 0x80, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        ADC("ADC", 0x88, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        CMP("CMP", 0xB8, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        ANA("ANA", 0xA0, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        ORA("ORA", 0xB0, 1, false, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        CALL("CALL", 0xCD, 3, false, false, 16, new Register[]{}),
        JMP("JMP", 0xC3, 3, false, false, 16, new Register[]{}),
        CNZ("CNC", 0xC4, 3, false, false, 16, new Register[]{}),
        CZ("CZ", 0xCC, 3, false, false, 16, new Register[]{}),
        CNC("CNC", 0xD4, 3, false, false, 16, new Register[]{}),
        CC("CC", 0xDC, 3, false, false, 16, new Register[]{}),
        CPO("CPO", 0xE4, 3, false, false, 16, new Register[]{}),
        CPE("CPE", 0xEC, 3, false, false, 16, new Register[]{}),
        CP("CP", 0xF4, 3, false, false, 16, new Register[]{}),
        CM("CM", 0xFC, 3, false, false, 16, new Register[]{}),
        JNZ("JNC", 0xC2, 3, false, false, 16, new Register[]{}),
        JZ("JZ", 0xCA, 3, false, false, 16, new Register[]{}),
        JNC("JNC", 0xD2, 3, false, false, 16, new Register[]{}),
        JC("JC", 0xDA, 3, false, false, 16, new Register[]{}),
        JPO("JPO", 0xE2, 3, false, false, 16, new Register[]{}),
        JPE("JPE", 0xEA, 3, false, false, 16, new Register[]{}),
        JP("JP", 0xF2, 3, false, false, 16, new Register[]{}),
        JM("JM", 0xFC, 3, false, false, 16, new Register[]{}),
        LDA("LDA", 0x3A, 3, false, false, 16, new Register[]{}),
        LXI("LXI", 0x01, 3, true, false, 16, new Register[]{Register.BC, Register.DE, Register.HL}),
        MOV("MOV", 0x40, 1, true, true, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M}),
        MVI("MVI", 0x06, 2, true, false, 8, new Register[]{Register.A, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.M});

        public final String mnem;
        public final int opcode;
        public final int opcodeSize;
        public final boolean destAllowed;
        public final boolean srcAllowed;
        public final int align;
        public final Register[] allowedRegisters;

        private Mnemonic(String mnem, int opcode, int opcodeSize, boolean destAllowed, boolean srcAllowed, int align, Register[] allowedRegisters) {
            this.mnem = mnem;
            this.opcode = opcode;
            this.destAllowed = destAllowed;
            this.srcAllowed = srcAllowed;
            this.align = align;
            this.allowedRegisters = allowedRegisters;
            this.opcodeSize = opcodeSize;
        }

        public Opcode create(int data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            Opcode op = new Opcode();
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = data;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(Register dest, int data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " should have destination register");
            }
            if (!checkRegister(dest, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + dest.regMnem + " not allowed");
            }
            Opcode op = new Opcode();
            op.align = align;
            op.dstReg = dest;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = data;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(Register dest, String data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " should have destination register");
            }
            if (!checkRegister(dest, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + dest.regMnem + " not allowed");
            }
            Opcode op = new Opcode();
            op.align = align;
            op.dstReg = dest;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = data;
            op.opcodeSize = opcodeSize;
            op.resolve = true;
            return op;
        }

        public Opcode create() {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            Opcode op = new Opcode();
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(String sym) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            Opcode op = new Opcode();
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = sym;
            op.opcodeSize = opcodeSize;
            op.resolve = true;
            return op;
        }

        public Opcode create(Register oneReg) {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (!srcAllowed) {
                if (!destAllowed) {
                    throw new RuntimeException("Opcode " + mnem + " should have source or destination register");
                }
            }
            if (!checkRegister(oneReg, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + oneReg.regMnem + " not allowed");
            }
            Opcode op = new Opcode();
            op.align = align;
            op.dstReg = srcAllowed ? oneReg : null;
            op.srcReg = destAllowed ? oneReg : null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public Opcode create(Register dst, Register src) {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination register");
            }
            if (!srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have source register");
            }
            if (!checkRegister(src, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + src.regMnem + " not allowed");
            }
            if (!checkRegister(dst, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": destination register " + src.regMnem + " not allowed");
            }

            Opcode op = new Opcode();
            op.align = align;
            op.dstReg = dst;
            op.srcReg = src;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            return op;
        }

        private boolean checkRegister(Register r, Register[] allowed) {
            for (Register c : allowed) {
                if (r == c) {
                    return true;
                }
            }

            return false;
        }
    }
}
