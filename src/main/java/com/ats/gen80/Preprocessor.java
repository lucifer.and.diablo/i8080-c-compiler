/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.Case;
import com.ats.gen80.node.DomesticType;
import com.ats.gen80.node.DataType;
import com.ats.gen80.node.Decl;
import com.ats.gen80.node.EncodingEnum;
import com.ats.gen80.node.Keyword;
import com.ats.gen80.node.Node;
import com.ats.gen80.node.Node.SourceLocation;
import com.ats.gen80.node.NodeBinary;
import com.ats.gen80.node.NodeCompoundStatement;
import com.ats.gen80.node.NodeDeclaration;
import com.ats.gen80.node.NodeFloat;
import com.ats.gen80.node.NodeFunctionPointer;
import com.ats.gen80.node.NodeGoto;
import com.ats.gen80.node.NodeIf;
import com.ats.gen80.node.NodeInitializer;
import com.ats.gen80.node.NodeInteger;
import com.ats.gen80.node.NodeLabel;
import com.ats.gen80.node.NodeLocalLabel;
import com.ats.gen80.node.NodeStruct;
import com.ats.gen80.node.NodeUnary;
import com.ats.gen80.node.Pair;
import com.ats.gen80.node.Token;
import com.ats.gen80.node.TokenEnum;
import com.ats.gen80.node.Type;
import com.ats.gen80.node.TypeKind;
import com.ats.gen80.preprocess.ConditionInclude;
import com.ats.gen80.preprocess.ConditionIncludeContext;
import com.ats.gen80.preprocess.Macro;
import com.ats.gen80.preprocess.MacroType;
import java.io.File;
import static java.lang.Character.isDigit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author lucifer
 */
public class Preprocessor extends Lexer {

    private static final long INT_MAX = 0x7fff;
    private static final long UINT_MAX = 0xffff;
    private static final long LONG_MAX = 0x7fffffff;

    protected final Map<String, Macro> macros = new LinearMap<>();
    protected final Map<String, Keyword> keywords = new LinearMap<>();
    protected final Map<String, String> includeGuard = new LinearMap<>();
    protected final Map<String, Type> tags = new LinearMap<>();
    protected final List<ConditionInclude> condInclStack = new ArrayList<>();
    protected SourceLocation sourceLocation;
    protected final Map<String, Boolean> once = new LinearMap<>();
    protected final List<String> stdIncludePath = new ArrayList<>();
    protected String lcontinue;
    protected String lbreak;
    protected List<Case> cases;
    protected String defaultcase;

    protected Type currentFuncType;

    public Preprocessor() {
        for (Keyword key : Keyword.values()) {
            if ((key.toString().startsWith("OP_") && key.keyword != null) || key.toString().startsWith("K")) {
                keywords.put(key.keyword, key);
            }
        }
    }

    public boolean isFuncDef() {
        List<Token> buf = new ArrayList<>();
        boolean r = false;
        while (true) {
            Token tok = getToken();
            buf.add(tok);
            if (tok.kind == TokenEnum.TEOF) {
                error("permature end of input");
            }

            if (isKeyword(tok, Keyword.C_SEMICOLON)) {
                break;
            }
            if (isType(tok)) {
                continue;
            }
            if (isKeyword(tok, Keyword.C_LPAREN)) {
                skipParentheses(buf);
                continue;
            }
            if (tok.kind != TokenEnum.TIDENT) {
                continue;
            }
            if (!isKeyword(peekToken(), Keyword.C_LPAREN)) {
                continue;
            }
            buf.add(getToken());
            skipParentheses(buf);
            r = (isKeyword(peekToken(), Keyword.C_LBRACE) || isType(peekToken()));
            break;
        }
        while (buf.size() > 0) {
            ungetToken(buf.remove(buf.size() - 1));
        }

        return r;
    }

    public void skipParentheses(List<Token> buf) {
        while (true) {
            Token tok = getToken();
            if (tok.kind == TokenEnum.TEOF) {
                error("permature end of file");
            }

            buf.add(tok);
            if (isKeyword(tok, Keyword.C_RPAREN)) {
                return;
            }
            if (isKeyword(tok, Keyword.C_RPAREN)) {
                skipParentheses(buf);
            }
        }
    }

    public Node readFuncBody(Type funcType, DataRef<String> fname, List<Node> params) {
        Globals.localEnv = new LinearMap<>(Globals.localEnv);
        Globals.localVars = new LinearMap<>();
        currentFuncType = funcType;

        Node funcName = Node.astString(EncodingEnum.ENC_NONE, fname.getData(), fname.getData().length());
        Globals.localEnv.put("__func__", funcName);
        Globals.localEnv.put("__FUNCTION__", funcName);

        Node body = readCompoundStmt();
        Node r = Node.astFunc(funcType, fname.getData(), params, body, Globals.localVars);

        currentFuncType = null;
        Globals.localEnv = null;
        Globals.localVars = null;

        return r;
    }

    public void backFillLabels() {
        for (int i = 0; i < Globals.gotos.size(); i++) {
            Node src = Globals.gotos.get(i);
            String label = src.cast(NodeGoto.class).label;
            Node dst = Globals.labels.get(label);
            if (dst == null) {
                error("stray %s: %s", src.kind == Keyword.AST_GOTO ? "goto" : "unary &", label);
            }

            if (dst.cast(NodeLabel.class).newLabel != null) {
                src.cast(NodeGoto.class).label = dst.cast(NodeLabel.class).newLabel;
            } else {
                src.cast(NodeGoto.class).label = dst.cast(NodeLabel.class).newLabel = Globals.makeLabel();
            }
        }
    }

    public void updateOldStyleParamType(List<Node> params, List<Node> vars) {
        for (int i = 0; i < vars.size(); i++) {
            Node decl = vars.get(i);
            Node var = decl.cast(NodeDeclaration.class).declarationVariable;
            boolean found = false;
            for (int j = 0; j < params.size(); j++) {
                Node param = params.get(j);
                if (param.cast(NodeLocalLabel.class).variableName.equals(var.cast(NodeLocalLabel.class).variableName)) {
                    continue;
                }
                param.type = var.type;
                found = true;
            }
            if (!found) {
                error("missing parameter: %s", var.cast(NodeLocalLabel.class).variableName);
            }
        }
    }

    public List<Node> readOldStyleParamArgs() {
        Map<String, Node> orig = Globals.localEnv;
        Globals.localEnv = null;
        List<Node> r = new ArrayList<>();
        while (true) {
            if (isKeyword(peekToken(), Keyword.C_LBRACE)) {
                break;
            }

            if (!isType(peekToken())) {
                error(peekToken(), "K&R style declarator expected, but got %s", peekToken());
            }

            readDecl(r, false);
        }

        Globals.localEnv = orig;
        return r;
    }

    public List<Type> paramTypes(List<Node> params) {
        List<Type> r = new ArrayList<>();
        for (int i = 0; i < params.size(); i++) {
            Node param = params.get(i);
            r.add(param.type);
        }

        return r;
    }

    public void readOldStyleParamType(List<Node> params) {
        List<Node> vars = readOldStyleParamArgs();
        updateOldStyleParamType(params, vars);
    }

    public Node readFuncDef() {
        DataRef<DomesticType> sclass = new DataRef<>(null);
        Type baseType = readDeclSpecOpt(sclass);
        Globals.localEnv = new LinearMap<>(Globals.globalEnv);
        Globals.gotos = new ArrayList<>();
        Globals.labels = new LinearMap<>();
        DataRef<String> name = new DataRef<>(null);
        List<Node> params = new ArrayList<>();
        Type funcType = readDeclarator(name, baseType, params, Decl.DECL_BODY);
        if (funcType.isOldStyle) {
            if (params.isEmpty()) {
                funcType.hasVarArg = false;
            }
            readOldStyleParamType(params);
            funcType.params = paramTypes(params);
        }

        funcType.isStatic = (sclass.getData() == DomesticType.S_STATIC);
        Node.astGVar(funcType, name.getData());
        expect(Keyword.C_LBRACE);

        Node r = readFuncBody(funcType, name, params);
        backFillLabels();
        Globals.localEnv = null;
        return r;
    }

    public List<Node> readTopLevels() {
        List<Node> topLevels = new ArrayList<>();
        while (true) {
            if (peekToken().kind == TokenEnum.TEOF) {
                topLevels.addAll(Globals.topLevels);
                Globals.topLevels.clear();
                return topLevels;
            }
            if (isFuncDef()) {
                topLevels.add(readFuncDef());
            } else {
                readDecl(topLevels, true);
            }
        }

    }

    public Token readToken() {
        Token tok;
        while (true) {
            tok = readExpand();
            if (tok.bol && isKeyword(tok, Keyword.C_HASH)) {
                readDirective(tok);
                continue;
            }

            return maybeConvertKeyword(tok);
        }
    }

    public Token maybeConvertKeyword(Token tok) {
        if (tok.kind != TokenEnum.TIDENT) {
            return tok;
        }
        Keyword id = keywords.get(tok.sval);
        if (id == null || id.toString().startsWith("C_")) {
            return tok;
        }

        Token r = tok.copy();
        r.kind = TokenEnum.TKEYWORD;
        r.id = id;

        return r;
    }

    public Token readExpand() {
        while (true) {
            Token tok = readExpandNewline();
            if (tok.kind != TokenEnum.TNEWLINE) {
                return tok;
            }
        }
    }

    public Token readExpandNewline() {
        Token tok = lex();
        if (tok.kind != TokenEnum.TIDENT) {
            return tok;
        }

        String name = tok.sval;
        Macro macro = macros.get(name);
        if (macro == null || CustomSet.setHas(tok.hideset, name)) {
            return tok;
        }

        switch (macro.kind) {
            case MACRO_OBJ: {
                CustomSet<String> hideset = CustomSet.setAdd(tok.hideset, name);
                List<Token> tokens = subst(macro, null, hideset);
                propagateSpace(tokens, tok);
                ungetAll(tokens);
                return readExpand();
            }
            case MACRO_FUNC: {
                if (!next('(')) {
                    return tok;
                }
                List<List<Token>> args = readArgs(tok, macro);
                Token rparen = peekToken();
                expect(Keyword.C_RPAREN);
                CustomSet<String> hideset = CustomSet.setAdd(CustomSet.setIntersection(tok.hideset, rparen.hideset), name);
                List<Token> tokens = subst(macro, args, hideset);
                propagateSpace(tokens, tok);
                ungetAll(tokens);
                return readExpand();
            }
            case MACRO_SPECIAL:
                macro.fn.handle(this, tok);
                return readExpand();
            default:
                error("internal error");
        }

        return null;

    }

    public void ungetAll(List<Token> tokens) {
        for (int i = tokens.size() - 1; i >= 0; i--) {
            ungetToken(tokens.get(i));
        }
    }

    public List<Token> subst(Macro macro, List<List<Token>> args, CustomSet<String> hideset) {
        List<Token> r = new ArrayList<>();
        int len = macro.body.size();
        for (int i = 0; i < len; i++) {
            Token t0 = macro.body.get(i);
            Token t1 = (i == len - 1) ? null : macro.body.get(i + 1);
            boolean t0param = (t0.kind == TokenEnum.TMACRO_PARAM);
            boolean t1param = (t1 != null && t1.kind == TokenEnum.TMACRO_PARAM);

            if (isKeyword(t0, Keyword.C_HASH) && t1param) {
                List<Token> arg = args.get(t1.position);
                if (t1.isVararg && r.size() > 0 && isKeyword(r.get(r.size() - 1), Keyword.C_COMMA)) {
                    if (arg.size() > 0) {
                        r.addAll(arg);
                    } else {
                        r.remove(r.size() - 1);
                    }
                } else if (arg.size() > 0) {
                    gluePush(r, arg.get(0));
                    for (int t = 1; t < arg.size(); t++) {
                        r.add(arg.get(t));
                    }
                }
                i++;
                continue;
            }

            if (isKeyword(t0, Keyword.KHASHHASH) && t1 != null) {
                hideset = t1.hideset;
                gluePush(r, t1);
                i++;
                continue;
            }

            if (t0param) {
                List<Token> arg = args.get(t0.position);
                r.addAll(expandAll(arg, t0));
                continue;
            }

            r.add(t0);
        }
        return addHideSet(r, hideset);
    }

    public List<Token> addHideSet(List<Token> tokens, CustomSet<String> hideset) {
        List<Token> r = new ArrayList<>();

        for (Token token : tokens) {
            Token t = token.copy();
            t.hideset = CustomSet.setUnion(t.hideset, hideset);
            r.add(t);
        }

        return r;
    }

    public Token glueTokens(Token t, Token u) {
        String s = t.toString() + u.toString();
        return lexString(s);
    }

    public void gluePush(List<Token> tokens, Token tok) {
        Token last = tokens.get(tokens.size() - 1);
        tokens.add(glueTokens(last, tok));
    }

    public List<Token> expandAll(List<Token> tokens, Token tmpl) {
        tokenBuffersStash(reverse(tokens));
        List<Token> r = new ArrayList<>();

        while (true) {
            Token tok = readExpand();
            if (tok.kind == TokenEnum.TEOF) {
                break;
            }

            r.add(tok);
        }
        propagateSpace(r, tmpl);
        tokenBufferUnstash();
        return r;
    }

    public List<Token> reverse(List<Token> tokens) {
        List<Token> r = new ArrayList<>();
        for (int i = 0; i < tokens.size(); i++) {
            r.add(tokens.get(tokens.size() - 1 - i));
        }

        return r;
    }

    public void propagateSpace(List<Token> tokens, Token tmpl) {
        if (tokens.isEmpty()) {
            return;
        }

        Token tok = tokens.get(0).copy();
        tok.space = tmpl.space;
        tokens.set(0, tok);

    }

    public Token peekToken() {
        Token r = readToken();
        ungetToken(r);
        return r;
    }

    public List<List<Token>> readArgs(Token tok, Macro macro) {
        if (macro.nargs == 0 && isKeyword(tok, Keyword.C_RPAREN)) {
            return new ArrayList<>();
        }

        List<List<Token>> args = doReadArgs(tok, macro);
        if (args.size() != macro.nargs) {
            error(tok, "macro argument number does not match");
        }

        return args;
    }

    public List<List<Token>> doReadArgs(Token indent, Macro macro) {
        List<List<Token>> r = new ArrayList<>();
        DataRef<Boolean> end = new DataRef<>(false);
        while (!end.getData()) {
            boolean inEllipsis = (macro.isVarg && r.size() + 1 == macro.nargs);
            r.add(readOneArg(indent, end, inEllipsis));
        }

        if (macro.isVarg && r.size() == macro.nargs - 1) {
            r.add(new ArrayList<Token>());
        }

        return r;
    }

    public List<Token> readOneArg(Token ident, DataRef<Boolean> end, boolean readAll) {
        List<Token> r = new ArrayList<>();

        int level = 0;

        while (true) {
            Token tok = lex();
            if (tok.kind == TokenEnum.TEOF) {
                error(ident, "unterminated macro argument list");
            }

            if (tok.kind == TokenEnum.TNEWLINE) {
                continue;
            }
            if (tok.bol && isKeyword(tok, Keyword.C_HASH)) {
                readDirective(tok);
                continue;
            }

            if (level == 0 && isKeyword(tok, Keyword.C_RPAREN)) {
                ungetToken(tok);
                end.setData(true);
                return r;
            }

            if (level == 0 && isKeyword(tok, Keyword.C_COMMA) && !readAll) {
                return r;
            }

            if (isKeyword(tok, Keyword.C_LPAREN)) {
                level++;
            }
            if (isKeyword(tok, Keyword.C_RPAREN)) {
                level--;
            }

            if (tok.bol) {
                tok = tok.copy();
                tok.bol = false;
                tok.space = true;
            }

            r.add(tok);
        }
    }

    public int evalStructRef(Node node, int offset) {
        if (node.kind == Keyword.AST_STRUCT_REF) {
            return evalStructRef(node.cast(NodeStruct.class).struct, offset + node.type.offset);
        }

        return (int) (evalIntExpr(node, null) + offset);
    }

    public void expectNewLine() {
        Token tok = lex();
        if (tok.kind != TokenEnum.TNEWLINE) {
            error(tok, "newline expected but got %s", tok.toString());
        }
    }

    public void readElse(Token hash) {
        if (condInclStack.isEmpty()) {
            error(hash, "stray #else");
        }

        ConditionInclude ci = condInclStack.get(condInclStack.size() - 1);
        if (ci.context == ConditionIncludeContext.IN_ELSE) {
            error(hash, "#else appear in #else");
        }
        expectNewLine();
        ci.context = ConditionIncludeContext.IN_ELSE;
        ci.includeGuard = null;
        if (ci.wasTrue) {
            skipCondIncl();
        }
    }

    public void skipCondIncl() {
        int nest = 0;
        while (true) {
            boolean bol = currentFile().column == 1;
            skipSpace();
            int c = read();
            if (c == -1) {
                return;
            }
            if (c == '\'') {
                skipChar();
                continue;
            }
            if (c == '"') {
                skipString();
                continue;
            }
            if (c != '#' || !bol) {
                continue;
            }
            int column = currentFile().column - 1;
            Token tok = lex();
            if (tok.kind != TokenEnum.TIDENT) {
                continue;
            }
            if (nest == 0 && (isIdent(tok, "else") || isIdent(tok, "elif") || isIdent(tok, "endif"))) {
                ungetToken(tok);
                Token hash = makeKeyword(Keyword.C_HASH);
                hash.bol = true;
                hash.column = column;
                ungetToken(hash);
                return;
            }

            if (isIdent(tok, "if") || isIdent(tok, "ifdef") || isIdent(tok, "ifndef")) {
                nest++;
            } else if (nest != 0 && isIdent(tok, "endif")) {
                nest--;
            }
            skipLine();

        }

    }

    public void readEndif(Token hash) {
        if (condInclStack.isEmpty()) {
            error(hash, "stary #endif");
        }

        ConditionInclude ci = condInclStack.remove(condInclStack.size() - 1);
        expectNewLine();

        if (ci.includeGuard != null || ci.file != hash.file) {
            return;
        }

        Token last = skipNewLines();
        if (ci.file != last.file) {
            includeGuard.put(ci.file.name, ci.includeGuard);
        }
    }

    public String readErrorMessage() {
        String b = "";
        while (true) {
            Token tok = lex();
            if (tok.kind == TokenEnum.TNEWLINE) {
                return b;
            }
            if (tok.space) {
                b += " ";
            }
            b += tok.toString();
        }
    }

    public void readError(Token hash) {
        error(hash, "#error: %s", readErrorMessage());
    }

    public void readWarning(Token hash) {
        warning(hash, "#warning: %s", readErrorMessage());
    }

    public void readIf() {
        doReadIf(readConstExpr());
    }

    public ConditionInclude makeCondIncl(boolean wasTrue) {
        ConditionInclude r = new ConditionInclude();
        r.context = ConditionIncludeContext.IN_THEN;
        r.wasTrue = wasTrue;

        return r;
    }

    public void doReadIf(boolean istrue) {
        condInclStack.add(makeCondIncl(istrue));
        if (!istrue) {
            skipCondIncl();
        }
    }

    public void readIfdef() {
        Token tok = lex();
        if (tok.kind != TokenEnum.TIDENT) {
            error(tok, "identifier expected, but got %s", tok.toString());
        }
        expectNewLine();
        doReadIf(macros.get(tok.sval) != null);
    }

    public String readCppHeaderName(Token hash, DataRef<Boolean> std) {
        String path = readHeaderFileName(std);
        if (path != null) {
            return path;
        }

        Token tok = readExpandNewline();
        if (tok.kind == TokenEnum.TNEWLINE) {
            error(hash, "expected filename, but got newline");
        }
        if (tok.kind == TokenEnum.TSTRING) {
            std.setData(false);
            return tok.sval;
        }
        if (!isKeyword(tok, Keyword.C_LT)) {
            error(tok, "< expected but got %s", tok.toString());
        }
        List<Token> tokens = new ArrayList<>();
        while (true) {

            if (tok.kind == TokenEnum.TNEWLINE) {
                error(hash, "permature end of header name");
            }
            if (isKeyword(tok, Keyword.C_GT)) {
                break;
            }
            tokens.add(tok);
        }
        std.setData(true);
        return joinPaths(tokens);
    }

    public String joinPaths(List<Token> args) {
        String b = "";
        for (Token arg : args) {
            b += arg.toString();
        }

        return b;
    }

    public boolean guarded(String path) {
        String guard = includeGuard.get(path);

        boolean r = (guard != null && macros.get(guard) != null);
        defineObjMacro("__include_guard", r ? Token.CPP_TOKEN_ONE : Token.CPP_TOKEN_ZERO);
        return r;
    }

    public boolean tryInclude(String dir, String filename, boolean isimport) {
        String path = fullpath(String.format("%s/%s", dir, filename));
//        if (path.startsWith("/")) {
//            path = path.substring(1); // WINDOWS
//        }
        if (once.get(path) != null) {
            return true;
        }

        if (guarded(path)) {
            return true;
        }

        File fp = new File(path);
        if (!fp.exists()) {
            return false;
        }
        if (isimport) {
            once.put(path, true);
        }
        streamPush(makeFile(fp, path));
        return true;

    }

    public void defineObjMacro(String name, Token value) {
        List<Token> tok = new ArrayList<>();
        tok.add(value);
        macros.put(name, makeObjMacro(tok));
    }

    public String fullpath(String path) {

        if (path.startsWith("/") || (path.charAt(1) == ':' || path.charAt(2) == ':')) {
            return clean(path);
        }

        String cwd = new File(".").getAbsolutePath().replace("\\", "/");

        path = clean(String.format("%s/%s", cwd, path));

        if (path.charAt(0) == '/' && path.charAt(2) == ':') {
            path.substring(1);
        }

        return path;
    }

    public String clean(String p) {
        String buf = "/";
        int i = 0;
        while (true) {
            if (p.charAt(i) == '/') {
                i++;
                continue;
            }

            if (p.substring(i).indexOf("./") == 0) {
                i += 2;
                continue;
            }
            if (p.substring(i).indexOf("../") == 0) {
                i += 3;
                if (i == 1) {
                    continue;
                }

                if (buf.lastIndexOf("/") == -1) {
                    buf = "/";
                } else {
                    buf = buf.substring(0, buf.substring(0, buf.length() - 1).lastIndexOf("/") + 1);
                }
                continue;
            }
            while (i < p.length() && p.charAt(i) != '/') {
                buf += p.charAt(i);
                i++;
            }
            if (i < p.length() && p.charAt(i) == '/') {

                buf += p.charAt(i);
                i++;
                continue;
            }
            return buf;
        }
    }

    public void readIncludeNext(Token hash, FileStruct file) {
        DataRef<Boolean> std = new DataRef<>(true);
        String filename = readCppHeaderName(hash, std);
        expectNewLine();
        if (filename.startsWith("/")) {
            if (tryInclude("/", filename, false)) {
                return;
            }
            error(hash, "cannot find header file: %s", filename);
        }

        String cur = fullpath(file.name);
        int i = stdIncludePath.size() - 1;
        for (; i >= 0; i--) {
            String dir = stdIncludePath.get(i);
            if (cur.equals(fullpath(String.format("%s/%s", dir, filename)))) {
                break;
            }
        }

        for (i--; i > 0; i--) {
            if (tryInclude(stdIncludePath.get(i), filename, false)) {
                return;
            }
        }
        error(hash, "cannot find header file: %s", filename);
    }

    public void readLine() {
        Token tok = readExpandNewline();
        if (tok.kind != TokenEnum.TNUMBER || isDigitSequence(tok.sval)) {
            error(tok, "number expected after #line, but got %s", tok.toString());
        }

        int line = Integer.parseInt(tok.sval);

        tok = readExpandNewline();
        String filename = null;
        if (tok.kind == TokenEnum.TSTRING) {
            filename = tok.sval;
            expectNewLine();
        } else if (tok.kind != TokenEnum.TNEWLINE) {
            error(tok, "newline or a source are expected, but got %s", tok.toString());
        }

        FileStruct f = currentFile();
        f.line = line;
        if (filename != null) {
            f.name = filename;
        }
    }

    public boolean isDigitSequence(String sval) {
        for (int i = 0; i < sval.length(); i++) {
            if (!isDigit(sval.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private void parsePragmaOperand(Token tok) {
        String s = tok.sval;
        if (null != s) {
            switch (s) {
                case "once":
                    String path = fullpath(tok.file.name);
                    once.put(path, Boolean.TRUE);
                    break;
//                case "enable_warning":
//                    enableWarning = true;
//                    break;
//                case "disable_warning":
//                    enableWarning = false;
//                    break;
                default:
                    error(tok, "unknown #pragma: %s", s);
                    break;
            }
        }
    }

    public void readPragma() {
        Token tok = readIdent();
        parsePragmaOperand(tok);
    }

    public void readIfndef() {
        Token tok = lex();
        if (tok.kind != TokenEnum.TIDENT) {
            error(tok, "identifier expected, but got %s", tok.toString());
        }

        expectNewLine();
        doReadIf(macros.get(tok.sval) == null);

        if (tok.count == 2) {
            ConditionInclude ci = condInclStack.get(condInclStack.size() - 1);
            ci.includeGuard = tok.sval;
            ci.file = tok.file;
        }
    }

    public void readInclude(Token hash, FileStruct file, boolean isimport) {
        DataRef<Boolean> std = new DataRef<>(false);
        String filename = readCppHeaderName(hash, std);
        expectNewLine();
        if (filename.startsWith("/")) {
            if (tryInclude("/", filename, isimport)) {
                return;
            }
            error(hash, "cannot find head file: %s", filename);
        }
        if (!std.getData()) {
            String dir = file.name != null ? dirname(file.name) : ".";

            if (tryInclude(dir, filename, isimport)) {
                return;
            }
        }
        for (int i = stdIncludePath.size() - 1; i >= 0; i--) {
            if (tryInclude(stdIncludePath.get(stdIncludePath.size() - 1), filename, isimport)) {
                return;
            }
        }
        error(hash, "cannot find head file: %s", filename);
    }

    public String dirname(String name) {
        String path = new File(name).getParent();
        if (path == null) {
            return ".";
        }

        return path.replace("\\", "/");
    }

    public void readDirective(Token hash) {
        Token tok = lex();
        if (tok.kind == TokenEnum.TNEWLINE) {
            return;
        }
        if (tok.kind == TokenEnum.TNUMBER) {
            return;
        }

        if (tok.kind == TokenEnum.TIDENT) {

            String s = tok.sval;

            switch (s) {
                case "define":
                    readDefine();
                    break;
                case "elif":
                    readElif(hash);
                    break;
                case "else":
                    readElse(hash);
                    break;
                case "endif":
                    readEndif(hash);
                    break;
                case "error":
                    readError(hash);
                    break;
                case "if":
                    readIf();
                    break;
                case "ifdef":
                    readIfdef();
                    break;
                case "ifndef":
                    readIfndef();
                    break;
                case "import":
                    readInclude(hash, tok.file, true);
                    break;
                case "include":
                    readInclude(hash, tok.file, false);
                    break;
                case "include_next":
                    readIncludeNext(hash, tok.file);
                    break;
                case "line":
                    readLine();
                    break;
                case "pargma":
                    readPragma();
                    break;
                case "undef":
                    readUndef();
                    break;
                case "warning":
                    readWarning(hash);
                    break;
                default:
                    error(hash, "unsuported preprocessor directive: %s", s);
            }
        } else {
            error(hash, "unsuported preprocessor directive: %s", tok.toString());
        }
    }

    public void readUndef() {
        Token name = readIdent();
        expectNewLine();
        macros.remove(name.sval);
    }

//    public void expect(char id) {
//        Token tok = lex();
//        if (!isKeyword(tok, id)) {
//            error(tok, "%c expected, but got %s", id, tok.toString());
//        }
//    }
    public void expect(Keyword id) {
        Token tok = lex();
        if (!isKeyword(tok, id)) {
            error(tok, "%s expected, but got %s", id, tok.toString());
        }
    }

    public void readDefine() {
        Token name = readIdent();
        Token tok = lex();

        if (isKeyword(tok, Keyword.C_LPAREN) && !tok.space) {
            readFuncLikeMacro(name);
            return;
        }

        ungetToken(tok);
        readObjMacro(name.sval);

    }

    public void readObjMacro(String name) {
        List<Token> body = new ArrayList<>();
        while (true) {
            Token tok = lex();
            if (tok.kind == TokenEnum.TNEWLINE) {
                break;
            }
            body.add(tok);
        }

        hashhashCheck(body);
        macros.put(name, makeObjMacro(body));
    }

    public void hashhashCheck(List<Token> body) {
        if (body.isEmpty()) {
            return;
        }

        if (isKeyword(body.get(0), Keyword.KHASHHASH)) {
            error(body.get(0), "## cannot appear at start of macro expansion");
        }
        if (isKeyword(body.get(body.size() - 1), Keyword.KHASHHASH)) {
            error(body.get(0), "## cannot appear at end of macro expansion");
        }
    }

    public Macro makeObjMacro(List<Token> body) {
        Macro r = new Macro();
        r.kind = MacroType.MACRO_OBJ;
        r.body = body;

        return r;
    }

    public void readFuncLikeMacro(Token name) {
        Map<String, Token> param = new LinearMap<>();
        boolean isVararg = readFuncLikeMacroParams(name, param);
        List<Token> body = readFuncLikeMacroBody(param);
        hashhashCheck(body);
        Macro macro = makeFuncMacro(body, param.size(), isVararg);
        macros.put(name.sval, macro);
    }

    public boolean readFuncLikeMacroParams(Token name, Map<String, Token> param) {
        int pos = 0;
        while (true) {
            Token tok = lex();
            if (isKeyword(tok, Keyword.C_RPAREN)) {
                return false;
            }
            if (pos != 0) {
                if (!isKeyword(tok, Keyword.C_COMMA)) {
                    error(tok, ", expected, but got %s", tok.toString());
                }
                tok = lex();
            }
            if (tok.kind == TokenEnum.TNEWLINE) {
                error(name, "missing ')' in macro parametr list");
            }
            if (isKeyword(tok, Keyword.KELLIPSIS)) {
                param.put("__VA_ARGS__", makeMacroToken(pos++, true));
                expect(Keyword.C_RPAREN);
                return true;
            }
            if (tok.kind != TokenEnum.TIDENT) {
                error(tok, "identifier expected, bug tos %s", tok.toString());
            }
            String arg = tok.sval;
            if (next(Keyword.KELLIPSIS)) {
                expect(Keyword.C_RPAREN);
                param.put(arg, makeMacroToken(pos++, true));
                return true;
            }
            param.put(arg, makeMacroToken(pos++, false));
        }
    }

    public Token makeMacroToken(int position, boolean isVararg) {
        Token r = new Token(currentFile(), pos);
        r.kind = TokenEnum.TMACRO_PARAM;
        r.isVararg = isVararg;
        r.hideset = null;
        r.position = position;
        r.space = false;
        r.bol = false;

        return r;
    }

    public List<Token> readFuncLikeMacroBody(Map<String, Token> param) {
        List<Token> r = new ArrayList<>();
        while (true) {
            Token tok = lex();
            if (tok.kind == TokenEnum.TNEWLINE) {
                return r;
            }
            if (tok.kind == TokenEnum.TIDENT) {
                Token subst = param.get(tok.sval);
                if (subst != null) {
                    subst = subst.copy();
                    subst.space = tok.space;
                    r.add(subst);
                    continue;
                }
            }
            r.add(tok);
        }
    }

    public Macro makeFuncMacro(List<Token> body, int nargs, boolean isVarg) {
        Macro r = new Macro();
        r.nargs = nargs;
        r.body = body;
        r.isVarg = isVarg;
        r.kind = MacroType.MACRO_FUNC;

        return r;
    }

    public void readElif(Token hash) {
        if (condInclStack.isEmpty()) {
            error(hash, "stray #elif");
        }

        ConditionInclude ci = condInclStack.get(condInclStack.size() - 1);
        if (ci.context == ConditionIncludeContext.IN_ELSE) {
            error(hash, "#elif after #else");
        }

        ci.context = ConditionIncludeContext.IN_ELIF;
        ci.includeGuard = null;
        if (ci.wasTrue || !readConstExpr()) {
            skipCondIncl();
            return;
        }

        ci.wasTrue = true;
    }

    public boolean readConstExpr() {
        tokenBuffersStash(readIntExprLine());
        Node expr = readExpr();
        Token tok = lex();
        if (tok.kind == TokenEnum.TEOF) {
            error(tok, "stray token: %s", tok.toString());
        }

        tokenBufferUnstash();
        return evalIntExpr(expr, null) != 0;

    }

    public List<Token> readIntExprLine() {
        List<Token> r = new ArrayList<>();
        while (true) {
            Token tok = readExpandNewline();
            if (tok.kind == TokenEnum.TNEWLINE) {
                return r;
            }

            if (isIdent(tok, "defined")) {
                r.add(readDefinedOp());
            } else if (tok.kind == TokenEnum.TIDENT) {
                r.add(Token.CPP_TOKEN_ZERO);
            } else {
                r.add(tok);
            }
        }
    }

    public Token readDefinedOp() {
        Token tok = lex();
        if (isKeyword(tok, Keyword.C_RPAREN)) {
            tok = lex();
            expect(Keyword.C_LPAREN);
        }

        if (tok.kind == TokenEnum.TIDENT) {
            error(tok, "identifier expected, but got %s", tok.toString());
        }
        return macros.get(tok.sval) != null ? Token.CPP_TOKEN_ONE : Token.CPP_TOKEN_ZERO;
    }

    public Node readExpr() {
        Token tok = peekToken();
        Node r = readCommaExpr();
        if (r == null) {
            error(tok, "expression expected");
        }
        return r;
    }

    public Node readCommaExpr() {
        Node node = readAssigmentExpr();
        while (nextToken(Keyword.C_COMMA)) {
            Node expr = readAssigmentExpr();
            node = Node.astBinOp(expr.type, Keyword.C_COMMA, node, expr);
        }
        return node;
    }

//    public boolean nextToken(char c) {
//        Token tok = getToken();
//        if (isKeyword(tok, c)) {
//            return true;
//        }
//        ungetToken(tok);
//        return false;
//    }
    public boolean nextToken(Keyword c) {
        Token tok = getToken();
        if (isKeyword(tok, c)) {
            return true;
        }
        ungetToken(tok);
        return false;
    }

    public Token getToken() {
        Token r = readToken();
        if (r.kind == TokenEnum.TINVALID) {
            error(r, "stray character in program: %c", r.c);
        }
        if (r.kind == TokenEnum.TSTRING && peekToken().kind == TokenEnum.TSTRING) {
            concatenateString(r);
        }
        return r;
    }

    public void concatenateString(Token tok) {
        EncodingEnum enc = tok.enc;
        String b = "";
        b += tok.sval;
        while (peekToken().kind == TokenEnum.TSTRING) {
            Token tok2 = readToken();
            b += tok2.sval;
            EncodingEnum enc2 = tok2.enc;
            if (enc != EncodingEnum.ENC_NONE && enc2 != EncodingEnum.ENC_NONE && enc != enc2) {
                error(tok2, "unsuported non-standard concatenation of string literals: %s", tok.toString());
            }
        }

        tok.sval = b;
        tok.slen = b.length();
        tok.enc = enc;
    }

    public Node readAssigmentExpr() {
        Node node = readLogorExpr();
        Token tok = getToken();
        if (tok == null) {
            return node;
        }

        if (isKeyword(tok, Keyword.C_QMARK)) {
            return doReadConditionalExpr(node);
        }
        Keyword cop = Node.getCompoundAssignOp(tok);
        if (isKeyword(tok, Keyword.C_EQ) || cop != null) {
            Node value = Node.conv(readAssigmentExpr());
            if (isKeyword(tok, Keyword.C_EQ) || cop != null) {
                ensureLValue(node);
            }
            Node right = cop != null ? Node.binOp(cop, Node.conv(node), value) : value;
            if (Node.isArithType(node.type) && node.type.kind != right.type.kind) {
                right = Node.astConv(node.type, right);
            }

            return Node.astBinOp(node.type, Keyword.C_EQ, node, right);
        }
        ungetToken(tok);
        return node;
    }

    public long evalIntExpr(Node node, DataRef<Node> addr) {
        switch (node.kind) {
            case AST_LITERAL:
                if (Node.isIntType(node.type)) {
                    return node.cast(NodeInteger.class).val;
                }
                error("Integer expression expected, but gor %s", node.toString());
                return 0;
            case C_EXPOINT:
                return evalIntExpr(node.cast(NodeUnary.class).operand, addr) == 0 ? 1 : 0;
            case C_TILDA:
                return ~evalIntExpr(node.cast(NodeUnary.class).operand, addr);
            case OP_CAST:
                return evalIntExpr(node.cast(NodeUnary.class).operand, addr);
            case AST_CONV:
                return evalIntExpr(node.cast(NodeUnary.class).operand, addr);
            case AST_ADDR:
                if (node.cast(NodeUnary.class).operand.kind == Keyword.AST_STRUCT_REF) {
                    return evalStructRef(node.cast(NodeUnary.class).operand, 0);
                }
            case AST_GVAR:
                if (addr != null) {
                    addr.setData(Node.conv(node));
                    return 0;
                }
                break;
            case AST_DEREF:
                if (node.cast(NodeUnary.class).operand.type.kind == TypeKind.KIND_PTR) {
                    return evalIntExpr(node.cast(NodeUnary.class).operand, addr);
                }
                break;
            case AST_TERNARY:
                long cond = evalIntExpr(node.cast(NodeIf.class).condition, addr);
                if (cond != 0) {
                    return node.cast(NodeIf.class).then != null ? evalIntExpr(node.cast(NodeIf.class).then, addr) : cond;
                }
                return evalIntExpr(node.cast(NodeIf.class).els, addr);
            case C_PLUS:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) + evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case C_MINUS:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) - evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case C_MUL:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) * evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case C_DIV:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) / evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case C_LT:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) < evalIntExpr(node.cast(NodeBinary.class).right, addr) ? 1 : 0;
            case C_XOR:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) ^ evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case C_AND:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) & evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case C_OR:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) | evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case C_MOD:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) % evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case OP_EQ:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) == evalIntExpr(node.cast(NodeBinary.class).right, addr) ? 1 : 0;
            case OP_LE:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) <= evalIntExpr(node.cast(NodeBinary.class).right, addr) ? 1 : 0;
            case OP_NE:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) != evalIntExpr(node.cast(NodeBinary.class).right, addr) ? 1 : 0;
            case OP_SAL:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) << evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case OP_SAR:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) >> evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case OP_SHR:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) >> evalIntExpr(node.cast(NodeBinary.class).right, addr);
            case OP_LOGAND:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) > 0 && evalIntExpr(node.cast(NodeBinary.class).right, addr) > 0 ? 1 : 0;
            case OP_LOGOR:
                return evalIntExpr(node.cast(NodeBinary.class).left, addr) > 0 || evalIntExpr(node.cast(NodeBinary.class).right, addr) > 0 ? 1 : 0;
            default:
        }

        error("Integer expression expected, but got %s", node.toString());
        return 0;
    }

    public float evalFloExpr(Node node, DataRef<Node> addr) {
        switch (node.kind) {
            case AST_LITERAL:
                if (Node.isFloType(node.type)) {
                    return (float) node.cast(NodeFloat.class).val;
                }
                if (Node.isIntType(node.type)) {
                    return (float) node.cast(NodeInteger.class).val;
                }
                error("Float expression expected, but gor %s", node.toString());
                return 0;
            case OP_CAST:
                return evalFloExpr(node.cast(NodeUnary.class).operand, addr);
            case AST_CONV:
                return evalFloExpr(node.cast(NodeUnary.class).operand, addr);
            case AST_GVAR:
                if (addr != null) {
                    addr.setData(Node.conv(node));
                    return 0;
                }
                break;
            case C_PLUS:
                return evalFloExpr(node.cast(NodeBinary.class).left, addr) + evalFloExpr(node.cast(NodeBinary.class).right, addr);
            case C_MINUS:
                return evalFloExpr(node.cast(NodeBinary.class).left, addr) - evalFloExpr(node.cast(NodeBinary.class).right, addr);
            case C_MUL:
                return evalFloExpr(node.cast(NodeBinary.class).left, addr) * evalFloExpr(node.cast(NodeBinary.class).right, addr);
            case C_DIV:
                return evalFloExpr(node.cast(NodeBinary.class).left, addr) / evalFloExpr(node.cast(NodeBinary.class).right, addr);
            case C_MOD:
                return evalFloExpr(node.cast(NodeBinary.class).left, addr) % evalFloExpr(node.cast(NodeBinary.class).right, addr);
            default:
        }

        error("Float expression expected, but got %s", node.toString());
        return 0;
    }

    public void ensureLValue(Node node) {
        switch (node.kind) {
            case AST_LVAR:
            case AST_GVAR:
            case AST_DEREF:
            case AST_STRUCT_REF:
                return;
            default:
                errorC("lvalue expected, but got %s", node.toString());
        }
    }

    public void ensureIntType(Node node) {
        if (!Node.isIntType(node.type)) {
            errorC("integer type expected, but gos %s", node.toString());
        }
    }

    public boolean isType(Token tok) {
        if (tok.kind == TokenEnum.TIDENT) {
            return getTypeDef(tok.sval) != null;
        }
        if (tok.kind != TokenEnum.TKEYWORD) {
            return false;
        }

        return tok.id.bool;

    }

    public Type getTypeDef(String name) {
        Node node = Globals.env().get(name);
        return node != null && node.kind == Keyword.AST_TYPEDEF ? node.type : null;
    }

    public Node readLogorExpr() {
        Node node = readLogandExpr();
        while (nextToken(Keyword.OP_LOGOR)) {
            node = Node.astBinOp(Globals.typedefs.get("int"), Keyword.OP_LOGOR, node, readLogandExpr());
        }
        return node;
    }

    public Node readLogandExpr() {
        Node node = readBitorExpr();
        while (nextToken(Keyword.OP_LOGAND)) {
            node = Node.astBinOp(Globals.typedefs.get("int"), Keyword.OP_LOGAND, node, readBitorExpr());
        }
        return node;
    }

    public Node readBitorExpr() {
        Node node = readBitxorExpr();
        while (nextToken(Keyword.C_OR)) {
            node = Node.binOp(Keyword.C_OR, Node.conv(node), Node.conv(readBitxorExpr()));
        }
        return node;
    }

    public Node readBitxorExpr() {
        Node node = readBitandExpr();
        while (nextToken(Keyword.C_XOR)) {
            node = Node.binOp(Keyword.C_XOR, Node.conv(node), Node.conv(readBitandExpr()));
        }
        return node;
    }

    public Node readBitandExpr() {
        Node node = readEqualityExpr();
        while (nextToken(Keyword.C_AND)) {
            node = Node.binOp(Keyword.C_AND, Node.conv(node), Node.conv(readEqualityExpr()));
        }
        return node;
    }

    public Node readEqualityExpr() {
        Node node = readRelationalExpr();
        Node r;
        if (nextToken(Keyword.OP_EQ)) {
            r = Node.binOp(Keyword.OP_EQ, Node.conv(node), Node.conv(readEqualityExpr()));
        } else if (next(Keyword.OP_NE)) {
            r = Node.binOp(Keyword.OP_NE, Node.conv(node), Node.conv(readEqualityExpr()));
        } else {
            return node;
        }
        r.type = Globals.typedefs.get("int");
        return r;
    }

    public Node readRelationalExpr() {
        Node node = readShiftExpr();
        while (true) {
            if (nextToken(Keyword.C_LT)) {
                node = Node.binOp(Keyword.C_LT, Node.conv(node), Node.conv(readShiftExpr()));
            } else if (nextToken(Keyword.C_GT)) {
                node = Node.binOp(Keyword.C_LT, Node.conv(readShiftExpr()), Node.conv(node));
            } else if (nextToken(Keyword.OP_LE)) {
                node = Node.binOp(Keyword.OP_LE, Node.conv(node), Node.conv(readShiftExpr()));
            } else if (nextToken(Keyword.OP_GE)) {
                node = Node.binOp(Keyword.OP_LE, Node.conv(readShiftExpr()), Node.conv(node));
            } else {
                return node;
            }
            node.type = Globals.typedefs.get("int");
        }
    }

    public Node readShiftExpr() {
        Node node = readAdditiveExpr();
        while (true) {
            Keyword op;
            if (nextToken(Keyword.OP_SAL)) {
                op = Keyword.OP_SAL;
            } else if (nextToken(Keyword.OP_SAR)) {
                op = node.type.unsigned ? Keyword.OP_SHR : Keyword.OP_SAR;
            } else {
                break;
            }
            Node right = readAdditiveExpr();
            ensureIntType(node);
            ensureIntType(right);
            node = Node.astBinOp(node.type, op, Node.conv(node), Node.conv(right));
        }

        return node;
    }

    public Node readAdditiveExpr() {
        Node node = readMultiplicativeExpr();
        while (true) {
            if (nextToken(Keyword.C_PLUS)) {
                node = Node.binOp(Keyword.C_PLUS, Node.conv(node), Node.conv(readMultiplicativeExpr()));
            } else if (nextToken(Keyword.C_MINUS)) {
                node = Node.binOp(Keyword.C_MINUS, Node.conv(node), Node.conv(readMultiplicativeExpr()));
            } else {
                return node;
            }
        }
    }

    public Node readMultiplicativeExpr() {
        Node node = readCastExpr();
        while (true) {
            if (nextToken(Keyword.C_MUL)) {
                node = Node.binOp(Keyword.C_MUL, Node.conv(node), Node.conv(readCastExpr()));
            } else if (nextToken(Keyword.C_DIV)) {
                node = Node.binOp(Keyword.C_DIV, Node.conv(node), Node.conv(readCastExpr()));
            } else if (nextToken(Keyword.C_MOD)) {
                node = Node.binOp(Keyword.C_MOD, Node.conv(node), Node.conv(readCastExpr()));
            } else {
                return node;
            }
        }
    }

    public Node readUnaryDeRef(Token tok) {
        Node operand = Node.conv(readCastExpr());
        if (operand.type.kind != TypeKind.KIND_PTR) {
            error(tok, "pointer type expected, but got %s", operand.type.kind.toString());
        }

        if (operand.type.pointer.kind == TypeKind.KIND_FUNC) {
            return operand;
        }

        return Node.astUOp(Keyword.AST_DEREF, operand.type.pointer, operand);
    }

    public Node readUnaryMinus() {
        Node expr = readCastExpr();
        //Node.ensureArithType(expr)
        if (Node.isIntType(expr.type)) {
            return Node.binOp(Keyword.C_MINUS, Node.conv(Node.astIntType(expr.type, 0)), Node.conv(expr));
        }
        return Node.binOp(Keyword.C_MINUS, Node.conv(Node.astFloatType(expr.type, 0)), Node.conv(expr));
    }

    public Node readUnaryBitNot(Token tok) {
        Node expr = readCastExpr();
        expr = Node.conv(expr);
        if (!Node.isIntType(expr.type)) {
            error(tok, "invalid use of ~: %s", expr.toString());
        }

        return Node.astUOp(Keyword.C_TILDA, expr.type, expr);
    }

    public Node readUnaryLogNot() {

        Node operand = readCastExpr();
        operand = Node.conv(operand);
        return Node.astUOp(Keyword.C_EXPOINT, Globals.typedefs.get("int"), operand);

    }

    public Node readUnaryExpr() {
        Token tok = getToken();
        if (tok.kind == TokenEnum.TKEYWORD) {
            switch (tok.id) {
                case KSIZEOF:
                    return readSizeofOperand();
                case OP_INC:
                    return readUnaryIncDec(Keyword.OP_PRE_INC);
                case OP_DEC:
                    return readUnaryIncDec(Keyword.OP_PRE_DEC);
                case OP_LOGAND:
                    return readLabelAddr(tok);
                case C_AND:
                    return readUnaryAddr();
                case C_MUL:
                    return readUnaryDeRef(tok);
                case C_PLUS:
                    return readCastExpr();
                case C_MINUS:
                    return readUnaryMinus();
                case C_TILDA:
                    return readUnaryBitNot(tok);
                case C_EXPOINT:
                    return readUnaryLogNot();
            }
        }
        ungetToken(tok);
        return readPostfixExpr();
    }

    public Node readSizeofOperand() {
        Type ty = readSizeofOperandSub();
        int size = ty.kind == TypeKind.KIND_VOID || ty.kind == TypeKind.KIND_FUNC ? 1 : ty.size;
        return Node.astIntType(Globals.typedefs.get("unsigned int"), size);
    }

    public Type readSizeofOperandSub() {
        Token tok = getToken();
        if (isKeyword(tok, Keyword.C_LPAREN) && isType(peekToken())) {
            Type r = readCastType();
            expect(Keyword.C_RPAREN);
            return r;
        }

        ungetToken(tok);
        return readUnaryExpr().type;
    }

    public Node readCastExpr() {
        Token tok = getToken();
        if (isKeyword(tok, Keyword.C_LPAREN) && isType(peekToken())) {
            Type ty = readCastType();
            expect(Keyword.C_RPAREN);
            if (isKeyword(peekToken(), Keyword.C_LBRACE)) {
                Node node = readCompoundLiteral(ty);
                return readPostfixExprTail(node);
            }
            return Node.astUOp(Keyword.OP_CAST, ty, readCastExpr());
        }
        ungetToken(tok);
        return readUnaryExpr();
    }

    public Node readCompoundLiteral(Type ty) {
        String name = Globals.makeLabel();
        List<Node> init = readDeclInit(ty);
        NodeLocalLabel r = Node.astLVar(ty, name);
        r.localVariableInit = init;
        return r;
    }

    public Type readCastType() {
        return readAbstractDeclarator(readDeclSpec(null));
    }

    public Type readDeclaratorTail(Type baseType, List<Node> params) {
        if (nextToken(Keyword.C_LSBRACE)) {
            return readDeclaratorArray(baseType);
        }
        if (nextToken(Keyword.C_LPAREN)) {
            return readDeclaratorFunc(baseType, params);
        }
        return baseType;
    }

    public void skipTypeQualifiers() {
        while (nextToken(Keyword.KCONST) || nextToken(Keyword.KRESTRICT)) {
        }
    }

    public Type readDeclaratorFunc(Type baseType, List<Node> params) {
        if (baseType.kind == TypeKind.KIND_FUNC) {
            error("function returning a function");
        }

        if (baseType.kind == TypeKind.KIND_ARRAY) {
            error("function returning an array");
        }

        return readFuncParamList(params, baseType);
    }

    public Type readFuncParam(DataRef<String> name, boolean optional) {
        DataRef<DomesticType> sclass = new DataRef<>(null);
        Type baseType = Globals.typedefs.get("int");
        if (isType(peekToken())) {
            baseType = readDeclSpec(sclass);
        } else if (optional) {
            error(peekToken(), "type expected, but got %s", peekToken().toString());
        }

        Type type = readDeclarator(name, baseType, null, optional ? Decl.DECL_PARAM_TYPEONLY : Decl.DECL_PARAM);

        if (type.kind == TypeKind.KIND_ARRAY) {
            return Type.makePointerType(type.pointer);
        }

        if (type.kind == TypeKind.KIND_FUNC) {
            return Type.makePointerType(type);
        }

        return type;

    }

    public void readDeclaratorParams(List<Type> types, List<Node> vars, DataRef<Boolean> ellipsis) {
        boolean typeOnly = vars != null;
        ellipsis.setData(false);
        while (true) {
            Token tok = peekToken();
            if (nextToken(Keyword.KELLIPSIS)) {
                if (types.isEmpty()) {
                    error(tok, "at least one parameter is required before ...");
                }
                expect(Keyword.C_RPAREN);
                ellipsis.setData(true);
                return;
            }
            DataRef<String> name = new DataRef<>(null);
            Type ty = readFuncParam(name, typeOnly);
            // Node.ensureNotVoid(ty);
            types.add(ty);
            if (typeOnly) {
                vars.add(Node.astLVar(ty, name.getData()));
            }

            tok = getToken();
            if (isKeyword(tok, Keyword.C_RPAREN)) {
                return;
            }
            if (!isKeyword(tok, Keyword.C_COMMA)) {
                error(tok, "comma expected, but got %s", tok.toString());
            }
        }
    }

    public Type readFuncParamList(List<Node> paramvars, Type retType) {
        Token tok = getToken();
        if (isKeyword(tok, Keyword.KVOID) && nextToken(Keyword.C_RPAREN)) {
            return Type.makeFuncType(retType, new ArrayList<Type>(), false, false);
        }

        if (isKeyword(tok, Keyword.C_RPAREN)) {
            return Type.makeFuncType(retType, new ArrayList<Type>(), true, true);
        }

        ungetToken(tok);

        Token tok2 = peekToken();
        if (nextToken(Keyword.KELLIPSIS)) {
            error(tok2, "at least one parametr is required before ...");
        }
        if (isType(peekToken())) {
            DataRef<Boolean> ellipsis = new DataRef<>(false);
            List<Type> paramTypes = new ArrayList<>();
            readDeclaratorParams(paramTypes, paramvars, ellipsis);
            return Type.makeFuncType(retType, paramTypes, ellipsis.getData(), false);
        }

        if (paramvars == null) {
            error(tok, "invalid function definition");
        }

        readDeclaratorParamsOldStyle(paramvars);
        List<Type> paramTypes = new ArrayList<>();
        for (Node paramvar : paramvars) {
            paramTypes.add(Globals.typedefs.get("int"));
        }

        return Type.makeFuncType(retType, paramTypes, false, true);
    }

    public void readDeclaratorParamsOldStyle(List<Node> vars) {
        while (true) {
            Token tok = getToken();
            if (tok.kind != TokenEnum.TIDENT) {
                error(tok, "identifier expected but got %s", tok.toString());
            }

            vars.add(Node.astLVar(Globals.typedefs.get("int"), tok.sval));
            if (nextToken(Keyword.C_RPAREN)) {
                return;
            }
            if (!nextToken(Keyword.C_COMMA)) {
                error(tok, "comma expected, but gor %s", tok.toString());
            }
        }
    }

    public Type readDeclarator(DataRef<String> rname, Type baseType, List<Node> params, Decl ctx) {
        if (nextToken(Keyword.C_LPAREN)) {
            if (isType(peekToken())) {
                return readDeclaratorFunc(baseType, params);
            }

            Type stub = Type.makeStubType();
            Type t = readDeclarator(rname, stub, params, ctx);
            expect(Keyword.C_RPAREN);
            readDeclaratorTail(baseType, params).copyTo(stub);
            return t;
        }

        if (nextToken(Keyword.C_MUL)) {
            skipTypeQualifiers();
            return readDeclarator(rname, Type.makePointerType(baseType), params, ctx);
        }

        Token tok = getToken();
        if (tok.kind == TokenEnum.TIDENT) {
            if (ctx == Decl.DECL_CAST) {
                error(tok, "identifier is not expected but got %s", tok.toString());
            }
            rname.setData(tok.sval);
            return readDeclaratorTail(baseType, params);
        }

        if (ctx == Decl.DECL_BODY || ctx == Decl.DECL_PARAM) {
            error(tok, "identifier, ( or * are expected, but got %s", tok.toString());
        }

        ungetToken(tok);
        return readDeclaratorTail(baseType, params);
    }

    public Type readAbstractDeclarator(Type baseType) {
        return readDeclarator(null, baseType, null, Decl.DECL_CAST);
    }

    public Node readUnaryIncDec(Keyword op) {
        Node operand = readUnaryExpr();
        operand = Node.conv(operand);
        ensureLValue(operand);
        return Node.astUOp(op, operand.type, operand);
    }

    public String readRecTypeTag() {
        Token tok = getToken();
        if (tok.kind == TokenEnum.TIDENT) {
            return tok.sval;
        }

        ungetToken(tok);
        return null;
    }

    public int readBitSize(DataRef<String> name, Type ty) {
        if (!Node.isIntType(ty)) {
            error("Non-integer type cannot be a bitfield: %s", ty.toString());
        }

        Token tok = peekToken();
        int r = readIntExpr();
        int maxSize = ty.size;
        if ((r < 0) || (maxSize < r)) {
            error(tok, "invalid bitfield size for %s: %d", ty.toString(), r);
        }

        if (r == 0 && name != null) {
            error(tok, "zero-width bitfield needs to be unnamed: %s", name.getData());
        }

        return r;
    }

    public Pair makePair(Object first, Object second) {
        Pair pair = new Pair();
        pair.first = first;
        pair.second = second;

        return pair;
    }

    public List<Pair> readRecTypeFieldsSub() {
        List<Pair> r = new ArrayList<>();
        while (true) {
            if (!isType(peekToken())) {
                break;
            }
            Type baseType = readDeclSpec(null);
            if (baseType.kind == TypeKind.KIND_STRUCT && nextToken(Keyword.C_SEMICOLON)) {
                r.add(makePair(null, baseType));
                continue;
            }
            while (true) {
                DataRef<String> name = new DataRef<>(null);
                Type fieldType = readDeclarator(name, baseType, null, Decl.DECL_PARAM_TYPEONLY);
                // ensureNotVoid(fieldType);
                fieldType = Type.copy(fieldType);
                fieldType.bitSize = nextToken(Keyword.C_COLON) ? readBitSize(name, fieldType) : -1;
                r.add(makePair(name.getData(), fieldType));
                if (nextToken(Keyword.C_COMMA)) {
                    continue;
                }

                if (isKeyword(peekToken(), Keyword.C_RBRACE)) {
                    warning(peekToken(), "missing ';' at the end of field list");
                } else {
                    expect(Keyword.C_SEMICOLON);
                }
                break;
            }
        }
        expect(Keyword.C_RBRACE);
        return r;
    }

    public void fixRecTypeFlexibleMember(List<Pair> fields) {
        for (int i = 0; i < fields.size(); i++) {
            Pair pair = fields.get(i);
            String name = (String) pair.first;
            Type ty = (Type) pair.second;
            if (ty.kind != TypeKind.KIND_ARRAY) {
                continue;
            }

            if (ty.len == -1) {
                if (i != fields.size() - 1) {
                    error("flexible member may only appear as the last member: %s %s", ty.toString(), name);
                }
                if (fields.size() == 1) {
                    error("flexible member with no other fields: %s %s", ty.toString(), name);
                }

                ty.len = 0;
                ty.size = 0;
            }
        }
    }

    public void finishBitField(DataRef<Integer> off, DataRef<Integer> bitoff) {
        off.setData(off.getData() + (bitoff.getData() + 7) / 8);
        bitoff.setData(0);
    }

    public int computePadding(int offset, int align) {
        return (offset % align == 0) ? 0 : align - offset % align;
    }

    public void squashUnnamedStruct(Map<String, Type> dict, Type unnamed, int offset) {
        List<String> keys = new ArrayList<>(unnamed.fields.keySet());
        for (String name : keys) {
            Type t = Type.copy(unnamed.fields.get(name));
            t.offset += offset;
            dict.put(name, t);
        }
    }

    public Map<String, Type> updateStructOffset(DataRef<Integer> rsize, DataRef<Integer> align, List<Pair> fields) {
        DataRef<Integer> off = new DataRef<>(0);
        DataRef<Integer> bitOff = new DataRef<>(0);
        Map<String, Type> r = new LinearMap<>();
        for (Pair pair : fields) {
            String name = (String) pair.first;
            Type fieldType = (Type) pair.second;

            if (name != null) {
                align.setData(Math.max(align.getData(), fieldType.align));
            }

            if (name == null && fieldType.kind == TypeKind.KIND_STRUCT) {
                finishBitField(off, bitOff);
                off.setData(off.getData() + computePadding(off.getData(), fieldType.align));
                squashUnnamedStruct(r, fieldType, off.getData());
                off.setData(off.getData() + fieldType.size);
                continue;
            }

            if (fieldType.bitSize == 0) {
                finishBitField(off, bitOff);
                off.setData(off.getData() + computePadding(off.getData(), fieldType.align));
                bitOff.setData(0);
                continue;
            }
            if (fieldType.bitSize > 0) {
                int bit = fieldType.size * 8;
                int room = bit - (off.getData() * 8 + bitOff.getData()) % bit;
                if (fieldType.bitSize <= room) {
                    fieldType.offset = off.getData();
                    fieldType.bitOffset = bitOff.getData();
                } else {
                    finishBitField(off, bitOff);
                    off.setData(off.getData() + computePadding(off.getData(), fieldType.align));
                    fieldType.offset = off.getData();
                    fieldType.bitOffset = 0;
                }
                bitOff.setData(bitOff.getData() + fieldType.bitSize);
            } else {
//                finishBitField(off, bitOff);
//                off.setData(off.getData() + computePadding(off.getData(), fieldType.align));
                fieldType.offset = off.getData();
                off.setData(off.getData() + fieldType.size);

            }
            if (name != null) {
                r.put(name, fieldType);
            }
        }
        finishBitField(off, bitOff);
        rsize.setData(off.getData());// + computePadding(off.getData(), align.getData()));

        return r;
    }

    public Map<String, Type> readRecTypeFields(DataRef<Integer> rsize, DataRef<Integer> align, boolean isStruct) {
        if (!nextToken(Keyword.C_LBRACE)) {
            return null;
        }

        List<Pair> fields = readRecTypeFieldsSub();
        fixRecTypeFlexibleMember(fields);
        if (isStruct) {
            return updateStructOffset(rsize, align, fields);
        }

        return updateUnionOffset(rsize, align, fields);
    }

    public Map<String, Type> updateUnionOffset(DataRef<Integer> rsize, DataRef<Integer> align, List<Pair> fields) {
        int maxsize = 0;
        Map<String, Type> r = new LinearMap<>();
        for (Pair pair : fields) {
            String name = (String) pair.first;
            Type fieldType = (Type) pair.second;
            maxsize = Math.max(maxsize, fieldType.size);
            align.setData(Math.max(align.getData(), fieldType.align));
            if (name == null && fieldType.kind == TypeKind.KIND_STRUCT) {
                squashUnnamedStruct(r, fieldType, 0);
                continue;
            }

            fieldType.offset = 0;
            if (fieldType.bitSize >= 0) {
                fieldType.bitOffset = 0;
            }

            if (name != null) {
                r.put(name, fieldType);
            }
        }
        rsize.setData(maxsize + computePadding(maxsize, align.getData()));
        return r;
    }

    public Type readRecTypeDef(boolean isStruct) {
        String tag = readRecTypeTag();
        Type r;
        if (tag != null) {
            r = tags.get(tag);
            if (r != null && (r.kind == TypeKind.KIND_ENUM || r.isStruct != isStruct)) {
                error("declarations of %s does not match", tag);
            }

            if (r == null) {
                r = Type.makeRecType(isStruct);
                tags.put(tag, r);
            }
        } else {
            r = Type.makeRecType(isStruct);
        }

        DataRef<Integer> size = new DataRef<>(0);
        DataRef<Integer> align = new DataRef<>(1);
        Map<String, Type> fields = readRecTypeFields(size, align, isStruct);
        r.align = align.getData();
        if (fields != null) {
            r.fields = fields;
            r.size = size.getData();
        }

        return r;
    }

    public Type readStructDef() {
        return readRecTypeDef(true);
    }

    public Type readUnionDef() {
        return readRecTypeDef(false);
    }

    public Type readEnumDef() {
        String tag = null;
        Token tok = getToken();
        if (tok.kind == TokenEnum.TIDENT) {
            tag = tok.sval;
            tok = getToken();
        }

        if (tag != null) {
            Type ty = tags.get(tag);
            if (ty != null && ty.kind != TypeKind.KIND_ENUM) {
                error(tok, "declarations of %s does not match", tag);
            }
        }
        if (!isKeyword(tok, Keyword.C_LBRACE)) {
            if (tag == null || tags.get(tag) == null) {
                error(tok, "enum tag %s is not defined", tag);
            }
            ungetToken(tok);
            return Globals.typedefs.get("int");
        }
        if (tag != null) {
            tags.put(tag, Globals.typedefs.get("int"));
        }

        int val = 0;

        while (true) {
            tok = getToken();
            if (isKeyword(tok, Keyword.C_RBRACE)) {
                break;
            }
            if (tok.kind != TokenEnum.TIDENT) {
                error(tok, "identifier expected, but got %s", tok.toString());
            }
            String name = tok.sval;

            if (nextToken(Keyword.C_EQ)) {
                val = readIntExpr();
            }

            Node constval = Node.astIntType(Globals.typedefs.get("int"), val++);
            Globals.env().put(name, constval);
            if (nextToken(Keyword.C_COMMA)) {
                continue;
            }
            if (nextToken(Keyword.C_RBRACE)) {
                break;
            }
            error(peekToken(), "',' or '}' expected, but got %s", peekToken().toString());
        }

        return Globals.typedefs.get("int");
    }

    public Type readDeclSpec(DataRef<DomesticType> rsclass) {
        DomesticType sclass = DomesticType.UNKNOWN;
        Token tok = peekToken();
        if (!isType(tok)) {
            error(tok, "type name expected, but got %s", tok.toString());
        }

        Type usertype = null;

        DataType kind = DataType.UNKNOWN;
        DataType size = DataType.UNKNOWN;
        DataType sig = DataType.UNKNOWN;

        boolean isNotDone = true;

        while (isNotDone) {
            tok = getToken();
            if (tok.kind == TokenEnum.TEOF) {
                error("permature end of input");
            }

            if (kind == DataType.UNKNOWN && tok.kind == TokenEnum.TIDENT && usertype == null) {
                Type def = getTypeDef(tok.sval);
                if (def != null) {
                    if (usertype != null) {
                        error(tok, "type mismatch: %s", tok.toString());
                    }
                    usertype = def;
                    checkError(tok, kind, size, sig, usertype);
                    continue;
                }
            }
            if (tok.kind != TokenEnum.TKEYWORD) {
                ungetToken(tok);
                break;
            }

            switch (tok.id) {
                case KTYPEDEF:
                    if (sclass != DomesticType.UNKNOWN) {
                        declErr(tok);
                    }
                    sclass = DomesticType.S_TYPEDEF;
                    break;
                case KEXTERN:
                    if (sclass != DomesticType.UNKNOWN) {
                        declErr(tok);
                    }
                    sclass = DomesticType.S_EXTERN;
                    break;
                case KSTATIC:
                    if (sclass != DomesticType.UNKNOWN) {
                        declErr(tok);
                    }
                    sclass = DomesticType.S_STATIC;
                    break;
                case KAUTO:
                    if (sclass != DomesticType.UNKNOWN) {
                        declErr(tok);
                    }
                    sclass = DomesticType.S_AUTO;
                    break;
                case KREGISTER:
                    if (sclass != DomesticType.UNKNOWN) {
                        declErr(tok);
                    }
                    sclass = DomesticType.S_REGISTER;
                    break;
                case KCONST:
                case KINLINE:
                    break;
                case KVOID:
                    if (kind != DataType.UNKNOWN) {
                        declErr(tok);
                    }
                    kind = DataType.VOID;
                    break;
                case KCHAR:
                    if (kind != DataType.UNKNOWN) {
                        declErr(tok);
                    }
                    kind = DataType.CHAR;
                    break;
                case KINT:
                    if (kind != DataType.UNKNOWN) {
                        declErr(tok);
                    }
                    kind = DataType.INT;
                    break;
                case KFLOAT:
                    if (kind != DataType.UNKNOWN) {
                        declErr(tok);
                    }
                    kind = DataType.FLOAT;
                    break;
                case KSIGNED:
                    if (sig != DataType.UNKNOWN) {
                        declErr(tok);
                    }
                    sig = DataType.SIGNED;
                    break;
                case KUNSIGNED:
                    if (sig != DataType.UNKNOWN) {
                        declErr(tok);
                    }
                    sig = DataType.UNSIGNED;
                    break;
                case KSTRUCT:
                    if (usertype != null) {
                        declErr(tok);
                    }
                    usertype = readStructDef();
                    break;
                case KUNION:
                    if (usertype != null) {
                        declErr(tok);
                    }
                    usertype = readUnionDef();
                    break;
                case KENUM:
                    if (usertype != null) {
                        declErr(tok);
                    }
                    usertype = readEnumDef();
                    break;
                case KLONG: {
                    if (size == DataType.UNKNOWN) {
                        size = DataType.LONG;
                    } else {
                        declErr(tok);
                    }
                    break;
                }
                case KTYPEOF:
                    if (usertype != null) {
                        declErr(tok);
                    }
                    usertype = readTypeof();
                    break;
                default:
                    ungetToken(tok);
                    isNotDone = false;
                    continue;
            }
            checkError(tok, kind, size, sig, usertype);

        }
        if (rsclass != null) {
            rsclass.setData(sclass);
        }
        if (usertype != null) {
            return usertype;
        }

        switch (kind) {
            case VOID:
                return Globals.typedefs.get("void");
            case CHAR:
                return Type.makeNumType(TypeKind.KIND_CHAR, sig == DataType.UNSIGNED);
            case FLOAT:
                return Type.makeNumType(TypeKind.KIND_FLOAT, false);
        }

        switch (size) {
            case LONG:
                return Type.makeNumType(TypeKind.KIND_LONG, sig == DataType.UNSIGNED);
            default:
                return Type.makeNumType(TypeKind.KIND_INT, sig == DataType.UNSIGNED);
        }
    }

    public void declErr(Token tok) {
        error(tok, "type mismatch: %s", tok.toString());
    }

    public void checkError(Token tok, DataType kind, DataType size, DataType sig, Type usertype) {
        if ((size == DataType.LONG && (kind != DataType.UNKNOWN && kind != DataType.INT))
                || (sig != DataType.UNKNOWN && (kind == DataType.VOID || kind == DataType.FLOAT))
                || (usertype != null && (kind != DataType.UNKNOWN || size != DataType.UNKNOWN || sig != DataType.UNKNOWN))) {
            declErr(tok);
        }
    }

    public Node readLabelAddr(Token tok) {
        Token tok2 = getToken();
        if (tok2.kind != TokenEnum.TIDENT) {
            error(tok, "label name expected after &&, but got %s", tok.toString());
        }
        Node r = Node.astLabelAddr(tok2.sval);
        Globals.gotos.add(r);
        return r;
    }

    public Type readTypeof() {
        expect(Keyword.C_LBRACE);
        Type r = isType(peekToken()) ? readCastType() : readCastExpr().type;
        expect(Keyword.C_RBRACE);
        return r;
    }

    public Node readSubscriptExpr(Node node) {
        Token tok = peekToken();
        Node sub = readExpr();
        if (sub == null) {
            error(tok, "subscription expected");
        }

        expect(Keyword.C_RSBRACE);
        Node t = Node.binOp(Keyword.C_PLUS, Node.conv(node), Node.conv(sub));
        return Node.astUOp(Keyword.AST_DEREF, t.type.pointer, t);
    }

    public Node readPostfixExprTail(Node node) {
        if (node == null) {
            return null;
        }
        while (true) {
            if (nextToken(Keyword.C_LPAREN)) {
                Token tok = peekToken();
                node = Node.conv(node);
                Type t = node.type;
                if (t.kind != TypeKind.KIND_PTR || t.pointer.kind != TypeKind.KIND_FUNC) {
                    error(tok, "function expected, but got %s", node.toString());
                }
                node = readFuncCall(node);
                continue;
            }

            if (nextToken(Keyword.C_LSBRACE)) {
                node = readSubscriptExpr(node);
                continue;
            }

            if (nextToken(Keyword.C_POINT)) {
                node = readStructField(node);
                continue;
            }

            if (nextToken(Keyword.OP_ARROW)) {
                if (node.type.kind != TypeKind.KIND_PTR) {
                    errorC("pointer type expected, but gos %s %s", node.type.toString(), node.toString());
                }
                node = Node.astUOp(Keyword.AST_DEREF, node.type.pointer, node);
                node = readStructField(node);
                continue;
            }

            Token tok = peekToken();
            if (nextToken(Keyword.OP_INC) || nextToken(Keyword.OP_DEC)) {
                ensureLValue(node);
                Keyword op = isKeyword(tok, Keyword.OP_INC) ? Keyword.OP_POST_INC : Keyword.OP_POST_DEC;
                return Node.astUOp(op, node.type, node);
            }

            return node;
        }
    }

    public Node readFuncCall(Node fp) {
        if (fp.kind == Keyword.AST_ADDR && fp.cast(NodeUnary.class).operand.kind == Keyword.AST_FUNCDESG) {
            Node desg = fp.cast(NodeUnary.class).operand;
            List<Node> args = readFuncArgs(desg.type.params);
            return Node.astFuncCall(desg.type, desg.cast(NodeFunctionPointer.class).functionName, args);
        }

        List<Node> args = readFuncArgs(fp.type.pointer.params);
        return Node.astFuncPtrCall(fp, args);
    }

    public List<Node> readFuncArgs(List<Type> params) {
        List<Node> args = new ArrayList<>();
        int i = 0;
        while (true) {
            if (nextToken(Keyword.C_RPAREN)) {
                break;
            }
            Node arg = Node.conv(readAssigmentExpr());
            Type paramType;
            if (i < params.size()) {
                paramType = params.get(i++);
            } else {
                paramType = Node.isFloType(arg.type) ? Globals.typedefs.get("float") : Node.isIntType(arg.type) ? Globals.typedefs.get("int") : arg.type;
            }

            ensureAssignable(paramType, arg.type);
            if (paramType.kind != arg.type.kind) {
                arg = Node.astConv(paramType, arg);
            }
            args.add(arg);
            Token tok = getToken();
            if (isKeyword(tok, Keyword.C_RPAREN)) {
                break;
            }
            if (!isKeyword(tok, Keyword.C_COMMA)) {
                error(tok, "unexpected token: %s", tok.toString());
            }
        }
        return args;
    }

    public Node doReadConditionalExpr(Node cond) {

        Node then = Node.conv(readCommaExpr());
        expect(Keyword.C_COLON);
        Node els = Node.conv(readConditionalExpr());

        Type t = then != null ? then.type : cond.type;
        Type u = els.type;

        if (Node.isArithType(t) && Node.isArithType(u)) {
            Type r = Node.usualArithConv(t, u);
            return Node.astTernary(r, cond, (then != null ? Node.wrap(r, then) : null), Node.wrap(r, els));
        }

        return Node.astTernary(u, cond, then, els);

    }

    public Node readConditionalExpr() {
        Node cond = readLogorExpr();
        if (!nextToken(Keyword.C_QMARK)) {
            return cond;
        }
        return doReadConditionalExpr(cond);
    }

    public Node readPostfixExpr() {
        Node node = readPrimaryExpr();
        return readPostfixExprTail(node);
    }

    public Node readNumber(Token tok) {
        String s = tok.sval;
        boolean isFloat = StrUtil.strpbrk(s, ".pP") || (!s.toLowerCase().startsWith("0x") && StrUtil.strpbrk(s, "eE"));
        return isFloat ? readFloat(tok) : readInt(tok);
    }

    public Node readInt(Token tok) {
        String s = tok.sval;
        DataRef<Integer> end = new DataRef<>(0);
        long v = s.toLowerCase().startsWith("0b") ? StrUtil.strtoul(s.substring(2), end, 2) : StrUtil.strtoul(s, end, 0);

        Type ty = readIntSuffix(s.substring(end.getData()));
        if (ty != null) {
            return Node.astIntType(ty, (int) v);
        }
        if (end.getData() != s.length()) {
            error(tok, "invalid character '%c': %s", s.charAt(end.getData()), s);
        }

        boolean base10 = s.charAt(0) != '0';
        if (base10) {
            ty = 0 == (v & ~INT_MAX) ? Globals.typedefs.get("int") : Globals.typedefs.get("long");
            return Node.astIntType(ty, (int) v);
        }

        ty = 0 == (v & ~INT_MAX) ? Globals.typedefs.get("int")
                : 0 == (v & ~UINT_MAX) ? Globals.typedefs.get("unsigned int")
                        : 0 == (v & ~LONG_MAX) ? Globals.typedefs.get("long") : Globals.typedefs.get("long");
        return Node.astIntType(ty, (int) v);
    }

    public Node readFloat(Token tok) {
        String s = tok.sval;
        DataRef<Integer> end = new DataRef<>(0);
        double v = StrUtil.strod(s, end);
        if (s.substring(end.getData()).equalsIgnoreCase("l")) {
            return Node.astFloatType(Globals.typedefs.get("float"), v);
        }
        if (s.substring(end.getData()).equalsIgnoreCase("f")) {
            return Node.astFloatType(Globals.typedefs.get("float"), v);
        }
        if (s.length() != end.getData()) {
            error(tok, "invalid character '%c': %s", s.charAt(end.getData() + 1), s);
        }
        return Node.astFloatType(Globals.typedefs.get("float"), v);
    }

    public Node readPrimaryExpr() {
        Token tok = getToken();
        if (tok == null) {
            return null;
        }
        if (isKeyword(tok, Keyword.C_LPAREN)) {
            if (nextToken(Keyword.C_LBRACE)) {
                return readStmtExpr();
            }
            Node r = readExpr();
            expect(Keyword.C_RPAREN);
            return r;
        }

        switch (tok.kind) {
            case TIDENT:
                return readVarOrFunc(tok.sval);
            case TNUMBER:
                return readNumber(tok);
            case TCHAR:
                return Node.astIntType(Globals.typedefs.get("char"), tok.c);
            case TSTRING:
                return Node.astString(tok.enc, tok.sval, tok.slen);
            case TKEYWORD:
                ungetToken(tok);
                return null;
            default:
                error("internal error: unknown token kind: %s", tok.kind);
        }
        return null;
    }

    public Type readIntSuffix(String s) {
        if (s.equalsIgnoreCase("u")) {
            return Globals.typedefs.get("unsigned int");
        } else if (s.equalsIgnoreCase("l")) {
            return Globals.typedefs.get("long");
        } else if (s.equalsIgnoreCase("ul") || s.equalsIgnoreCase("lu")) {
            return Globals.typedefs.get("long");
        } else if (s.equalsIgnoreCase("ull") || s.equalsIgnoreCase("llu")) {
            return Globals.typedefs.get("long");
        } else if (s.equalsIgnoreCase("ll")) {
            return Globals.typedefs.get("long");
        }

        return null;
    }

    public Node readVarOrFunc(String name) {
        Node v = Globals.env().get(name);
        if (v == null) {
            Token tok = peekToken();
            if (!isKeyword(tok, Keyword.C_LPAREN)) {
                error(tok, "undefined variable: %s", name);
            }
            Type ty = Type.makeFuncType(Globals.typedefs.get("int"), new ArrayList<Type>(), true, false);
            warning(tok, "assume returning int: %s()", name);
            return Node.astFuncDesg(ty, name);
        }

        if (v.type.kind == TypeKind.KIND_FUNC) {
            return Node.astFuncDesg(v.type, name);
        }

        return v;
    }

    public Node readUnaryAddr() {
        Node operand = readCastExpr();
        if (operand.kind == Keyword.AST_FUNCDESG) {
            return Node.conv(operand);
        }

        ensureLValue(operand);
        return Node.astUOp(Keyword.AST_ADDR, Type.makePointerType(operand.type), operand);
    }

    public Node readCompoundStmt() {
        Map<String, Node> orig = Globals.localEnv;
        Globals.localEnv = new LinearMap<>(orig);
        List<Node> list = new ArrayList<>();
        while (true) {
            if (nextToken(Keyword.C_RBRACE)) {
                break;
            }
            readDeclOrStmt(list);
        }

        Globals.localEnv = orig;
        return Node.astCompoundStmt(list);
    }

    public void readDeclOrStmt(List<Node> list) {
        Token tok = peekToken();
        if (tok.kind == TokenEnum.TEOF) {
            error("permature end of input");
        }
        markLocation();
        if (isType(tok)) {
            readDecl(list, false);
        } else {
            Node stmt = readStmt();
            if (stmt != null) {
                list.add(stmt);
            }
        }
    }

    public void markLocation() {
        Token tok = peekToken();
        sourceLocation = new SourceLocation();
        sourceLocation.file = tok.file.name;
        sourceLocation.line = tok.line;
    }

    public Node readStmtExpr() {
        Node r = readCompoundStmt();
        expect(Keyword.C_RPAREN);
        Type rtype = Globals.typedefs.get("void");
        if (r.cast(NodeCompoundStatement.class).statements.size() > 0) {
            Node lastExpr = r.cast(NodeCompoundStatement.class).statements.get(r.cast(NodeCompoundStatement.class).statements.size() - 1);
            if (lastExpr.type != null) {
                rtype = lastExpr.type;
            }
        }
        r.type = rtype;
        return r;
    }

    public void ensureNotVoid(Type ty) {
        if (ty.kind == TypeKind.KIND_VOID) {
            error("void is not allowed");
        }
    }

    public void readStaticLocalVar(Type ty, String name) {
        Node var = Node.astStaticLVar(ty, name);
        List<Node> init = null;
        if (nextToken(Keyword.C_EQ)) {
            Map<String, Node> orig = Globals.localEnv;
            Globals.localEnv = null;
            init = readDeclInit(ty);
            Globals.localEnv = orig;
        }
        Globals.topLevels.add(Node.astDecl(var, init));
    }

    public void readDecl(List<Node> block, boolean isGlobal) {
        DataRef<DomesticType> sclass = new DataRef<>(null);
        Type baseType = readDeclSpecOpt(sclass);
        if (nextToken(Keyword.C_SEMICOLON)) {
            return;
        }
        while (true) {
            DataRef<String> name = new DataRef<>(null);
            Type ty = readDeclarator(name, Type.copy(baseType), null, Decl.DECL_BODY);
            ty.isStatic = sclass.getData() == DomesticType.S_STATIC;
            if (sclass.getData() == DomesticType.S_TYPEDEF) {
                Node.astTypedef(ty, name.getData());
            } else if (ty.isStatic && !isGlobal) {
                ensureNotVoid(ty);
                readStaticLocalVar(ty, name.getData());
            } else {
                ensureNotVoid(ty);
                Node var = (isGlobal ? Node.astGVar(ty, name.getData()) : Node.astLVar(ty, name.getData()));
                if (nextToken(Keyword.C_EQ)) {
                    block.add(Node.astDecl(var, readDeclInit(ty)));
                } else if (sclass.getData() != DomesticType.S_EXTERN && ty.kind != TypeKind.KIND_FUNC) {
                    block.add(Node.astDecl(var, null));
                }
            }
            if (nextToken(Keyword.C_SEMICOLON)) {
                return;
            }
            if (!nextToken(Keyword.C_COMMA)) {
                error(peekToken(), "; or , are expected but got %s", peekToken().toString());
            }
        }
    }

    public Type readDeclSpecOpt(DataRef<DomesticType> sclass) {
        if (isType(peekToken())) {
            return readDeclSpec(sclass);
        }

        warning(peekToken(), "type specifier missing, assuming int");

        return Globals.typedefs.get("int");
    }

    public List<Node> readDeclInit(Type ty) {
        List<Node> r = new ArrayList<>();
        if (isKeyword(peekToken(), Keyword.C_LBRACE) || Type.isString(ty)) {
            readInitializerList(r, ty, 0, false);
        } else {
            Node init = Node.conv(readAssigmentExpr());
            if (Node.isArithType(init.type) && init.type.kind != ty.kind) {
                init = Node.astConv(ty, init);
            }
            /*else if (init.type.kind == TypeKind.KIND_PTR) {
                if (init.type.pointer.kind == ty.pointer.kind) {
                    init = Node.astConv(ty, init);
                }
            }*/
            r.add(Node.astInit(init, ty, 0));
        }

        return r;
    }

    public void assignString(List<Node> inits, Type ty, String p, int off) {
        if (ty.len == -1) {
            ty.len = ty.size = p.length() + 1;
        }
        int i = 0;
        for (; i < ty.len && i < p.length(); i++) {
            inits.add(Node.astInit(Node.astIntType(Globals.typedefs.get("char"), (int) p.charAt(i)), Globals.typedefs.get("char"), off + i));
        }
        for (; i < ty.len; i++) {
            inits.add(Node.astInit(Node.astIntType(Globals.typedefs.get("char"), 0), Globals.typedefs.get("char"), off + i));
        }
    }

    public void readInitializerList(List<Node> inits, Type ty, int off, boolean designated) {
        Token tok = getToken();
        if (Type.isString(ty)) {
            if (tok.kind == TokenEnum.TSTRING) {
                assignString(inits, ty, tok.sval, off);
                return;
            }
            if (isKeyword(tok, Keyword.C_LBRACE) && peekToken().kind == TokenEnum.TSTRING) {
                tok = getToken();
                assignString(inits, ty, tok.sval, off);
                return;
            }
        }

        ungetToken(tok);
        if (ty.kind == TypeKind.KIND_ARRAY) {
            readArrayInitializer(inits, ty, off, designated);
        } else if (ty.kind == TypeKind.KIND_STRUCT) {
            readStructInitializer(inits, ty, off, designated);
        } else {
            Type arrayType = Type.makeArrayType(ty, 1);
            readArrayInitializer(inits, arrayType, off, designated);
        }
    }

    public void readStructInitializer(List<Node> inits, Type type, int off, boolean designated) {
        readStructInitializerSub(inits, type, off, designated);
        sortInits(inits);
    }

    public void sortInits(List<Node> inits) {
        Collections.sort(inits, new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                int x = o1.cast(NodeInitializer.class).initOffset;
                int y = o2.cast(NodeInitializer.class).initOffset;
                if (x < y) {
                    return -1;
                }
                if (x > y) {
                    return 1;
                }
                return 0;
            }
        });
    }

    public void readArrayInitializer(List<Node> inits, Type type, int off, boolean designated) {
        readArrayInitializerSub(inits, type, off, designated);
        sortInits(inits);
    }

    public Type readDeclaratorArray(Type baseType) {
        long len = 0;
        if (nextToken(Keyword.C_RSBRACE)) {
            len = -1;
        } else {
            len = readIntExpr();
            expect(Keyword.C_RSBRACE);
        }
        Token tok = peekToken();
        Type t = readDeclaratorTail(baseType, null);
        if (t.kind == TypeKind.KIND_FUNC) {
            error(tok, "array of function");
        }

        return Type.makeArrayType(t, (int) len);
    }

    public void readStructInitializerSub(List<Node> inits, Type type, int off, boolean designated) {
        boolean hasBrace = maybeReadBrace();
        List<String> keys = new ArrayList<>(type.fields.keySet());
        int i = 0;
        while (true) {
            Token tok = getToken();
            if (isKeyword(tok, Keyword.C_RBRACE)) {
                if (!hasBrace) {
                    ungetToken(tok);
                }
                return;
            }

            String fieldName;
            Type fieldType;
            if ((!isKeyword(tok, Keyword.C_POINT) || isKeyword(tok, Keyword.C_LSBRACE)) && !hasBrace && !designated) {
                ungetToken(tok);
                return;
            }
            if (isKeyword(tok, Keyword.C_POINT)) {
                tok = getToken();
                if (tok == null || tok.kind != TokenEnum.TIDENT) {
                    error(tok, "malformed designated initializer: %s", tok.toString());
                }
                fieldName = tok.sval;
                fieldType = type.fields.get(fieldName);
                if (fieldType == null) {
                    error(tok, "field does not exist: ", tok.toString());
                }

                keys = new ArrayList<>(type.fields.keySet());

                i = 0;
                for (String s : keys) {
                    if (s.equals(fieldName)) {
                        break;
                    }
                    i++;
                }
                designated = true;
            } else {
                ungetToken(tok);
                if (i == keys.size()) {
                    break;
                }
                fieldName = keys.get(i++);
                fieldType = type.fields.get(fieldName);
            }

            readInitializerElem(inits, fieldType, off + fieldType.offset, designated);
            maybeSkipComma();
            designated = false;
            if (!type.isStruct) {
                break;
            }
        }
        if (hasBrace) {
            skipToBrace();
        }
    }

    public void skipToBrace() {
        while (true) {
            if (nextToken(Keyword.C_RBRACE)) {
                return;
            }
            if (nextToken(Keyword.C_POINT)) {
                getToken();
                expect(Keyword.C_EQ);
            }
            Token tok = peekToken();
            Node ignore = readAssigmentExpr();
            if (ignore == null) {
                return;
            }
            warning(tok, "excessive initializer: %s", ignore.toString());
            maybeSkipComma();
        }
    }

    public void maybeSkipComma() {
        nextToken(Keyword.C_COMMA);
    }

    public boolean maybeReadBrace() {
        return nextToken(Keyword.C_LBRACE);
    }

    public void readInitializerElem(List<Node> inits, Type type, int off, boolean designated) {
        nextToken(Keyword.C_EQ);
        if (type.kind == TypeKind.KIND_ARRAY || type.kind == TypeKind.KIND_STRUCT) {
            readInitializerList(inits, type, off, designated);
        } else if (nextToken(Keyword.C_LBRACE)) {
            readInitializerElem(inits, type, off, true);
            expect(Keyword.C_RBRACE);
        } else {
            Node expr = Node.conv(readAssigmentExpr());
            ensureAssignable(type, expr.type);
            inits.add(Node.astInit(expr, type, off));
        }
    }

    public void readArrayInitializerSub(List<Node> inits, Type type, int off, boolean designated) {
        boolean hasBrace = maybeReadBrace();
        boolean flexible = type.len <= 0;
        int elemsize = type.pointer.size;
        int i;
        for (i = 0; flexible || i < type.len; i++) {
            Token tok = getToken();
            if (isKeyword(tok, Keyword.C_RBRACE)) {
                if (!hasBrace) {
                    ungetToken(tok);
                }

                if (type.len < 0) {
                    type.len = i;
                    type.size = elemsize * i;
                }
                return;
            }
            if ((isKeyword(tok, Keyword.C_POINT) || isKeyword(tok, Keyword.C_LSBRACE)) && !hasBrace && !designated) {
                ungetToken(tok);
                return;
            }

            if (isKeyword(tok, Keyword.C_LSBRACE)) {
                tok = peekToken();
                int idx = readIntExpr();
                if (idx < 0 || (!flexible && type.len <= idx)) {
                    error(tok, "array designator exceeds array bounds: %d", idx);
                }
                i = idx;
                expect(Keyword.C_RSBRACE);
                designated = true;
            } else {
                ungetToken(tok);
            }

            readInitializerElem(inits, type.pointer, off + elemsize * i, designated);
            maybeSkipComma();
            designated = false;
        }
        if (hasBrace) {
            skipToBrace();
        }
        if (type.len < 0) {
            type.len = i;
            type.size = elemsize * i;
        }
    }

    public int readIntExpr() {
        return (int) evalIntExpr(readConditionalExpr(), null);
    }

    public static boolean isSameStruct(Type toType, Type fromType) {
        if (toType.kind != fromType.kind) {
            return false;
        }

        switch (toType.kind) {
            case KIND_ARRAY:
                return toType.len == fromType.len && isSameStruct(toType.pointer, fromType.pointer);
            case KIND_PTR:
                return isSameStruct(toType.pointer, fromType.pointer);
            case KIND_STRUCT:
                if (toType.isStruct != fromType.isStruct) {
                    return false;
                }
                Set<String> ka = toType.fields.keySet();
                Set<String> kb = fromType.fields.keySet();

                if (ka.size() != kb.size()) {
                    return false;
                }

                for (String key : ka) {
                    if (!isSameStruct(toType.fields.get(key), fromType.fields.get(key))) {
                        return false;
                    }
                }
                return true;
            default:
                return true;
        }
    }

    public void ensureAssignable(Type toType, Type fromType) {
        if ((Node.isArithType(toType) || toType.kind == TypeKind.KIND_PTR) && (Node.isArithType(fromType) || fromType.kind == TypeKind.KIND_PTR)) {
            return;
        }
        if (isSameStruct(toType, fromType)) {
            return;
        }
        errorC("incopatible kind: <%s> <%s>", toType.toString(), fromType.toString());
    }

    public Node readBooleanExpr() {
        Node cond = readExpr();
        return Node.isFloType(cond.type) ? Node.astConv(Globals.typedefs.get("int"), cond) : cond;
    }

    public Node readIfStmt() {
        expect(Keyword.C_LPAREN);
        Node cond = readBooleanExpr();
        expect(Keyword.C_RPAREN);
        Node then = readStmt();
        if (!nextToken(Keyword.KELSE)) {
            return Node.astIf(cond, then, null);
        }

        Node els = readStmt();

        return Node.astIf(cond, then, els);
    }

    public Node readWhileStmt() {
        expect(Keyword.C_LPAREN);
        Node cond = readBooleanExpr();
        expect(Keyword.C_RPAREN);

        String beg = Globals.makeLabel();
        String end = Globals.makeLabel();
        String obreak = lbreak;
        String ocontinue = lcontinue;

        lbreak = end;
        lcontinue = beg;

        Node body = readStmt();

        lbreak = obreak;
        lcontinue = ocontinue;

        List<Node> nodes = new ArrayList<>();
        nodes.add(Node.astDest(beg));
        nodes.add(Node.astIf(cond, body, Node.astJump(end)));
        nodes.add(Node.astJump(beg));
        nodes.add(Node.astDest(end));

        return Node.astCompoundStmt(nodes);
    }

    public Node readStmt() {
        Token tok = getToken();
        if (tok.kind == TokenEnum.TKEYWORD) {
            switch (tok.id) {
                case C_LBRACE:
                    return readCompoundStmt();
                case KIF:
                    return readIfStmt();
                case KFOR:
                    return readForStmt();
                case KWHILE:
                    return readWhileStmt();
                case KDO:
                    return readDoStmt();
                case KRETURN:
                    return readReturnStmt();
                case KSWITCH:
                    return readSwitchStmt();
                case KCASE:
                    return readCaseLabel(tok);
                case KDEFAULT:
                    return readDefaultLabel(tok);
                case KBREAK:
                    return readBreakStmt(tok);
                case KCONTINUE:
                    return readContinueStmt(tok);
                case KGOTO:
                    return readGotoStmt();
                case KASM:
                    return readAsmStmt();
            }
        }
        if (tok.kind == TokenEnum.TIDENT && nextToken(Keyword.C_COLON)) {
            return readLabel(tok);
        }
        ungetToken(tok);
        Node r = readExprOpt();
        expect(Keyword.C_SEMICOLON);
        return r;
    }

    public Node readGotoStmt() {
        if (nextToken(Keyword.C_MUL)) {
            Token tok = peekToken();
            Node expr = readCastExpr();
            if (expr.type.kind != TypeKind.KIND_PTR) {
                error(tok, "pointer expected for computed goto but got %s", expr.toString());
            }

            return Node.astComputedGoto(expr);
        }

        Token tok = getToken();

        if (tok == null || tok.kind != TokenEnum.TIDENT) {
            error(tok, "identifier expected, but got %s", tok != null ? tok.toString() : "(null)");
        }

        expect(Keyword.C_SEMICOLON);
        Node r = Node.astGoto(tok.sval);
        Globals.gotos.add(r);
        return r;
    }

    public Node readOptDeclOrStmt() {
        if (nextToken(Keyword.C_SEMICOLON)) {
            return null;
        }
        List<Node> list = new ArrayList<>();
        readDeclOrStmt(list);
        return Node.astCompoundStmt(list);
    }

    public Node readExprOpt() {
        return readCommaExpr();
    }

    public Node readForStmt() {
        expect(Keyword.C_LPAREN);
        String beg = Globals.makeLabel();
        String mid = Globals.makeLabel();
        String end = Globals.makeLabel();
        Map<String, Node> orig = Globals.localEnv;
        Globals.localEnv = new LinearMap<>(Globals.localEnv);

        Node init = readOptDeclOrStmt();
        Node cond = readExprOpt();
        if (cond != null && Node.isFloType(cond.type)) {
            cond = Node.astConv(Globals.typedefs.get("int"), cond);
        }
        expect(Keyword.C_SEMICOLON);
        Node step = readExprOpt();
        expect(Keyword.C_RPAREN);

        String ocontinue = lcontinue;
        String obreak = lbreak;
        lbreak = end;
        lcontinue = mid;

        Node body = readStmt();

        lcontinue = ocontinue;
        lbreak = obreak;

        Globals.localEnv = orig;

        List<Node> v = new ArrayList<>();
        if (init != null) {
            v.add(init);
        }
        v.add(Node.astDest(beg));
        if (cond != null) {
            v.add(Node.astIf(cond, null, Node.astJump(end)));
        }
        if (body != null) {
            v.add(body);
        }
        v.add(Node.astDest(mid));
        if (step != null) {
            v.add(step);
        }

        v.add(Node.astJump(beg));
        v.add(Node.astDest(end));

        return Node.astCompoundStmt(v);
    }

    public Node readStructField(Node struc) {
        if (struc.type.kind != TypeKind.KIND_STRUCT) {
            error("struct expected, but got %s", struc.toString());
        }

        Token name = getToken();
        if (name.kind != TokenEnum.TIDENT) {
            error("field name expected, but got %s", name.toString());
        }

        Type field = struc.type.fields.get(name.sval);

        if (field == null) {
            error("struct has no such field: %s", name.toString());
        }

        return Node.astStructRef(field, struc, name.sval);
    }

    public Node readLabel(Token tok) {
        String label = tok.sval;
        if (Globals.labels.containsKey(label)) {
            error(tok, "duplicate label: %s", tok.toString());
        }

        Node r = Node.astLabel(label);
        Globals.labels.put(label, r);
        return readLabelTail(r);
    }

    public Node readLabelTail(Node label) {
        Node stmt = readStmt();
        List<Node> v = new ArrayList<>();
        v.add(label);
        if (stmt != null) {
            v.add(stmt);
        }

        return Node.astCompoundStmt(v);
    }

    public Node readDoStmt() {
        String beg = Globals.makeLabel();
        String end = Globals.makeLabel();

        String ocontinue = lcontinue;
        String obreak = lbreak;
        lbreak = end;
        lcontinue = end;
        Node body = readStmt();
        lcontinue = ocontinue;
        lbreak = obreak;

        Token tok = getToken();
        if (!isKeyword(tok, Keyword.KWHILE)) {
            error(tok, "'while' is expected, but gor %s", tok.toString());
        }

        expect(Keyword.C_LPAREN);
        Node cond = readBooleanExpr();
        expect(Keyword.C_RPAREN);
        expect(Keyword.C_SEMICOLON);

        List<Node> v = new ArrayList<>();

        v.add(Node.astDest(beg));

        if (body != null) {
            v.add(body);
        }

        v.add(Node.astIf(cond, Node.astJump(beg), null));
        v.add(Node.astDest(end));

        return Node.astCompoundStmt(v);
    }

    public Node readReturnStmt() {
        Node retval = readExprOpt();
        expect(Keyword.C_SEMICOLON);
        if (retval != null) {
            return Node.astReturn(Node.astConv(currentFuncType.returnType, retval));
        }

        return Node.astReturn(null);
    }

    public Node makeSwitchJump(Node var, Case c) {
        Node cond;
        if (c.beg == c.end) {
            cond = Node.astBinOp(Globals.typedefs.get("int"), Keyword.OP_EQ, var, Node.astIntType(Globals.typedefs.get("int"), c.beg));
        } else {
            Node x = Node.astBinOp(Globals.typedefs.get("int"), Keyword.OP_LE, Node.astIntType(Globals.typedefs.get("int"), c.beg), var);
            Node y = Node.astBinOp(Globals.typedefs.get("int"), Keyword.OP_LE, var, Node.astIntType(Globals.typedefs.get("int"), c.end));
            cond = Node.astBinOp(Globals.typedefs.get("int"), Keyword.OP_LOGAND, x, y);
        }

        return Node.astIf(cond, Node.astJump(c.label), null);
    }

    public Node readSwitchStmt() {
        expect(Keyword.C_LPAREN);
        Node expr = Node.conv(readExpr());
        ensureIntType(expr);
        expect(Keyword.C_RPAREN);

        String end = Globals.makeLabel();

        List<Case> ocases = cases;
        String odefaultcase = defaultcase;
        String obreak = lbreak;
        cases = new ArrayList<>();
        defaultcase = null;
        lbreak = end;

        Node body = readStmt();
        List<Node> v = new ArrayList<>();
        Node var = Node.astLVar(expr.type, Globals.makeTempname());
        v.add(Node.astBinOp(expr.type, Keyword.C_EQ, var, expr));
        for (int i = 0; i < cases.size(); i++) {
            v.add(makeSwitchJump(var, cases.get(i)));
        }

        v.add(Node.astJump(defaultcase != null ? defaultcase : end));

        if (body != null) {
            v.add(body);
        }

        v.add(Node.astDest(end));

        cases = ocases;
        defaultcase = odefaultcase;
        lbreak = obreak;

        return Node.astCompoundStmt(v);
    }

    public Case makeCase(int beg, int end, String label) {
        Case c = new Case();
        c.beg = beg;
        c.end = end;
        c.label = label;
        return c;
    }

    public void checkCaseDuplicates(List<Case> cases) {
        int len = cases.size();
        Case x = cases.get(len - 1);
        for (int i = 0; i < len - 1; i++) {
            Case y = cases.get(i);
            if (x.end < y.beg || y.end < x.beg) {
                continue;
            }

            if (x.beg == y.beg) {
                error("duplicaate case value: %d", x.beg);
            }
            error("duplicaate case value: %d ... %d", x.beg, x.end);
        }
    }

    public Node readCaseLabel(Token tok) {
        if (cases == null) {
            error(tok, "stray case label");
        }

        String label = Globals.makeLabel();
        int beg = readIntExpr();
        if (nextToken(Keyword.KELLIPSIS)) {
            int end = readIntExpr();
            expect(Keyword.C_COLON);
            if (beg > end) {
                error(tok, "case region is not in correct order: %d ... %d", beg, end);
            }
            cases.add(makeCase(beg, end, label));
        } else {
            expect(Keyword.C_COLON);
            cases.add(makeCase(beg, beg, label));
        }

        checkCaseDuplicates(cases);
        return readLabelTail(Node.astDest(label));
    }

    public Node readDefaultLabel(Token tok) {
        expect(Keyword.C_COLON);
        if (defaultcase != null) {
            error(tok, "duplicate default");
        }

        defaultcase = Globals.makeLabel();
        return readLabelTail(Node.astDest(defaultcase));
    }

    public Node readBreakStmt(Token tok) {
        expect(Keyword.C_SEMICOLON);
        if (lbreak == null) {
            error(tok, "stray break statement");
        }

        return Node.astJump(lbreak);
    }

    public Node readContinueStmt(Token tok) {
        expect(Keyword.C_SEMICOLON);
        if (lcontinue == null) {
            error(tok, "stray continue statement");
        }

        return Node.astJump(lcontinue);
    }

    private Node readAsmStmt() {
        expect(Keyword.C_LPAREN);

        Token t = getToken();
        if (t.kind != TokenEnum.TSTRING) {
            error(t, "Expected string but got %s", t);
        }

        expect(Keyword.C_RPAREN);

        return Node.astAsm(t.sval);
    }

}
