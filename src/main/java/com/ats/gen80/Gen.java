/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.Keyword;
import com.ats.gen80.node.Node;
import com.ats.gen80.node.NodeBinary;
import com.ats.gen80.node.NodeCompoundStatement;
import com.ats.gen80.node.NodeDeclaration;
import com.ats.gen80.node.NodeFloat;
import com.ats.gen80.node.NodeFunctionDeclaration;
import com.ats.gen80.node.NodeGlobalLabel;
import com.ats.gen80.node.NodeInitializer;
import com.ats.gen80.node.NodeInteger;
import com.ats.gen80.node.NodeLocalLabel;
import com.ats.gen80.node.NodeString;
import com.ats.gen80.node.NodeStruct;
import com.ats.gen80.node.NodeUnary;
import com.ats.gen80.node.NodeVariable;
import com.ats.gen80.node.Type;
import com.ats.gen80.node.TypeKind;
import com.ats.gen80.opcodes.Opcode;
import com.ats.gen80.opcodes.Register;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author lucifer
 */
public class Gen {

    public List<Opcode> opcodes = new ArrayList<>();
    public int stack = 0;

    public Gen label(String label) {
        opcodes.add(new Opcode.Label(label));
        return this;
    }

    public Gen mvi(Register reg, int data) {
        opcodes.add(Opcode.Mnemonic.MVI.create(reg, data));
        return this;
    }

    public Gen cpi(int data) {
        opcodes.add(Opcode.Mnemonic.CPI.create(data));
        return this;
    }

    public Gen ori(int data) {
        opcodes.add(Opcode.Mnemonic.ORI.create(data));
        return this;
    }

    public Gen xri(int data) {
        opcodes.add(Opcode.Mnemonic.XRI.create(data));
        return this;
    }

    public Gen ani(int data) {
        opcodes.add(Opcode.Mnemonic.ANI.create(data));
        return this;
    }

    public Gen adi(int data) {
        opcodes.add(Opcode.Mnemonic.ADI.create(data));
        return this;
    }

    public Gen sbi(int data) {
        opcodes.add(Opcode.Mnemonic.SBI.create(data));
        return this;
    }

    public Gen mov(Register reg1, Register reg2) {
        opcodes.add(Opcode.Mnemonic.MOV.create(reg1, reg2));
        return this;
    }

    public Gen lxi(Register reg, int data) {
        opcodes.add(Opcode.Mnemonic.LXI.create(reg, data));
        return this;
    }

    public Gen lxi(Register reg, String data) {
        opcodes.add(Opcode.Mnemonic.LXI.create(reg, data));
        return this;
    }

    public Gen push(Register reg) {
        stack += 2;

        opcodes.add(Opcode.Mnemonic.PUSH.create(reg));
        return this;
    }

    public Gen pop(Register reg) {
        stack -= 2;
        opcodes.add(Opcode.Mnemonic.POP.create(reg));
        return this;
    }

    public Gen dad(Register reg) {
        opcodes.add(Opcode.Mnemonic.DAD.create(reg));
        return this;
    }

    public Gen stax(Register reg) {
        opcodes.add(Opcode.Mnemonic.STAX.create(reg));
        return this;
    }

    public Gen ldax(Register reg) {
        opcodes.add(Opcode.Mnemonic.LDAX.create(reg));
        return this;
    }

    public Gen ora(Register reg) {
        opcodes.add(Opcode.Mnemonic.ORA.create(reg));
        return this;
    }

    public Gen sbb(Register reg) {
        opcodes.add(Opcode.Mnemonic.SBB.create(reg));
        return this;
    }

    public Gen ana(Register reg) {
        opcodes.add(Opcode.Mnemonic.ANA.create(reg));
        return this;
    }

    public Gen xra(Register reg) {
        opcodes.add(Opcode.Mnemonic.XRA.create(reg));
        return this;
    }

    public Gen jmp(String label) {
        opcodes.add(Opcode.Mnemonic.JMP.create(label));
        return this;
    }

    public Gen jz(String label) {
        opcodes.add(Opcode.Mnemonic.JZ.create(label));
        return this;
    }

    public Gen jnz(String label) {
        opcodes.add(Opcode.Mnemonic.JNZ.create(label));
        return this;
    }

    public Gen call(String label) {
        opcodes.add(Opcode.Mnemonic.CALL.create(label));
        return this;
    }

    public Gen cz(String label) {
        opcodes.add(Opcode.Mnemonic.CZ.create(label));
        return this;
    }

    public Gen cnz(String label) {
        opcodes.add(Opcode.Mnemonic.CNZ.create(label));
        return this;
    }

    public Gen xchg() {
        opcodes.add(Opcode.Mnemonic.XCHG.create());
        return this;
    }

    public Gen xthl() {
        opcodes.add(Opcode.Mnemonic.XTHL.create());
        return this;
    }

    public Gen pchl() {
        opcodes.add(Opcode.Mnemonic.PCHL.create());
        return this;
    }

    public Gen sphl() {
        opcodes.add(Opcode.Mnemonic.SPHL.create());
        return this;
    }

    public Gen ret() {
        opcodes.add(Opcode.Mnemonic.RET.create());
        return this;
    }

    public Gen rlc() {
        opcodes.add(Opcode.Mnemonic.RLC.create());
        return this;
    }

    public Gen lda(String label) {
        opcodes.add(Opcode.Mnemonic.LDA.create(label));
        return this;
    }

    public Gen sta(String label) {
        opcodes.add(Opcode.Mnemonic.STA.create(label));
        return this;
    }

    public Gen lhld(String label) {
        opcodes.add(Opcode.Mnemonic.LHLD.create(label));
        return this;
    }

    public Gen shld(String label) {
        opcodes.add(Opcode.Mnemonic.SHLD.create(label));
        return this;
    }

    public Gen inx(Register reg) {
        opcodes.add(Opcode.Mnemonic.INX.create(reg));
        return this;
    }

    public Gen dcx(Register reg) {
        opcodes.add(Opcode.Mnemonic.DCX.create(reg));
        return this;
    }

    public void genProlog(NodeFunctionDeclaration n) {
        int val = 0;
        int localoff = 0;
        for (Node l : n.localVariables) {
            val += l.cast(NodeLocalLabel.class).type.size;
            l.cast(NodeLocalLabel.class).localOffset = localoff;
            localoff += l.cast(NodeLocalLabel.class).type.size;
        }

        stack += val;

        if (stack != 0) {
            lxi(Register.HL, -stack);
            dad(Register.SP);
            sphl();
        }
    }

    public void genEpilog(NodeFunctionDeclaration n) {
        // TODO: Need to know how we should modify stack
        if (stack != 0) {
            lxi(Register.HL, stack);
            dad(Register.SP);
            sphl();
            stack = 0;
        }
        ret();

    }

    public Gen emitExpr(Node node) {
        System.out.println(node.kind);
        switch (node.kind) {
            case AST_LITERAL:
                emitLiteral(node);
                return this;
            case AST_LVAR:
                emitLVar(node);
                return this;
//            case AST_GVAR:
//                emitGVar(node);
//                return this;
//            case AST_FUNCDESG:
//                return this;
//            case AST_FUNCALL:
//                if (maybeEmitBuiltin(node)) {
//                    return this;
//                }
//            case AST_FUNCPTR_CALL:
//                emitFuncCall(node);
//                return this;
            case AST_DECL:
                emitDecl(node);
                return this;
            case AST_CONV:
                emitConv(node);
                return this;
//            case AST_ADDR:
//                emit_addr(node.cast(NodeUnary.class).operand);
//                return this;
//            case AST_DEREF:
//                emit_deref(node);
//                return this;
//            case AST_IF:
//            case AST_TERNARY:
//                emit_ternary(node);
//                return this;
//            case AST_GOTO:
//                emit_goto(node);
//                return this;
//            case AST_LABEL:
//                if (node.cast(NodeLabel.class).newLabel != null) {
//                    emit_label(node.cast(NodeLabel.class).newLabel);
//                }
//                return this;
//            case AST_RETURN:
//                emit_return(node);
//                return this;
            case AST_COMPOUND_STMT:
                emitCompoundStmt(node);
                return this;
//            case AST_STRUCT_REF:
//                emit_load_struct_ref(node -> struc, node -> ty, 0);
//                return this;
//            case OP_PRE_INC:
//                emit_pre_inc_dec(node, "add");
//                return this;
//            case OP_PRE_DEC:
//                emit_pre_inc_dec(node, "sub");
//                return this;
//            case OP_POST_INC:
//                emit_post_inc_dec(node, "add");
//                return this;
//            case OP_POST_DEC:
//                emit_post_inc_dec(node, "sub");
//                return this;
//            case C_EXPOINT:
//                emit_lognot(node);
//                return this;
//            case C_AND:
//                emit_bitand(node);
//                return this;
//            case C_OR:
//                emit_bitor(node);
//                return this;
//            case C_TILDA:
//                emit_bitnot(node);
//                return this;
//            case OP_LOGAND:
//                emit_logand(node);
//                return this;
//            case OP_LOGOR:
//                emit_logor(node);
//                return this;
//            case OP_CAST:
//                emit_cast(node);
//                return this;
//            case C_COMMA:
//                emit_comma(node);
//                return this;
            case C_EQ:
                emitAssign(node);
                return this;
//            case OP_LABEL_ADDR:
//                emit_label_addr(node);
//                return this;
//            case AST_COMPUTED_GOTO:
//                emit_computed_goto(node);
//                return this;
            default:
                emitBinOp(node);
        }
        return this;
    }

    public Gen emitFunc(Node node) {
        NodeFunctionDeclaration n = node.cast();
        label(n.functionName);

        genProlog(n);
        emitExpr(n.functionBody);
        genEpilog(n);

        return this;
    }

    public Gen emitNode(Node node) {

        if (node.kind == Keyword.AST_FUNC) {
            emitFunc(node);
        }

        return this;
    }

    private void emitLiteral(Node node) {
        switch (node.type.kind) {
            case KIND_CHAR:
                mvi(Register.D, node.cast(NodeInteger.class).val);
                mvi(Register.E, 0);
                break;
            case KIND_INT:
                lxi(Register.DE, node.cast(NodeInteger.class).val);
                break;
            case KIND_LONG:
                lxi(Register.DE, node.cast(NodeInteger.class).val & 0xffff);
                lxi(Register.BC, (node.cast(NodeInteger.class).val >> 16) & 0xffff);
                break;
            case KIND_FLOAT:
                float t = (float) node.cast(NodeFloat.class).val;
                int val = Float.floatToIntBits(t);
                lxi(Register.DE, val & 0xffff);
                lxi(Register.BC, (val >> 16) & 0xffff);
                break;
            case KIND_ARRAY:
                String name = node.cast(NodeString.class).name = Globals.makeLabel();
                lxi(Register.DE, name);
                break;
        }
    }

    private void emitCompoundStmt(Node node) {
        for (Node n : node.cast(NodeCompoundStatement.class).statements) {
            emitExpr(n);
        }
    }

    private void emitAssign(Node node) {
        NodeBinary n = node.cast(NodeBinary.class);

        if (n.left.type.kind == TypeKind.KIND_STRUCT && n.left.type.size > 2) {
            emitCopyStruct(n.left, n.right);
        } else {
            emitExpr(n.right);
            emitLoadConvert(n.type, n.right.type);
            emitStore(n.left);
        }
    }

    private void emitDecl(Node node) {
        if (node.cast(NodeDeclaration.class).declarationInit == null) {
            return;
        }
        emitDeclInit(node.cast(NodeDeclaration.class).declarationInit, node.cast(NodeDeclaration.class).declarationVariable.cast(NodeLocalLabel.class).localOffset, node.cast(NodeDeclaration.class).declarationVariable.type.size);
    }

    private void emitDeclInit(List<Node> declarationInit, int localOffset, int size) {
        emitFillHoles(declarationInit, localOffset, size);
        for (Node n : declarationInit) {
            NodeInitializer node = n.cast(NodeInitializer.class);
            boolean isBitField = node.toType.bitSize > 0;
            if (node.initValue.kind == Keyword.AST_LITERAL && !isBitField) {
                emitSaveLiteral(node.initValue, node.toType, node.initOffset + localOffset);
            } else {
                emitExpr(node.initValue);
                emitLSave(node.toType, node.initOffset + localOffset);
            }
        }
    }

    private void emitFillHoles(List<Node> declarationInit, int localOffset, int size) {
        List<Node> buf = new ArrayList<>(declarationInit);
        Collections.sort(buf, new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                return o1.cast(NodeInitializer.class).initOffset < o2.cast(NodeInitializer.class).initOffset ? -1
                        : o1.cast(NodeInitializer.class).initOffset > o2.cast(NodeInitializer.class).initOffset ? 1 : 0;
            }
        });

        int lastEnd = 0;
        for (Node node : buf) {
            if (lastEnd < node.cast(NodeInitializer.class).initOffset) {
                emitZeroFiller(lastEnd + localOffset, node.cast(NodeInitializer.class).initOffset + localOffset);
            }
            lastEnd = node.cast(NodeInitializer.class).initOffset + node.cast(NodeInitializer.class).toType.size;
        }
        emitZeroFiller(lastEnd + localOffset, size + localOffset);
    }

    private void emitSaveLiteral(Node initValue, Type toType, int i) {
        NodeInitializer node = initValue.cast(NodeInitializer.class);

        switch (toType.kind) {
            case KIND_CHAR:
//                mvi(Register.A, node.cast(NodeInteger.class).val);
                lxi(Register.HL, stack - i);
                dad(Register.SP);
                mvi(Register.M, node.cast(NodeInteger.class).val);
                break;
            case KIND_INT:
                lxi(Register.DE, node.cast(NodeInteger.class).val);
                lxi(Register.HL, stack - i);
                dad(Register.SP);
                call(".pint");
                break;
            case KIND_LONG:
                lxi(Register.BC, node.cast(NodeInteger.class).val >> 16);
                lxi(Register.DE, node.cast(NodeInteger.class).val);
                lxi(Register.HL, stack - i);
                dad(Register.SP);
                call(".plong");
                break;
            case KIND_FLOAT:
                int val = Float.floatToIntBits((float) node.cast(NodeFloat.class).val);
                lxi(Register.BC, val >> 16);
                lxi(Register.DE, val);
                lxi(Register.HL, stack - i);
                dad(Register.SP);
                call(".pfloat");
                break;
        }
    }

    private void emitZeroFiller(int i, int i0) {

    }

    private void emitLSave(Type toType, int i) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void emitConv(Node node) {
        emitExpr(node.cast(NodeUnary.class).operand);
        emitLoadConvert(node.type, node.cast(NodeUnary.class).operand.type);
    }

    private void emitLoadConvert(Type to, Type from) {
        if (Node.isIntType(from) && to.kind == TypeKind.KIND_FLOAT) {

        } else if (Node.isIntType(from) && Node.isIntType(to)) {
            emitIntCast(from);
        }
    }

    private void emitIntCast(Type from) {
        switch (from.kind) {
            case KIND_CHAR:
                lxi(Register.BC, 0);
                if (from.unsigned) {
                    mvi(Register.D, 0);
                } else {
                    emitSxt();
                }
                break;
            case KIND_INT:
                lxi(Register.BC, 0);
                if (from.unsigned) {
                } else {
                    emitSxt();
                }
                break;
        }
    }

    private void emitSxt() {
        mov(Register.A, Register.E);
        rlc();
        sbb(Register.A);
        mov(Register.D, Register.A);
    }

    private void emitLVar(Node node) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        emitLLoad(node.type, node.cast(NodeGlobalLabel.class).);
    }

    private void emitLLoad(Type type, NodeGlobalLabel cast) {
        if (type.kind == TypeKind.KIND_ARRAY) {

        }
    }

    private void emitCopyStruct(Node left, Node right) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void emitStore(Node var) {
        switch (var.kind) {
            case AST_DEREF:
                emitAssignDeref(var);
                break;
            case AST_STRUCT_REF:
                emitAssignStructRef(var.cast(NodeStruct.class).struct, var.type);
                break;
            case AST_LVAR:
                ensureLVarInit(var);
                emitLSave(var.type, var.cast(NodeLocalLabel.class).localOffset);
                break;
            case AST_GVAR:
                emitGSave(var.cast(NodeGlobalLabel.class).globaLabel, var.type, 0);
                break;
            default:
                throw new RuntimeException("Internal error");
        }
    }

    private void emitAssignDeref(Node var) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void emitAssignStructRef(Node struct, Type type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void ensureLVarInit(Node var) {
        NodeLocalLabel nl = var.cast(NodeLocalLabel.class);
        if (nl.localVariableInit != null) {
            emitDeclInit(nl.localVariableInit, nl.localOffset, nl.type.size);
        }
        nl.localVariableInit = null;
    }

    private void emitGSave(String globaLabel, Type type, int i) {
        if (i != 0) {
            lxi(Register.HL, globaLabel + "+" + i);
        } else {
            lxi(Register.HL, globaLabel);
        }

        switch (type.kind) {
            case KIND_CHAR:
                mov(Register.M, Register.E);
                break;
            case KIND_INT:
                call(".pint");
                break;
            case KIND_LONG:
                call(".plong");
                break;
            case KIND_FLOAT:
                call(".pfloat");
                break;
        }
    }

    private void emitBinOp(Node node) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
