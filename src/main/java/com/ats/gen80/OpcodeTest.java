/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

/**
 *
 * @author lucifer
 */
public class OpcodeTest {

    protected RegisterTest dstReg;
    protected RegisterTest srcReg;
    protected int align;
    protected int opcode;
    protected String sym;
    protected int data;
    protected String mnemonic;
    protected int opcodeSize;

    public int alignDestReg(int op, RegisterTest reg) {
        return op | (reg.dst << 3);
    }

    public int alignDestSrcReg(int op, RegisterTest dst, RegisterTest src) {
        return op | (dst.dst << 3) | src.src;
    }

    public boolean is8Bit(RegisterTest reg) {
        return reg.bits == 8;
    }

    public boolean is16Bit(RegisterTest reg) {
        return reg.bits == 16;
    }

    public int alignData(int data) {
        return data & (align == 8 ? 0xFF : 0xFFFF);
    }

    public void fillCode(StringBuilder sb) {
        if (mnemonic == null) {
            return;
        }
        sb.append("\t");
        sb.append(String.format("%-8s", mnemonic));

        if (dstReg != null) {
            sb.append(dstReg.regMnem);
            if (opcodeSize > 1) {
                sb.append(",");
            }
        }

        if (srcReg != null) {
            sb.append(srcReg.regMnem);
        } else {
            if (opcodeSize > 1) {
                if (sym != null) {
                    sb.append(sym);
                } else {
                    sb.append(toHex(alignData(data)));
                }
            }
        }

        sb.append("\n");

    }

    public String toHex(int data) {
        String ret = String.format("%0" + (opcodeSize == 2 ? "2" : "4") + "xH", data).toUpperCase();
        if (ret.charAt(0) >= 'A' && ret.charAt(0) <= 'F') {
            ret = "0" + ret;
        }

        return ret;
    }

    public static enum Mnemonic {

        ADI("ADI", 0xC6, 2, false, false, 8, new RegisterTest[]{}),
        ACI("ACI", 0xCE, 2, false, false, 8, new RegisterTest[]{}),
        ANI("ANI", 0xE6, 2, false, false, 8, new RegisterTest[]{}),
        XRI("XRI", 0xEE, 2, false, false, 8, new RegisterTest[]{}),
        ORI("ORI", 0xF6, 2, false, false, 8, new RegisterTest[]{}),
        CPI("CPI", 0xFE, 2, false, false, 8, new RegisterTest[]{}),
        CMA("CMA", 0x2F, 1, false, false, 8, new RegisterTest[]{}),
        CMC("CMC", 0x3F, 1, false, false, 8, new RegisterTest[]{}),
        DAA("DAA", 0x27, 1, false, false, 8, new RegisterTest[]{}),
        DCR("DCR", 0x05, 1, true, false, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        INR("INR", 0x04, 1, true, false, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        DAD("DAD", 0x09, 1, false, false, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE, RegisterTest.HL, RegisterTest.SP}),
        LDAX("LDAX", 0x0A, 3, false, true, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE}),
        STAX("STAX", 0x02, 3, false, true, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE}),
        LHLD("LHLD", 0x2A, 3, false, false, 16, new RegisterTest[]{}),
        SHLD("SHLD", 0x22, 3, false, false, 16, new RegisterTest[]{}),
        INX("INX", 0x03, 1, false, true, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE, RegisterTest.HL, RegisterTest.SP}),
        DCX("DCX", 0x0B, 1, false, true, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE, RegisterTest.HL, RegisterTest.SP}),
        PUSH("PUSH", 0xC5, 1, false, true, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE, RegisterTest.HL, RegisterTest.PSW}),
        POP("POP", 0xC1, 1, false, true, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE, RegisterTest.HL, RegisterTest.PSW}),
        DI("DI", 0xF3, 1, false, false, 8, new RegisterTest[]{}),
        EI("EI", 0xE3, 1, false, false, 8, new RegisterTest[]{}),
        HLT("HLT", 0x76, 1, false, false, 8, new RegisterTest[]{}),
        PCHL("PCHL", 0xE9, 1, false, false, 8, new RegisterTest[]{}),
        IN("IN", 0xDB, 2, false, false, 8, new RegisterTest[]{}),
        OUT("OUT", 0xD3, 2, false, false, 8, new RegisterTest[]{}),
        ADD("ADD", 0x80, 1, false, true, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        ADC("ADC", 0x88, 1, false, true, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        CMP("CMP", 0xB8, 1, false, true, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        ANA("ANA", 0xA0, 1, false, true, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        ORA("ORA", 0xB0, 1, false, true, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        CALL("CALL", 0xCD, 3, false, false, 16, new RegisterTest[]{}),
        JMP("JMP", 0xC3, 3, false, false, 16, new RegisterTest[]{}),
        CNZ("CNC", 0xC4, 3, false, false, 16, new RegisterTest[]{}),
        CZ("CZ", 0xCC, 3, false, false, 16, new RegisterTest[]{}),
        CNC("CNC", 0xD4, 3, false, false, 16, new RegisterTest[]{}),
        CC("CC", 0xDC, 3, false, false, 16, new RegisterTest[]{}),
        CPO("CPO", 0xE4, 3, false, false, 16, new RegisterTest[]{}),
        CPE("CPE", 0xEC, 3, false, false, 16, new RegisterTest[]{}),
        CP("CP", 0xF4, 3, false, false, 16, new RegisterTest[]{}),
        CM("CM", 0xFC, 3, false, false, 16, new RegisterTest[]{}),
        JNZ("JNC", 0xC2, 3, false, false, 16, new RegisterTest[]{}),
        JZ("JZ", 0xCA, 3, false, false, 16, new RegisterTest[]{}),
        JNC("JNC", 0xD2, 3, false, false, 16, new RegisterTest[]{}),
        JC("JC", 0xDA, 3, false, false, 16, new RegisterTest[]{}),
        JPO("JPO", 0xE2, 3, false, false, 16, new RegisterTest[]{}),
        JPE("JPE", 0xEA, 3, false, false, 16, new RegisterTest[]{}),
        JP("JP", 0xF2, 3, false, false, 16, new RegisterTest[]{}),
        JM("JM", 0xFC, 3, false, false, 16, new RegisterTest[]{}),
        LDA("LDA", 0x3A, 3, false, false, 16, new RegisterTest[]{}),
        LXI("LXI", 0x01, 3, true, false, 16, new RegisterTest[]{RegisterTest.BC, RegisterTest.DE, RegisterTest.HL}),
        MOV("MOV", 0x40, 1, true, true, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M}),
        MVI("MVI", 0x06, 2, true, false, 8, new RegisterTest[]{RegisterTest.A, RegisterTest.B, RegisterTest.C, RegisterTest.D, RegisterTest.E, RegisterTest.H, RegisterTest.L, RegisterTest.M});

        public final String mnem;
        public final int opcode;
        public final int opcodeSize;
        public final boolean destAllowed;
        public final boolean srcAllowed;
        public final int align;
        public final RegisterTest[] allowedRegisters;

        private Mnemonic(String mnem, int opcode, int opcodeSize, boolean destAllowed, boolean srcAllowed, int align, RegisterTest[] allowedRegisters) {
            this.mnem = mnem;
            this.opcode = opcode;
            this.destAllowed = destAllowed;
            this.srcAllowed = srcAllowed;
            this.align = align;
            this.allowedRegisters = allowedRegisters;
            this.opcodeSize = opcodeSize;
        }

        public OpcodeTest create(int data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            OpcodeTest op = new OpcodeTest();
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = data;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public OpcodeTest create(RegisterTest dest, int data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " should have destination register");
            }
            if (!checkRegister(dest, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + dest.regMnem + " not allowed");
            }
            OpcodeTest op = new OpcodeTest();
            op.align = align;
            op.dstReg = dest;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = data;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public OpcodeTest create(RegisterTest dest, String data) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " should have destination register");
            }
            if (!checkRegister(dest, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + dest.regMnem + " not allowed");
            }
            OpcodeTest op = new OpcodeTest();
            op.align = align;
            op.dstReg = dest;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = data;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public OpcodeTest create() {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " should have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            OpcodeTest op = new OpcodeTest();
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public OpcodeTest create(String sym) {
            if (opcodeSize == 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (destAllowed || srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination or source register");
            }
            OpcodeTest op = new OpcodeTest();
            op.align = align;
            op.dstReg = null;
            op.srcReg = null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = sym;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public OpcodeTest create(RegisterTest oneReg) {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (!srcAllowed) {
                if (!destAllowed) {
                    throw new RuntimeException("Opcode " + mnem + " should have source or destination register");
                }
            }
            if (!checkRegister(oneReg, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + oneReg.regMnem + " not allowed");
            }
            OpcodeTest op = new OpcodeTest();
            op.align = align;
            op.dstReg = srcAllowed ? oneReg : null;
            op.srcReg = destAllowed ? oneReg : null;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            op.opcodeSize = opcodeSize;
            return op;
        }

        public OpcodeTest create(RegisterTest dst, RegisterTest src) {
            if (opcodeSize != 1) {
                throw new RuntimeException("Opcode " + mnem + " couldn't have operand");
            }
            if (!destAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have destination register");
            }
            if (!srcAllowed) {
                throw new RuntimeException("Opcode " + mnem + " shouldn't have source register");
            }
            if (!checkRegister(src, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": source register " + src.regMnem + " not allowed");
            }
            if (!checkRegister(dst, allowedRegisters)) {
                throw new RuntimeException("Opcode " + mnem + ": destination register " + src.regMnem + " not allowed");
            }

            OpcodeTest op = new OpcodeTest();
            op.align = align;
            op.dstReg = dst;
            op.srcReg = src;
            op.opcode = opcode;
            op.mnemonic = mnem;
            op.data = 0;
            op.sym = null;
            return op;
        }

        private boolean checkRegister(RegisterTest r, RegisterTest[] allowed) {
            for (RegisterTest c : allowed) {
                if (r == c) {
                    return true;
                }
            }

            return false;
        }
    }
}
