/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.preprocess;

import com.ats.gen80.Preprocessor;
import com.ats.gen80.node.Token;

/**
 *
 * @author lucifer
 */
public interface MacroHandler {

    Macro handle(Preprocessor p, Token tok);
}
