/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80.preprocess;

import com.ats.gen80.FileStruct;
import com.ats.gen80.node.Token;
import java.util.List;

/**
 *
 * @author lucifer
 */
public class Macro {

    public MacroType kind;
    public int nargs;
    public List<Token> body;
    public boolean isVarg;
    public MacroHandler fn;
}
