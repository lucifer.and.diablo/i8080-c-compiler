/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ats.gen80;

import com.ats.gen80.node.Node;
import com.ats.gen80.node.Type;
import com.ats.gen80.node.TypeKind;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lucifer
 */
public class Globals {

    public final static Map<String, Type> typedefs = new LinearMap<>();
    public final static Map<String, Node> globalEnv = new LinearMap<>();
    public static Map<String, Node> localEnv = null;
    public static Map<String, Node> localVars = null;
    public static Map<String, Node> labels = new HashMap<>();
    public static List<Node> topLevels = new ArrayList<>();
    public static List<Node> gotos = new ArrayList<>();
    public static int locals = 0;
    public static int temps = 0;
    public static int statics = 0;

    static {
        typedefs.put("void", new Type(TypeKind.KIND_VOID, 0, 0, false));
        typedefs.put("char", new Type(TypeKind.KIND_CHAR, 1, 1, false));
        typedefs.put("int", new Type(TypeKind.KIND_INT, 2, 2, false));
        typedefs.put("long", new Type(TypeKind.KIND_LONG, 4, 4, false));
        typedefs.put("float", new Type(TypeKind.KIND_FLOAT, 4, 4, false));
        typedefs.put("unsigned char", new Type(TypeKind.KIND_CHAR, 1, 1, true));
        typedefs.put("unsigned int", new Type(TypeKind.KIND_INT, 2, 2, true));
    }

    public static Type globalEnv(String unsigned_char) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static Map<String, Node> env() {
        return localEnv != null ? localEnv : globalEnv;
    }

    public static String makeLabel() {
        return String.format(".L%d", locals++);
    }

    public static String makeTempname() {
        return String.format(".T%d", temps++);
    }

    public static String makeStaticLabel(String name) {
        return String.format(".S%d.%s", statics++, name);
    }

}
